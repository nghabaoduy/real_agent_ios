//
//  AddCurLand.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/13/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "AddCurLand.h"
#import "ExtraDetailLandVC.h"

@interface AddCurLand ()

@end

@implementation AddCurLand

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"run");
    ExtraDetailLandVC * view = segue.destinationViewController;
    [view addBackBtn];
}

@end
