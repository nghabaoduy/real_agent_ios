//
//  PropertyReviewVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 22/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"

@interface PropertyReviewVC : UITableViewController
{
    //Basic Information
    IBOutlet UITextField * address;
    IBOutlet UITextField * postalCode;
    IBOutlet UITextField * blockNo;
    IBOutlet UITextField * levelNo;
    IBOutlet UITextField * unitNo;
    IBOutlet UITextField * district;
    IBOutlet UITextField * noOfBedroom;
    IBOutlet UITextField * area;
    IBOutlet UITextField * price;
    IBOutlet UITextField * tenure;
    IBOutlet UITextField * adder;
    
    //Status
    IBOutlet UITextField * status;
    IBOutlet UITextField * rentedPrice;
    
    
    //Project Information
    IBOutlet UITextField * projectName;
    IBOutlet UITextField * PSF;
    IBOutlet UITextField * topYear;
    IBOutlet UITextField * rentalYield;
}
@property (strong, nonatomic) Property * property;


- (IBAction)upload:(id)sender;
@end
