//
//  CalculatorTabBarVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/26/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "CalculatorTabBarVC.h"

@interface CalculatorTabBarVC ()

@end

@implementation CalculatorTabBarVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    if (item.tag == 1) {
        self.navigationItem.title = @"Mortgage Calculator";
    }
    else if (item.tag == 2)
    {
        self.navigationItem.title = @"Residential Calculator";
    }
    else if (item.tag == 3)
    {
        self.navigationItem.title = @"Commercial Calculator";
    }
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];
}

@end
