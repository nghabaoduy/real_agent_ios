//
//  MyFavoriteVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/19/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyFavoriteVC.h"
#import "ListPattern.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "DetailPropertyNavVC.h"
#import "DetailPropertyTabBarVC.h"
#import "DetailPropertyVC.h"
#import "GalleryPropertyVC.h"
#import "LocationPropertyVC.h"
#import "DetailComercialVC.h"

@interface MyFavoriteVC ()

@end

@implementation MyFavoriteVC

@synthesize allPropertyList;
@synthesize curUser;
- (void)actionRun
{
    [super actionRun];
    curUser = [[DataEngine sharedInstance] GetCurUser];
    [[DatabaseEngine sharedInstance] loadFavorite:curUser :self];
}

- (void)actionSuceed:(NSString *)message
{
    NSLog(@"success");
    [super actionSuceed:message];
    [self.tableView reloadData];
}

- (void)actionFail:(NSString *)message
{
     NSLog(@"fail");
    [super actionFail:message];
    [self.tableView reloadData];
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewDidAppear:(BOOL)animated
{
    
    [self actionRun];
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    curUser = [[DataEngine sharedInstance] GetCurUser];
    [self actionRun];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [allPropertyList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"indexPath = %i",indexPath.row);
    static NSString *CellIdentifier = @"listPattern3";
    
    
    Property *curPro = [allPropertyList objectAtIndex:indexPath.row];
    NSLog(@"property line %i-%i = %@",indexPath.section,indexPath.row,curPro);
    
    
    ListPattern * cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell1 == nil)
    {
        NSLog(@"cell is nil");
        cell1 = [[ListPattern alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UIImage *background = [UIImage imageNamed:@"cell_odd"];
    
    
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cell1.backgroundView = cellBackgroundView;
    if (curPro.iconImage != nil) {
        cell1.displayer.image = curPro.iconImage;
    }
    
    [cell1.projectName setText:curPro.projectName];
    [cell1.priceAndBedroom setText:[NSString stringWithFormat:@"S$ %@", curPro.price]];
    [cell1.projectType setText:curPro.propertyType];
    [cell1.areaAndPsf setText:[NSString stringWithFormat:@"%@ sqft  - PFS S$ %@", curPro.area, curPro.PSF]];
    [cell1.location setText:curPro.adder.fullName];
    
    
    
    return cell1;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Property *curPro = [allPropertyList objectAtIndex:indexPath.row];
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"DetailStoryboard" bundle:nil];
    
    
    
    DetailPropertyNavVC * navView =[newStoryboard instantiateViewControllerWithIdentifier:@"detailProperty"];
    
    DetailPropertyTabBarVC * tabBar = (DetailPropertyTabBarVC*) navView.topViewController;
    [tabBar initTabByProperty:curPro :newStoryboard ];
    
    [self presentViewController:navView animated:YES completion:nil];
    
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.000001f;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.000001f;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] ;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
