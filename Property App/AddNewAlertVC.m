//
//  AddNewAlertVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/20/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "AddNewAlertVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"


@interface AddNewAlertVC ()

@end

@implementation AddNewAlertVC
@synthesize myAlertsView, isEdit, alertInfo;
@synthesize subjectTF, transactionTF, typeTF, projectTF, districtTF,blockUnitNoTF, locationTF, tenureTF, priceBeginTF, priceEndTF, areaBeginTF, areaEndTF, levelFromTF, levelToTF, bedroomBeginTF, bedroomEndTF, remarkTF;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)initEditAlert
{
    [subjectTF setText:alertInfo.subject];
    [transactionTF setText:alertInfo.transaction];
    [typeTF setText:alertInfo.type];
    [projectTF setText:alertInfo.project];
    [districtTF setText:alertInfo.district];
    [blockUnitNoTF setText:alertInfo.blockUnitNo];
    [locationTF setText:alertInfo.location];
    [tenureTF setText:alertInfo.tenure];
    [remarkTF setText:alertInfo.remark];
    
    [submitBtn setTitle:@"Done"];
    
    NSArray *priceSplit = [alertInfo.price componentsSeparatedByString:@" to "];
    if (priceList.count > 1  && ![alertInfo.price isEqualToString:@"0 to 0"]) {
        [priceBeginTF setText:[priceSplit objectAtIndex:0]];
        [priceEndTF setText:[priceSplit objectAtIndex:1]];
    }
    NSArray *areaSplit = [alertInfo.area componentsSeparatedByString:@" to "];
    if (areaSplit.count > 1  && ![alertInfo.area isEqualToString:@"0 to 0"]) {
        [areaBeginTF setText:[areaSplit objectAtIndex:0]];
        [areaEndTF setText:[areaSplit objectAtIndex:1]];
    }
    NSArray *bedroomSplit = [alertInfo.bedroom componentsSeparatedByString:@" to "];
    if (bedroomSplit.count > 1  && ![alertInfo.bedroom isEqualToString:@"0 to 0"]) {
        [bedroomBeginTF setText:[bedroomSplit objectAtIndex:0]];
        [bedroomEndTF setText:[bedroomSplit objectAtIndex:1]];
    }
    
    deleteBtn.hidden = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    //Array list here
    transactionList = [[NSArray alloc]initWithObjects:
                       @"",
                       @"Sales",
                       @"Rent", nil];
    typeList = [[NSArray alloc]initWithObjects:
                @"",
                @"Private Residential",
                @"Landed",
                @"HDB",
                @"Commercial",nil];
    projectList = [[NSArray alloc]initWithObjects:
                   @"",
                   @"1",
                   @"2", nil];
    
    districList = [[DataEngine sharedInstance] getDistrictList];
    tenureList = [[NSArray alloc]initWithObjects:
                    @"",
                    @"FH",
                    @"999",
                    @"99",
                    @"60",
                    @"30",
                  @"<30yrs",nil];
    
    priceList = [[NSArray alloc]initWithObjects:
                 @"",
                 @"1",
                 @"a", nil];
    
    areaList = [[NSArray alloc]initWithObjects:
                @"",
                @"a",
                @"b", nil];
    
    
    curUser = [[DataEngine sharedInstance] GetCurUser];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self initTextfieldPlaceHolder];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    if (isEdit) {
        [self initEditAlert];
    }
}

- (void) initTextfieldPlaceHolder
{
    
    
    
    subjectTF.placeholder = @"Subject: ";
    transactionTF.placeholder = @"Transaction: ";
    typeTF.placeholder = @"Type: ";
    blockUnitNoTF.placeholder = @"Block: ";
    projectTF.placeholder = @"Project: ";
    districtTF.placeholder = @"District: ";
    locationTF.placeholder = @"Location: ";
    bedroomBeginTF.placeholder = @"Bedroom: ";
    bedroomEndTF.placeholder = @"Bedroom: ";
    areaBeginTF.placeholder = @"Area (sqft): ";
    areaEndTF.placeholder = @"Area (sqft): ";
    priceBeginTF.placeholder = @"Price: ";
    priceEndTF.placeholder = @"Price: ";
    tenureTF.placeholder = @"Tenure: ";
    remarkTF.placeholder = @"Remark: ";

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    // Add the picker
    /*
     UIDatePicker *pickerView = [[UIDatePicker alloc] init];
     pickerView.datePickerMode = UIDatePickerModeDate;
     [menu addSubview:pickerView];
     [menu showInView:self.view];
     [menu setBounds:CGRectMake(0,0,320, 500)];*/
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;

}

-(void)categoryDoneButtonPressed{
    //categoryLable.text = selectedCategory;
    if (activeTextField == transactionTF) {
        [transactionTF setText:selectedCategory];
    }
    else if (activeTextField == typeTF)
    {
        [typeTF setText:selectedCategory];
    }
    else if (activeTextField == projectTF)
    {
        [projectTF setText:selectedCategory];
    }
    else if (activeTextField == districtTF)
    {
        [districtTF setText:selectedCategory];
    }
    else if (activeTextField == tenureTF)
    {
        [tenureTF setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if (activeTextField == transactionTF) {
        selectedCategory = [NSString stringWithFormat:@"%@",[transactionList objectAtIndex:row]];
    }
    else if (activeTextField == typeTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[typeList objectAtIndex:row]];
    }
    else if (activeTextField == projectTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[projectList objectAtIndex:row]];
    }
    else if (activeTextField == districtTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[districList objectAtIndex:row]];
    }
    else if (activeTextField == tenureTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[tenureList objectAtIndex:row]];
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (activeTextField == transactionTF)
    {
        return [transactionList count];
    }
    else if (activeTextField == typeTF)
    {
        return [typeList count];
    }
    else if (activeTextField == projectTF)
    {
        return [projectList count];
    }
    else if (activeTextField == districtTF)
    {
        return [districList count];
    }
    else if (activeTextField == tenureTF)
    {
        return [tenureList count];
    }
    
    return [transactionList count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == transactionTF)
    {
        return [transactionList objectAtIndex:row];
    }
    else if (activeTextField == typeTF)
    {
        return [typeList objectAtIndex:row];
    }
    else if (activeTextField == projectTF)
    {
        return [projectList objectAtIndex:row];
    }
    else if (activeTextField == districtTF)
    {
        return [districList objectAtIndex:row];
    }
    else if (activeTextField == tenureTF)
    {
        return [tenureList objectAtIndex:row];
    }
    return [NSString stringWithFormat:@"%d", row+1];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:transactionTF] || [textField isEqual:typeTF] || [textField isEqual:projectTF] || [textField isEqual:districtTF] || [textField isEqual:tenureTF])
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

-(void)dismissKeyboard {
    [activeTextField resignFirstResponder];
}

-(BOOL) validateBeginEnd:(int) start :(int) end : (NSString *)type
{
    if (start == 0 && end == 0) {
        return YES;
    }
    else
    {
        if (end > start) {
            return YES;
        }
        else
        {
            NSString *errorMessage = [NSString stringWithFormat:@"%@ end value has to be larger then %@ start value",type,type];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    }
    return NO;
}
-(BOOL) validateSubject
{
    if ([subjectTF.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Alert Subject can not be blank"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    return YES;
}

-(NSString *) getRangeString:(NSString*)begin :(NSString*)end
{
    NSString *beginStr = [begin isEqualToString:@""]? @"0":begin;
    NSString *endStr = [end isEqualToString:@""]? @"0":end;
    NSString *rangeStr = [NSString stringWithFormat:@"%@ to %@",beginStr,endStr];
    return rangeStr;
}

- (IBAction)submit:(id)sender
{
    if ([self validateSubject])
    if ([self validateBeginEnd:[priceBeginTF.text intValue] :[priceEndTF.text intValue] :@"Price"])
    if ([self validateBeginEnd:[bedroomBeginTF.text intValue] :[bedroomEndTF.text intValue] :@"Bedroom"])
    if ([self validateBeginEnd:[areaBeginTF.text intValue] :[areaEndTF.text intValue] :@"Area"])
    {
        //price
        
        
        NSString *priceStr = [self getRangeString:priceBeginTF.text: priceEndTF.text];
        NSString *bedStr = [self getRangeString:bedroomBeginTF.text: bedroomEndTF.text];
        NSString *areaStr = [self getRangeString:areaBeginTF.text: areaEndTF.text];
        
        if (isEdit) {
            alertInfo.subject = subjectTF.text;
            alertInfo.transaction  = transactionTF.text;
            alertInfo.type = typeTF.text;
            alertInfo.project = projectTF.text;
            alertInfo.district = districtTF.text;
            alertInfo.blockUnitNo = blockUnitNoTF.text;
            alertInfo.location = locationTF.text;
            alertInfo.tenure = tenureTF.text;
            alertInfo.remark = remarkTF.text;
           
            alertInfo.price = priceStr;
            alertInfo.bedroom = bedStr;
            alertInfo.area = areaStr;
            
            
            [[DatabaseEngine sharedInstance] editAlert:self :alertInfo];
        }
        else
        {
            AlertInfo * newAlert = [[AlertInfo alloc]init];
            newAlert.subject = subjectTF.text;
            newAlert.transaction  = transactionTF.text;
            newAlert.type = typeTF.text;
            newAlert.project = projectTF.text;
            newAlert.district = districtTF.text;
            newAlert.blockUnitNo = blockUnitNoTF.text;
            newAlert.location = locationTF.text;
            newAlert.tenure = tenureTF.text;
            newAlert.remark = remarkTF.text;
            
            newAlert.price = priceStr;
            newAlert.area = areaStr;
            newAlert.bedroom = bedStr;
            
            [[DatabaseEngine sharedInstance] addNewAlert:self :newAlert :curUser];
        }
    }
}

- (void) addAlertDone
{
    [self.navigationController popViewControllerAnimated:YES];
    [myAlertsView actionRun];
}

- (IBAction)delete:(id)sender
{
    [alertInfo.alert deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [self addAlertDone];
    }];
}
@end
