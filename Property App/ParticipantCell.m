//
//  ParticipantCell.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/12/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ParticipantCell.h"
#import "DatabaseEngine.h"

@implementation ParticipantCell
@synthesize groupInfo, groupInfoView, username, phoneNumber, deleteBtn, curUser;


-(void)deleteStart:(id)sender
{
    [[DatabaseEngine sharedInstance] DeleteParticipants:groupInfo :curUser :self];
}

- (void) deleteDone
{
    NSLog(@"Doneeeeee");
    
    [groupInfoView actionRun];
    
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
