//
//  ReviewImageVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 22/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewImageVC : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView * theView;
@property (strong, nonatomic) UIImage * curImage;
@property int test;

- (IBAction) Back:(id)sender;


@end
