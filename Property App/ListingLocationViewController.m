//
//  ListingLocationViewController.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ListingLocationViewController.h"
#import "MyUIEngine.h"
#import "DataEngine.h"
#import "SWRevealViewController.h"

@interface ListingLocationViewController ()

@end

@implementation ListingLocationViewController
@synthesize curProperty;
- (IBAction) Back:(id)sender
{
    
	SWRevealViewController * mainView = [self.storyboard instantiateViewControllerWithIdentifier:@"sw"];
	[self presentViewController:mainView animated:YES completion:nil];
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
	
	curProperty = [[DataEngine sharedInstance] GetCurPropertyInstace];
	NSLog(@"curProperty = %@",curProperty.propertyName);
	
    [self GetLocationByAddress];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) GetLocationByAddress {
	CLGeocoder *geocoder = [[CLGeocoder alloc] init];
	NSString* postCode = curProperty.postCode;
	
	NSLog(@"%@", postCode);
	[geocoder geocodeAddressString:postCode
				 completionHandler:^(NSArray *placemarks, NSError *error) {
					 
					 if (error) {
						 NSLog(@"Geocode failed with error: %@", error);
						 return;
					 }
					 
					 if(placemarks && placemarks.count > 0)
					 {
						 CLPlacemark* place = [placemarks objectAtIndex:0];
						 
						 CLLocation *location = place.location;
						 CLLocationCoordinate2D coords = location.coordinate;
						 
						 NSLog(@"Latitude = %f, Longitude = %f",
							   coords.latitude, coords.longitude);
						 NSLog(@"%@", place.addressDictionary);
						 [self GetAddressByLatitude:coords.latitude :coords.longitude];
						 
						 
						 
					 }
				 }
	 ];
}

-(void) GetAddressByLatitude: (double) lat : (double) longt {
	CLGeocoder *geocoder = [[CLGeocoder alloc] init];
	
	CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:lat
														longitude:longt];
	
	[geocoder reverseGeocodeLocation:newLocation
				   completionHandler:^(NSArray *placemarks, NSError *error) {
					   
					   if (error) {
						   NSLog(@"Geocode failed with error: %@", error);
						   return;
					   }
					   
					   if (placemarks && placemarks.count > 0)
					   {
						   CLPlacemark *placemark = placemarks[0];
						   
						   NSDictionary *addressDictionary =
                           placemark.addressDictionary;
						   
						   //NSLog(@"location: %@", [placemark location]);
						   NSLog(@"addressdict: %@", [placemark addressDictionary]);
						   
						   //[placemark ]
						   
						   // NSLog(@"%@ %@ %@ %@", address,city, state, zip);
						   CLLocationCoordinate2D coord = {.latitude =  placemark.location.coordinate.latitude, .longitude = placemark.location.coordinate.longitude};
						   MKCoordinateSpan span = {.latitudeDelta = 0.01, .longitudeDelta =  0.01};
						   MKCoordinateRegion region = {coord, span};
						   
						   MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
						   point.coordinate = coord;
						   point.title = placemark.name;
						   point.subtitle = [NSString stringWithFormat:@"%@ %@", placemark.country, placemark.postalCode];
						   [self.mapView addAnnotation:point];
						   
						   NSLog(@"View loading");
						   [self.mapView setRegion:region];
						   NSLog(@"view loaded");
						   [self.mapView selectAnnotation:point animated:YES];
					   }
					   
				   }];
}


@end
