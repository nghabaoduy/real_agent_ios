//
//  SignUpViewController.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "SignUpViewController.h"
#import "DatabaseEngine.h"
@interface SignUpViewController()

@end

@implementation SignUpViewController

@synthesize loginView;
@synthesize password = _password;
@synthesize phone = _phone;
@synthesize email = _email;
@synthesize agentPhone = _agentPhone;
@synthesize firstname = _firstname;
@synthesize lastname = _lastname;
@synthesize activeTextField;
BOOL isAgentSignup;
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    
    
    [textField resignFirstResponder];
    
    return YES;
    
}



- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
- (IBAction) Back:(id)sender
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.delegate = self;
    
    [self initTextfieldPlaceHolder];
}
- (void) initTextfieldPlaceHolder
{
    self.phone.placeholder = @"Phone No: ";
    self.password.placeholder = @"Password: ";
    self.repassword.placeholder = @"Re-Password: ";
    self.firstname.placeholder = @"First name: ";
    self.lastname.placeholder = @"Last name: ";
    self.email.placeholder = @"Email: ";
    self.agentPhone.placeholder = @"AgentPhone: ";
    
    self.phone.text = @"";
     self.repassword.text = @"";
     self.password.text = @"";
     self.firstname.text = @"";
     self.lastname.text = @"";
     self.email.text = @"";
     self.agentPhone.text = @"";
}
-(IBAction)assignAgent:(id)sender
{
    self.agentPhone.text = @"98765432";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) phoneValidation
{
    BOOL is8chars = self.phone.text.length == 8;
    if (!is8chars) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sign Up Error"
                                                        message:@"Phone Number is not valid."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    return YES;
}

- (BOOL) passwordValidation
{
    BOOL passwordCorrect = self.password.text.length >=5 && [self.password.text isEqualToString:self.repassword.text];
    if (!passwordCorrect) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sign Up Error"
                                                        message:@"Password and Re Password are invalid"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    return YES;
}


- (IBAction) SignUp:(id)sender
{
    if ([self phoneValidation] && [self passwordValidation]) {
        NSMutableDictionary *userInfoDict = [[NSMutableDictionary alloc] init];
        [userInfoDict setObject:_phone.text forKey:@"phone"];
        [userInfoDict setObject:_password.text forKey:@"password"];
        [userInfoDict setObject:_email.text forKey:@"email"];
        [userInfoDict setObject:@"" forKey:@"agentPhone"];
        [userInfoDict setObject:_firstname.text forKey:@"firstName"];
        [userInfoDict setObject:_lastname.text forKey:@"lastName"];
        
        if (_agentPhone.text.length == 0) {
            isAgentSignup = YES;
            NSString *userType = @"agent";
            [userInfoDict setObject:userType forKey:@"userType"];
			[[DatabaseEngine sharedInstance] UserSignUp:userInfoDict userType:userType signupView:self];
											  
        } else {
            isAgentSignup = NO;
             NSString *userType = @"client";
            [userInfoDict setObject:userType forKey:@"userType"];
            [[DatabaseEngine sharedInstance] UserSignUp:userInfoDict userType:userType signupView:self];
        }
    }
}

- (void) SearchAgentFailed : (NSString*) errorString
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Signup Error"
													message:errorString
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}

- (void) SignUpSuccessful
{
    NSLog(@"sign up successful call");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Signup Status"
													message:@"Signup Successfully"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
    [alert show];
  [  loginView inputSignUpLogin:_phone.text :_password.text ];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void) SignUpFail: (NSString*) errorString
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Signup Error"
													message:errorString
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
	//NSLog(@"%@", errorString);
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([touch.view isKindOfClass:[UIButton class]]){
        return NO;
    }
    return YES; // handle the touch
}

- (void) dismissKeyboard{
    [self.activeTextField resignFirstResponder];
}



- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
    
    
    // Step 3: Scroll the target text field into view.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(aRect, self.activeTextField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.activeTextField.frame.origin.y - (keyboardSize.height-15));
        [theScrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
}

// Set activeTextField to the current active textfield

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
}

// Set activeTextField to nil

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextField = nil;
}


// Dismiss the keyboard

- (IBAction)dismissKeyboard:(id)sender
{
    [self.activeTextField resignFirstResponder];
}

@end
