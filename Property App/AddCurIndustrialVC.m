//
//  AddCurPropertyVC.m
//  Property App
//
//  Created by Brian on 1/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "AddCurIndustrialVC.h"
#import "ExtraDetailIndustryVC.h"
#import "MyCustomBack.h"

@interface AddCurIndustrialVC ()

@end

@implementation AddCurIndustrialVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"run");
    ExtraDetailIndustryVC * view = segue.destinationViewController;
    [view addBackBtn];
}


@end
