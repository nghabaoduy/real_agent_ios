//
//  GroupInfoVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "GroupInfoVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "User.h"
#import "PhoneBookVC.h"
#import "ParticipantCell.h"

@interface GroupInfoVC ()

@end

@implementation GroupInfoVC
@synthesize userList;
@synthesize groupInfo;
@synthesize groupParticipantList;
@synthesize removeBtnList;


- (void)actionRun
{
    [super actionRun];
    [[DatabaseEngine sharedInstance] LoadParticipant:groupInfo :self];
}

- (void) actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    User * user = [[DataEngine sharedInstance] GetCurUser];
    PFUser * firstUser = [userList objectAtIndex: 0];
    
    [userList replaceObjectAtIndex:0 withObject:user.userObject];
    
    for (int i = 0; i < [userList count]; i++) {
        PFObject * tempUser = [userList objectAtIndex:i];
        
        if ([user.userObject.objectId isEqualToString:tempUser.objectId] && i > 0) {
            [userList replaceObjectAtIndex:i withObject:firstUser];
        }
    }
    
    
    NSMutableArray * newUserList = userList;
    [newUserList removeObjectAtIndex:0];
    
    
    newUserList = [self getSortedParticipants:newUserList];
    
    NSMutableArray * finalUserList = [[NSMutableArray alloc]init];
    [finalUserList addObject:user.userObject];
    [finalUserList addObjectsFromArray:[self getSortedParticipants:newUserList]];
    
    userList = finalUserList;
    
    [tableView reloadData];
    
    
    //[self enableRemoveBtn:&(editEnable)];
    
    
    
    
    
    
}

- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [userList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"participantCell";
    
    ParticipantCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[ParticipantCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    PFUser * curUser = [userList objectAtIndex:indexPath.row];

    [cell.phoneNumber setText: curUser.username];
    cell.groupInfo = groupInfo;
    cell.curUser = curUser;
    cell.groupInfoView = self;
    cell.index = indexPath.row;
    cell.deleteBtn.hidden = YES;
   
    
    
    if (indexPath.row == 0) {
        if ([admin.objectId isEqual:curUser.objectId]) {
            [cell.username setText: @"You - Admin"];
            add.enabled = YES;
            edit.enabled = YES;
        }
        else
        {
            [cell.username setText: @"You"];
        }
    }
    else
    {
        User *tempUser = [[User alloc] initWithUser:curUser];
        NSString * phone = [NSString stringWithFormat:@"%@", tempUser.fullName];
        if ([admin.objectId isEqual:curUser.objectId]) {
            NSString * temp2 = [NSString stringWithFormat:@"%@ - Admin", tempUser.fullName];
            [cell.username setText: temp2];
        }
        else
        {
            [cell.username setText: phone];
            [removeBtnList addObject:cell.deleteBtn];
        }
    }
    
    return cell;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSLog(@"%@ tab03 load", groupInfo.objectId);
    admin = [groupInfo objectForKey:@"creator"];
    add.enabled = NO;
    edit.enabled = NO;
    editEnable = YES;
    removeBtnList = [[NSMutableArray alloc]init];
    [self actionRun];
}

- (void)ReloadData
{
    NSLog(@"reload");
    [self actionRun];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"addParticipant"]) {
        PhoneBookVC * destination = segue.destinationViewController;
    }
}

- (IBAction)edit:(id)sender
{
    editEnable = !editEnable;
    [self enableRemoveBtn:&(editEnable)];
}

- (void) enableRemoveBtn : (BOOL*) enable
{
    for (UIButton * remove in removeBtnList) {
        remove.hidden = *(enable);
    }
}

- (NSMutableArray *) getSortedParticipants: (NSMutableArray*) list
{
    NSLog(@"do do do ");
    NSMutableArray * a = [[NSMutableArray alloc]init];
    NSMutableArray * b = [[NSMutableArray alloc]init];
    NSMutableArray * c = [[NSMutableArray alloc]init];
    NSMutableArray * d = [[NSMutableArray alloc]init];
    NSMutableArray * e = [[NSMutableArray alloc]init];
    NSMutableArray * f = [[NSMutableArray alloc]init];
    NSMutableArray * g = [[NSMutableArray alloc]init];
    NSMutableArray * h = [[NSMutableArray alloc]init];
    NSMutableArray * i = [[NSMutableArray alloc]init];
    NSMutableArray * j = [[NSMutableArray alloc]init];
    NSMutableArray * k = [[NSMutableArray alloc]init];
    NSMutableArray * l = [[NSMutableArray alloc]init];
    NSMutableArray * m = [[NSMutableArray alloc]init];
    NSMutableArray * n = [[NSMutableArray alloc]init];
    NSMutableArray * o = [[NSMutableArray alloc]init];
    NSMutableArray * p = [[NSMutableArray alloc]init];
    NSMutableArray * q = [[NSMutableArray alloc]init];
    NSMutableArray * s = [[NSMutableArray alloc]init];
    NSMutableArray * t = [[NSMutableArray alloc]init];
    NSMutableArray * u = [[NSMutableArray alloc]init];
    NSMutableArray * v = [[NSMutableArray alloc]init];
    NSMutableArray * w = [[NSMutableArray alloc]init];
    NSMutableArray * x = [[NSMutableArray alloc]init];
    NSMutableArray * y = [[NSMutableArray alloc]init];
    NSMutableArray * z = [[NSMutableArray alloc]init];
    NSMutableArray * other = [[NSMutableArray alloc]init];
    
    NSLog(@"coutn %lu", (unsigned long)list.count);
    
    for (int ii = 0; ii < list.count; ii++) {
    
        PFUser * cur = [list objectAtIndex:ii];
        
        NSString * firstName = [cur objectForKey:@"firstName"];
        NSString * lastName = [cur objectForKey:@"lastName"];
        
        NSString * fullName  = @"";
        
        if ([firstName isEqualToString:@""]) {
            fullName = [NSString stringWithFormat:@"%@", lastName];
        }
        else
        {
            fullName  = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
        }
        
        NSLog(@"fullname: %@", fullName);
        
        
    
        
        
        NSString * firstchar = [fullName substringToIndex:1];
        
        if ([firstchar  isEqual: @"a"] || [firstchar  isEqual: @"A"]) {
            [a addObject:cur];
            NSLog(@"to a");
        }
        else if ([firstchar  isEqual: @"b"]|| [firstchar  isEqual: @"B"]) {
            [b addObject:cur];
            NSLog(@"to b");
        }
        else if ([firstchar  isEqual: @"c"]|| [firstchar  isEqual: @"C"]) {
            [c addObject:cur];
            NSLog(@"to c");
        }
        else if ([firstchar  isEqual: @"d"]|| [firstchar  isEqual: @"D"]) {
            [d addObject:cur];
            NSLog(@"to d");
        }
        else if ([firstchar  isEqual: @"e"]|| [firstchar  isEqual: @"E"]) {
            [e addObject:cur];
            NSLog(@"to e");
        }
        else if ([firstchar  isEqual: @"f"] || [firstchar  isEqual: @"F"]) {
            [f addObject:cur];
            NSLog(@"to f");
        }
        else if ([firstchar  isEqual: @"g"]|| [firstchar  isEqual: @"G"]) {
            [g addObject:cur];
            NSLog(@"to g");
        }
        else if ([firstchar  isEqual: @"h"]|| [firstchar  isEqual: @"H"]) {
            [h addObject:cur];
            NSLog(@"to h");
        }
        else if ([firstchar  isEqual: @"i"]|| [firstchar  isEqual: @"I"]) {
            [i addObject:cur];
            NSLog(@"to i");
        }
        else if ([firstchar  isEqual: @"j"]|| [firstchar  isEqual: @"J"]) {
            [j addObject:cur];
            NSLog(@"to j");
        }
        else if ([firstchar  isEqual: @"k"]|| [firstchar  isEqual: @"K"]) {
            [k addObject:cur];
            NSLog(@"to k");
        }
        else if ([firstchar  isEqual: @"l"]|| [firstchar  isEqual: @"L"]) {
            [l addObject:cur];
            NSLog(@"to l");
        }
        else if ([firstchar  isEqual: @"n"]|| [firstchar  isEqual: @"N"]) {
            [n addObject:cur];
            NSLog(@"to n");
        }
        else if ([firstchar  isEqual: @"m"]|| [firstchar  isEqual: @"M"]) {
            [m addObject:cur];
            NSLog(@"to m");
        }
        else if ([firstchar  isEqual: @"o"]|| [firstchar  isEqual: @"O"]) {
            [o addObject:cur];
            NSLog(@"to o");
        }
        else if ([firstchar  isEqual: @"p"]|| [firstchar  isEqual: @"P"]) {
            [p addObject:cur];
            NSLog(@"to p");
        }
        else if ([firstchar  isEqual: @"q"]|| [firstchar  isEqual: @"Q"]) {
            [q addObject:cur];
            NSLog(@"to q");
        }
        else if ([firstchar  isEqual: @"s"]|| [firstchar  isEqual: @"S"]) {
            [s addObject:cur];
            NSLog(@"to s");
        }
        else if ([firstchar  isEqual: @"t"]|| [firstchar  isEqual: @"T"]) {
            [t addObject:cur];
            NSLog(@"to t");
        }
        else if ([firstchar  isEqual: @"u"]|| [firstchar  isEqual: @"U"]) {
            [u addObject:cur];
            NSLog(@"to u");
        }
        else if ([firstchar  isEqual: @"v"]|| [firstchar  isEqual: @"V"]) {
            [v addObject:cur];
            NSLog(@"to v");
        }
        else if ([firstchar  isEqual: @"w"]|| [firstchar  isEqual: @"W"]) {
            [w addObject:cur];
            NSLog(@"to w");
        }
        else if ([firstchar  isEqual: @"x"]|| [firstchar  isEqual: @"X"]) {
            [x addObject:cur];
            NSLog(@"to x");
        }
        else if ([firstchar  isEqual: @"y"]|| [firstchar  isEqual: @"Y"]) {
            [y addObject:cur];
            NSLog(@"to y");
        }
        else if ([firstchar  isEqual: @"z"]|| [firstchar  isEqual: @"Z"]) {
            [z addObject:cur];
            NSLog(@"to z");
        }
        else
        {
            [other addObject:cur];
            NSLog(@"to other");
        }
    }
    [a addObjectsFromArray:b];
    [a addObjectsFromArray:c];
    [a addObjectsFromArray:d];
    [a addObjectsFromArray:e];
    [a addObjectsFromArray:f];
    [a addObjectsFromArray:g];
    [a addObjectsFromArray:h];
    [a addObjectsFromArray:i];
    [a addObjectsFromArray:j];
    [a addObjectsFromArray:k];
    [a addObjectsFromArray:l];
    [a addObjectsFromArray:n];
    [a addObjectsFromArray:m];
    [a addObjectsFromArray:o];
    [a addObjectsFromArray:p];
    [a addObjectsFromArray:q];
    [a addObjectsFromArray:s];
    [a addObjectsFromArray:t];
    [a addObjectsFromArray:u];
    [a addObjectsFromArray:v];
    [a addObjectsFromArray:w];
    [a addObjectsFromArray:x];
    [a addObjectsFromArray:y];
    [a addObjectsFromArray:z];
    [a addObjectsFromArray:other];
    
    return a;
    
}

@end
