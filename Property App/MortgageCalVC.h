//
//  MortgageCalVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/26/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MortgageCalVC : UIViewController<UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    NSArray * termOfLoanList;
    IBOutlet UIButton * calculateBtn;
    IBOutlet UILabel * monthlyPaymentLb;
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
    IBOutlet UISlider * slider;
    
    double loanAmount;
    double annualPercentageRate;
    double interestRate;
    double loanPeriodInMonths;
    double ratePower;
    double paymentAmount;
    double years;
    int termIndex;
}

@property (nonatomic, retain) IBOutlet UITextField * totalLoanTF;
@property (nonatomic, retain) IBOutlet UITextField * annualInterestTF;
@property (nonatomic, retain) IBOutlet UITextField * termOfLoanTF;
@property (nonatomic, retain) IBOutlet UILabel * termOfLoanLB;

@end
