//
//  GalleryPropertyVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "GalleryPropertyVC.h"
#import "PatternViewCell.h"
#import "MyUIEngine.h"
#import "DatabaseEngine.h"
#import "SWRevealViewController.h"
#import "ImagePickCell.h"
#import "ReviewImageVC.h"
@interface GalleryPropertyVC ()

@end



@implementation GalleryPropertyVC

@synthesize collectionView = cv;
//@synthesize property = _property;
@synthesize imageList, property;



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [imageList addObject:chosenImage];
    //NSData * imageData = UIImageJPEGRepresentation(chosenImage, 0.05f);
    
    //[_property uploadImageToProperty:imageData];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    NSLog(@"%i", [imageList count]);
    [cv reloadData];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [imageList count];
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImagePickCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imageCell2" forIndexPath:indexPath];
    cell.patternImageView.image = [imageList objectAtIndex:indexPath.row];
    return cell;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100.0, 100.0);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReviewImageVC * view = [self.storyboard instantiateViewControllerWithIdentifier:@"reviewImageView2"];
    view.curImage = [imageList objectAtIndex:indexPath.row];
    view.theView.image = [imageList objectAtIndex:indexPath.row];
    view.test = 25;
    [self presentViewController:view animated:YES completion:nil];
}
- (IBAction) Back:(id)sender
{
    
	SWRevealViewController * mainView = [self.storyboard instantiateViewControllerWithIdentifier:@"sw"];
	[self presentViewController:mainView animated:YES completion:nil];
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
	// Do any additional setup after loading the view.
	NSLog(@"Load VIewwwww");
    [self downloadImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) downloadImage
{
	[[DatabaseEngine sharedInstance] downloadImages:self:property];
    //imageList = _property.imageList;
    //NSLog(@"runnnnnn %i", imageList.count);
    //[cv reloadData];
}

- (void) DownloadImageSuccessfully : (NSMutableArray*) imageArray
{
	if (imageArray.count > 0) {
		imageList = imageArray;
		[cv reloadData];
	} else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No image"
														message:@"No image added"
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
	}
	
}

- (void) DownloadImageFail : (NSString*) errorString
{
	
	
	NSLog(@"%@", errorString);
}


@end
