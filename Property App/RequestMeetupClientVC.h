//
//  RequestMeetupClientVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/13/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
#import "ScheduleEvent.h"
#import "User.h"
#import "ScheduleListVC.h"

@interface RequestMeetupClientVC : UITableViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    IBOutlet UIImageView * thumbnailTV;
    IBOutlet UILabel * projectyNameLB;
    IBOutlet UILabel * projectTypeLB;
    
    IBOutlet UILabel *propertyAgentLB;
    IBOutlet UILabel * addressLB;
    IBOutlet UITextView *addressTV;
    IBOutlet UITextField * idealBuyingPriceTF;
    IBOutlet UITextField * proposedMeetupDateTF;
    IBOutlet UITextField * proposedMeetupTimeTF;
    
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
    
    
    UIDatePicker * datePick;
    UIDatePicker * timePick;
    NSDate * curDate;
    
    
    User * curUser;
    
    
    IBOutlet UIButton * acceptBtn;
    IBOutlet UIActivityIndicatorView * indicator;
    BOOL isReschedule;
    
    IBOutlet UITableViewCell * acceptCell;
    IBOutlet UITableViewCell * rescheduleCell;
    
}

@property (nonatomic) BOOL isEvent;
@property (nonatomic, retain) ScheduleEvent * event;
@property (nonatomic, retain) Property * property;
@property (nonatomic, retain) ScheduleListVC * scheduleList;

- (void)addDone;


@end
