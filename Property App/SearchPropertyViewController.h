//
//  SearchPropertyViewController.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
@interface SearchPropertyViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSArray * salesData;
    NSArray * rentsData;
    NSArray * data;
    IBOutlet UISegmentedControl * Segment;
	
}
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) Property* property;
- (IBAction)SegmentedChange:(id)sender;

@end
