//
//  SalesRowViewController.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "SalesRowViewController.h"
#import "SalesListViewController.h"
#import "DatabaseEngine.h"
#import "MyUIEngine.h"
#import "DataEngine.h"
@interface SalesRowViewController ()

@end

@implementation SalesRowViewController
@synthesize picker = _picker;
//old
@synthesize type = _type;
@synthesize property;
//new


@synthesize priceRangeChoice, psfRangeChoice, districtChoice, tenureRangeChoice, yieldRangeChoice;
@synthesize keyBoardIsActive, mypickerToolbar, doneBtn, activeTextField;

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
- (void) DefaultSetting
{
    /*
	 NSString * price = [NSString stringWithFormat:@"%i-%i", 0, 1000000];
	 [_price setText:price];
	 
	 NSString * psf = [NSString stringWithFormat:@""];
	 [_psf setText:psf];*/
	
    
}

- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) Search:(id)sender
{
	property.district = _districtTxF.text;
	property.bedroom = _bedroomTxF.text;
	property.area = _areaTxF.text;
	property.price = _priceTxF.text;
	property.tenure = _tenureTxF.text;
	property.rentedPrice = _rentedPriceTxF.text;
	property.projectName = _projectTxF.text;
	property.PSF = _psfTxF.text;
	property.topYear = _topYearTxF.text;
	property.rentalYeild = _rentedPriceTxF.text;
	[[DatabaseEngine sharedInstance] SearchProperty:property : self];
	NSLog(@"search type%@", _type);
    //[[DatabaseEngine sharedInstance] SearchProperty:_type :_location.text :_project.text :_tenure.text :_price.text :_bedroom.text :_floorSize.text :_psf.text :_age.text :_sortBy.text :self];
	//[self SearchProperty:_type :_location.text :_project.text :_tenure.text :_price.text :_bedroom.text :_floorSize.text :_psf.text :_age.text :_sortBy.text];
    //search Button
    
    /*
	 if ([_district.text isEqualToString:@"District"]) {
	 [_district setText:@""];
	 }
	 if ([_price.text isEqualToString:@"Price"]) {
	 [_price setText:@""];
	 }
	 if ([_area.text isEqualToString:@"Area"]) {
	 [_area setText:@""];
	 }
	 if ([_tenure.text isEqualToString:@"Tenure"]) {
	 [_tenure setText:@""];
	 }
	 if ([_psf.text isEqualToString:@"PSF"]) {
	 [_psf setText:@""];
	 }
	 if ([_rentalYield.text isEqualToString:@"Rental Yield"]) {
	 [_rentalYield setText:@""];
	 }
	 [[DatabaseEngine sharedInstance] SearchProperty: _type: _district.text :_price.text :_area.text :_tenure.text :_psf.text :_rentalYield.text :self];
     */
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}


- (IBAction) togglePicker : (id)sender {
	
}

- (void)viewDidLoad
{
	
    [super viewDidLoad];
	[_picker setHidden:true];
	[mypickerToolbar setHidden:true];
	//init choices for pickers
	self.districtChoice = [[NSArray alloc] initWithObjects:
						   @"",
                           @"D01 Boat Quay / Raffles Place",
						   @"D02 Chinatown / Tanjong Pagar",
						   @"D03 Alexandra / Commonwealth",
                           @"D04 Habourfront / Telok Blangah",
						   @"D05 Buona Vista / West Coast",
						   @"D06 City Hall / Clarke Quay",
						   @"D07 Beach Road / Bugis / Rohor",
						   @"D08 Farrer Park / Serangoon Rd",
						   @"D09 Orchard / River Valley",
						   @"D10 Tanglin / Holland",
						   @"D11 Newton / Novena",
						   @"D12 Balestier / Toa Payoh",
						   nil];
	self.priceRangeChoice = [[NSArray alloc] initWithObjects:
                             @"", @"0-100000", @"100001-500000", @"500001-700000",
                             @"700001-1000000", nil];
	
	self.psfRangeChoice = [[NSArray alloc] initWithObjects:
                           @"", @"0-10", @"11-30", @"31-50",
                           @"51-100", nil];
	self.tenureRangeChoice = [[NSArray alloc] initWithObjects:
							  @"", @"FH",
							  @"999",
							  @"99",
							  @"60",
							  @"30",
							  @"<30yrs", nil];;
	self.yieldRangeChoice = [[NSArray alloc] initWithObjects:
							 @"", @"0-1%", @"1-3%", @"3-5%",
							 @"5-10%", nil];;
	
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
	// Do any additional setup after loading the view.
    [self DefaultSetting];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.delegate = self;
	
	
	
	mypickerToolbar.barStyle = UIBarStyleBlackOpaque;
	
	[mypickerToolbar sizeToFit];
	
	
	NSMutableArray *barItems = [[NSMutableArray alloc] init];
	
	
	UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
	
	[barItems addObject:flexSpace];
	
	doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDoneClicked)];
	
	[barItems addObject:doneBtn];
	
	
	[mypickerToolbar setItems:barItems animated:YES];
	
	[self initTextfieldPlaceHolder];
	//_location.inputAccessoryView = mypickerToolbar;
}

- (void) initTextfieldPlaceHolder
{
    _bedroomTxF.placeholder = @"Bedroom: ";
    _districtTxF.placeholder = @"District: ";
	_areaTxF.placeholder = @"Area: ";
	_priceTxF.placeholder = @"Price: ";
	_tenureTxF.placeholder = @"Tenure: ";
	_rentedPriceTxF.placeholder = @"Rental Price: ";
	_projectTxF.placeholder = @"Project: ";
	_psfTxF.placeholder = @"PSF: ";
    _topYearTxF.placeholder = @"Top Year: ";
	_rentalYeildTxF.placeholder = @"Rental Yield: ";
    
	
	[_bedroomTxF setText:@""];
	[_districtTxF setText:@""];
	[_areaTxF setText:@""];
	[_priceTxF setText:@""];
	[_tenureTxF setText:@""];
	[_rentedPriceTxF setText:@""];
	[_projectTxF setText:@""];
	[_psfTxF setText:@""];
	[_topYearTxF setText:@""];
	[_rentalYeildTxF setText:@""];
    
}


-(void)pickerDoneClicked

{
  	NSLog(@"Done Clicked");
	[_picker setHidden:true];
	[mypickerToolbar setHidden:true];
	[self.activeTextField resignFirstResponder];
	
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([touch.view isKindOfClass:[UIButton class]]){
        return NO;
    }
    return YES; // handle the touch
}

- (void) dismissKeyboard{
    [self.activeTextField resignFirstResponder];
}



- (void)keyboardWasShown:(NSNotification *)notification
{
	keyBoardIsActive = true;
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
    
    
    // Step 3: Scroll the target text field into view.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(aRect, self.activeTextField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.activeTextField.frame.origin.y - (keyboardSize.height-15));
        [theScrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void) keyboardWillHide:(NSNotification *)notification {
    keyBoardIsActive = false;
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
}

// Set activeTextField to the current active textfield

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
	self.activeTextField = textField;
	
	if (_picker.hidden == false) {
		[_picker setHidden:true];
		[mypickerToolbar setHidden:true];
	}
    
	if ((textField == self.priceTxF) || (textField == self.psfTxF)
		|| (textField == self.districtTxF) || (textField == self.tenureTxF)
		|| (textField == self.rentalYeildTxF)){
		[self.activeTextField resignFirstResponder];
		//NSLog(@"Hey there");
		[textField resignFirstResponder];
		[_picker reloadAllComponents];
		[mypickerToolbar setHidden:false];
		[_picker setHidden:false];
		
	}
}

// Set activeTextField to nil

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //self.activeTextField = nil;
}


// Dismiss the keyboard

- (IBAction)dismissKeyboard:(id)sender
{
    [self.activeTextField resignFirstResponder];
}

- (void) SearchSuccessfully : (NSArray*) objects
{
	NSMutableArray * propertyAray = [[NSMutableArray alloc] init];
	NSMutableArray * titles = [[NSMutableArray alloc] init];
	//NSLog(@"%i", [objects count]);
	for (PFObject *object in objects) {
		[propertyAray addObject:object];
		[titles addObject:[object objectForKey:@"propertyName"]];
	}
	[[DataEngine sharedInstance] SetPropertyDataArray:propertyAray];
	[[DataEngine sharedInstance] SetPropertyNameList:titles];
	
	SalesListViewController *pushview = [self.storyboard instantiateViewControllerWithIdentifier:@"salesListView"];
	pushview.navBar.topItem.title = @"Search Result";
	[self presentViewController:pushview animated:YES completion:nil];
}

- (void) SearchFailed : (NSString*) errorString
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:errorString
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
	//NSLog(@"%@" , errorString);
}

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
	return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
	//NSLog(@"he heee %@", self.activeTextField);
	if (self.activeTextField == self.priceTxF) {
		return [self.priceRangeChoice count];
	} else if (self.activeTextField == self.psfTxF) {
		return [self.psfRangeChoice count];
	} else if (self.activeTextField == self.districtTxF) {
		return [self.districtChoice count];
	} else if (self.activeTextField == self.tenureTxF) {
		return [self.tenureRangeChoice count];
	} else if (self.activeTextField == self.rentalYeildTxF) {
		return [self.yieldRangeChoice count];
	}
	return -1;
	
}
- (NSString *)pickerView:(UIPickerView *)pickerView
			 titleForRow:(NSInteger)row
			forComponent:(NSInteger)component
{
	
	if (self.activeTextField == self.priceTxF) {
		return [priceRangeChoice objectAtIndex:row];
	} else if (self.activeTextField == self.psfTxF) {
		return [psfRangeChoice objectAtIndex:row];
	} else if (self.activeTextField == self.districtTxF) {
		NSLog(@"ya hee %@", [districtChoice objectAtIndex:row]);
		return [districtChoice objectAtIndex:row];
	} else if (self.activeTextField == self.tenureTxF) {
		return [tenureRangeChoice objectAtIndex:row];
	} else if (self.activeTextField == self.rentalYeildTxF) {
		return [yieldRangeChoice objectAtIndex:row];
	}
	return @"nil";
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
	  inComponent:(NSInteger)component
{
	if (self.activeTextField == self.priceTxF) {
		[self.priceTxF setText:[NSString stringWithFormat:@"%@", [priceRangeChoice objectAtIndex:row]]];
	} else if (self.activeTextField == self.psfTxF) {
		[self.psfTxF setText:[NSString stringWithFormat:@"%@", [psfRangeChoice objectAtIndex:row]]];
	} else if (self.activeTextField == self.districtTxF) {
		[self.districtTxF setText:[NSString stringWithFormat:@"%@", [districtChoice objectAtIndex:row]]];
	} else if (self.activeTextField == self.tenureTxF) {
		[self.tenureTxF setText:[NSString stringWithFormat:@"%@", [tenureRangeChoice objectAtIndex:row]]];
	} else if (self.activeTextField == self.rentalYeildTxF) {
		[self.rentalYeildTxF setText:[NSString stringWithFormat:@"%@", [yieldRangeChoice objectAtIndex:row]]];
	}
	
	//[_picker setHidden:true];
}

-(IBAction)textFieldReturn:(id)sender
{
    [sender resignFirstResponder];
}


@end
