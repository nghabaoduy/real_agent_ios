//
//  FounderCell.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FounderCell : UITableViewCell


@property (nonatomic, strong) IBOutlet UILabel * nameLb;

@end
