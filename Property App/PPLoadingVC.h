//
//  PPLoadingVC.h
//  Property App
//
//  Created by Brian on 29/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
#import "ParentLoadingVC.h"
@interface PPLoadingVC :ParentLoadingVC

@property (strong, nonatomic) Property * property;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
