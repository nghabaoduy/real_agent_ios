//
//  RequestMeetupAgentVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/13/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "RequestMeetupAgentVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"

@interface RequestMeetupAgentVC ()

@end

@implementation RequestMeetupAgentVC
@synthesize isEvent, event;

- (void) hideCell
{
    if (event.agentAccept) {
        acceptCell.hidden = YES;
        rescheduleCell.hidden = YES;
        meetupVenueTF.enabled = NO;
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [thumbnailTV setImage:event.picture];
    [projectyNameLB setText:event.projectName];
    [projectTypeLB setText:event.projectType];
    [addressLB setText:event.address];
    [addressTV setText:event.address];
    [meetupVenueTF setText:event.meetupVenue];
    [requesterLB setText:[NSString stringWithFormat:@"Requester: %@",event.requester.fullName]];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *title = [outputFormatter stringFromDate:event.eventSchedule];
    
    [proposedMeetupDateTF setText:title];
    
    [outputFormatter setDateFormat:@"HH:mm a"];
    
    NSString *title1 = [outputFormatter stringFromDate:event.eventSchedule];
    
    [proposedMeetupTimeTF setText:title1];
    
    curDate =event.eventSchedule;
    
    meetupVenueList = [[NSArray alloc] initWithObjects:
                       @"[select]",
                       @"Actual Unit",
                       @"Void Deck/Lobby",
                       @"Call When Reach"
                       , nil];
    
    curUser = [[DataEngine sharedInstance]GetCurUser];
    
    if (!event.isReschedule) {
        [self hideCell];
    }
    NSLog(@"schedule = %hhd", isReschedule);
    
    [self initTextfieldPlaceHolder];
    
}

- (void) initTextfieldPlaceHolder
{
    meetupVenueTF.placeholder = @"Meetup Venue: ";
    proposedMeetupDateTF.placeholder = @"Proposed Meetup Date: ";
    proposedMeetupTimeTF.placeholder = @"Proposed Meetup Time: ";
    
    
    //address.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
    
}

- (void)showDatePicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    datePick = [[UIDatePicker alloc] init];
    datePick.datePickerMode = YES;
    [datePick setDate:curDate];
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    
    
    
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    [datePick addTarget:self
                 action:@selector(SetDatePickerTime:)
       forControlEvents:UIControlEventValueChanged];
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:datePick];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    CGRect pickerRect = datePick.bounds;
    pickerRect.origin.y = 0;
    datePick.bounds = pickerRect;
    
}

-(void)SetDatePickerTime:(id)sender
{
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *title = [outputFormatter stringFromDate:datePick.date];
    
    [proposedMeetupDateTF setText:title];
    curDate = datePick.date;
}


- (void)showTimePicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    timePick = [[UIDatePicker alloc] init];
    timePick.datePickerMode = UIDatePickerModeTime;
    [timePick setDate:curDate];
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    
    
    
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    [timePick addTarget:self
                 action:@selector(setTimePicker:)
       forControlEvents:UIControlEventValueChanged];
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:timePick];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    CGRect pickerRect = timePick.bounds;
    pickerRect.origin.y = 0;
    timePick.bounds = pickerRect;
    
}

-(void)setTimePicker:(id)sender
{
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm a"];
    
    NSString *title = [outputFormatter stringFromDate:timePick.date];
    
    [proposedMeetupTimeTF setText:title];
    curDate = timePick.date;
}



-(void)categoryDoneButtonPressed{
    //categoryLable.text = selectedCategory;
    if (activeTextField == meetupVenueTF) {
        [meetupVenueTF setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if (activeTextField == meetupVenueTF) {
        selectedCategory = [NSString stringWithFormat:@"%@",[meetupVenueList objectAtIndex:row]];
    }
    
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (activeTextField == meetupVenueTF)
    {
        return [meetupVenueList count];
    }
    
    
    return [meetupVenueList count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == meetupVenueTF)
    {
        return [meetupVenueList objectAtIndex:row];
    }
    return [NSString stringWithFormat:@"%d", row+1];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:proposedMeetupDateTF] )
    {
        [textField resignFirstResponder];
        [self showDatePicker];
    }
    else if ([textField isEqual:proposedMeetupTimeTF])
    {
        [textField resignFirstResponder];
        [self showTimePicker];
    }
    else if ([textField isEqual:meetupVenueTF])
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}





- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    float keyboardHeight = 216.0f;
    if (textField.frame.origin.y + textField.frame.size.height > self.view.frame.size.height - keyboardHeight) {
        const int movementDistance = 100; // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

-(void)dismissKeyboard {
    [activeTextField resignFirstResponder];
}

- (BOOL)validateMeetupVenue
{
    if (![meetupVenueTF.text isEqualToString:@""]) {
        return YES;
    }
    return NO;
}

- (IBAction)accept:(id)sender
{
    if ([self validateMeetupVenue]) {
        NSLog(@"accept");
        event.meetupVenue = meetupVenueTF.text;
        event.eventSchedule = curDate;
        [[DatabaseEngine sharedInstance]acceptAgent:self :event ];
        
    }
    
}

- (IBAction)done:(id)sender
{
    
}

- (IBAction)reschedule:(id)sender
{
    NSLog(@"reschedule");
    proposedMeetupTimeTF.enabled = YES;
    [proposedMeetupTimeTF setPlaceholder:@"Proposed Viewing Time"];
    proposedMeetupDateTF.enabled = YES;
    [proposedMeetupDateTF setPlaceholder:@"Proposed Viewing Date"];
    event.isReschedule = YES;
    isReschedule = YES;
    
    [acceptBtn setTitle:@"Done" forState:UIControlStateNormal];
    [rescheduleCell setHidden:YES];
    
}

@end
