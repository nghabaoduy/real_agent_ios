//
//  DataEngine.h
//  Property App
//
//  Created by Jonathan Le on 10/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Property.h"
#import "User.h"
#import "LoadingViewController.h"
#import "MyClientViewController.h"
#import "ClientSearchViewController.h"


@interface DataEngine : NSObject
{
	
}
@property (nonatomic, retain) LoadingViewController * loadingView;

@property (nonatomic, retain) User* curUser;
@property (nonatomic, retain) User* viewingUser;
@property (nonatomic, retain) Property* curPropertyInstance;
@property (nonatomic, retain) NSMutableArray* propertyDataArray;
@property (nonatomic, retain) NSMutableArray* propertyNameList;
@property (nonatomic, retain) Property* addingProperty;
@property (nonatomic, retain) MyClientViewController *myClientView;
@property (nonatomic, retain) ClientSearchViewController *clientSearchView;


@property (nonatomic, retain) NSMutableArray * participantsList;

@property (nonatomic) BOOL isAgent;
+ (DataEngine*)sharedInstance;

- (void) SetCurUser:(User *)user;
- (User*) GetCurUser;

- (void) setViewingUser:(User *)viewingUser;
- (User*) getViewingUser;

- (void) SetCurPropertyInstace:(Property *)property;
- (Property*) GetCurPropertyInstace;

- (void) SetPropertyDataArray:(NSMutableArray *)propertyArray;
- (NSMutableArray*) GetPropertyDataArray;

- (void) SetPropertyNameList:(NSMutableArray *)nameList;
- (NSMutableArray*) GetPropertyNameList;


- (void) setIsAgent: (BOOL) isAg;
- (BOOL) IsAgent;

-(NSString *) cleanPhoneNo : (NSString *) phoneNumber;

- (NSString*) getBlockNumber : (NSString*)PostalCode;
- (NSString*) getDistrict : (NSString*)PostalCode;
- (NSArray*) getDistrictList;

-(void) saveLoginInfo:(NSString*) username : (NSString*) password;
-(NSDictionary*) loadLoginInfo;

@end
