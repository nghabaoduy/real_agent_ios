//
//  ResidentialCalVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/26/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResidentialCalVC : UIViewController<UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
    
    
    NSArray * profileOfBuyerList;
    NSArray * currentResidentialList;
    
    IBOutlet UILabel * value;
    IBOutlet UITextField * purchasePriceTF;
    IBOutlet UITextField * profileOfBuyerTF;
    IBOutlet UITextField * currentResidentialTF;
    
    double purchasePrice;
    double stampDuty;
    double priceABSD;
    double numberOfProperty;
}


@end
