//
//  DetailHDBVC.m
//  Property App
//
//  Created by Brian on 7/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailLandVC.h"
#import "MyUIEngine.h"
#import "RequestMeetupClientVC.h"
#import "RequestMeetupAgentVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"

@interface DetailLandVC ()

@end

@implementation DetailLandVC

@synthesize property = _p;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    
    [self initTextfieldPlaceHolder];
    [self LoadInformation];
    [self hideSchedule];
    
    curUser = [[DataEngine sharedInstance]GetCurUser];
    type = @"Client";
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId])
    {
        NSLog(@"trung`");
        schedule.hidden = YES;
    }
    
    NSLog(@"is share %hhd", _isSharing);
    if (!_isSharing) {
        NSLog(@"add to share");
        addToShare.hidden = YES;
    }
}


- (void)LoadInformation
{
    [_p printInfo];
    [_p removeNullData];
    [_p printInfo];
    [propertyName setText:_p.propertyName];
    [propertyType setText:_p.propertyType];
    
    if (_p.unitNo!=nil && ![_p.unitNo isEqualToString:@""]) {
        if (self.allowUnitNo || [_p.agent.username isEqualToString:[DataEngine sharedInstance].curUser.username]) {
            addressTV.text= [NSString stringWithFormat:@"#%@-%@ %@ %@",_p.levelNumber,_p.unitNo,_p.address,_p.postCode];
        }
    }
    else
        
    {
        addressTV.text= [NSString stringWithFormat:@"%@ %@",_p.address,_p.postCode];
    }
    NSLog(@"%@",_p.district);
    [price setText:_p.price];
    
    icon.image = _p.iconImage;
    [district setText:_p.district];
    [noOfBedroom setText:_p.bedroom];
    [floorSize setText:_p.area];
    
    [tenure setText:_p.tenure];
    [status setText:_p.status];
    [rentedPrice setText:_p.rentedPrice];
    
    
    
    [projectName setText:_p.projectName];
    [topYear setText:_p.topYear];
    [psf setText:_p.PSF];
    [rentalYield setText:_p.rentalYeild];
    
    
    //land
    [landType setText:_p.landType];
    [landSize setText:_p.landSize];
    [carPark setText:_p.carPark];
    [furnishing setText:_p.furnishing];
    [mainDoorDirection setText:_p.mainDoorDirection];
    [mainGateFacing setText:_p.mainGateFacing];
    [garden setText:_p.garden];
    [swimmingPool setText:_p.swimmingPool];
    [basement setText:_p.basement];
    [roofTerrance setText:_p.roofTerrance];
    
    
    Agent* agent = _p.agent;
    [agentName setText:[NSString stringWithFormat:@"%@ %@",agent.firstName, agent.lastName]];
    [agentMobile setText:[NSString stringWithFormat:@"Tel: %@",agent.agentPhone]];
    [agentEmail setText:[NSString stringWithFormat:@"%@",agent.email]];
    
}

- (void) initTextfieldPlaceHolder
{
    
    district.placeholder = @"District: ";
    noOfBedroom.placeholder = @"Bedroom: ";
    floorSize.placeholder = @"Built Up: ";
    price.placeholder = @"Price: ";
    tenure.placeholder = @"Tenure: ";
    status.placeholder = @"Status: ";
    rentedPrice.placeholder = @"Rental Price: ";
    landType.placeholder = @"Land Type: ";
    
    
    projectName.placeholder = @"Project Name: ";
    topYear.placeholder = @"Top Year: ";
    psf.placeholder = @"PSF: ";
    rentalYield.placeholder = @"Rental Yield: ";
    
    //hdb
    furnishing.placeholder = @"Furnishing: ";
    landSize.placeholder = @"Land Size: ";
    mainGateFacing.placeholder = @"Main Gate Facing: ";
    mainDoorDirection.placeholder = @"Main Door Dir: ";
    carPark.placeholder = @"Car Park: ";
    garden.placeholder = @"Garden: ";
    swimmingPool.placeholder = @"Swimming Pool: ";
    basement.placeholder = @"Basement: ";
    roofTerrance.placeholder = @"Roof Terrance: ";
    
    district.enabled = NO;
    noOfBedroom.enabled = NO;
    floorSize.enabled = NO;
    price.enabled = NO;
    tenure.enabled = NO;
    status.enabled = NO;
    rentedPrice.enabled = NO;
    landType.enabled = NO;
    
    
    projectName.enabled = NO;
    topYear.enabled = NO;
    psf.enabled = NO;
    rentalYield.enabled = NO;
    
    //hdb
    furnishing.enabled = NO;
    landSize.enabled = NO;
    mainGateFacing.enabled = NO;
    mainDoorDirection.enabled = NO;
    carPark.enabled = NO;
    garden.enabled = NO;
    swimmingPool.enabled = NO;
    basement.enabled = NO;
    roofTerrance.enabled = NO;
    
}
- (IBAction)callAgent:(id)sender {
    Agent* agent = _p.agent;
    NSLog(@"agentPhone = %@",agent.phone);
    
    NSString * call = [NSString stringWithFormat:@"tel:%@", agent.phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}
- (IBAction)emailAgent:(id)sender {
    Agent* agent = _p.agent;
    NSString * email = agent.email;
    NSArray * recipients = [NSArray arrayWithObject:email];
    
    NSMutableString *body = [NSMutableString string];
    
    [body appendString:@"\n"];
    [body appendString:@"\n"];
    [body appendString:@"<a>Send from property : </a>\n"];
    [body appendString:[NSString stringWithFormat:@"<a href=\"realagent://property-%@\">Track Property</a>\n",self.property.propertyObject.objectId]];
    if ([MFMailComposeViewController canSendMail]) {
        // Show the composer
        NSLog(@"agentEmail = %@",agent.email);
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:recipients];
        [controller setMessageBody:body isHTML:YES];
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Handle the error
    }
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)schedule:(id)sender
{
    [self performSegueWithIdentifier:@"goMeetupClient" sender:self];
}

-(void)hideSchedule
{
    NSLog(@"cur = %@", curUser.userObject.objectId);
    NSLog(@"proper = %@", _p.agent.userObject.objectId);
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId])
    {
        NSLog(@"hide");
        schedule.hidden = YES;
    }
}

- (IBAction)AddToShare:(id)sender
{
    [sender setEnabled:NO];
    [[DatabaseEngine sharedInstance] valiadteLand:self];
}

- (void)showDialog:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
    [alert show];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goMeetupClient"])
    {
        RequestMeetupClientVC * destination = segue.destinationViewController;
        destination.property = _p;
    }
}
@end
