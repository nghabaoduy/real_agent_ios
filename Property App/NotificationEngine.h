//
//  NotificationEngine.h
//  Property App
//
//  Created by Brian on 22/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "MainViewController.h"

@interface NotificationEngine : NSObject

@property (nonatomic, retain) NSString *passInfo;
@property (nonatomic, retain) MainViewController *mainView;

+ (NotificationEngine*)sharedInstance;

-(void) registerPush: (NSData *) deviceToken;
-(void) setPushUser:(PFObject *)userObj;

-(void) pushToAll:(NSString *) message;
-(void) pushToUserObj:(PFObject *) userObj :(NSDictionary *) messageDict;

-(void) handleNotification:(NSDictionary *)userInfo;

-(void) analyzePassingInfo;

-(void) simulatePush;

@end
