//
//  NewAddGroupVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "NewAddGroupVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"

@interface NewAddGroupVC ()

@end

@implementation NewAddGroupVC
@synthesize groupListView = _groupListView;

- (void)add:(id)sender
{
    [self startLoading];
    [self validateTitle];
}

- (void)cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)validateTitle
{
    [[DatabaseEngine sharedInstance] validateTitle:self :groupNameTxF.text];
}

- (void)addToDatabase
{
    NSMutableDictionary * newGroup = [[NSMutableDictionary alloc]init];
    [newGroup setValue:groupNameTxF.text forKey:@"title"];
    [newGroup setValue:[[DataEngine sharedInstance] GetCurUser].userObject forKey:@"creator"];
    
    [[DatabaseEngine sharedInstance] addNewGroup:self :newGroup];
}

- (void)showDialog:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
    [alert show];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
