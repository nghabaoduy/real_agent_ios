//
//  ImagePickCell.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 22/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ImagePickCell.h"
#import "ReviewImageVC.h"

@implementation ImagePickCell
@synthesize curImage = _curImage;
@synthesize patternImageView = _patternImageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



@end
