//
//  Agent.h
//  Property App
//
//  Created by Jonathan Le on 12/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "User.h"
#import "Client.h"
#import "GetUserViewController.h"
#import "ProPortfolioViewController.h"
@interface Agent : User

@property (nonatomic, retain) NSMutableArray* myClientList;
@property (nonatomic, retain) NSMutableArray* myClientPropertyList;
@property (nonatomic, retain) GetUserViewController* parentView;

@property (nonatomic, retain) NSMutableArray* refreshViewArray;
-(void) addRefreshView: (MyUIViewController *) view;


- (PFUser*) getUserObject;
- (void) GetMyClientList;
- (void) GetClientSuccessful : (NSMutableArray*) clientList;
- (void) GetClientFailed : (NSString*) errorString;

- (void) GetAgentClientPropertyList;
- (void) GetAgentClientPropertySuccessful : (NSMutableArray*) propList;
- (void) RefreshData;
- (void) GetAgentClientPropertyFailed: (NSString*) errorString;
@end

