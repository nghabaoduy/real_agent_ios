//
//  CalendarDisplayCV.m
//  Property App
//
//  Created by Brian on 17/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "CalendarDisplayCV.h"

#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "ScheduleEvent.h"
#import "CalendarDetailCell.h"

@interface CalendarDisplayCV ()

@end

@implementation CalendarDisplayCV

@synthesize myRequestList, otherRequestList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)actionRun
{
    [super actionRun];
    [[DatabaseEngine sharedInstance] getRequestListForCalendar:self :curUser];
}

- (void)actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    [calendar reset];
    selectedDate = [NSDate date];
    
    [self refreshAcceptList];
    [tv reloadData];
    [self resetTableViewFrame];
}

- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
}

-(void) refreshAcceptList
{
    acceptRequestList = [[NSMutableArray alloc] init];
    dailyAcceptList = [[NSMutableArray alloc] init];
    
    NSLog(@"my %i", [myRequestList count]);
    NSLog(@"other %i", [otherRequestList count]);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    for (ScheduleEvent *event in myRequestList) {
        NSLog(@"my event type [%i] = %@",[myRequestList indexOfObject:event],[event getEventType]);
        if ([[event getEventType] isEqualToString:@"Event accepted"]) {
            [acceptRequestList addObject:event];
        }
        
        
    }
    for (ScheduleEvent *event in otherRequestList) {
        NSLog(@"other event type [%i] = %@",[otherRequestList indexOfObject:event],[event getEventType]);
        if ([[event getEventType] isEqualToString:@"Event accepted"]) {
            [acceptRequestList addObject:event];
        }
    }
    
    for(ScheduleEvent *event in acceptRequestList)
    {
        if ([[formatter stringFromDate:event.eventSchedule] isEqualToString:[formatter stringFromDate:selectedDate]]) {
            [dailyAcceptList addObject:event];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    acceptRequestList = [[NSMutableArray alloc] init];
    dailyAcceptList = [[NSMutableArray alloc] init];
    selectedDate = [NSDate date];
    
	// Do any additional setup after loading the view, typically from a nib.
    
    calendar = [[VRGCalendarView alloc] init];
    calendar.delegate=self;
    
    calendar.frame = CGRectMake(0, 60, self.view.frame.size.width, calendar.frame.size.height);
    [self.view addSubview:calendar];
    
    
    curUser = [[DataEngine sharedInstance] GetCurUser];
    myRequestList = [[NSArray alloc]init];
    otherRequestList = [[NSArray alloc]init];
    [self actionRun];
    
}

-(void)calendarView:(VRGCalendarView *)calendarView switchedToMonth:(int)month targetHeight:(float)targetHeight animated:(BOOL)animated {
    
    selectedDate = NULL;
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* thiscalendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [thiscalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
    
    calendarTargetHeight = targetHeight;
    
    acceptRequestList = [[NSMutableArray alloc] init];
    dailyAcceptList = [[NSMutableArray alloc] init];
    [self refreshAcceptList];
    
    NSMutableArray *markDates = [[NSMutableArray alloc] init];
    for (ScheduleEvent *event in acceptRequestList) {
        if ([event getEventYear] == [components year] &&
            [event getEventMonth] == month) {
            [markDates addObject:[NSNumber numberWithInteger:[event getEventDay]]];
        }
    }
    [calendarView markDates:markDates];
    
    [tv reloadData];
    
    /*if (month==[components month]) {
     NSArray *dates = [NSArray arrayWithObjects:[NSNumber numberWithInt:1],[NSNumber numberWithInt:5], nil];
     [calendarView markDates:dates];
     }*/
    [self resetTableViewFrame];
    NSLog(@"-target height = %f",targetHeight);
    NSLog(@"-target month = %i",month);
}


-(void)calendarView:(VRGCalendarView *)calendarView dateSelected:(NSDate *)date {
    selectedDate = date;
    NSLog(@"Selected date = %@",date);
    [self refreshAcceptList];
    [tv reloadData];
    [self resetTableViewFrame];
    
    
}

-(void) resetTableViewFrame
{
    [tv removeConstraints:tv.constraints];
    double delayInSeconds = 0.001;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        tv.frame = CGRectMake(0, 60 +calendarTargetHeight , self.view.frame.size.width, self.view.frame.size.height-60-calendarTargetHeight-49);
    });
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//table part

- (UITableViewCell *) tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"calendarDetailCell";
    CalendarDetailCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CalendarDetailCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    ScheduleEvent *event = [dailyAcceptList objectAtIndex:indexPath.row];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    
    cell.eventTimeLbl.text = [formatter stringFromDate:event.eventSchedule];
    cell.eventTitleLbl.text = event.projectName;
    cell.eventClientLbl.text = [NSString stringWithFormat:@"Project Type: %@",event.projectType];
    cell.eventAddressLbl.text = [NSString stringWithFormat:@"Meetup Venue: %@",event.meetupVenue];
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    
    return [dailyAcceptList count];
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0.0001f)];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.001f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScheduleEvent *event = [dailyAcceptList objectAtIndex:indexPath.row];
    self.selectedEvent = event;
    if (![event.requester.userObject.objectId isEqualToString:[[DataEngine sharedInstance] GetCurUser].userObject.objectId])
    {
        [self performSegueWithIdentifier:@"goReceiver" sender:self];
    }
    else
    {
        [self performSegueWithIdentifier:@"goRequester" sender:self];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goRequester"])
    {
        RequestMeetupClientVC * destination = segue.destinationViewController;
        destination.event = self.selectedEvent;
        destination.isEvent = YES;
        
    }
    else if ([[segue identifier] isEqualToString:@"goReceiver"])
    {
        RequestMeetupAgentVC * destination = segue.destinationViewController;
        destination.event = self.selectedEvent;
        destination.isEvent = YES;
        
    }
}


@end
