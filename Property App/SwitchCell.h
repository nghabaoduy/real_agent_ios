//
//  SwitchCell.h
//  Property App
//
//  Created by Brian on 9/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UISwitch *selectSwitch;
@property (nonatomic, retain) IBOutlet UILabel *mainLabel;

@end
