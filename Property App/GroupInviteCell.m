//
//  GroupInviteCell.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 1/26/14.
//  Copyright (c) 2014 Nguyen Ha Bao Duy. All rights reserved.
//

#import "GroupInviteCell.h"

@implementation GroupInviteCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)decline:(id)sender
{
    [_groupListView declineInvite:_index :object];
}

- (IBAction)accept:(id)sender
{
    [_groupListView acceptInvite:_index :object];
}

@end
