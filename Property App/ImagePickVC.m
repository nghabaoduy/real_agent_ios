//
//  ImagePickVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 21/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ImagePickVC.h"
#import "ImagePickCell.h"
#import "ReviewImageVC.h"
#import "PropertyReviewVC.h"


@interface ImagePickVC ()

@end

@implementation ImagePickVC
@synthesize property = _property;

- (void)deleteAllContent
{
    [imageList removeAllObjects];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque
                                                animated:NO];
    imageList = [[NSMutableArray alloc]init];
    NSLog(@"%i values", [imageList count]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) SelectImage
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void) SelectCamera
{
    if (UIImagePickerControllerSourceTypeCamera)
    {
        UIImagePickerController * imgPicker = [[UIImagePickerController alloc] init];
        imgPicker.delegate = self;
        
        imgPicker.allowsEditing = NO;
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
        [self presentViewController:imgPicker animated:YES completion:NULL];
    }
}

- (IBAction) AddImageOption:(id)sender
{
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle: nil otherButtonTitles:@"Take Picture", @"Choose Existing", nil];
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [popupQuery showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self SelectCamera];
    } else if (buttonIndex == 1) {
        [self SelectImage];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    [imageList addObject:chosenImage];
    [cv reloadData];
	[picker dismissViewControllerAnimated:YES completion:NULL];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque
                                                animated:NO];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [imageList count];
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImagePickCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imageCell" forIndexPath:indexPath];
    cell.patternImageView.image = [imageList objectAtIndex:indexPath.row];
    return cell;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100.0, 100.0);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReviewImageVC * view = [self.storyboard instantiateViewControllerWithIdentifier:@"reviewImageView"];
    view.curImage = [imageList objectAtIndex:indexPath.row];
    view.theView.image = [imageList objectAtIndex:indexPath.row];
    view.test = 25;
    [self presentViewController:view animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"imagePickNext"]) {
        _property.imageList = imageList;
        if (imageList.count > 0) {
            _property.iconImage = [imageList objectAtIndex:0];
        }
        PropertyReviewVC * destination = segue.destinationViewController;
        destination.property = _property;
        if ([self.navigationItem.title isEqualToString:@"Step 2/3"]) {
            [destination.navigationItem setTitle:@"Step 3/3"];
        }
    }
    
}

@end
