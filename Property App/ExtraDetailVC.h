//
//  ExtraDetailVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 21/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
#import "MyUITableViewController.h"

@interface ExtraDetailVC : MyUITableViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    //Basic Information
    IBOutlet UITextField * address;
    IBOutlet UITextField * postalCode;
    IBOutlet UITextField * blockNo;
    IBOutlet UITextField * levelNo;
    IBOutlet UITextField * unitNo;
    IBOutlet UITextField * district;
    IBOutlet UITextField * noOfBedroom;
    IBOutlet UITextField * area;
    IBOutlet UITextField * price;
    IBOutlet UITextField * tenure;
    IBOutlet UITextField * adder;
    IBOutlet UITextField * PRType;
    IBOutlet UITextField * condition;
    IBOutlet UITextField * valuation;
    IBOutlet UITextField * level;
    IBOutlet UITextField * landSize;
    
    //Status
    IBOutlet UITextField * status;
    IBOutlet UITextField * rentedPrice;
    
    
    //Project Information
    IBOutlet UITextField * projectName;
    IBOutlet UITextField * PSF;
    IBOutlet UITextField * topYear;
    IBOutlet UITextField * rentalYield;
    
    
    UITextField * activeTextField;
    NSArray * rentalYieldList;
    NSArray * districtList;
    NSArray * tenureList;
    NSArray * statusList;
    NSArray * PRTypeList;
    NSArray * conditionList;
    NSArray * levelList;
    NSMutableArray * adderList;
    NSMutableArray * adderNameList;
    
    UIActionSheet * menu;
    NSString * selectedCategory;
    
    int deleteSection;
    int addRow;
    int deductRow;
    
    
    IBOutlet UITableViewCell * conditionCell;
    IBOutlet UITableViewCell * valuationCell;
    IBOutlet UITableViewCell * landSizeCell;
    
}
@property (strong, nonatomic) Property * property;
-(void) addBackBtn;

@end
