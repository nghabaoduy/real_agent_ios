//
//  DetailPropertyVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailComercialVC.h"
#import "MyUIEngine.h"
#import "RequestMeetupClientVC.h"
#import "RequestMeetupAgentVC.h"
#import "DataEngine.h"
#import "DatabaseEngine.h"

@interface DetailComercialVC ()

@end

@implementation DetailComercialVC

@synthesize property = _p;

-(void)hideSchedule
{
    NSLog(@"cur = %@", curUser.userObject.objectId);
    NSLog(@"proper = %@", _p.agent.userObject.objectId);
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId])
    {
        NSLog(@"hide");
        schedule.hidden = YES;
    }
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    [self initTextfieldPlaceHolder];
    [self LoadInformation];
    NSLog(@"section header = %f", tv.sectionHeaderHeight);
    NSLog(@"section footer = %f", tv.sectionFooterHeight);
    
    type = @"Client";
    
    
    curUser = [[DataEngine sharedInstance]GetCurUser];
    [self hideSchedule];
    
    
    if (!_isSharing) {
        addToShare.hidden = YES;
    }
}

#define OFFICE_SECTION 4
#define OFFICE_RETAIL 5
#define OFFICE_FAB 6
#define OFFICE_LAND 7

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId] && indexPath.section == tableView.numberOfSections-1)
    {
        return 0.1f;
    }
    if (indexPath.section == 0) {
        return 140;
    }
    if (indexPath.section == tableView.numberOfSections-2) {
        if (indexPath.row == 0) {
            
            return 253;
        }
        else
        {
            if (!_isSharing) {
                return 0;
            }
            else
            {
                return 66;
            }
        }
    }
    if (indexPath.section == tableView.numberOfSections-1) {
        return 43;
    }
    if (indexPath.section > 3) {
        if ([_p.commercialType isEqualToString:@"Office"] && indexPath.section!=OFFICE_SECTION) {
            return 0;
            
        }
        if ([_p.commercialType isEqualToString:@"Retail"] && indexPath.section!=OFFICE_RETAIL) {
            return 0;
            
        }

        if ([_p.commercialType isEqualToString:@"F&B"] && indexPath.section!=OFFICE_FAB) {
            return 0;
            
        }

        if ([_p.commercialType isEqualToString:@"Land/Building"] && indexPath.section!=OFFICE_LAND) {
            return 0;
            
        }

    }
    
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId] && section == tableView.numberOfSections-1)
    {
        return 0.1f;
    }
    if (section == 0) {
        return 35.0f;
    }
    if (section == tableView.numberOfSections-1) {
        return 35.0f;
    }
    if (section == tableView.numberOfSections-2) {
        return 35.0f;
    }
    if (section > 3) {

            return 0.1f;
        
    }
    
    return 35.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId] && section == tableView.numberOfSections-1)
    {
        return 0.1f;
    }
    if (section == 0) {
        return 10.0f;
    }
    if (section == tableView.numberOfSections-1) {
        return 10.0f;
    }
    if (section == tableView.numberOfSections-2) {
        return 10.0f;
    }
    if (section > 3) {
            return 0.1f;
    }
    
    return 10.0f;
}


- (void)LoadInformation
{
    [_p printInfo];
    [_p removeNullData];
    [_p printInfo];
    [propertyName setText:_p.propertyName];
    [propertyType setText:_p.propertyType];
    
    if (_p.unitNo!=nil && ![_p.unitNo isEqualToString:@""]) {
        if (self.allowUnitNo || [_p.agent.username isEqualToString:[DataEngine sharedInstance].curUser.username]) {
            addressTV.text= [NSString stringWithFormat:@"#%@-%@ %@ %@",_p.levelNumber,_p.unitNo,_p.address,_p.postCode];
        }
    }
    else
        
    {
        addressTV.text= [NSString stringWithFormat:@"%@ %@",_p.address,_p.postCode];
    }
    NSLog(@"%@",_p.district);
    [price setText:_p.price];
    [comType setText:_p.commercialType];
    
    //[self.blkNoTextF setText:_p.blkNo];
    // [self.levelTextF setText:_p.levelNumber];
    //[self.unitTextF setText:_p.unitNo];
    icon.image = _p.iconImage;
    [district setText:_p.district];
    [floorSize setText:_p.area];
    
    [tenure setText:_p.tenure];
    [status setText:_p.status];
    [rentedPrice setText:_p.rentedPrice];
    
    
    
    [projectName setText:_p.projectName];
    [topYear setText:_p.topYear];
    [psf setText:_p.PSF];
    [rentalYield setText:_p.rentalYeild];
    
    
    Agent* agent = _p.agent;
    [agentName setText:[NSString stringWithFormat:@"%@ %@",agent.firstName, agent.lastName]];
    [agentMobile setText:[NSString stringWithFormat:@"Tel: %@",agent.agentPhone]];
    [agentEmail setText:[NSString stringWithFormat:@"%@",agent.email]];
    
    //Office extra field
    [OFFICElevelTF setText:_p.groundLevel];
    [OFFICEgstTF setText:_p.gst];
    [OFFICEelectLoadTF setText:_p.electricalLoad];
    [OFFICEamps setText:_p.amps];
    [OFFICEairconTF setText:_p.aircon];
    [OFFICEownPantryTF setText:_p.ownPantry];
    [OFFICEgradeTF setText:_p.grade];
    [OFFICEfloorLoadTF setText:_p.floorLoad];
    [OFFICEserverRoomTF setText:_p.serverRoom];
    
    //retail extra field
    [RETAILlevelTF setText:_p.groundLevel];
    [RETAILgstTF setText:_p.gst];
    [RETAILelectLoadTF setText:_p.electricalLoad];
    [RETAILamps setText:_p.amps];
    [RETAILretailTypeTF setText:_p.retailType];
    [RETAILconditionTF setText:_p.condition];

    //FAB extra field
    [FABlevelTF setText:_p.groundLevel];
    [FABgstTF setText:_p.gst];
    [FABelectLoadTF setText:_p.electricalLoad];
    [FABampsTF setText:_p.amps];
    [FABfabTypeTF setText:_p.fAndBType];
    [FABconditionTF setText:_p.condition];
    [FABgreaseTrapTF setText:_p.greaseTrap];
    [FABexhaustTF setText:_p.exhaust];
    
    //land extra field
    [LANDgstTF setText:_p.gst];
    [LANDelectLoadTF setText:_p.electricalLoad];
    [LANDampsTF setText:_p.amps];
    [LANDbuiltUpTF setText:_p.area];
    [LANDnoOfStoreysTF setText:_p.noOfStorey];

    
    
    
    
}

- (void) initTextfieldPlaceHolder
{
    
    //self.blkNoTextF.placeholder = @"Block: ";
    //self.levelTextF.placeholder = @"Level: ";
    //self.unitTextF.placeholder = @"Unit: ";
    district.placeholder = @"District: ";
    floorSize.placeholder = @"Floor Size: ";
    price.placeholder = @"Price: ";
    tenure.placeholder = @"Tenure: ";
    status.placeholder = @"Status: ";
    rentedPrice.placeholder = @"Rental Price: ";
    projectName.placeholder = @"Project Name: ";
    topYear.placeholder = @"Top Year: ";
    psf.placeholder = @"PSF: ";
    rentalYield.placeholder = @"Rental Yield: ";
    
    price.enabled = NO;
    district.enabled = NO;
    floorSize.enabled = NO;
    tenure.enabled = NO;
    status.enabled = NO;
    rentedPrice.enabled = NO;
    projectName.enabled = NO;
    psf.enabled = NO;
    topYear.enabled = NO;
    rentalYield.enabled = NO;
    
    
    comType.placeholder = @"Commercial Type: ";
    
    //Office extra field
    OFFICElevelTF.placeholder = @"Level: ";
    OFFICEgstTF.placeholder = @"GST: ";
    OFFICEelectLoadTF.placeholder = @"Electrical Load: ";
    OFFICEelectLoadTF.text = @"Single";
    OFFICEamps.placeholder = @"Amps: ";
    OFFICEairconTF.placeholder = @"Aircon: ";
    OFFICEownPantryTF.placeholder = @"Own Pantry: ";
    OFFICEgradeTF.placeholder = @"Grade: ";
    OFFICEfloorLoadTF.placeholder = @"Floor Load : ";
    OFFICEserverRoomTF.placeholder = @"Server Room: ";
    
    //Office extra field
    RETAILlevelTF.placeholder = @"Level: ";
    RETAILgstTF.placeholder = @"GST: ";
    RETAILelectLoadTF.placeholder = @"Electrical Load: ";
    RETAILelectLoadTF.text = @"Single";
    RETAILamps.placeholder = @"Amps: ";
    RETAILretailTypeTF.placeholder = @"Retail Type: ";
    RETAILconditionTF.placeholder = @"Condition: ";

    
    //FAB extra field
    FABlevelTF.placeholder = @"Level: ";
    FABgstTF.placeholder = @"GST: ";
    FABelectLoadTF.placeholder = @"Electrical Load: ";
    FABelectLoadTF.text = @"Single";
    FABampsTF.placeholder = @"Amps: ";
    FABfabTypeTF.placeholder = @"F&B Type: ";
    FABconditionTF.placeholder = @"Condition: ";
    FABgreaseTrapTF.placeholder = @"Grease Trap: ";
    FABexhaustTF.placeholder = @"Exhaust: ";
    
    //land extra field

    LANDgstTF.placeholder = @"GST: ";
    LANDelectLoadTF.placeholder = @"Electrical Load: ";
    LANDelectLoadTF.text = @"Single";
    LANDampsTF.placeholder = @"Amps: ";
    LANDbuiltUpTF.placeholder = @"Built Up: ";
    LANDnoOfStoreysTF.placeholder = @"No of Storeys: ";
    
    comType.enabled = NO;
    
    //Office extra field
    OFFICElevelTF.enabled = NO;
    OFFICEgstTF.enabled = NO;
    OFFICEelectLoadTF.enabled = NO;
    OFFICEairconTF.enabled = NO;
    OFFICEamps.enabled = NO;
    OFFICEownPantryTF.enabled = NO;
    OFFICEgradeTF.enabled = NO;
    OFFICEfloorLoadTF.enabled = NO;
    OFFICEserverRoomTF.enabled = NO;
    
    //Office extra field
    RETAILlevelTF.enabled = NO;
    RETAILgstTF.enabled = NO;
    RETAILelectLoadTF.enabled = NO;
    RETAILamps.enabled = NO;
    RETAILretailTypeTF.enabled = NO;
    RETAILconditionTF.enabled = NO;
    
    
    //FAB extra field
    FABlevelTF.enabled = NO;
    FABgstTF.enabled = NO;
    FABelectLoadTF.enabled = NO;
    FABfabTypeTF.enabled = NO;
    FABampsTF.enabled = NO;
    FABconditionTF.enabled = NO;
    FABgreaseTrapTF.enabled = NO;
    FABexhaustTF.enabled = NO;
    
    //land extra field
    
    LANDgstTF.enabled = NO;
    LANDelectLoadTF.enabled = NO;
    LANDampsTF.enabled = NO;
    LANDbuiltUpTF.enabled = NO;
    LANDnoOfStoreysTF.enabled = NO;
    
}
- (IBAction)callAgent:(id)sender {
    Agent* agent = _p.agent;
    NSLog(@"agentPhone = %@",agent.phone);
    
    NSString * call = [NSString stringWithFormat:@"tel:%@", agent.phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}
- (IBAction)emailAgent:(id)sender {
    Agent* agent = _p.agent;
    NSString * email = agent.email;
    NSArray * recipients = [NSArray arrayWithObject:email];
    
    NSMutableString *body = [NSMutableString string];
    
    [body appendString:@"\n"];
    [body appendString:@"\n"];
    [body appendString:@"<a>Send from property : </a>\n"];
    [body appendString:[NSString stringWithFormat:@"<a href=\"realagent://property-%@\">Track Property</a>\n",self.property.propertyObject.objectId]];
    if ([MFMailComposeViewController canSendMail]) {
        // Show the composer
        NSLog(@"agentEmail = %@",agent.email);
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:recipients];
        [controller setMessageBody:body isHTML:YES];
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Handle the error
    }
    
    
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)schedule:(id)sender
{
    [self performSegueWithIdentifier:@"goMeetupClient" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goMeetupClient"])
    {
        RequestMeetupClientVC * destination = segue.destinationViewController;
        destination.property = _p;
    }
}
- (IBAction)addToShare:(id)sender
{
    [sender setEnabled:NO];
    [[DatabaseEngine sharedInstance] valiadteCommercial:self];
}

- (void)showDialog:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
    [alert show];
}

@end
