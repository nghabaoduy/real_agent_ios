//
//  NewGroupListVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "NewGroupListVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "NewAddGroupVC.h"
#import "DetailGroupTabbar.h"
#import "DetailGroupMemberVC.h"
#import "DetailGroupSettingVC.h"
#import "DetailGroupListingVC.h"
#import "DeleteCell.h"
#import "GroupInviteCell.h"

@interface NewGroupListVC ()

@end

@implementation NewGroupListVC
@synthesize groupList = _groupList;

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)actionRun
{
    [super actionRun];
    User * curUser = [[DataEngine sharedInstance] GetCurUser];
    [[DatabaseEngine sharedInstance] getGroupList:self : curUser];
}

- (void)actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    [self.tableView reloadData];
}

- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _groupList = [[NSMutableArray alloc]init];
    _inviteGroupList = [[NSMutableArray alloc]init];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self actionRun];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [_inviteGroupList count];
    }
    else
    {
        return [_groupList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        static NSString *CellIdentifier = @"InviteCell";
        
        GroupInviteCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[GroupInviteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        PFObject * curObject = [_inviteGroupList objectAtIndex:indexPath.row];
        PFObject * curGroup = [curObject objectForKey:@"groupId"];
        
        [cell.label setText:[curGroup objectForKey:@"title"]];
        cell.index = indexPath;
        cell.object = curObject;
        cell.groupListView = self;
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"newCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        PFObject * curObject = [_groupList objectAtIndex:indexPath.row];
        
        cell.textLabel.text = [curObject objectForKey:@"title"];
        
        return cell;
    }
}

- (void)declineInvite:(NSIndexPath *)myPath :(PFObject *)curGroup
{
    if (myPath.section == 0) {
        
        PFObject * temp = [_inviteGroupList objectAtIndex:myPath.row];
        
        [_inviteGroupList removeObjectAtIndex:myPath.row];
        
        [temp deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [self.tableView beginUpdates];
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:myPath] withRowAnimation:UITableViewRowAnimationFade];
                [self.tableView endUpdates];
            }
        }];
    }
}

- (void)acceptInvite:(NSIndexPath *)myPath :(PFObject *)curGroup
{
    
    
    if (myPath.section == 0) {
        
        PFObject * temp = [_inviteGroupList objectAtIndex:myPath.row];
        
        temp[@"isAccept"] = @YES;
        [temp saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                PFObject * curTemp = [temp objectForKey:@"groupId"];
                [_inviteGroupList removeObjectAtIndex:myPath.row];
                
                
                [self.tableView beginUpdates];
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:myPath] withRowAnimation:UITableViewRowAnimationFade];
                [self.tableView endUpdates];
                
                [_groupList insertObject:curTemp atIndex:0];
                [self.tableView reloadData];
                [self autoAddPropertiesToNewAcceptGroup:curTemp];
            }
        }];
        
    }
}
-(void) autoAddPropertiesToNewAcceptGroup:(PFObject *)curGroup
{
    [[DatabaseEngine sharedInstance] autoAddPropertiesToNewAcceptGroup:curGroup];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Inviting group";
    }
    else
    {
        return @"Current Group";
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goAdd"])
    {
        NewAddGroupVC * destination = segue.destinationViewController;
        destination.groupListView = self;
    }
    else if ([[segue identifier] isEqualToString:@"goDetail"])
    {
        DetailGroupTabbar * destination = segue.destinationViewController;
        if (self.tableView.indexPathForSelectedRow.section == 0) {
            
            PFObject * curObject = [_inviteGroupList objectAtIndex:self.tableView.indexPathForSelectedRow.row];

            PFObject * curGroup = [curObject objectForKey:@"groupId"];
            
            destination.navigationItem.title = [[_inviteGroupList objectAtIndex:self.tableView.indexPathForSelectedRow.row] objectForKey:@"title"];
            
            DetailGroupMemberVC * tab01 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab01"];
            tab01.curGroup = curGroup;
            DetailGroupSettingVC * tab02 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab02"];
            tab02.curGroup = curGroup;
            DetailGroupListingVC * tab03 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab03"];
            tab03.curGroup = curGroup;
            
            PFUser * founder = [curGroup objectForKey:@"creator"];
            if ([[[DataEngine sharedInstance] GetCurUser].userObject.objectId isEqualToString:founder.objectId])
            {
                destination.viewControllers = [NSArray arrayWithObjects:tab01, tab02, tab03, nil];
                
            }
            else
            {
                destination.viewControllers = [NSArray arrayWithObjects:tab01,  tab03, nil];
            }
            
            
            curObject[@"isAccept"] = @YES;
            [curObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    [tab01 actionRun];
                }
            }];
        }
        else
        {
            destination.navigationItem.title = [[_groupList objectAtIndex:self.tableView.indexPathForSelectedRow.row] objectForKey:@"title"];
            
            DetailGroupMemberVC * tab01 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab01"];
            tab01.curGroup = [_groupList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
            DetailGroupSettingVC * tab02 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab02"];
            tab02.curGroup = [_groupList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
            DetailGroupListingVC * tab03 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab03"];
            tab03.curGroup = [_groupList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
            PFUser * founder = [tab01.curGroup objectForKey:@"creator"];
            if ([[[DataEngine sharedInstance] GetCurUser].userObject.objectId isEqualToString:founder.objectId])
            {
                destination.viewControllers = [NSArray arrayWithObjects:tab01, tab02, tab03, nil];
            }
            else
            {
                destination.viewControllers = [NSArray arrayWithObjects:tab01,  tab03, nil];
            }
        }
        
    }
}
- (IBAction)edit:(id)sender {
}


@end
