//
//  DetailGroupMemberVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "MyUITableViewController.h"
#import "User.h"

@interface DetailGroupMemberVC : MyUITableViewController

@property (nonatomic, strong) PFObject * curGroup;
@property (nonatomic, strong) NSMutableArray * acceptedMember;
@property (nonatomic, strong) NSMutableArray * pendingMember;

@end
