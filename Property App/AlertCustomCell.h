//
//  AlertCustomCell.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/21/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertCustomCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel * subjectLB;
@property (nonatomic, retain) IBOutlet UILabel * priceLB;
@property (nonatomic, retain) IBOutlet UILabel * typeLB;
@property (nonatomic, retain) IBOutlet UILabel * roomLB;
@property (nonatomic, retain) IBOutlet UILabel * tenureLB;
@property (nonatomic, retain) IBOutlet UILabel * remarkLB;

@property (nonatomic, retain) IBOutlet UIButton * editBtn;
@property (strong, nonatomic) IBOutlet UILabel *statusNewLb;

@end
