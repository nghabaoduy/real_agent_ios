
//
//  MessageChatVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/17/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//
#define TABBAR_HEIGHT 49.0f
#define TEXTFIELD_HEIGHT 70.0f
#define MAX_ENTRIES_LOADED 25


#import "MessageChatVC.h"
#import "DataEngine.h"
#import "DatabaseEngine.h"
#import "NotificationEngine.h"

@interface MessageChatVC ()

@end

@implementation MessageChatVC
@synthesize curUser;
@synthesize messageInfo;


@synthesize chatList;
@synthesize userInfoList;
@synthesize chatDisplayArray;
@synthesize userDisplayArray;
@synthesize propArray;


- (void)actionRun
{
    [super actionRun];
    [[DatabaseEngine sharedInstance] loadMessageList:messageInfo :self];

}

- (void) actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    [self reloadChatDisplayArray];
    
    
    [tv reloadData];
    if ([tv numberOfSections] > 0) {
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [((NSMutableArray *)[chatDisplayArray lastObject]) count]-1 inSection: userDisplayArray.count-1];
        [tv scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    }
    [self timerStart];
}

- (void) actionFail:(NSString *)message
{
    [super actionFail:message];
}
/*
 PFObject * chatInfo = [chatList objectAtIndex:indexPath.row];
 
 NSString * content = [chatInfo objectForKey:@"content"];
 
 PFUser * sender = [chatInfo objectForKey:@"userId"];
 
 
 NSString * fullText = @"";
 
 if ([sender.objectId isEqualToString:curUser.userObject.objectId]) {
 fullText = [NSString stringWithFormat:@"%@: %@", curUser.username, content];
 }
 else
 {
 fullText = [NSString stringWithFormat:@"%@: %@", messageInfo.connector.userObject.username, content];
 }
 */
-(void) reloadChatDisplayArray
{
    self.userInfoList = [[NSMutableArray alloc] init];
    for (PFObject *message in chatList) {
        
         PFUser * sender = [message objectForKey:@"userId"];
        
        NSLog(@"message.sender.object ID = %@",sender.objectId);
        NSLog(@"curUser object ID = %@",curUser.userObject.objectId);
        NSLog(@"messageInfo.connector object ID = %@",messageInfo.connector.userObject.objectId);
        
        NSString *compareResult = [sender.objectId isEqualToString:curUser.userObject.objectId]? @"yes":@"no";
        
        NSLog(@"%@",compareResult);
       
        if ([sender.objectId isEqualToString:curUser.userObject.objectId]) {
            [self.userInfoList addObject:curUser.userObject];
            
        }
        else
        {
            [self.userInfoList addObject:messageInfo.connector.userObject];
        }
    }
    
    NSLog(@"chatList = %@",chatList);
    NSLog(@"userInfoList = %@",userInfoList);
    
    chatDisplayArray = [[NSMutableArray alloc] init];
    userDisplayArray = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i< chatList.count; i++) {
        if (i == 0) {
            //NSLog(@"userInfoList = %@",userInfoList);
            
            [userDisplayArray addObject:[userInfoList objectAtIndex:0]];
            NSMutableArray *chatArray = [[NSMutableArray alloc] init];
            [chatArray addObject:[chatList objectAtIndex:0]];
            [chatDisplayArray addObject:chatArray];
        }
        else
        {
            PFUser * indexUser =[userInfoList objectAtIndex:i];
            NSString * indexUserName = [NSString stringWithFormat:@"%@", indexUser.username];
            PFUser * lastUser = [userDisplayArray lastObject];
            NSString * lastUserName = [NSString stringWithFormat:@"%@", lastUser.username];
            
            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
            timeFormatter.dateFormat = @"dd-MM-yyyy";
            
            NSMutableArray *lastChatLast = [chatDisplayArray lastObject];
            PFObject *chatBefore = [lastChatLast lastObject];
            NSString *timeBefore = [timeFormatter stringFromDate: chatBefore.createdAt];
            
            PFObject *chatNew = [chatList objectAtIndex:i];
            NSString *timeNew = [timeFormatter stringFromDate: chatNew.createdAt];
            
            if ([timeBefore isEqualToString:timeNew]) {
                if ([indexUserName isEqualToString:lastUserName]) {
                    [lastChatLast addObject:[chatList objectAtIndex:i]];
                }
                else
                {
                    [userDisplayArray addObject:indexUser];
                    NSMutableArray *newChatArray = [[NSMutableArray alloc] init];
                    [newChatArray addObject:[chatList objectAtIndex:i]];
                    [chatDisplayArray addObject:newChatArray];
                }
            }
            else
            {
                [userDisplayArray addObject:indexUser];
                NSMutableArray *newChatArray = [[NSMutableArray alloc] init];
                [newChatArray addObject:[chatList objectAtIndex:i]];
                [chatDisplayArray addObject:newChatArray];
            }
        }
    }
    NSLog(@"userDisplayArray = %@",userDisplayArray);
    NSLog(@"chatDisplayArray = %@",chatDisplayArray);
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    curUser = [[DataEngine sharedInstance] GetCurUser];
    NSLog(@"chat info = %@", messageInfo.messageObject.objectId);
    NSLog(@"user info = %@", curUser.userObject.username);
    //[contentTF setText:@"I'm testing"];
    
    
    tv.separatorStyle = UITableViewCellSeparatorStyleNone;
    userInfoList = [[NSMutableArray alloc] init];
    chatList = [[NSMutableArray alloc] init];
    propArray = [[NSMutableArray alloc] init];
    [self registerForKeyboardNotifications];
}
- (void)viewWillAppear:(BOOL)animated
{
    [self actionRun];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//custom section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20)];
    //sectionView.backgroundColor = [UIColor blueColor];
    UILabel *dateTxt = [[UILabel alloc] initWithFrame:sectionView.frame];
    [sectionView addSubview:dateTxt];
    dateTxt.textAlignment = NSTextAlignmentCenter;
    dateTxt.center = sectionView.center;
    
    //text
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
    timeFormatter.dateFormat = @"dd-MM-yyyy";
    
    NSMutableArray *chatArray = [chatDisplayArray objectAtIndex:section];
    PFObject * thisChatInfo = [chatArray firstObject];
    NSString *dateString = [timeFormatter stringFromDate: thisChatInfo.createdAt];
    if (section == 0) {
        [dateTxt setText:dateString];
    }
    else
    {
        NSMutableArray *preChatArray = [chatDisplayArray objectAtIndex:section-1];
        PFObject * preChatInfo = [preChatArray lastObject];
        NSString *preDateString = [timeFormatter stringFromDate: preChatInfo.createdAt];
        
        if (![dateString isEqualToString:preDateString]) {
            [dateTxt setText:dateString];
        }
    }
    
    
    
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
    [dateTxt setFont:font];
    [dateTxt setTextColor:[UIColor grayColor]];
    
    
    return sectionView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return userDisplayArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"%i ting ting", [chatList count]);
    NSMutableArray *chatArray = [chatDisplayArray objectAtIndex:section];
    return [chatArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"chatCell";
    
    ChatCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    NSMutableArray *chatArray = [chatDisplayArray objectAtIndex:indexPath.section];
    
    PFObject * chatInfo = [chatArray objectAtIndex:indexPath.row];
    
    PFUser * sender = [chatInfo objectForKey:@"userId"];
    
    User *senderUser;
    if ([sender.objectId isEqualToString:curUser.userObject.objectId]) {
        senderUser = curUser;
    }
    else
    {
         senderUser = messageInfo.connector;
    }
    
    NSString * theContent = [chatInfo objectForKey:@"content"];
    
    //username
    if (indexPath.row == 0) {
        cell.usernameTF.text = senderUser.fullName;
    }
    else
    {
        cell.usernameTF.text = @"";
    }
    //time
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
    timeFormatter.dateFormat = @"hh:mm a";
    NSString *dateString = [timeFormatter stringFromDate: chatInfo.createdAt];
    cell.timeTF.text = dateString;
    
    //content
    cell.contentTF.userInteractionEnabled = NO;
    cell.propAddress.userInteractionEnabled = NO;
    
    if ([self checkNormalContent:cell:theContent]) {
        cell.chatView.hidden = NO;
        cell.propView.hidden = YES;
        cell.contentTF.text = theContent;
    }
    else
    {
        cell.chatView.hidden = YES;
        cell.propView.hidden = NO;
    }
    
    
    //custom cell
    [self customCell:cell :indexPath : tv : senderUser.userObject.objectId];
    
    return cell;
}



-(BOOL) checkNormalContent:(ChatCell *) cCell : (NSString *)theContent
{
    if ([theContent hasPrefix:@"share Property:"]) {
        NSLog(@"has prefix share prop");
        NSString *propID = [[theContent componentsSeparatedByString:@": "] lastObject];
        BOOL foundProp = NO;
        for (Property *prop in self.propArray) {
            if ([prop.propID isEqualToString:propID] && !foundProp) {
                
                cCell.propIcon.image = prop.iconImage;
                
                [cCell.propAddress setText:prop.address];
                [cCell.propPrice setText:[NSString stringWithFormat:@"Price: %@",prop.price]];
                foundProp = YES;
                
            }
        }
        if (foundProp == NO) {
            [cCell.propAddress setText:@"Loading..."];
            UIImage *cellImage = [UIImage imageNamed:@"chatDefaultProperty"];
            cCell.propIcon.image = cellImage;
            [cCell setCellPropertyId:propID:self.propArray];
        }
        return NO;
    }
    return YES;
}


-(void) customCell:(ChatCell *) cCell :(NSIndexPath *)indexPath :(UITableView *) tableView : (NSString *)userId
{
    NSString *yourId = [[DataEngine sharedInstance] curUser].userObject.objectId;
    
    if ([userId isEqualToString:yourId]) {
        if ([tableView numberOfRowsInSection:indexPath.section] > 1) {
            if (indexPath.row == 0) {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_top"]];
            }
            else if(indexPath.row == [tableView numberOfRowsInSection:indexPath.section] -1)
            {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_bot"]];
            }
            else
            {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_mid"]];
            }
        }
        else
        {
            cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_one"]];
        }
    }
    else
    {
        if ([tableView numberOfRowsInSection:indexPath.section] > 1) {
            if (indexPath.row == 0) {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_top2"]];
            }
            else if(indexPath.row == [tableView numberOfRowsInSection:indexPath.section] -1)
            {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_bot2"]];
            }
            else
            {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_mid2"]];
            }
        }
        else
        {
            cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_one2"]];
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    NSMutableArray *chatArray = [chatDisplayArray objectAtIndex:indexPath.section];
    if (chatArray.count > 0) {
        if (indexPath.row == chatArray.count-1) {
            return 91;
        }
    }
    return 83;
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    [self timerStop];
}

- (void)refreshStart
{
    NSLog(@"start refresh");
    [[DatabaseEngine sharedInstance] refreshMessageList:messageInfo :chatList :self];
    contentTF.text = @"";
}

- (void)refreshDone
{
    NSLog(@"done refresh");
    [self reloadChatDisplayArray];
    [tv reloadData];
    if ([tv numberOfSections] > 0) {
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [((NSMutableArray *)[chatDisplayArray lastObject]) count]-1 inSection: userDisplayArray.count-1];
        [tv scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    }
}

- (void)timerStart
{
    
    myTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                               target:self
                                             selector:@selector(refreshStart)
                                             userInfo:nil
                                              repeats:YES];
}

- (void)timerStop
{
    [myTimer invalidate];
    myTimer = nil;
}
- (void)sendDone:(PFObject *)contentObject
{
    [self refreshStart];
    
    NSString *chatContent = [NSString stringWithFormat:@"%@: %@",[DataEngine sharedInstance].curUser.fullName, [contentObject objectForKey:@"content"]];
    NSMutableDictionary *pushDict = [[NSMutableDictionary alloc] init];
    [pushDict setObject:chatContent forKey:@"alert"];
    [pushDict setObject:messageInfo.messageObject forKey:@"pushPFObject"];
    [pushDict setObject:@"message" forKey:@"pushType"];
    [[NotificationEngine sharedInstance] pushToUserObj:messageInfo.connector.userObject :pushDict];
    [[NotificationEngine sharedInstance] pushToUserObj:[DataEngine sharedInstance].curUser.userObject :pushDict];
    //send.enabled = YES;
    //	[self timerStart];
    [tv reloadData];
}

- (IBAction)sendMessage:(id)sender
{
    [self timerStop];
    [[DatabaseEngine sharedInstance] chat: messageInfo:curUser :self :contentTF.text];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



-(void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


-(void) freeKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


-(void) keyboardWasShown:(NSNotification*)aNotification
{
    
    NSLog(@"Keyboard was shown");
    NSDictionary* info = [aNotification userInfo];
    
    // Get animation info from userInfo
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardFrame;
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&keyboardFrame];
    
    // Move
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    NSLog(@"frame..%f..%f..%f..%f",self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    NSLog(@"keyboard..%f..%f..%f..%f",keyboardFrame.origin.x, keyboardFrame.origin.y, keyboardFrame.size.width, keyboardFrame.size.height);
    [self.view setFrame:CGRectMake(0, 0 , self.view.frame.size.width, self.view.frame.size.height- keyboardFrame.size.height)];
    
    
    [UIView commitAnimations];
    
    [tv setFrame:CGRectMake(tv.frame.origin.x, tv.frame.origin.y , tv.frame.size.width, tv.frame.size.height- keyboardFrame.size.height)];
    if ([tv numberOfSections] > 0) {
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [((NSMutableArray *)[chatDisplayArray lastObject]) count]-1 inSection: userDisplayArray.count-1];
        [tv scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    }
    
}
-(void) keyboardWillHide:(NSNotification*)aNotification
{
    NSLog(@"Keyboard will hide");
    NSDictionary* info = [aNotification userInfo];
    
    // Get animation info from userInfo
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardFrame;
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&keyboardFrame];
    
    // Move
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    [self.view setFrame:CGRectMake(0, 0 , self.view.frame.size.width, self.view.frame.size.height+ keyboardFrame.size.height)];
    //[chatTable setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-TABBAR_HEIGHT)];
    [UIView commitAnimations];
}

@end
