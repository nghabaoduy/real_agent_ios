//
//  PersonInfoViewController.m
//  PhoneBookController
//
//  Created by Brian on 9/9/13.
//  Copyright (c) 2013 Brian. All rights reserved.
//

#import "PersonInfoViewController.h"
#import "ChangeUserInfoViewController.h"
#import "MyUIEngine.h"
#import "DataEngine.h"

#import "AddPropertyViewController.h"
#import "ClientPropertyInfoVC.h"

#import "Agent.h"
#import "Client.h"
#import "BasicInfoCurPropVC.h"

#import "DetailPropertyTabBarVC.h"
#import "DetailPropertyVC.h"
#import "GalleryPropertyVC.h"
#import "LocationPropertyVC.h"
#import "DetailPropertyNavVC.h"
#import "DetailComercialVC.h"
@interface PersonInfoViewController ()

@end

@implementation PersonInfoViewController


@synthesize nameLbl = _nameLbl;
@synthesize phoneLbl = _phoneLbl;
@synthesize emailLbl = _emailLbl;
@synthesize user = _user;
@synthesize addedProperty = _addedProperty;

@synthesize tableView;

@synthesize propertyList;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[[MyUIEngine sharedUIEngine] customizeViewSetting:self:nil];
    if(self.user != nil)
    {
        [self refreshPersonInfo];
    }
    Agent *curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
    propertyList = [[NSMutableArray alloc] init];
    self.nonMarketingList = [[NSMutableArray alloc] init];
    self.marketingList = [[NSMutableArray alloc] init];
    for (NSMutableArray *proArray in curAgent.myClientPropertyList)
    {
        
        NSLog(@"proArray: %@",proArray);
        NSLog(@"%@.phone: %@",_user.fullName, _user.phone);
        
        for (Property *prop in proArray)
        {
            if ([prop.adder.phone isEqualToString:_user.phone]) {
                NSLog(@"Prop: %@ - %@",prop.propertyName,prop.isSubmited);
                [self addPropertyToListAndRefresh:prop];

            }
            
        }
        
    }
    
    NSLog(@"This isprop list %@", self.propertyList);

	NSLog(@"This is nonmarketing prop list %@", self.nonMarketingList);
    NSLog(@"This is marketing prop list %@", self.marketingList);
}
-(void) refreshPersonInfo
{
    [self.nameLbl setText:self.user.fullName];
    [self.phoneLbl setText:self.user.phone];
    NSLog(@"userEmail = %@",self.user.email);
    if (![self.user.email hasSuffix:@"unknow.com"]) {
        [self.emailLbl setText:self.user.email];
        
    }
    else
    {
        [self.emailLbl setText:@""];
    }
    NSString *addressStr = self.user.address;
    NSArray * addressArray = [addressStr componentsSeparatedByString:@","];
    NSLog(@"%@",addressArray);
    if (addressArray.count >0) {
        [self.addressLbl1 setText:[addressArray objectAtIndex:0]];
        if (addressArray.count>1) {
            [self.addressLbl2 setText:[addressArray objectAtIndex:1]];
        }
        else
        {
            [self.addressLbl2 setText:@""];
        }
    }
    else
    {
        [self.addressLbl1 setText:@"N/A"];
        [self.addressLbl1 setText:@""];
    }

}

-(void) addPropertyToListAndRefresh:(Property *)prop
{
    [propertyList addObject:prop];
    if (![prop.isSubmited isEqualToString:@"YES"]) {
        [self.nonMarketingList addObject:prop];
    }
    else{
        [self.marketingList addObject:prop];
    }
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) EditClientInfo:(id)sender
{
    ChangeUserInfoViewController* view = [self.storyboard instantiateViewControllerWithIdentifier:@"changeUserInfo"];
    view.personInfoView = self;
    [self presentViewController:view animated:YES completion:nil];
}

-(IBAction)addProperty:(id)sender
{
    AddPropertyViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"addPropertyView"];
    view.adder = _user;
    [self presentViewController:view animated:YES completion:nil];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Marketing Properties";
    }
    else
    {
        return @"Non-marketing Properties";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.marketingList.count;
    }
    else
    {
        return self.nonMarketingList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"infoPropCell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    Property * curProp;
    if (indexPath.section == 0) {
        curProp = [self.marketingList objectAtIndex:indexPath.row];
    }
    else if (indexPath.section == 1)
    {
        curProp = [self.nonMarketingList objectAtIndex:indexPath.row];
    }
    
    cell.textLabel.text = curProp.address;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Property * curProp;
    if (indexPath.section == 0) {
        curProp = [self.marketingList objectAtIndex:indexPath.row];
    }
    else if (indexPath.section == 1)
    {
        curProp = [self.nonMarketingList objectAtIndex:indexPath.row];
    }
    
    
    NSMutableArray *containArray;
    Property *targetProp;
    Agent *curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
    for (NSMutableArray *proArray in curAgent.myClientPropertyList)
    {
        
        for (Property *prop in proArray)
        {
            if ([prop.propertyObject.objectId isEqualToString:curProp.propertyObject.objectId]) {
                containArray = proArray;
                targetProp = prop;
            }
        }
    }
    if (targetProp != NULL) {
        [curProp.propertyObject deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                
                [self removePropFromDataArrays:curProp:indexPath];
                
                [containArray removeObject:targetProp];
                
                [self.tableView beginUpdates];
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                      withRowAnimation:UITableViewRowAnimationFade];
                [self.tableView endUpdates];
            }
        }];
    }
    
    
}
-(void) removePropFromDataArrays:(Property *)curProp :(NSIndexPath *)indexPath
{
    [propertyList removeObject:curProp];
    
    if (indexPath.section == 0) {
        [self.marketingList removeObject:curProp];
    }
    else if (indexPath.section == 1)
    {
        [self.nonMarketingList removeObject:curProp];
    }
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"AddPropNext"])
    {
        Property *newProp = [[Property alloc] init];
        BasicInfoCurPropVC * destinate = segue.destinationViewController;
        newProp.adder = self.user;
        _addedProperty = newProp;
        destinate.property = newProp;
    }
    if ([[segue identifier] isEqualToString:@"goPropertyInfo"])
    {
         NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Property * clientProp;
        if (indexPath.section == 0) {
            clientProp = [self.marketingList objectAtIndex:indexPath.row];
        }
        else if (indexPath.section == 1)
        {
            clientProp = [self.nonMarketingList objectAtIndex:indexPath.row];
        }
;
        ClientPropertyInfoVC * destinate = segue.destinationViewController;
        destinate.property = clientProp;
    }
    if ([[segue identifier] isEqualToString:@"goUserEdit"])
    {
        ChangeUserInfoViewController * destinate = segue.destinationViewController;
        destinate.updatingUser = _user;
    }
    
}
- (IBAction)unwindToPersonInfoVC:(UIStoryboardSegue *)unwindSegue
{
    NSLog(@"unwind backToPersonalInfo run");
    if ([unwindSegue.identifier isEqualToString:@"backToPersonalInfo"]) {
        [self addPropertyToListAndRefresh:_addedProperty];
        
        
    }
    
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"goPropertyInfo"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Property * clientProp;
        if (indexPath.section == 0) {
            clientProp = [self.marketingList objectAtIndex:indexPath.row];
        }
        else if (indexPath.section == 1)
        {
            clientProp = [self.nonMarketingList objectAtIndex:indexPath.row];
        }
        ;
        if ([clientProp.isSubmited isEqualToString:@"NO"]) {
            return YES;
        }
        else
        {
            Property * curProp;
            if (indexPath.section == 0) {
                curProp = [self.marketingList objectAtIndex:indexPath.row];
            }
            else if (indexPath.section == 1)
            {
                curProp = [self.nonMarketingList objectAtIndex:indexPath.row];
            }
            UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"DetailStoryboard" bundle:nil];
            
            
            
            DetailPropertyNavVC * navView =[newStoryboard instantiateViewControllerWithIdentifier:@"detailProperty"];
            
            DetailPropertyTabBarVC * tabBar = (DetailPropertyTabBarVC*) navView.topViewController;
            [tabBar initTabByProperty:curProp :newStoryboard ];
            
            [self presentViewController:navView animated:YES completion:nil];
            return NO;
        }
    }
    return YES;
}

- (IBAction)emailAgent:(id)sender {

    NSString * email = _user.email;
    NSArray * recipients = [NSArray arrayWithObject:email];
    
    NSMutableString *body = [NSMutableString string];
    
    [body appendString:@"\n"];
    [body appendString:@"\n"];
    [body appendString:[NSString stringWithFormat:@"<a>Send from agent %@ </a>\n",[DataEngine sharedInstance].curUser.fullName]];
    if ([MFMailComposeViewController canSendMail]) {
        // Show the composer
        
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:recipients];
        [controller setMessageBody:body isHTML:YES];
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Handle the error
    }
    
    
}
- (IBAction)callAgent:(id)sender {
    
    NSString * call = [NSString stringWithFormat:@"tel:%@", _user.phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
