//
//  ScheduleListVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/13/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ScheduleListVC.h"
#import "ScheduleEvent.h"
#import "ScheduleCell.h"
#import "RequestMeetupAgentVC.h"
#import "RequestMeetupClientVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "User.h"
#import "MyUIEngine.h"

@interface ScheduleListVC ()

@end

@implementation ScheduleListVC
@synthesize myRequestList, otherRequestList;

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)actionRun
{
    [super actionRun];
    [[DatabaseEngine sharedInstance] getRequestList:self :curUser];
}

- (void)actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    

    NSLog(@"my %i", [myRequestList count]);
    NSLog(@"other %i", [otherRequestList count]);
    [self.tableView reloadData];
}

- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    curUser = [[DataEngine sharedInstance] GetCurUser];
    myRequestList = [[NSMutableArray alloc]init];
    otherRequestList = [[NSMutableArray alloc]init];
    [self actionRun];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Request from others";
    }
    else if (section == 1)
    {
        return @"My Request";
    }
    return @"";
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [otherRequestList count];
    }
    else
    {
        return [myRequestList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScheduleEvent * curEvent;
    if (indexPath.section == 0) {
        curEvent = [otherRequestList objectAtIndex:indexPath.row];
    }
    else
    {
        curEvent = [myRequestList objectAtIndex:indexPath.row];
    }
    
    
    static NSString *CellIdentifier = @"scheduleCell";
    ScheduleCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScheduleCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    NSLog(@"%@ name", curEvent.projectName);
    
    [cell.displayer setImage:curEvent.picture];
    [cell.eventTypeLB setText:curEvent.eventType];
    [cell.projectNameLB setText:[NSString stringWithFormat:@"%@ - %@",curEvent.projectName,curEvent.projectType]];
    
    [cell.addressLB setText:curEvent.address];
    cell.event = curEvent;
    
    NSDate * date = curEvent.eventSchedule;
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *dateStr = [outputFormatter stringFromDate:date];
    [cell.dateLB setText:[NSString stringWithFormat:@"Date: %@",dateStr]];
    
    [outputFormatter setDateFormat:@"HH:mm a"];
    NSString *timeStr = [outputFormatter stringFromDate:date];
    
    [cell.timeLB setText:[NSString stringWithFormat:@"Time: %@",timeStr]];
    User *curUser = [User alloc];
    if (indexPath.section == 0) {
        cell.receiverLB.text = [NSString stringWithFormat:@"Client: %@",curEvent.requester.fullName];
    }
    else
    {
        [curEvent.receiverAgent fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                [curUser initWithUser:curEvent.receiverAgent];
                cell.receiverLB.text = [NSString stringWithFormat:@"Owner's agent: %@",curUser.fullName];
            }
        }];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
       [self performSegueWithIdentifier:@"goReceiver" sender:self];
    }
    else
    {
        [self performSegueWithIdentifier:@"goRequester" sender:self];
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 160;
}



#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goRequester"])
    {
        RequestMeetupClientVC * destination = segue.destinationViewController;
        ScheduleEvent * curEvent = [myRequestList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
        destination.event = curEvent;
        destination.isEvent = YES;
        destination.scheduleList = self;
        
    }
    else if ([[segue identifier] isEqualToString:@"goReceiver"])
    {
        RequestMeetupAgentVC * destination = segue.destinationViewController;
        ScheduleEvent * curEvent = [otherRequestList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
        destination.event = curEvent;
        destination.scheduleList = self;
        
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScheduleEvent * curEvent;
    
    if (indexPath.section == 0)
    {
        curEvent = [otherRequestList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
    }
    else
    {
        curEvent = [myRequestList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
    }

    
    [curEvent.eventObject deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            if (indexPath.section == 0)
            {
                [otherRequestList removeObject:curEvent];
            }
            else
            {
                [myRequestList removeObject:curEvent];
            }
            
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView endUpdates];
            //[self viewDidLoad];
        }
    }];
}





@end
