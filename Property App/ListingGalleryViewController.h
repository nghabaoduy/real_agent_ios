//
//  ListingGalleryViewController.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Property.h"
@interface ListingGalleryViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>
{
    NSMutableArray * imageList;
}

@property (strong, nonatomic) IBOutlet UICollectionView * collectionView;
@property (strong, nonatomic) NSMutableArray * imageList;
@property (strong, nonatomic) Property* property;

- (void) DownloadImageSuccessfully : (NSMutableArray*) imageArray;
- (void) DownloadImageFail : (NSString*) errorString;

@end
