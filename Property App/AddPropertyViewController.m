//
//  AddPropertyViewController.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "AddPropertyViewController.h"
#import "MyUIEngine.h"
#import "Property.h"
#import "AddPropertyNavVC.h"
#import "AddCurPropertyVC.h"
#import "ExtraDetailVC.h"
#import "BasicInfoVC.h"
#import "Property.h"

#import "ExtraDetailCommercialVC.h"
#import "ExtraDetailHDBVC.h"
#import "ExtraDetailIndustryVC.h"

#import "AddCurCommercialVC.h"
#import "AddCurIndustrialVC.h"
#import "AddCurHDBVC.h"
#import "AddCurLand.h"
#import "ExtraDetailLandVC.h"

@interface AddPropertyViewController ()

@end

@implementation AddPropertyViewController

@synthesize tableView = _tableView;

- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"AddPropertyStoryBoard" bundle:nil];
    if (self.property == NULL) {
    
        Property *newPro;
        if (self.property != NULL) {
            newPro = self.property;
        }
        else
        {
            newPro = [[Property alloc] init];
            
        }
        newPro.propertyType = [data objectAtIndex:indexPath.section];
        if(Segment.selectedSegmentIndex == 0)
        {
            newPro.listingType = @"Sales";
        }
        else
        {
            newPro.listingType = @"Rent";
        }
        if (self.adder != NULL) {
            //newPro.adder = self.adder;
        }
        [newPro printInfo];
        
        //transition view
       
        AddPropertyNavVC * navView =[newStoryboard instantiateViewControllerWithIdentifier:@"addproperty"];
        BasicInfoVC * basicInfoVC = (BasicInfoVC*) navView.topViewController;
        basicInfoVC.property = newPro;
        [self presentViewController:navView animated:YES completion:nil];
    }
    else
    {
        self.property.propertyType = [data objectAtIndex:indexPath.section];
        if(Segment.selectedSegmentIndex == 0)
        {
            self.property.listingType = @"Sales";
        }
        else
        {
            self.property.listingType = @"Rent";
        }
        if (self.adder != NULL) {
            //newPro.adder = self.adder;
        }
        NSLog(@"push Property = %@",self.property.propertyType);
        
        if ([self.property.propertyType  isEqualToString: @"Private Residential"]) {
            AddCurPropertyVC* navView =[newStoryboard instantiateViewControllerWithIdentifier:@"addCurProperty"];
            ExtraDetailVC * extraview = (ExtraDetailVC*) navView.topViewController;
            [extraview addBackBtn];
            extraview.property = self.property;
            [extraview.navigationItem setTitle:@"Step 1/3"];
            [self presentViewController:navView animated:YES completion:nil];
        }
        else if ([self.property.propertyType isEqualToString:@"Commercial"])
        {
            AddCurCommercialVC* navView =[newStoryboard instantiateViewControllerWithIdentifier:@"addCurCommercial"];
            ExtraDetailCommercialVC * extraview = (ExtraDetailCommercialVC*) navView.topViewController;
            [extraview addBackBtn];
            extraview.property = self.property;
            [extraview.navigationItem setTitle:@"Step 1/3"];
            [self presentViewController:navView animated:YES completion:nil];
        }
        else if ([self.property.propertyType isEqualToString:@"HDB"])
        {
            AddCurHDBVC* navView =[newStoryboard instantiateViewControllerWithIdentifier:@"addCurHDB"];
            ExtraDetailHDBVC * extraview = (ExtraDetailHDBVC*) navView.topViewController;
            [extraview addBackBtn];
            extraview.property = self.property;
            [extraview.navigationItem setTitle:@"Step 1/3"];
            [self presentViewController:navView animated:YES completion:nil];
        }
        else if ([self.property.propertyType isEqualToString:@"Industrial"])
        {
            AddCurIndustrialVC* navView =[newStoryboard instantiateViewControllerWithIdentifier:@"addCurIndustrial"];
            ExtraDetailIndustryVC * extraview = (ExtraDetailIndustryVC*) navView.topViewController;
            [extraview addBackBtn];
            extraview.property = self.property;
            [extraview.navigationItem setTitle:@"Step 1/3"];
            [self presentViewController:navView animated:YES completion:nil];
        }
        else if ([self.property.propertyType isEqualToString:@"Land"])
        {
            AddCurLand* navView =[newStoryboard instantiateViewControllerWithIdentifier:@"addCurLand"];
            
            
            
            ExtraDetailLandVC * extraview = (ExtraDetailLandVC*) navView.topViewController;
            [extraview addBackBtn];
            extraview.property = self.property;
            [extraview.navigationItem setTitle:@"Step 1/3"];
            [self presentViewController:navView animated:YES completion:nil];
        }
        
    }
    
    
}

- (IBAction)SegmentedChange:(id)sender
{
    if(Segment.selectedSegmentIndex == 0)
    {
        data = salesData;
        [_tableView reloadData];
        
    }
    if (Segment.selectedSegmentIndex == 1) {
        data = rentsData;
        [_tableView reloadData];
        
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [data count];
}


- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 3.0;
}

- (UITableViewCell *) tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * simpleTableIndentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [[MyUIEngine sharedUIEngine] getButtonLongCell:tableView1 :simpleTableIndentifier :indexPath.row :data.count :@""];
    
    cell.textLabel.text = [data objectAtIndex:indexPath.section];
    return cell;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :self.tableView];
	// Do any additional setup after loading the view.
    salesData = [NSArray arrayWithObjects:@"Private Residential", @"Industrial", @"HDB", @"Commercial", @"Land",nil];
    
    rentsData = [NSArray arrayWithObjects:@"Private Residential", @"Industrial", @"HDB", @"Commercial",@"Land", nil];
    data = salesData;
    [_tableView reloadData];
    NSLog(@"%i", [data count]);
    NSLog(@"%@", [data objectAtIndex:1]);
    _tableView.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
