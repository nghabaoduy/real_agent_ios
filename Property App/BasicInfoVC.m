//
//  BasicInfoVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 21/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "BasicInfoVC.h"
#import "LocationPickVC.h"
//#import "MyUIEngine.h"
//#import "MyCustomBack.h"

@interface BasicInfoVC ()

@end

@implementation BasicInfoVC
@synthesize responseData, requestType, data;
@synthesize property = _property;

- (BOOL) validatePostcode
{
    if (![postalCode.text isEqualToString:@""]) {
        return YES;
    }
    //NSLog(@"postcode can not empty");
    return NO;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //[[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    
	self.responseData = [NSMutableData data];
	data = [[NSMutableArray alloc] init];
    NSLog(@"BasicInfoVC runs");
    //[self.property printInfo];
    //postalCode.text = self.property.postCode;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
LocationPickVC * destinate;
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"basicInfoNext"])
    {
		NSString* searchText = [postalCode.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
		NSArray* arr = [NSArray arrayWithObjects:searchText, nil];
		requestType = @"search";
		[self URLRequest:arr];
		
        //_property.postCode = postalCode.text;
        destinate = segue.destinationViewController;
        
        //destinate.property = _property;
        
        
    }
    
}
int resultCount;
- (void) URLRequest: (NSArray*) requestData {
	NSURLRequest *request;
	if ([requestType isEqualToString:@"search"]) {
		NSString* url = [NSString stringWithFormat:@"http://www.onemap.sg/API/services.svc/basicSearch?token=qo/s2TnSUmfLz+32CvLC4RMVkzEFYjxqyti1KhByvEacEdMWBpCuSSQ+IFRT84QjGPBCuz/cBom8PfSm3GjEsGc8PkdEEOEr&searchVal=%@&otptFlds=SEARCHVAL,CATEGORY&returnGeom=1&rset=", [requestData objectAtIndex:0]];
		NSLog(@"url %@", url);
		request = [NSURLRequest requestWithURL:
                   [NSURL URLWithString:url]];
		[[NSURLConnection alloc] initWithRequest:request delegate:self];
	} else {
		NSString* url = [NSString stringWithFormat:@"http://www.onemap.sg/API/services.svc/revgeocode?token=qo/s2TnSUmfLz+32CvLC4RMVkzEFYjxqyti1KhByvEacEdMWBpCuSSQ+IFRT84QjGPBCuz/cBom8PfSm3GjEsGc8PkdEEOEr&location=%@,%@", [requestData objectAtIndex:0], [requestData objectAtIndex:1]];
		//NSLog(@"url %@", url);
		request = [NSURLRequest requestWithURL:
				   [NSURL URLWithString:url]];
		/*
         request = [NSURLRequest requestWithURL:
         [NSURL URLWithString:@"http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer/project?f=json&inSR=3414&outSR=4236&geometries=%7B%0D%0A%22geometryType%22%3A%22esriGeometryPoint%22%2C%0D%0A%22geometries%22%3A%5B%7B%22x%22%3A32668.3636%2C%22y%22%3A41140.4987%7D%5D%0D%0A%7D"]];*/
		[[NSURLConnection alloc] initWithRequest:request delegate:self];
	}
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    //NSLog(@"didReceiveResponse");
    [self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"didFailWithError");
    NSLog([NSString stringWithFormat:@"Connection failed: %@", [error description]]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    //NSLog(@"connectionDidFinishLoading");
    NSLog(@"Succeeded! Received %d bytes of data",[self.responseData length]);
	if (self.responseData) {
		// convert to JSON
		NSError *myError = nil;
		NSDictionary *res = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableLeaves error:&myError];
		
		// show all values
		for(id key in res) {
			/*
             id value = [res objectForKey:key];
             
             NSString *keyAsString = (NSString *)key;
             NSString *valueAsString = (NSString *)value;
             
             NSLog(@"key: %@", keyAsString);
             NSLog(@"value: %@", valueAsString);
             */
			
			if ([requestType isEqualToString:@"search"]) {
				NSArray* arr = [res objectForKey:key];
				NSLog(@"array : %@", arr);
				if (arr.count == 1) {
					NSLog(@"No result found!!!");
					break;
				}
				
				
				NSDictionary* placeDict = [arr objectAtIndex:1];
				//NSLog(@"dict %@", placeDict);
				NSArray* requestDat = [NSArray arrayWithObjects:[placeDict objectForKey:@"X"], [placeDict objectForKey:@"Y"], nil];
				requestType = @"detail";
				NSLog(@"lah lah count %@", requestDat);
				[self URLRequest:requestDat];
				
			} else {
				NSDictionary* placeDict = [[res objectForKey:key] objectAtIndex:0];
				
				for (int i = 0; i < data.count; i++) {
					NSString* dataObjPostalCode = [[data objectAtIndex:i] objectForKey:@"POSTALCODE"];
					NSString* receivedPostalCode = [placeDict objectForKey:@"POSTALCODE"];
					
					if (![dataObjPostalCode isEqualToString: receivedPostalCode]) {
						[data addObject:placeDict];
					}
				}
				if (data.count == 0) {
					[data addObject:placeDict];
				}
				
				
				NSLog(@"data now : %@", placeDict);
				[destinate GetMapLocation:data];
			}
			
		}
	}
	
}

- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
