//
//  PreferredPartnersVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/30/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "PreferredPartnersVC.h"
#import "PartnersVC.h"

@interface PreferredPartnersVC ()

@end

@implementation PreferredPartnersVC

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"goBank"]) {
        NSLog(@"a");
    }
    else if ([[segue identifier] isEqualToString:@"goContractor"])
    {
        PartnersVC * destination = segue.destinationViewController;
        destination.partnerType = @"Contractors";
    }
    else if ([[segue identifier]isEqualToString:@"goOther"])
    {
        PartnersVC * destination = segue.destinationViewController;
        destination.partnerType = @"Others";
    }
    else if ([[segue identifier]isEqualToString:@"goCommercial"])
    {
        PartnersVC * destination = segue.destinationViewController;
        destination.partnerType = @"Commercial Loan Bankers";
    }
    else if ([[segue identifier] isEqualToString:@"goMortgage"])
    {
        PartnersVC * destination = segue.destinationViewController;
        destination.partnerType = @"Mortgage Bankers";
    }
    else if ( [[segue identifier] isEqualToString:@"goResidential"])
    {
        PartnersVC * destination = segue.destinationViewController;
        destination.partnerType = @"Residential Loan Bankers";
    }
}

 

@end
