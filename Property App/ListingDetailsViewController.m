//
//  ListingDetailsViewController.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ListingDetailsViewController.h"
#import "SalesDetailsViewController.h"
#import "SWRevealViewController.h"
#import "MyUIEngine.h"
#import "DataEngine.h"
#import "Agent.h"
@interface ListingDetailsViewController ()

@end

@implementation ListingDetailsViewController
@synthesize property = _p;
@synthesize propertyName = _pName;
@synthesize propertyType = _pType;

@synthesize addressLbl, postcodeLbl;
@synthesize blkNoTextF,levelTextF,unitTextF,districtTextF,bedroomTextF,areaTextF,priceTextF,tenureTextF;
@synthesize statusTextF,rentedPriceTextF;
@synthesize projectTextF,topYearTextF,psfTextF,rentalYeildTextF;

@synthesize agentEmail, agentIcon,agentName,agentPhone;

- (void)LoadInformation
{
    [_p removeNullData];
    [_p printInfo];
    [_pName setText:_p.propertyName];
    [_pType setText:_p.propertyType];

    self.addressLbl.text=_p.address;
    self.postcodeLbl.text=_p.postCode;
    NSLog(@"%@",_p.district);
    [self.priceTextF setText:_p.district];
    

    [self.blkNoTextF setText:_p.blkNo];
    [self.levelTextF setText:_p.levelNumber];
    [self.unitTextF setText:_p.unitNo];
    [self.districtTextF setText:_p.district];
    [self.bedroomTextF setText:_p.bedroom];
    [self.areaTextF setText:_p.area];
    
    [self.tenureTextF setText:_p.tenure];
    [self.statusTextF setText:_p.status];
    [self.rentedPriceTextF setText:_p.rentedPrice];
    
    
    
    [self.projectTextF setText:_p.projectName];
    [self.topYearTextF setText:_p.topYear];
    [self.psfTextF setText:_p.PSF];
    [self.rentalYeildTextF setText:_p.rentalYeild];
    
    Agent* agent = _p.agent;
    [self.agentName setText:[NSString stringWithFormat:@"%@ %@",agent.firstName, agent.lastName]];
    [self.agentPhone setText:[NSString stringWithFormat:@"Tel: %@",agent.agentPhone]];
    [self.agentEmail setText:[NSString stringWithFormat:@"Email: %@",agent.email]];

    
    self.addressLbl.enabled = NO;
    self.postcodeLbl.enabled = NO;
    self.blkNoTextF.enabled = NO;
    self.levelTextF.enabled = NO;
    self.unitTextF.enabled = NO;
    self.districtTextF.enabled = NO;
    self.bedroomTextF.enabled = NO;
    self.areaTextF.enabled = NO;
    self.priceTextF.enabled = NO;
    self.tenureTextF.enabled = NO;
    self.statusTextF.enabled = NO;
    self.rentedPriceTextF.enabled = NO;
    
    
    self.projectTextF.enabled = NO;
    self.topYearTextF.enabled = NO;
    self.psfTextF.enabled = NO;
    self.rentalYeildTextF.enabled = NO;

}

- (IBAction) Back:(id)sender
{
    
	SWRevealViewController * mainView = [self.storyboard instantiateViewControllerWithIdentifier:@"sw"];
	[self presentViewController:mainView animated:YES completion:nil];

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
	// Do any additional setup after loading the view.
	_p = [[DataEngine sharedInstance] GetCurPropertyInstace];
    //[_p removeNullData];
    [self initTextfieldPlaceHolder];
    [self LoadInformation];
    self.navBar.topItem.title = [NSString stringWithFormat:@"For %@",_p.listingType];
    
    
}
- (void) initTextfieldPlaceHolder
{

    self.blkNoTextF.placeholder = @"Block: ";
    self.levelTextF.placeholder = @"Level: ";
    self.unitTextF.placeholder = @"Unit: ";
    self.districtTextF.placeholder = @"District: ";
    self.bedroomTextF.placeholder = @"Bedroom: ";
    self.areaTextF.placeholder = @"Floor Size: ";
    self.priceTextF.placeholder = @"Price: ";
    self.tenureTextF.placeholder = @"Tenure: ";
    self.statusTextF.placeholder = @"Status: ";
    self.rentedPriceTextF.placeholder = @"Rental Price: ";
    
    
    
    self.projectTextF.placeholder = @"Project Name: ";
    self.topYearTextF.placeholder = @"Top Year: ";
    self.psfTextF.placeholder = @"PSF: ";
    self.rentalYeildTextF.placeholder = @"Rental Yield: ";
    
    self.addressLbl.text = @"";
    self.postcodeLbl.text = @"";
    self.blkNoTextF.text = @"";
    self.levelTextF.text = @"";
    self.unitTextF.text = @"";
    self.districtTextF.text = @"";
    self.bedroomTextF.text = @"";
    self.areaTextF.text = @"";
    self.priceTextF.text = @"";
    self.tenureTextF.text = @"";
    self.statusTextF.text = @"";
    self.rentedPriceTextF.text = @"";
    
    
    self.projectTextF.text = @"";
    self.topYearTextF.text = @"";
    self.psfTextF.text = @"";
    self.rentalYeildTextF.text = @"";
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
