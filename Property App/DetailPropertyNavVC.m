//
//  DetailPropertyNavVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailPropertyNavVC.h"
#import "MyUIEngine.h"

@interface DetailPropertyNavVC ()

@end

@implementation DetailPropertyNavVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
