//
//  AgentProfileVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "AgentProfileVC.h"

@interface AgentProfileVC ()

@end

@implementation AgentProfileVC
@synthesize curUser = _curUser;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    [nameLb setText:_curUser.fullName];
    [phoneLb setText:_curUser.phone];
    [emailLB setText:_curUser.email];
    
    NSLog(@"%@ ===", _curUser.fullName);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
