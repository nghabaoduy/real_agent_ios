//
//  ListPattern.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 14/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListPattern : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView * displayer;
@property (strong, nonatomic) IBOutlet UILabel * projectName;
@property (strong, nonatomic) IBOutlet UILabel * priceAndBedroom;
@property (strong, nonatomic) IBOutlet UILabel * projectType;
@property (strong, nonatomic) IBOutlet UILabel * areaAndPsf;
@property (strong, nonatomic) IBOutlet UILabel * location;
@property (strong, nonatomic) IBOutlet UILabel *statusNew;


@end
