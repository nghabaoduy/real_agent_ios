//
//  SalesListViewController.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
#import "MyUIViewController.h"

@interface SalesListViewController : MyUIViewController <UITableViewDelegate, UITableViewDataSource>
{
    int count;
    int total;
    NSMutableArray * propertyArray;
}

@property (nonatomic, retain) IBOutlet UINavigationBar * navBar;
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) NSMutableArray * data;
@property (nonatomic, retain) NSMutableArray * Titles;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *addBtn;
@property (nonatomic, retain) NSMutableArray * toolbarButtons;


@property (nonatomic, retain) Property * propertySearch;

-(void) hideAddBtn;
-(void) showAddBtn;
- (void) AddToPropertyList: (Property*) cur;
@end