//
//  ClientSearchViewController.m
//  PhoneBookController
//
//  Created by Brian on 9/9/13.
//  Copyright (c) 2013 Brian. All rights reserved.
//

#import "ClientSearchViewController.h"

#import <AddressBook/AddressBook.h>
#import "MyUIEngine.h"
#import "User.h"
#import "Client.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "SwitchCell.h"

@interface ClientSearchViewController ()

@end

@implementation ClientSearchViewController

@synthesize itemSearchBar;
@synthesize tableView;
@synthesize toolbar;

@synthesize displayArray = _displayArray;
@synthesize addBookArray = _addBookArray;
@synthesize processingClients = _processingClients;

@synthesize clientView = _clientView;

NSMutableArray *clientArray;

bool addClientClicked = NO;
@synthesize selectedRowsArray;


-(void) pushClientArray:(NSMutableArray *) array
{
    clientArray = array;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self startLoading];
   // [self initViewData];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (animated) {
        
        [self initViewData];
    }
}

-(void) initViewData
{
    [[MyUIEngine sharedUIEngine] customToolbar:self.toolbar];
    
    [self loadAddressBook];
    self.displayArray = [self.addBookArray copy];
    selectedRowsArray = [[NSMutableArray alloc] init];
    
    NSLog(@"displayArray = %i",self.displayArray.count);
    
    [self.tableView reloadData];
    addClientClicked = NO;
    [self finishLoading];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSLog(@"numberOfRowsInSection = %i",self.displayArray.count);
    return self.displayArray.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"switchCell";
    
    SwitchCell *sCell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    User* user = [self.displayArray objectAtIndex:indexPath.row];
    sCell.mainLabel.text =[NSString stringWithFormat:@"%@",[user description]];
    
    if ([selectedRowsArray containsObject:[self.displayArray objectAtIndex:indexPath.row]]) {
     [sCell.selectSwitch setOn:YES animated:YES];
     }
     else {
     [sCell.selectSwitch setOn:NO animated:YES];
     }
    sCell.selectSwitch.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleChecking:)];
    [sCell.selectSwitch addGestureRecognizer:tap];
    
    CGRect containerFrame = sCell.selectSwitch.frame;
    containerFrame.origin = CGPointMake(0, 0);
    UIView *containerView = [[UIView alloc] initWithFrame:containerFrame];
    containerView.backgroundColor = [UIColor clearColor];
    [sCell.selectSwitch addSubview:containerView];
    //[containerView addGestureRecognizer:tap];
    //[containerView addSubview:sCell.selectSwitch];
    [sCell.selectSwitch setOn:[selectedRowsArray containsObject:[self.displayArray objectAtIndex:indexPath.row]] animated:NO];
    return sCell;
}

- (void) handleChecking:(UITapGestureRecognizer *)tapRecognizer {
    
    CGPoint tapLocation = [tapRecognizer locationInView:self.tableView];
    NSIndexPath *tappedIndexPath = [self.tableView indexPathForRowAtPoint:tapLocation];
    User *user = [self.displayArray objectAtIndex:tappedIndexPath.row];
    //user phone = null
    if (user.phone == NULL) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"User doesn't have phone number"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([selectedRowsArray containsObject:[self.displayArray objectAtIndex:tappedIndexPath.row]]) {
        [selectedRowsArray removeObject:[self.displayArray objectAtIndex:tappedIndexPath.row]];
    }
    else {
        [selectedRowsArray addObject:[self.displayArray objectAtIndex:tappedIndexPath.row]];
    }
    NSLog(@"tab row = %@",[self.displayArray objectAtIndex:tappedIndexPath.row]);
    //[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:tappedIndexPath] withRowAnimation: UITableViewRowAnimationFade];
    SwitchCell *sCell = (SwitchCell*)[self.tableView cellForRowAtIndexPath:tappedIndexPath];
    [sCell.selectSwitch setOn:!sCell.selectSwitch.on animated:YES];
}



-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)input
{

}

-(void) searchBarSearchButtonClicked:(UISearchBar *) searchBar{
    NSString *input = itemSearchBar.text;
    NSLog(@"%@",input);
    if([input length]==0)
    {
        self.displayArray = [self.addBookArray copy];
    }
    else
    {
        NSLog(@"search text change");
        self.displayArray = [[NSMutableArray alloc] init];
        for (User *user in self.addBookArray) {
            NSString *fullName = [NSString stringWithFormat:@"%@",user];
            NSLog(@"%@ - %@",fullName,input);
            NSRange range = [fullName rangeOfString:input options:NSCaseInsensitiveSearch];
            NSLog(@"range = %i",range.length);
            if(range.length!=0){
                [self.displayArray addObject:user];
            }
            NSLog(@"displayArray = %@",self.displayArray);
        }
        
    }
    [self.tableView reloadData];
    // NSLog(@"displayArray = %@",self.displayArray);
    NSLog(@"selectedRowsArray = %@",self.selectedRowsArray);

    [searchBar resignFirstResponder];
}

/*-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
 if([segue.identifier isEqualToString:@"ShowItemInfo"])
 {
 
 //   NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
 //   ItemInfoViewController *itemInfoVC= segue.destinationViewController;
 
 //  itemInfoVC.itemID = [self.idArray objectAtIndex:indexPath.row];
 //  //NSLog(@"id send = %@",[self.idArray objectAtIndex:indexPath.row]);
 
 
 }
 }*/


- (void)getPersonOutOfAddressBook
{
    self.addBookArray = [[NSMutableArray alloc] init] ;
    CFErrorRef error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    if (addressBook != nil)
    {
        NSLog(@"AddressBook loads Succesful.");
        
        NSArray *allContacts = (__bridge_transfer NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
        
        NSUInteger i = 0;
        for (i = 0; i < [allContacts count]; i++)
        {
            User *user = [[User alloc] init];
            //name
            ABRecordRef contactPerson = (__bridge ABRecordRef)allContacts[i];
            
            NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty);
            NSString *lastName =  (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonLastNameProperty);
            
            user.firstName = [[DatabaseEngine sharedInstance] removeClientString:firstName];
            user.lastName = [[DatabaseEngine sharedInstance] removeClientString:lastName];
            user.fullName = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
            NSLog(@"user.fullname = %@",user.fullName);
            //address
            
            
            ABMultiValueRef address = ABRecordCopyValue(contactPerson, kABPersonAddressProperty);
            
            NSArray * theArray  = (__bridge_transfer NSArray*)ABMultiValueCopyArrayOfAllValues(address);
            
            NSLog(@"total %i", [theArray count]);
            
            // check if array count = 0;
            NSDictionary *theDict = [theArray objectAtIndex:0];
            
            const NSUInteger theCount = [theDict count];
            id __unsafe_unretained objects[theCount];
            id __unsafe_unretained keys[theCount];
            
            [theDict getObjects:objects andKeys:keys];
            
            NSString *theAddress;
            
            theAddress = [NSString stringWithFormat:@" %@, %@ %@",
                          [theDict objectForKey:(NSString *)kABPersonAddressStreetKey] == nil?@"":[theDict objectForKey:(NSString *)kABPersonAddressStreetKey],
                          [theDict objectForKey:(NSString *)kABPersonAddressCityKey] == nil?@"":[theDict objectForKey:(NSString *)kABPersonAddressCityKey],
                          [theDict objectForKey:(NSString *)kABPersonAddressStateKey] == nil?@"":[theDict objectForKey:(NSString *)kABPersonAddressStateKey]
                          //[theDict objectForKey:(NSString *)kABPersonAddressZIPKey] == nil?@"":[theDict objectForKey:(NSString *)kABPersonAddressZIPKey],
                         // [theDict objectForKey:(NSString *)kABPersonAddressCountryKey] == nil?@"":[theDict objectForKey:(NSString *)kABPersonAddressCountryKey]
                          ];
            
            
            user.address = theAddress;
            NSLog(@"address = %@", user.address);
            //email
            
            ABMultiValueRef emails = ABRecordCopyValue(contactPerson, kABPersonEmailProperty);
            
            NSUInteger j = 0;
            for (j = 0; j < ABMultiValueGetCount(emails); j++)
            {
                NSString *email = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emails, j);
                if (j == 0)
                {
                    user.homeEmail = email;
                    //NSLog(@"person.homeEmail = %@ ", person.homeEmail);
                }
                else if (j==1)
                    user.email = email;
            }
            //phone no
            NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
            ABMultiValueRef multiPhones = ABRecordCopyValue(contactPerson,kABPersonPhoneProperty);
            for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);++i) {
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = (__bridge_transfer NSString *) phoneNumberRef;
                
                [phoneNumbers addObject:phoneNumber];
            }
            if ([phoneNumbers count]>0) {
                NSString* phoneNumber = [[DataEngine sharedInstance] cleanPhoneNo:[phoneNumbers objectAtIndex:0]];
                user.phone = phoneNumber;
                
            }
            //finish
            [self.addBookArray addObject:user];
        }
        //8
        CFRelease(addressBook);
    }
    else
    {
        //9
        NSLog(@"Error reading Address Book");
    }
}

- (IBAction) finishAddClient:(id)sender
{
    [self actionRun];
    _processingClients = [[NSMutableArray alloc] init];
    
    [DataEngine sharedInstance].clientSearchView = self;
    if (addClientClicked == NO) {
        NSLog(@"finishAddClient");
        [self addSearchResult:selectedRowsArray];
        addClientClicked = YES;
    }
    
   
}
-(void) actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    NSLog(@"finishedAdding runs");
    //[self.clientView refreshData];
    for (User *user in selectedRowsArray) {
        [user loadUserObjectByPhone];
    }
    [self.clientView addClientTemporary:self.selectedRowsArray];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void) actionFail:(NSString *)message
{

    [super actionFail:message];
    NSLog(@"Adding Fail - %@", message);
    int error = [message intValue];
    NSString *errorStr;
    if (error == 125) {
        errorStr = @"Email address is invalid";
    }
    else if (error == 202) {
        errorStr = @"Userphone Number has been taken";
    }
    else
    {
        errorStr = message;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:errorStr
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void) addSearchResult: (NSMutableArray *)searchResult
{
    NSLog(@"addSearchResult runs");
    for(User *user in searchResult)
    {
        NSLog(@"client phone no = %@",user.phone);
        if ([[DatabaseEngine sharedInstance] addNewClientToDatabase:user processingArray:_processingClients view:self]) {
            //[self.agentClientList addObject:user];
        }
    }
    [tableView reloadData];
    if ([searchResult count] != 0 && [_processingClients count] == 0) {
        [self actionSuceed:@""];
    }
    
    NSLog(@"contentArray = %@",self.contentArray);
}


- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) SelectAll:(id)sender
{
    UIBarButtonItem *selectBtn = (UIBarButtonItem *)sender;
    if(![selectBtn.title isEqualToString:@" Deselect All              "])
    {
        for(User *user in self.displayArray)
        {
            if (![selectedRowsArray containsObject:user]) {
                [selectedRowsArray addObject:user];
            }
            [selectBtn setTitle:@" Deselect All              "];
        }
    }
    else
    {
        selectedRowsArray = [[NSMutableArray alloc] init];
        [selectBtn setTitle:@" Select All                  "];
    }
    
    [self.tableView reloadData];
}

-(void)loadAddressBook
{
    // Request authorization to Address Book
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            // First time access has been granted, add the contact
            [self getPersonOutOfAddressBook];
            [self.tableView reloadData];
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        // The user has previously given access, add the contact
        [self getPersonOutOfAddressBook];
        [self.tableView reloadData];
    }
    else {
        // The user has previously denied access
        // Send an alert telling user to change privacy setting in settings app
    }
    [self.tableView reloadData];
    
}


@end
