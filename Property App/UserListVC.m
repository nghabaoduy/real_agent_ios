//
//  UserListVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/17/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "UserListVC.h"
#import "DataEngine.h"
#import "DatabaseEngine.h"

@interface UserListVC ()

@end

@implementation UserListVC
@synthesize userList;
@synthesize curUser;
@synthesize messageListView;

- (void)actionRun
{
    [super actionRun];
    [[DatabaseEngine sharedInstance] loadAllUser:self :curUser.userObject];
    
}

- (void)actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    
    userList = [userList sortedArrayUsingComparator:^NSComparisonResult(User *a, User*  b)
                {
                    //Default compare, to protect against cast
                    
                    return ([a.fullName compare:b.fullName]);
                    
                }];
    [self.tableView reloadData];
}

- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    curUser = [[DataEngine sharedInstance] GetCurUser];
    [self actionRun];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [userList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    User * user = [userList objectAtIndex:indexPath.row];
    
    cell.textLabel.text = user.fullName;
    cell.detailTextLabel.text = user.phone;
    cell.detailTextLabel.textColor = [UIColor grayColor];
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self startLoading];
    NSLog(@"hahahahh");
    User * user = [userList objectAtIndex:indexPath.row];
    
    if (user.isInstall == 0) {
        [self showSMS:user];
    }
    else
    {
        if ([self checkDuplicate:user]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [[DatabaseEngine sharedInstance] validateMessageConnector:curUser.userObject :user.userObject :self];
        }
    }
    
}

-(BOOL) checkDuplicate:(User*)user
{
    for (MessageInfo *message in messageListView.messageList) {
        if ([message.connector.username isEqualToString:user.username]) {
            return YES;
        }
    }
    return NO;
}

- (void)selectionFinish
{
    [self.navigationController popViewControllerAnimated:YES];
    [messageListView actionRun];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */

- (void)showSMS:(User*) _user{
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = @[_user.phone];
    NSString *message = [NSString stringWithFormat:@"\n\n\nClick on the link https://itunes.apple.com/en/app/angry-birds/id343200656 to download this app."];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
