//
//  Agent.m
//  Property App
//
//  Created by Jonathan Le on 12/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "Agent.h"
#import "DatabaseEngine.h"
#import "ProPortfolioViewController.h"
#import "DataEngine.h"
#import "LoadingViewController.h"
#import "MyUIViewController.h"

@implementation Agent
@synthesize myClientList, myClientPropertyList, parentView;
@synthesize refreshViewArray;

int clientNumber;
bool fetchUserInfoDone;
bool isRefreshing = NO;

- (PFUser*) getUserObject
{
	return self.userObject;
}


- (void) DownloadMyClientList {
	
}

-(void) addRefreshView: (MyUIViewController *) view
{
    if (refreshViewArray == nil) {
        refreshViewArray = [[NSMutableArray alloc] init];
    }
    [refreshViewArray addObject:view];
}


- (void) RefreshData {
    if (!isRefreshing) {
        isRefreshing = YES;
        [self GetPropertyList];
        [self GetMyClientList];
    }

}

- (void) GetMyClientList
{
	//parentView = pView;
	myClientList = [[NSMutableArray alloc] init];
	[[DatabaseEngine sharedInstance] GetMyClientList:self];
}

- (void) GetClientSuccessful : (NSMutableArray*) clientList
{
	NSLog(@"Successfully added %i client", clientList.count);
	for (PFUser* client in clientList) {
		Client* cli = [[Client alloc] initWithUser:client];
		[myClientList addObject:cli];
		//NSLog(@"client %@", agent.username);
	}
    isRefreshing = NO;
	
	[self GetAgentClientPropertyList];
}

- (void) GetClientFailed : (NSString*) errorString
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:errorString
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    isRefreshing = NO;
}

- (void) GetAgentClientPropertyList {
	myClientPropertyList = [[NSMutableArray alloc] init];
	clientNumber = myClientList.count;
	for(Client* cli in myClientList) {
		[[DatabaseEngine sharedInstance] GetAgentClientPropertyList:self :cli];
	}
}

- (void) GetAgentClientPropertySuccessful : (NSMutableArray*) propList
{
    NSLog(@"// GetAgentClientPropertySuccessful %i", clientNumber);
	clientNumber--;
	[myClientPropertyList addObject:propList];
	NSLog(@"Prop list %@", propList);
	
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Done"
													message:@"Load all done"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
    if(clientNumber <= 0)
    {
        NSLog(@"// Yah");
        [[DataEngine sharedInstance].clientSearchView actionSuceed:@""];
        [[DataEngine sharedInstance].loadingView loadMainMenu];
    }
	
    
    for (MyUIViewController *view in refreshViewArray) {
        [view refreshView];
        [view actionSuceed:@""];
    }
    [refreshViewArray removeAllObjects];
	/*if ((clientNumber == 0) && (parentView != nil)){
		[parentView LoadPropertyListDone];
	}*/
}

- (void) GetAgentClientPropertyFailed: (NSString*) errorString
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:errorString
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}

- (NSString*) description {
	return [NSString stringWithFormat:@"Agent - %@",[super description]];
}

@end
