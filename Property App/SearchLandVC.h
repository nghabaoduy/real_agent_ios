//
//  SearchLandVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/14/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"

@interface SearchLandVC : UITableViewController<UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    IBOutlet UITextField * propertyTypeTF;
    IBOutlet UITextField * listingTypeTF;
    IBOutlet UITextField * districtTF;
    IBOutlet UITextField * landTypeTF;
    IBOutlet UITextField * areaTF;
    IBOutlet UITextField * priceTF;
    IBOutlet UITextField * tenureTF;
    IBOutlet UITextField * rentedPriceTF;
    IBOutlet UITextField * projectNameTF;
    IBOutlet UITextField * psfTF;
    IBOutlet UITextField * topYearTF;
    IBOutlet UITextField * rentalYieldTF;
    
    
    IBOutlet UITextField * bedroom;
    IBOutlet UITextField * funishing;
    IBOutlet UITextField * landSize;
    IBOutlet UITextField * mainDoorFacing;
    IBOutlet UITextField * mainGateFacing;
    IBOutlet UITextField * carPark;
    IBOutlet UITextField * garden;
    IBOutlet UITextField * swimmingPool;
    IBOutlet UITextField * basement;
    IBOutlet UITextField * roofTerrance;
    
    
    
    
    
    NSArray * rentalYieldList;
    NSArray * districtList;
    NSArray * tenureList;
    NSArray * statusList;
    NSArray * landTypeList;
    NSArray * funishingList;
    NSArray * mainDoorFacingList;
    NSArray * mainGateFacingList;
    NSArray * carParkList;
    NSArray * gardenList;
    NSArray * swimmingPoolList;
    NSArray * basementList;
    NSArray * roofTerranceList;
    
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
}
@property (nonatomic, strong) Property * property;

@end
