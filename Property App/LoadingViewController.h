//
//  LoadingViewController.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 15/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingViewController : UIViewController

- (void) loadMainMenu;
@end
