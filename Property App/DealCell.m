//
//  DealCell.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/19/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DealCell.h"

@implementation DealCell
@synthesize phone, phoneNumber, isLoaded, imageURL, background;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)getButton
{
    [phoneNumber addTarget:self action:@selector(call:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)call:(id)sender
{
    NSLog(@"%@ ", phone);
    //NSString * call = [NSString stringWithFormat:@"tel:%@", phone];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}

- (void)getImage
{
    if (!isLoaded) {
        NSLog(@"start loading image ");;
        NSURL *url = [NSURL URLWithString:imageURL];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *img = [[UIImage alloc] initWithData:data];
        
        [background setImage:img];
    }
}
@end
