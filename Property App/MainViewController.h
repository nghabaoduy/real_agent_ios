//
//  MainViewController.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

@property (nonatomic, retain) UIButton * btn1;
@property (nonatomic, retain) UIButton * btn2;
@property (nonatomic, retain) UIButton * btn3;
@property (nonatomic, retain) UIButton * btn4;
@property (nonatomic, retain) UIButton * btn5;
@property (nonatomic, retain) UIButton * btn6;
@property (nonatomic, retain) UIButton * btn7;
@property (nonatomic, retain) UIButton * btn8;
@property (nonatomic, retain) UIButton * btn9;
@property (nonatomic, retain) UIButton * btn10;
@property (nonatomic, retain) UIButton * btn11;
@property (nonatomic, retain) UIButton * btn12;



@property (weak, nonatomic) IBOutlet UIBarButtonItem * sidebarButton;
- (void) GetMyListSuccessful : (NSArray*) objects;
- (void) GetMyListFailed : (NSString*) errorString;
@end
