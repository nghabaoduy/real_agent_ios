//
//  ScheduleEvent.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/13/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ScheduleEvent.h"

@implementation ScheduleEvent
@synthesize agentAccept, creatorAccept, eventType, requesterAccept;

- (NSString *)getEventType
{
    NSString * temp = @"";
    if (agentAccept && requesterAccept) {
        temp = @"Event accepted";
    }
    else if (requesterAccept)
    {
        temp = @"Pending Confirmation";
    }
    else if (agentAccept )
    {
        temp =@"Waiting requester";
    }
    else
    {
        temp = @"Proposed Meetup";
    }
    eventType = temp;
    return temp;
}

-(NSInteger) getEventMonth
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:self.eventSchedule];
    return [components month];
}
-(NSInteger) getEventDay
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:self.eventSchedule];
    return [components day];
}
-(NSInteger) getEventYear
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:self.eventSchedule];
    return [components year];
}

@end
