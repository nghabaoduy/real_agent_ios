//
//  DetailPropertyTabBarVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailPropertyTabBarVC.h"
#import "MyUIEngine.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "Property.h"

#import "DetailPropertyVC.h"
#import "GalleryPropertyVC.h"
#import "LocationPropertyVC.h"

#import "DetailComercialVC.h"
#import "DetailHDBVC.h"
#import "DetailIndustrialVC.h"
#import "DetailLandVC.h"


@interface DetailPropertyTabBarVC ()

@end

@implementation DetailPropertyTabBarVC
@synthesize homeVC;
@synthesize property, isFavorited, curUser, favoriteInfo, isHide;

- (IBAction) Back:(id)sender
{
    if (homeVC!= NULL) {
        [self presentViewController:homeVC animated:YES completion:nil];
        homeVC = NULL;
    }
    else
    {
         [self dismissViewControllerAnimated:YES completion:nil];
    }
   
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
	// Do any additional setup after loading the view.
    curUser = [[DataEngine sharedInstance] GetCurUser];
    [self startCheckFavorite];
    [addBtn setTitle:@""];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)favorite:(id)sender
{
    
    if (isFavorited) {
        [addBtn setImage:[UIImage imageNamed:@"unliked"]];
        [[DatabaseEngine sharedInstance] unaddFavorite:favoriteInfo :self];
        
    }
    else
    {
        [addBtn setImage:[UIImage imageNamed:@"liked"]];
        [[DatabaseEngine sharedInstance] addFavorite:curUser :property :self];
    }
    addBtn.enabled = NO;
}

- (void)startCheckFavorite
{
    [[DatabaseEngine sharedInstance] validateFavorite:curUser :property :self];
}

- (void)finishCheckFavorite
{
    
    if (isFavorited) {
        [addBtn setImage:[UIImage imageNamed:@"liked"]];
    }
    else
    {
        [addBtn setImage:[UIImage imageNamed:@"unliked"]];
    }
    addBtn.enabled = YES;
}

-(void) initTabByProperty:(Property*) curPro :(UIStoryboard *) newStoryboard
{
    self.property = curPro;
    DetailPropertyVC * tab01 = [newStoryboard instantiateViewControllerWithIdentifier:@"tab01"];
    GalleryPropertyVC * tab02 = [newStoryboard instantiateViewControllerWithIdentifier:@"tab02"];
    LocationPropertyVC * tab03 = [newStoryboard instantiateViewControllerWithIdentifier:@"tab03"];
    tab01.property = curPro;
    tab01.allowUnitNo = self.allowUnitNo;
    tab01.isSharing = _isShare;
    tab01.curGroup = _curGroup;
    tab01.proportView = _proportView;
    tab02.property = curPro;
    tab03.property = curPro;
    self.viewControllers = [NSArray arrayWithObjects:tab01, tab02, tab03, nil];
    
    if ([curPro.propertyType isEqualToString:@"Commercial"]) {
        DetailComercialVC *comTab = [newStoryboard instantiateViewControllerWithIdentifier:@"comTab"];
        comTab.isSharing = _isShare;
        comTab.property = curPro;
        comTab.allowUnitNo = self.allowUnitNo;
        comTab.curGroup = _curGroup;
        comTab.proportView = _proportView;
        self.viewControllers = [NSArray arrayWithObjects:comTab, tab02, tab03, nil];
    }
    if ([curPro.propertyType isEqualToString:@"HDB"]) {
        DetailComercialVC *hdbTab = [newStoryboard instantiateViewControllerWithIdentifier:@"hdbTab"];
        hdbTab.isSharing = _isShare;
        hdbTab.property = curPro;
        hdbTab.allowUnitNo = self.allowUnitNo;
        hdbTab.curGroup = _curGroup;
        hdbTab.proportView = _proportView;
        self.viewControllers = [NSArray arrayWithObjects:hdbTab, tab02, tab03, nil];
    }
    if ([curPro.propertyType isEqualToString:@"Industrial"]) {
        DetailIndustrialVC *industrialTab = [newStoryboard instantiateViewControllerWithIdentifier:@"industrialTab"];
        industrialTab.isSharing = _isShare;
        industrialTab.property = curPro;
        industrialTab.allowUnitNo = self.allowUnitNo;
        industrialTab.curGroup = _curGroup;
        industrialTab.proportView = _proportView;
        self.viewControllers = [NSArray arrayWithObjects:industrialTab, tab02, tab03, nil];
    }
    if ([curPro.propertyType isEqualToString:@"Land"]) {
        DetailLandVC *landTab = [newStoryboard instantiateViewControllerWithIdentifier:@"landTab"];
        landTab.isSharing = _isShare;
        landTab.property = curPro;
        landTab.allowUnitNo = self.allowUnitNo;
        landTab.curGroup = _curGroup;
        landTab.proportView = _proportView;
        self.viewControllers = [NSArray arrayWithObjects:landTab, tab02, tab03, nil];
    }

    
}

@end
