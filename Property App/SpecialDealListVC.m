//
//  SpecialDealListVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/19/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "SpecialDealListVC.h"
#import "DealCell.h"
#import "DatabaseEngine.h"
#import "SpecialDealDetailVC.h"

@interface SpecialDealListVC ()

@end

@implementation SpecialDealListVC
@synthesize fileData;

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)actionRun
{
    [super actionRun];
    [[DatabaseEngine sharedInstance]getSpecialDeal:self];
}

- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
}

- (void)actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    
    
    
    xmlParser = [[NSXMLParser alloc] initWithData:fileData];
    [xmlParser setDelegate:self];
    BOOL result = [xmlParser parse];
    NSLog(@"%hhd result", result);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    dealList = [[NSMutableArray alloc]init];
    imagelist = [[NSMutableArray alloc]init];
    
    
    
    
    
    [self actionRun];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dealList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"dealCell";
    DealCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[DealCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSMutableDictionary * curDic = [dealList objectAtIndex:indexPath.row];
    
    [cell.status setText:[curDic objectForKey:@"status"]];
    [cell.subject setText:[curDic objectForKey:@"subject"]];
    [cell.price setText:[curDic objectForKey:@"price"]];
    [cell.address setText:[curDic objectForKey:@"address"]];
    [cell.createdAt setText:[curDic objectForKey:@"createdAt"]];
    [cell.advertiser setText:[curDic objectForKey:@"advertiser"]];
    cell.phone  = [curDic objectForKey:@"phone"];
    
    NSString * phoneNumber = [NSString stringWithFormat:@"Call: %@", cell.phone];
    [cell.phoneNumber setTitle:phoneNumber forState:UIControlStateNormal];
    
    NSString * imageURL = [curDic objectForKey:@"background"];
    
    NSLog(@"%@", imageURL);
    
    
    
    
    if (imagelist.count > 0) {
        UIImage * curImage = [imagelist objectAtIndex:indexPath.row];
        if ([curImage isEqual:[NSNull null]]) {
            NSURL *url = [NSURL URLWithString:imageURL];
            NSData *data = [NSData dataWithContentsOfURL:url];
            
            UIImage *img = [[UIImage alloc] initWithData:data];
            NSLog(@"%@", img);
            
            [imagelist insertObject:img atIndex:indexPath.row];
            [cell.background setImage:img];
            
        }
        else
        {
            NSLog(@"Loadddddd");
            [cell.background setImage:curImage];

        }
        
    }
    
    
    [cell getButton];
    
    
    
    
    
    
    
    
    
    return cell;
}

- (void) call : (NSString *) phone
{
    NSLog(@"%@", phone);
    //NSString * call = [NSString stringWithFormat:@"tel:%@", phone];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    if (![elementName isEqualToString:@"item"])
    {
        NSLog(@"cant find");
        return;
    }
    
    
    [dealList addObject:attributeDict];
    [imagelist addObject:[NSNull null]];
    
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"item"])
    {
        NSLog(@"reach the end");
        [self.tableView reloadData];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goDetail"]) {
        SpecialDealDetailVC * destination = segue.destinationViewController;
        destination.curDic = [dealList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
        destination.image = [imagelist objectAtIndex:self.tableView.indexPathForSelectedRow.row];
        NSLog(@"go go go go");
        
    }
}
@end
