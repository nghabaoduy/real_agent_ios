//
//  DataEngine.m
//  Property App
//
//  Created by Jonathan Le on 10/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DataEngine.h"
#import "Agent.h"
@implementation DataEngine
@synthesize propertyDataArray, curUser, propertyNameList, addingProperty, curPropertyInstance, isAgent, viewingUser;
static DataEngine *sharedObject;


+ (DataEngine*)sharedInstance
{
	if (sharedObject == nil) {
		sharedObject = [[super allocWithZone:NULL] init];
	}
	return sharedObject;
}

- (void) SetCurUser:(User *)user
{
	self.curUser = user;
}
- (User*) GetCurUser
{
	return curUser;
}

- (void) setViewingUser:(User *)viewingU
{
	viewingUser = viewingU;
}
- (User*) getViewingUser
{
	return viewingUser;
}

- (void) SetCurPropertyInstace:(Property *)property
{
	self.curPropertyInstance = property;
}
- (Property*) GetCurPropertyInstace
{
	return curPropertyInstance;
}

- (void) SetPropertyDataArray:(NSMutableArray *)propertyArray
{
	self.propertyDataArray = propertyArray;
}
- (NSMutableArray*) GetPropertyDataArray
{
	return propertyDataArray;
}

- (void) SetPropertyNameList:(NSMutableArray *)nameList {
	self.propertyNameList = nameList;
}
- (NSMutableArray*) GetPropertyNameList {
	return propertyNameList;
}

- (void) setIsAgent: (BOOL) isAg
{
	isAgent = isAg;
}

- (BOOL) IsAgent
{
	return self.isAgent;
}

-(NSString *) cleanPhoneNo : (NSString *) phoneNumber
{
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    return phoneNumber;
}

- (NSString*)getBlockNumber:(NSString*)PostalCode
{
    NSString * blockNumber = [PostalCode substringFromIndex:[PostalCode length] - 3];
    NSLog(@"%@", blockNumber);
    return blockNumber;
}

- (NSString *)getDistrict:(NSString*)PostalCode
{
    NSString * code = [PostalCode substringToIndex:2];
    NSString * district;
    
    if ([code isEqualToString:@"01"] || [code isEqualToString:@"02"] || [code isEqualToString:@"03"]
        || [code isEqualToString:@"04"] || [code isEqualToString:@"05"] || [code isEqualToString:@"06"]) {
        district = @"Raffles Place, Cecil, Marina, People's Park";
    }
    else if ( [code isEqualToString:@"07"] || [code isEqualToString:@"08"])
    {
        district = @"Anson, Tanjong Pagar";
    }
    else if ( [code isEqualToString:@"14"]|| [code isEqualToString:@"15"] || [code isEqualToString:@"16"])
    {
        district = @"Queenstown, Tiong Bahru";
    }
    else if ([code isEqualToString:@"09"] || [code isEqualToString:@"10"])
    {
        district = @"Telok Blangah, Harbourfront";
    }
    else if ( [code isEqualToString:@"11"] || [code isEqualToString:@"12"] || [code isEqualToString:@"13"])
    {
        district = @"Pasir Panjang, Hong Leong Garden, Clementi New Town";
    }
    else if ([code isEqualToString:@"17"])
    {
        district = @"High Street, Beach Road (part)";
    }
    else if ([code isEqualToString:@"18"] || [code isEqualToString:@"19"])
    {
        district = @"Middle Road, Golden Mile";
    }
    else if ([code isEqualToString:@"21"] || [code isEqualToString:@"20"])
    {
        district = @"Little India";
    }
    else if ([code isEqualToString:@"22"]||[code isEqualToString:@"23"])
    {
        district = @"Orchard, Cairnhill, River Valley";
    }
    else if ([code isEqualToString:@"24"] || [code isEqualToString:@"25"] || [code isEqualToString:@"26"] || [code isEqualToString:@"27"])
    {
        district = @"Ardmore, Bukit Timah, Holland Road, Tanglin";
    }
    else if ([code isEqualToString:@"28"] || [code isEqualToString:@"29"] || [code isEqualToString:@"30"])
    {
        district = @"Watten Estate, Novena, Thomson";
    }
    else if ( [code isEqualToString:@"31"] || [code isEqualToString:@"32"] || [code isEqualToString:@"33"])
    {
        district = @"Balestier, Toa Payoh, Serangoon";
    }
    else if ([code isEqualToString:@"34"] || [code isEqualToString:@"35"] || [code isEqualToString:@"36"] || [code isEqualToString:@"37"])
    {
        district = @"Macpherson, Braddell";
    }
    else if ([code isEqualToString:@"38"] || [code isEqualToString:@"39"] || [code isEqualToString:@"40"] || [code isEqualToString:@"41"])
    {
        district = @"Geylang, Eunos";
    }
    else if ([code isEqualToString:@"42"] || [code isEqualToString:@"43"] || [code isEqualToString:@"44"] || [code isEqualToString:@"45"])
    {
        district = @"Katong, Joo Chiat, Amber Road";
    }
    else if ([code isEqualToString:@"46"] || [code isEqualToString:@"47"] || [code isEqualToString:@"48"])
    {
        district = @"Bedok, Upper East Coast, Eastwood, Kew Drive";
    }
    else if ([code isEqualToString:@"49"] || [code isEqualToString:@"50"] || [code isEqualToString:@"81"])
    {
        district = @"Loyang, Changi";
    }
    else if ([code isEqualToString:@"51"] || [code isEqualToString:@"52"])
    {
        district = @"Tampines, Pasir Ris";
    }
    else if ([code isEqualToString:@"53"] || [code isEqualToString:@"54"] || [code isEqualToString:@"55"] || [code isEqualToString:@"82"])
    {
        district = @"Serangoon Garden, Hougang, Ponggol";
    }
    else if ([code isEqualToString:@"56"] || [code isEqualToString:@"57"])
    {
        district = @"Bishan, Ang Mo Kio";
    }
    else if ( [code isEqualToString:@"58"] ||[code isEqualToString:@"59"])
    {
        district = @"Upper Bukit Timah, Clementi Park, Ulu Pandan";
    }
    else if ([code isEqualToString:@"60"] || [code isEqualToString:@"61"] || [code isEqualToString:@"62"] || [code isEqualToString:@"63"] || [code isEqualToString:@"64"])
    {
        district = @"Jurong";
    }
    else if ([code isEqualToString:@"65"] || [code isEqualToString:@"66"] || [code isEqualToString:@"67"] || [code isEqualToString:@"68"])
    {
        district = @"Hillview, Dairy Farm, Bukit Panjang, Choa Chu Kang";
    }
    else if ([code isEqualToString:@"69"] || [code isEqualToString:@"70"] || [code isEqualToString:@"71"])
    {
        district = @"Lim Chu Kang, Tengah";
    }
    else if ([code isEqualToString:@"72"] || [code isEqualToString:@"73"])
    {
        district = @"Kranji, Woodgrove";
    }
    else if ([code isEqualToString:@"78"] || [code isEqualToString:@"78"])
    {
        district = @"Upper Thomson, Springleaf";
    }
    else if ([code isEqualToString:@"75"] || [code isEqualToString:@"76"])
    {
        district = @"Yishun, Sembawang";
    }
    else if ([code isEqualToString:@"79"] || [code isEqualToString:@"80"])
    {
        district = @"Seletar";
    }
    else
    {
        district = @"Cant Find";
    }
    NSLog(@"%@", district);
    return  district;
}

- (NSArray *)getDistrictList
{
    NSArray * list = [NSArray arrayWithObjects:
                      @"",
                      @"Raffles Place, Cecil, Marina, People's Park",
                      @"Anson, Tanjong Pagar",
                      @"Queenstown, Tiong Bahru",
                      @"Telok Blangah, Harbourfront",
                      @"Pasir Panjang, Hong Leong Garden, Clementi New Town",
                      @"High Street, Beach Road (part)",
                      @"Middle Road, Golden Mile",
                      @"Little India",
                      @"Orchard, Cairnhill, River Valley",
                      @"Ardmore, Bukit Timah, Holland Road, Tanglin",
                      @"Watten Estate, Novena, Thomson",
                      @"Balestier, Toa Payoh, Serangoon",
                      @"Macpherson, Braddell",
                      @"Geylang, Eunos",
                      @"Katong, Joo Chiat, Amber Road",
                      @"Bedok, Upper East Coast, Eastwood, Kew Drive",
                      @"Loyang, Changi",
                      @"Tampines, Pasir Ris",
                      @"Serangoon Garden, Hougang, Ponggol",
                      @"Bishan, Ang Mo Kio",
                      @"Upper Bukit Timah, Clementi Park, Ulu Pandan",
                      @"Jurong",
                      @"Hillview, Dairy Farm, Bukit Panjang, Choa Chu Kang",
                      @"Lim Chu Kang, Tengah",
                      @"Kranji, Woodgrove",
                      @"Upper Thomson, Springleaf",
                      @"Yishun, Sembawang",
                      @"Seletar", nil];
    
    
    return list;
}

-(void) saveLoginInfo:(NSString*) username : (NSString*) password
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:username forKey:@"saveUsername"];
    [defaults setObject:password forKey:@"savePassword"];
    [defaults setObject:@"NO" forKey:@"isLogout"];
    [defaults synchronize];
}
-(NSDictionary*) loadLoginInfo
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *saveUsername = [defaults objectForKey:@"saveUsername"];
    NSString *savePassword = [defaults objectForKey:@"savePassword"];
    NSString *isLogout = [defaults objectForKey:@"isLogout"];
    NSMutableDictionary *loginDict = [[NSMutableDictionary alloc] init];
    if (saveUsername && savePassword) {
        [loginDict setObject:saveUsername forKey:@"saveUsername"];
        [loginDict setObject:savePassword forKey:@"savePassword"];
        if (isLogout != nil) {
            [loginDict setObject:isLogout forKey:@"isLogout"];
        }
        
    }
    
    return loginDict;
}
@end
