//
//  DetailPropertyVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailPropertyVC.h"
#import "MyUIEngine.h"
#import "RequestMeetupAgentVC.h"
#import "RequestMeetupClientVC.h"
#import "User.h"
#import "DataEngine.h"
#import "DatabaseEngine.h"

@interface DetailPropertyVC ()

@end

@implementation DetailPropertyVC

@synthesize property = _p;


-(void)hideSchedule
{
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId])
    {
        NSLog(@"hide");
        schedule.hidden = YES;
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    [self initTextfieldPlaceHolder];
    [self LoadInformation];
    type = @"Client";
    curUser = [[DataEngine sharedInstance]GetCurUser];
    [self hideSchedule];
    
    NSLog(@"isSHaring %hhd", _isSharing);
    
    if (!_isSharing) {
        addToShare.hidden = YES;
    }
}


- (void)LoadInformation
{
    [_p printInfo];
    [_p removeNullData];
    [_p printInfo];
    [propertyName setText:_p.propertyName];
    [propertyType setText:_p.propertyType];
    
    if (_p.unitNo!=nil && ![_p.unitNo isEqualToString:@""]) {
        if (self.allowUnitNo || [_p.agent.username isEqualToString:[DataEngine sharedInstance].curUser.username]) {
            adressTV.text= [NSString stringWithFormat:@"#%@-%@ %@ %@",_p.levelNumber,_p.unitNo,_p.address,_p.postCode];
        }
    }
    else
        
    {
        adressTV.text= [NSString stringWithFormat:@"%@ %@",_p.address,_p.postCode];
    }
    
    NSLog(@"%@",_p.district);
    [price setText:_p.price];
    
    icon.image = _p.iconImage;
    [district setText:_p.district];
    [noOfBedroom setText:_p.bedroom];
    [floorSize setText:_p.area];
    
    [tenure setText:_p.tenure];
    [status setText:_p.status];
    [rentedPrice setText:_p.rentedPrice];
    
    
    
    [projectName setText:_p.projectName];
    [topYear setText:_p.topYear];
    [psf setText:_p.PSF];
    [rentalYield setText:_p.rentalYeild];
    
    Agent* agent = _p.agent;
    [agentName setText:[NSString stringWithFormat:@"%@ %@",agent.firstName, agent.lastName]];
    [agentMobile setText:[NSString stringWithFormat:@"Tel: %@",agent.agentPhone]];
    [agentEmail setText:[NSString stringWithFormat:@"%@",agent.email]];
    
    
    price.enabled = NO;
    noOfBedroom.enabled = NO;
    district.enabled = NO;
    floorSize.enabled = NO;
    tenure.enabled = NO;
    status.enabled = NO;
    rentedPrice.enabled = NO;
    projectName.enabled = NO;
    psf.enabled = NO;
    topYear.enabled = NO;
    rentalYield.enabled = NO;
    
}


- (void) initTextfieldPlaceHolder
{
    district.placeholder = @"District: ";
    noOfBedroom.placeholder = @"Bedroom: ";
    floorSize.placeholder = @"Floor Size: ";
    price.placeholder = @"Price: ";
    tenure.placeholder = @"Tenure: ";
    status.placeholder = @"Status: ";
    rentedPrice.placeholder = @"Rental Price: ";
    
    
    
    projectName.placeholder = @"Project Name: ";
    topYear.placeholder = @"Top Year: ";
    psf.placeholder = @"PSF: ";
    rentalYield.placeholder = @"Rental Yield: ";
    
}
- (IBAction)callAgent:(id)sender {
    Agent* agent = _p.agent;
    NSLog(@"agentPhone = %@",agent.phone);
    
    NSString * call = [NSString stringWithFormat:@"tel:%@", agent.phone];
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}
- (IBAction)emailAgent:(id)sender {
    Agent* agent = _p.agent;
    NSString * email = agent.email;
    NSArray * recipients = [NSArray arrayWithObject:email];
    
    NSMutableString *body = [NSMutableString string];
   
    [body appendString:@"\n"];
    [body appendString:@"\n"];
    [body appendString:@"<a>Send from property : </a>\n"];
    [body appendString:[NSString stringWithFormat:@"<a href=\"realagent://property-%@\">Track Property</a>\n",self.property.propertyObject.objectId]];
    if ([MFMailComposeViewController canSendMail]) {
        // Show the composer
        NSLog(@"agentEmail = %@",agent.email);
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:recipients];
        [controller setMessageBody:body isHTML:YES];
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Handle the error
    }

    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId] && indexPath.section == tableView.numberOfSections-1)
    {
        return 0.1f;
    }
    if (indexPath.section == 0) {
        return 140;
    }
    if (indexPath.section == tableView.numberOfSections-2) {
        if (indexPath.row ==0) {
            
            return 253;
        }
        else
        {
            if (!_isSharing) {
                return 0;
            }
            else
            {
                return 66;
            }
        }
    }
    return 44;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId] && section == tableView.numberOfSections-1)
    {
        return 0.1f;
    }
    return 35.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId] && section == tableView.numberOfSections-1)
    {
        return 0.1f;
    }
    return 10.0f;
}



- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)schedule:(id)sender
{
    
    [self performSegueWithIdentifier:@"goMeetupClient" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goMeetupClient"])
    {
        RequestMeetupClientVC * destination = segue.destinationViewController;
        destination.property = _p;
    }
}
- (IBAction)addToShare:(id)sender
{
    [sender setEnabled:NO];
    [[DatabaseEngine sharedInstance] valiadtePR:self];
}

- (void)showDialog:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
    [alert show];
}

@end
