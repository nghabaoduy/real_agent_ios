//
//  ResetPasswordViewController.m
//  Property App
//
//  Created by Jonathan Le on 15/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "DatabaseEngine.h"
@interface ResetPasswordViewController ()

@end

@implementation ResetPasswordViewController
@synthesize email = _email;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) SendResetEmailRequest :(id)sender {
	[[DatabaseEngine sharedInstance] ResetPasswordRequest:_email.text :self];
}

- (void) RequestSuccessful {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Sent"
													message:@"Please check your email to change password"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}
- (void) RequestFailed: (NSString*) errorString {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Error"
													message:errorString
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}

@end
