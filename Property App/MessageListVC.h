//
//  MessageListVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/17/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUITableViewController.h"

@interface MessageListVC : MyUITableViewController



@property (nonatomic, retain) NSMutableArray * messageList;

@end
