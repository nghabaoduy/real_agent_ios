//
//  MessageChatVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/17/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MessageInfo.h"
#import "MyUIViewController.h"
#import "User.h"
#import "Property.h"


@interface MessageChatVC : MyUIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    User * user;
    IBOutlet UITableView * tv;
    IBOutlet UITextField * contentTF;
    NSTimer * myTimer;
    BOOL sending;
    Property *shareProp;
}


@property (nonatomic, retain) MessageInfo * messageInfo;
@property (nonatomic, retain) User * curUser;
@property (nonatomic, retain) NSMutableArray * chatList;

@property (nonatomic, retain) PFObject * chatRoomInfo;
@property (nonatomic, retain) NSMutableArray * userInfoList;
@property (nonatomic, retain) NSMutableArray * chatDisplayArray;
@property (nonatomic, retain) NSMutableArray * userDisplayArray;

@property (nonatomic, retain) NSMutableArray * propArray;

- (void) sendDone : (PFObject*) contentObject;
- (void) refreshStart;
- (void) refreshDone;
- (void) timerStop;
- (void) timerStart;
@end
