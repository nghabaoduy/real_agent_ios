//
//  MyClientViewController.h
//  PhoneBookController
//
//  Created by Brian on 8/9/13.
//  Copyright (c) 2013 Brian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GetUserViewController.h"
@interface MyClientViewController : GetUserViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
	//search function
    BOOL isSearchOn;
    BOOL canSelectRow;
    NSMutableArray *searchResult;
}
@property (nonatomic, retain) IBOutlet UISearchBar  *searchBar;
@property (nonatomic, retain) IBOutlet UINavigationBar *navBar;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *addBtn;

@property (nonatomic, retain) IBOutlet UITableView  *tableView;
@property (nonatomic, retain) NSMutableArray *contentArray;

@property (nonatomic, retain) NSMutableArray *addBookArray;

@property (nonatomic, retain) NSMutableArray *barSeperatorIndex;
@property (nonatomic, retain) NSMutableArray *agentClientNameList;
@property (nonatomic, retain) NSMutableArray *objectInEachSection;


-(void) addSearchResult: (NSMutableArray *)searchResult;

//search function
- (IBAction)doneSearching: (id)sender;
- (void) searchTableView;
-(void) refreshData;
-(void) addClientTemporary:(NSArray *)newUsers;
@end
