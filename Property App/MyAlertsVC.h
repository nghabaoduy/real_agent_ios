//
//  MyAlertsVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/20/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUITableViewController.h"
#import "User.h"

@interface MyAlertsVC : MyUITableViewController
{
    User * curUser;
    NSMutableArray * editList;
    int index;
}

@property (nonatomic,retain) NSMutableArray * alertList;
@property (nonatomic,retain) NSMutableArray * statusList;
-(void) refreshStatusNew:(NSString*)statusNew;
@end
