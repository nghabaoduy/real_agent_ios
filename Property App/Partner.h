//
//  Partner.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/30/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import <Parse/Parse.h>


@interface Partner : NSObject

@property (nonatomic, retain) NSString * partnerType;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * contact;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * bank;
@property (nonatomic, retain) User * creator;
@property (nonatomic, retain) PFObject * partnerObject;

@end
