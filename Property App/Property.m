//
//  Property.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 31/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "Property.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "SalesDetailsViewController.h"
#import "ListingDetailsViewController.h"
#import "ListingGalleryViewController.h"
#import "ListingLocationViewController.h"

#import "DetailPropertyTabBarVC.h"
#import "DetailPropertyVC.h"
#import "GalleryPropertyVC.h"
#import "LocationPropertyVC.h"
#import "DetailPropertyNavVC.h"

#import "SWRevealViewController.h"

#import "ParentLoadingVC.h"

#import "Agent.h"
//#import "MyImageView.h"


@implementation Property


@synthesize adder,agent;

@synthesize propID = _propID;

@synthesize propertyObject = _propertyObject;
@synthesize propertyName = _propertyName;
@synthesize propertyType = _propertyType;
@synthesize unitNo = _unitNo;
@synthesize address = _address;
@synthesize district = _district;
@synthesize area = _area;
@synthesize tenure = _tenure;
@synthesize price = _price;
@synthesize bedroom = _bedroom;

@synthesize PSF = _PSF;
@synthesize imageList = _imageList;
@synthesize imageCount, loadingView, iconImage;

- (id) init
{
    if (self == [super init]) {
		
        [self initBlank];
        
    
    }
    return self;
}

-(void) initBlank
{
    self.propID = @"";
    
    self.propertyName = @"";
	self.propertyType = @"";
	self.listingType = @"";
    
    self.blkNo = @"";
	self.address = @"";
	self.postCode = @"";
	self.levelNumber = @"";
	self.unitNo = @"";
	self.district = @"";
    
	self.price = @"";
	self.area = @"";
	self.tenure = @"";
    self.bedroom = @"";
    
    self.status = @"";
    self.rentedPrice = @"";
    
    self.projectName = @"";
    self.topYear = @"";
    self.PSF = @"";
    self.rentalYeild = @"";
    
    self.amps = @"";
    
    self.isSubmited = @"NO";
}

- (id) initWithProperty : (PFObject*) proper
{
    if (self == [super init]) {
		_imageList = [[NSMutableArray alloc] init];
        _propertyObject = proper;
        if(_propertyObject != NULL)
        {
            [self loadAllPropInfo];
            //[self loadIconImage];
            
        }
        //iconImage = [UIImage imageNamed:@"condo2.jpg"];
    }
    return self;
}

- (id) initWithPropertyWithNoIcon : (PFObject*) proper
{
    if (self == [super init]) {
		_imageList = [[NSMutableArray alloc] init];
        _propertyObject = proper;
        if(_propertyObject != NULL)
        {
            [self loadAllPropInfo];

        }
        //iconImage = [UIImage imageNamed:@"condo2.jpg"];
    }
    return self;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"Property: %@ - %@ :::Adder: %@ (%@)",self.propertyName, self.isSubmited,self.adder.fullName, self.address];
}

- (void) loadIconImage
{
    NSLog(@"Start getting Image 1");
    PFQuery *query = [PFQuery queryWithClassName:@"Photo"];
    [query whereKey:@"property" equalTo:_propertyObject];
    NSLog(@"Start getting Image 2");
    query.limit = 1;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        // If there are photos, we start extracting the data
        // Save a list of object IDs while extracting this data
        if (!error) {
            NSLog(@"Start getting Image 3");
            // The find succeeded.
            NSLog(@"Object count = %i",[objects count]);
            if ([objects count] == 0) {
                iconImage = [UIImage imageNamed:@"condo2.jpg"];
            }
            else
            {
                PFFile *theImage = [[objects objectAtIndex:0] objectForKey:@"photo"];
                NSData *imageData = [theImage getData];
                UIImage *image = [UIImage imageWithData:imageData];
                
                iconImage = image;
            }
            
        } else {
            // Log details of the failure
        }
    }];
}




- (void) loadAllPropInfo
{
    //self.adder = [[User alloc] initWithUser:[_propertyObject objectForKey:@"userAdded"]];
	[[_propertyObject objectForKey:@"userAdded"] fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
		if (!error) {
			self.adder = [[User alloc] initWithUser:[_propertyObject objectForKey:@"userAdded"]];
		}
	}];
	//self.agent = [[Agent alloc] initWithUser:[_propertyObject objectForKey:@"agent"]];
	[[_propertyObject objectForKey:@"agent"] fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
		if (!error) {
			self.agent = [[Agent alloc] initWithUser:[_propertyObject objectForKey:@"agent"]];
		}
	}];
    
    
    self.propID =_propertyObject.objectId;
    
	self.propertyName =[NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"propertyName"]];
	self.propertyType = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"propertyType"]];
	self.listingType = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"listingType"]];
    
    self.blkNo = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"blkNo"]];
	self.address = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"address"]];
	self.postCode = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"postcode"]];
	self.levelNumber = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"levelNo"]];
	self.unitNo = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"unitNo"]];
	self.district = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"district"]];
    
	self.price = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"price"]];
	self.area = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"area"]];
	self.tenure = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"tenure"]];
    self.bedroom = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"bedroom"]];

    self.status = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"status"]];
    self.rentedPrice = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"rentedPrice"]];
    
    self.projectName = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"project"]];
    self.topYear = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"topYear"]];
    self.PSF = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"psf"]];
    self.rentalYeild = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"rentalYield"]];
    
    self.isSubmited = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"submit"]];
    
    
    self.commercialType = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"commercialType"]];
    
    
    //Office extra field
    self.groundLevel = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"groundLevel"]];
    self.gst = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"gst"]];
    self.electricalLoad = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"electricalLoad"]];
    self.amps = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"amps"]];
    self.aircon = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"aircon"]];
    self.ownPantry = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"ownPantry"]];
    self.grade = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"grade"]];
    self.floorLoad = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"floorLoad"]];
    self.serverRoom = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"serverRoom"]];
    self.condition = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"condition"]];
    
    //retail
    self.retailType = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"retailType"]];
    
    //F&B
    self.fAndBType = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"fAndBType"]];
    self.greaseTrap = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"greaseTrap"]];
    self.exhaust = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"exhaust"]];
    
    //Land
    //self.builtUp = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"builtUp"]];
    self.noOfStorey = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"noOfStorey"]];
    self.landType = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"landType"]];
    self.furnishing = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"furnishing"]];
    self.mainGateFacing = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"mainGateFacing"]];
    self.carPark = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"carPark"]];
    self.garden = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"garden"]];
     self.swimmingPool = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"swimmingPool"]];
     self.basement = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"basement"]];
     self.roofTerrance = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"roofTerrance"]];
    
    //HDB extra field
    self.furnishing = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"furnishing"]];
    self.hdbType = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"hdbType"]];
    self.liftLevel = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"liftLevel"]];
    self.mainGateFacing = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"mainGateFacing"]];
    self.mainDoorDirection = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"mainDoorDirection"]];
    NSLog(@"mainDoorDirection = %@",self.mainDoorDirection);
    self.town = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"town"]];
    self.valuation = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"valuation"]];
    self.condition = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"condition"]];
    self.roomPosition = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"roomPosition"]];
    
    //industrial extra field
    self.industryType = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"industryType"]];
    self.noOfCargoLift = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"noOfCargoLift"]];
    self.flatted = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"flatted"]];
    self.ceilingHeight = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"ceilingHeight"]];
    self.vehicleAccess = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"vehicleAccess"]];
    self.landSize = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"landSize"]];
    self.noOfStorey = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"noOfStorey"]];
    self.type = [NSString stringWithFormat:@"%@",[_propertyObject objectForKey:@"type"]];
    
    PFFile *theImage = [_propertyObject objectForKey:@"picture"];
    if (theImage != Nil) {
        NSData *imageData = [theImage getData];
        UIImage *image = [UIImage imageWithData:imageData];
        self.iconImage = image;
    }
    else
    {
        iconImage = [UIImage imageNamed:@"condo2.jpg"];
    }
    
    
    
    
    
    
    
    
  //  [self removeNullData];
    
    [self printInfo];
    
}

-(void) removeNullData
{

    
    [self checkNull:self.propertyName];
    [self checkNull:self.propertyType];
    [self checkNull:self.listingType];
    
    [self checkNull:self.blkNo];
    [self checkNull:self.unitNo];
    [self checkNull:self.levelNumber];
    [self checkNull:self.address];
    [self checkNull:self.district];
    [self checkNull:self.postCode];
    
    
    [self checkNull:self.area];
    [self checkNull:self.tenure];
    [self checkNull:self.price];
    [self checkNull:self.bedroom];
    
    [self checkNull:self.status];
    [self checkNull:self.rentedPrice];
    
    //condo extra field
    [self checkNull:self.projectName];
    [self checkNull:self.PSF];
    [self checkNull:self.rentalYeild];
    [self checkNull:self.topYear];
    

}
-(void) checkNull:(NSString *)obj
{
    if (!obj) {
        obj = @"unknown";
    }
}
-(void) printInfo
{
    NSLog(@" - propertyName: %@",self.propertyName);
    NSLog(@" - propertyID: %@",self.propID);
    NSLog(@" - propertyType: %@",self.propertyType);
    NSLog(@" - listingType: %@",self.listingType);
    
    NSLog(@" - blkNo: %@",self.blkNo);
    NSLog(@" - unitNo: %@",self.unitNo);
    NSLog(@" - levelNumber: %@",self.levelNumber);
    NSLog(@" - address: %@",self.address);
    NSLog(@" - district: %@",self.district);
    NSLog(@" - postCode: %@",self.postCode);
    
    
    NSLog(@" - area: %@",self.area);
    NSLog(@" - tenure: %@",self.tenure);
    NSLog(@" - price: %@",self.price);
    NSLog(@" - bedroom: %@",self.bedroom);
    
    NSLog(@" - status: %@",self.status);
    NSLog(@" - rentedPrice: %@",self.rentedPrice);
    
    //condo extra field
    NSLog(@" - projectName: %@",self.projectName);
    NSLog(@" - PSF: %@",self.PSF);
    NSLog(@" - rentalYeild: %@",self.rentalYeild);
    NSLog(@" - topYear: %@",self.topYear);

    NSLog(@" - submit: %@",self.isSubmited);
    

}
/*

- (id) initWithInformation: (NSString *) propertyName : (NSString *) propertyType : (NSString *) roomType : (NSString*) unitNo : (NSString *) address : (NSString *) project : (NSString *) tenure : (NSString *) price : (NSString *) bedroom : (NSString *) floorSize : (NSString *) PSF : (NSString *) age : (NSString *) sortBy : (PFUser *) adder;
{
    if (self == [super init]) {
		_imageList = [[NSMutableArray alloc] init];
        _propertyName = propertyName;
        _propertyType = propertyType;
        _roomType = roomType;
        _unitNo = unitNo;
        _address = address;
        _tenure = tenure;
        _price = price;
        _bedroom = bedroom;
        _floorSize = floorSize;
        _PSF = PSF;
        _age = age;
        _sortBy = sortBy;
        _adder = adder;
        
        //[self uploadInfomationToProperty];
    }
    return self;
}*/

- (NSString *)ToString
{
    
    return @"Property ToString";
}

- (void)loadAllData
{
   // [self reLoadInfomation];
    
}

- (void) DeleteObject
{
    [_propertyObject deleteInBackground];
    NSLog(@"file is deleted");
}

- (void) AddImage : (UIImage*) image
{
	[_imageList addObject:image];
}

- (void) UploadProperty : (ParentLoadingVC*) parentV
{
	self.loadingView = parentV;
	[[DatabaseEngine sharedInstance] AddProperty:self];
}

- (void) AddPropertySuccessfully : (PFObject*) propertyOb
{
    NSLog(@"AddPropertySuccessfully run");
	self.propertyObject = propertyOb;
	[[DataEngine sharedInstance] SetCurPropertyInstace:self];
    
	[self UploadImage];
    
}

- (void) AddPropertyFailed : (NSString*) errorString
{
	
}

- (void) UploadImage
{
     
	imageCount = _imageList.count;
    NSLog(@"UploadImage run with imagelist.count = %i",_imageList.count);
	if (imageCount == 0) {
		[self SwitchView];	
	}
	NSLog(@"There are %i", _imageList.count);
	for (UIImage* image in _imageList) {
		[[DatabaseEngine sharedInstance] uploadImage:image :self];
	}
}

- (void) ImageUploadSuccessfully
{
	imageCount--;
    NSLog(@"image upload successfully - %i", imageCount);
	if (imageCount == 0) {
		[self SwitchView];
	}
}
- (void) ImageUploadFailed : (NSString*) errorString
{
	NSLog(@"upload Image failed - %@",errorString);
    [loadingView fail:errorString];
}

- (void) SwitchView {
    NSLog(@"switchview runs");
	//LoadView
    [loadingView sucess:@""];


}
-(void) loadIconForIV: (UIImageView *) imageView
{
    PFQuery *query = [PFQuery queryWithClassName:@"Photo"];
    [query whereKey:@"property" equalTo:_propertyObject];
    query.limit = 1;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if ([objects count] == 0) {
                iconImage = [UIImage imageNamed:@"condo2.jpg"];
            }
            else
            {
                PFFile *theImage = [[objects objectAtIndex:0] objectForKey:@"photo"];
                NSData *imageData = [theImage getData];
                UIImage *image = [UIImage imageWithData:imageData];
                iconImage = image;
                imageView.image = iconImage;
            }
        } else {
            // Log details of the failure
        }
    }];
}

@end
