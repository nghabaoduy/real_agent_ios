//
//  GroupInviteCell.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 1/26/14.
//  Copyright (c) 2014 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "NewGroupListVC.h"

@interface GroupInviteCell : UITableViewCell
{
    IBOutlet UILabel * label;
    IBOutlet UIButton * accept;
    IBOutlet UIButton * decline;
    PFObject * object;
}


@property IBOutlet UILabel * label;
@property IBOutlet UIButton * accept;
@property IBOutlet UIButton * decline;
@property PFObject * object;
@property NSIndexPath * index;
@property NewGroupListVC * groupListView;

@end
