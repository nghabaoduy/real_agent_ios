//
//  NewGroupListVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyUITableViewController.h"
#import "User.h"
#import <Parse/Parse.h>

@interface NewGroupListVC : MyUITableViewController
{
}


@property (nonatomic, strong) NSMutableArray * groupList;
@property (nonatomic, strong) NSMutableArray * inviteGroupList;
@property (nonatomic, strong) NSMutableArray * participantList;


- (void) declineInvite: (NSIndexPath*) myPath : (PFObject *) curGroup;
- (void) acceptInvite: (NSIndexPath*) myPath : (PFObject *) curGroup;

@end
