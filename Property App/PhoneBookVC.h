//
//  PhoneBookVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUIViewController.h"
#import "AddGroupVC.h"
#import "GroupInfoVC.h"
#import "DetailGroupMemberVC.h"

@interface PhoneBookVC : MyUIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
{
    IBOutlet UITableView * tv;
    NSMutableArray * memberList;
    IBOutlet UISearchBar *itemSearchBar;
}



@property (nonatomic, retain) IBOutlet UISearchBar  *itemSearchBar;
@property (nonatomic, retain) NSMutableArray * agentList;
@property (nonatomic, retain) NSMutableArray * unneededList;
@property (nonatomic, retain) NSMutableArray *displayArray;
@property (nonatomic, retain) NSMutableArray *phonebookPhoneList;
@property (nonatomic, retain) NSMutableArray *selectedRowsArray;
@property (nonatomic, retain) PFObject * curGroup;
@property (nonatomic, retain) DetailGroupMemberVC * groupMemberVC;

- (void) addFinished;


@end
