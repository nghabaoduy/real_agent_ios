//
//  LocationPropertyVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Property.h"

@interface LocationPropertyVC : UIViewController

@property (nonatomic, retain) Property* property;
@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@end
