//
//  PartnerCell.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/30/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Partner.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "PartnersVC.h"

@interface PartnerCell : UITableViewCell

@property (nonatomic, retain) PartnersVC * partnerView;

@property (nonatomic, retain) Partner * curPartner;

@property (nonatomic, retain) IBOutlet UILabel * nameLB;
@property (nonatomic, retain) IBOutlet UILabel * AddressLB;
@property (nonatomic, retain) IBOutlet UILabel * bankLB;

@property (nonatomic, retain) IBOutlet UIButton * callBtn;
@property (nonatomic, retain) IBOutlet UIButton * mailBtn;
@property (strong, nonatomic) IBOutlet UITextView *AdrTextView;
@property (strong, nonatomic) IBOutlet UIImageView *bankLogo;
-(void) switchBankLogo :(NSString *)bankName;
@end
