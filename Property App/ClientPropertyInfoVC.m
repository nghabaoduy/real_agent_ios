//
//  ClientPropertyInfoVC.m
//  Property App
//
//  Created by Brian on 2/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ClientPropertyInfoVC.h"
#import "AddPropertyViewController.h"
@interface ClientPropertyInfoVC ()

@end

@implementation ClientPropertyInfoVC
@synthesize property= _property;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self initInfo];
    self.isEditing = NO;
    editBtn.title = @"Edit";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) initInfo
{
    address.placeholder = @"Address: ";
    postalCode.placeholder = @"Postal Code: ";
    blockNo.placeholder = @"Block: ";
    levelNo.placeholder = @"Level: ";
    unitNo.placeholder = @"Unit: ";
    district.placeholder = @"District: ";
    
    
    [address setText:_property.address];
    [postalCode setText:_property.postCode];
    [blockNo setText:_property.blkNo];
    [levelNo setText:_property.levelNumber];
    [unitNo setText:_property.unitNo];
    [district setText:_property.district];
    
    
    [address setEnabled:NO];
    [postalCode setEnabled:NO];
    [blockNo setEnabled:NO];
    [levelNo setEnabled:NO];
    [district setEnabled:NO];
    [unitNo setEnabled:NO];
    
}
-(IBAction)marketProperty:(id)sender
{
    UIStoryboard * mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    AddPropertyViewController * view = [mainStoryboard instantiateViewControllerWithIdentifier:@"addPropertyView"];
    view.property = _property;
    [self presentViewController:view animated:YES completion:nil];
}
- (IBAction)editPropertyInfo:(id)sender {
    if (self.isEditing) {
        self.isEditing = NO;
        editBtn.title = @"Saving";
        [levelNo setEnabled:NO];
        [unitNo setEnabled:NO];
        [self updateLevelAndUnitInfo];
        
        
    }
    else
    {
        if (![editBtn.title isEqualToString:@"Saving"]) {
            self.isEditing = YES;
            editBtn.title = @"Done";
            [levelNo setEnabled:YES];
            [unitNo setEnabled:YES];
        }
        
    }
}
-(void) updateLevelAndUnitInfo
{
    PFObject * editProperty = _property.propertyObject;
    editProperty[@"unitNo"] = unitNo.text;
    editProperty[@"levelNo"] = levelNo.text;
    [editProperty saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            editBtn.title = @"Edit";
            [_property loadAllPropInfo];
        }
        else
        {
            editBtn.title = @"Edit";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Saving fail"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    }];
}

@end
