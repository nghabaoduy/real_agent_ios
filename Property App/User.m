//
//  User.m
//  Property App
//
//  Created by Jonathan Le on 12/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "User.h"
#import "DatabaseEngine.h"
@implementation User
@synthesize username;
@synthesize agentPhone, email, phone, userType, userObject, propertyList;
@synthesize fullName, firstName, lastName, homeEmail;

- (id) initWithUser : (PFUser*) userOb
{
	if (self == [super init]) {
		self.userObject = userOb;
        //self.username = [userObject username];
		[self LoadUserInfo];
		propertyList = [[NSMutableArray alloc] init];
		//[self GetPropertyList];
    }
    return self;
}

-(NSString*) description
{
    return [NSString stringWithFormat:@"%@ - %@",self.fullName, self.phone];
}

-(void) loadUserObjectByPhone
{
    PFQuery *query = [PFUser query];
	//Search criteria. Can use equalTo, lessThan, nearGeopoint....
	[query whereKey:@"phone" equalTo:self.phone];
    [query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error) {
        if ([users count] > 0) {
            self.userObject = [users objectAtIndex:0];
            [self LoadUserInfo];
        }
    }];
}

- (void) LoadUserInfo
{
    NSLog(@"Client added: %@", userObject.username);
    [userObject fetchIfNeeded];
	self.username = [userObject username];
	self.agentPhone = [userObject objectForKey:@"agentPhone"];
	self.email = [userObject objectForKey:@"email"];
	self.phone = [userObject objectForKey:@"phone"];
	self.userType = [userObject objectForKey:@"userType"];
    self.firstName = [userObject objectForKey:@"firstName"];
	self.lastName = [userObject objectForKey:@"lastName"];
    self.address = [userObject objectForKey:@"address"];
    self.isInstall = [userObject objectForKey:@"isInstall"];
    if ([firstName isEqualToString:@""]) {
        self.fullName = self.lastName;
    }
    else
    {
        if ([lastName isEqualToString:@""]) {
            self.fullName = self.firstName;
        }
        else
        {
            self.fullName = [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
        }
    }  
	
}

- (PFUser*) getUserObject
{
	return userObject;
}

- (void) GetPropertyList
{
	[[DatabaseEngine sharedInstance] GetUserPropertyList:self];
}

- (void) GetPropertySuccessful : (NSMutableArray*) propList
{
	self.propertyList = propList;
}
- (void) GetPropertyFailed: (NSString*) errorString
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:errorString
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}

-(void) InitWithUserInfo:(User *)user
{
    self.firstName = user.firstName == nil? @"":user.firstName;
    self.lastName =user.firstName == nil? @"":user.lastName;
    self.fullName = user.fullName == nil? @"":user.fullName;
    self.homeEmail = user.homeEmail == nil? @"":user.homeEmail;
    self.phone = user.phone == nil? @"":user.phone;
    self.address = user.address == nil? @"":user.address;
}
@end