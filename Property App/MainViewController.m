//
//  MainViewController.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#import "MainViewController.h"
#import "SearchPropertyViewController.h"
#import "AddPropertyViewController.h"
#import "MyClientsNavVC.h"
#import "SalesListViewController.h"

#import "ProPortfolioViewController.h"

#import "MyUIEngine.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "NotificationEngine.h"

#import "SWRevealViewController.h"

#import "MyGroupNavVC.h"
#import "MyGroupVC.h"
#import "MessageNavVC.h"
#import "MyFavoriteNavVC.h"
#import "MyAlertNavVC.h"
#import "CalculatorNavVC.h"
#import "PartnersNavVC.h"
#import "ScheduleNavVC.h"
#import "ScheduleTabBarVC.h"
#import "ScheduleListVC.h"
#import "SpecialDealNavVC.h"
#import "NewMyGroupNavVC.h"


@interface MainViewController ()

@end

@implementation MainViewController

@synthesize btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12;

@synthesize sidebarButton = _sidebarButton;

- (IBAction)SignOut:(id)sender
{
	[[DatabaseEngine sharedInstance] UserLogout];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)SearchProperty:(id)sender
{
    //search
    SearchPropertyViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"searchPropertyView"];
    [self presentViewController:view animated:YES completion:nil];
}

- (IBAction)AddProperty:(id)sender
{
    // AddPropertyDetailsViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"addPropertyView"];
    // [self presentViewController:view animated:YES completion:nil];
    
}

- (void)Message:(id)sender
{
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MessageStoryboard" bundle:nil];
    
    MessageNavVC * view = [newStoryboard instantiateViewControllerWithIdentifier:@"messageNav"];
    
    [self presentViewController:view animated:YES completion:nil];
}

- (void)MyClient:(id)sender
{
    // Message Button
    //MyClientViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"myClientView"];
    //[self presentViewController:view animated:YES completion:nil];
    
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MyClientsStoryboard" bundle:nil];
    
    MyClientsNavVC * view =[newStoryboard instantiateViewControllerWithIdentifier:@"myClientsNav"];
    
    [self presentViewController:view animated:YES completion:nil];
}





- (IBAction)Alert:(id)sender
{
    //Alert button
}

- (IBAction)Favorite:(id)sender
{
    
}

- (IBAction)Partners:(id)sender
{
    //Partners button
}

- (void)MyGroup:(id)sender
{
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"NewMyGroupStoryboard" bundle:nil];
    
    NewMyGroupNavVC * view = [newStoryboard instantiateViewControllerWithIdentifier:@"myGroupNav"];
    
    
    [self presentViewController:view animated:YES completion:nil];
}


- (void)MyAlert:(id)sender
{
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MyAlertStoryboard" bundle:nil];
    
    MyAlertNavVC * view = [newStoryboard instantiateViewControllerWithIdentifier:@"myAlert"];
    
    
    [self presentViewController:view animated:YES completion:nil];
}



- (IBAction)AboutMe:(id)sender
{
    
}
- (void)MyList:(id)sender
{
    ProPortfolioViewController *pushview = [self.storyboard instantiateViewControllerWithIdentifier:@"proPortfolioView"];
	[self presentViewController:pushview animated:YES completion:nil];
	//[[DatabaseEngine sharedInstance] GetMyListing:self];
    //[self getAllPropertyByUser];
}

- (void)myPartner: (id)sender
{
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"PartnerStoryboard" bundle:nil];
    
    PartnersNavVC * view = [newStoryboard instantiateViewControllerWithIdentifier:@"myPartner"];
    
    [self presentViewController:view animated:YES completion:nil];
}

- (void)MyFavorite : (id) sender
{
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MyShortlistedStoryboard" bundle:nil];
    
    MyFavoriteNavVC * view = [newStoryboard instantiateViewControllerWithIdentifier:@"myFavorite"];
    
    [self presentViewController:view animated:YES completion:nil];
}

- (void)Schedule : (id) sender
{
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"ScheduleStoryboard" bundle:nil];
    
    ScheduleNavVC * view = [newStoryboard instantiateViewControllerWithIdentifier:@"schedule"];
    
    [self presentViewController:view animated:YES completion:nil];
    
}

- (void)calculator : (id) sender
{
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"CalculatorStoryboard" bundle:nil];
    
    CalculatorNavVC * view = [newStoryboard instantiateViewControllerWithIdentifier:@"myCalculator"];
    
    [self presentViewController:view animated:YES completion:nil];
}

- (void)specialDeal : (id) sender
{
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"SpecialDealStoryboard" bundle:nil];
    
    SpecialDealNavVC * view = [newStoryboard instantiateViewControllerWithIdentifier:@"specialDeal"];
    
    [self presentViewController:view animated:YES completion:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    // Change button color
    // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"purpleBG"]];
    NSString *version = [[UIDevice currentDevice] systemVersion];
    
    BOOL isAtLeast6 = [version floatValue] >= 6.0;
    BOOL isAtLeast7 = [version floatValue] >= 7.0;
    //
    if (isAtLeast7) {
        //self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    //position menu
    
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self initMenuButtons];
    [NotificationEngine sharedInstance].mainView = self;
    [[NotificationEngine sharedInstance] analyzePassingInfo];
    
    
	// Do any additional setup after loading the view.
}
-(void) viewDidAppear:(BOOL)animated
{
    if (animated) {
    }
}

-(void) initMenuButtons
{
    // NSLog(@"height = %f",self.view.frame.size.height);
    //NSLog(@"width = %f",self.view.frame.size.width);
    //NSLog(@"ratio = %f",self.view.frame.size.height/self.view.frame.size.width);
    int gap = 95;//115
    int line1Y = 20;
    
    
    if( IS_IPHONE_5 )
    {
        gap = 115;//115
        line1Y = 35;
    }
    
    NSString *version = [[UIDevice currentDevice] systemVersion];
    
    BOOL isAtLeast6 = [version floatValue] >= 6.0;
    BOOL isAtLeast7 = [version floatValue] >= 7.0;
    //
    if (isAtLeast7) {
        line1Y= line1Y + 64;
    }
    
    
    int buttonW = 75;
    int buttonH = 75;
    int col1X = 25;
    int col2X = col1X+95;
    int col3X = col2X+95;
    
    int line2Y = line1Y+gap;
    int line3Y = line2Y+gap;
    int line4Y = line3Y+gap;
    //line 1
    btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(col1X, line1Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn1 setBackgroundImage:[UIImage imageNamed:@"my agents"] forState:UIControlStateNormal];
    [btn1 setBackgroundImage:[UIImage imageNamed:@"my-agents_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn1 addTarget:self action:@selector(MyClient:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:btn1];
    btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(col2X, line1Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn2 setBackgroundImage:[UIImage imageNamed:@"my groups"] forState:UIControlStateNormal];
    [btn2 setBackgroundImage:[UIImage imageNamed:@"my-groups_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn2 addTarget:self action:@selector(MyGroup:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:btn2];
    btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn3.frame = CGRectMake(col3X, line1Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn3 setBackgroundImage:[UIImage imageNamed:@"my-calendar"] forState:UIControlStateNormal];
    [btn3 setBackgroundImage:[UIImage imageNamed:@"my-calendar_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn3 addTarget:self action:@selector(Schedule:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:btn3];
    
    //line 2
    btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn4.frame = CGRectMake(col1X, line2Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn4 setBackgroundImage:[UIImage imageNamed:@"calculator"] forState:UIControlStateNormal];
    [btn4 setBackgroundImage:[UIImage imageNamed:@"calculator_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn4 addTarget:self action:@selector(calculator:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:btn4];
    btn5 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn5.frame = CGRectMake(col2X, line2Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn5 setBackgroundImage:[UIImage imageNamed:@"partners"] forState:UIControlStateNormal];
    [btn5 setBackgroundImage:[UIImage imageNamed:@"partners_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn5 addTarget:self action:@selector(myPartner:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:btn5];
    btn6 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn6.frame = CGRectMake(col3X, line2Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn6 setBackgroundImage:[UIImage imageNamed:@"my listing"] forState:UIControlStateNormal];
    [btn6 setBackgroundImage:[UIImage imageNamed:@"my-listing_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn6 addTarget:self action:@selector(MyList:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:btn6];
    
    //line3
    btn7 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn7.frame = CGRectMake(col1X, line3Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn7 setBackgroundImage:[UIImage imageNamed:@"property search"] forState:UIControlStateNormal];
    [btn7 setBackgroundImage:[UIImage imageNamed:@"property-search_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn7 addTarget:self action:@selector(SearchProperty:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:btn7];
    btn8 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn8.frame = CGRectMake(col2X, line3Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn8 setBackgroundImage:[UIImage imageNamed:@"special deals"] forState:UIControlStateNormal];
    [btn8 setBackgroundImage:[UIImage imageNamed:@"special-deals_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn8 addTarget:self action:@selector(specialDeal:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:btn8];
    btn9 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn9.frame = CGRectMake(col3X, line3Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn9 setBackgroundImage:[UIImage imageNamed:@"shortlisted"] forState:UIControlStateNormal];
    [btn9 setBackgroundImage:[UIImage imageNamed:@"shortlisted_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn9 addTarget:self action:@selector(MyFavorite:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:btn9];
    
    //line4
    //line4
    btn10 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn10.frame = CGRectMake(col1X, line4Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn10 setBackgroundImage:[UIImage imageNamed:@"alerts"] forState:UIControlStateNormal];
    [btn10 setBackgroundImage:[UIImage imageNamed:@"alerts_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn10 addTarget:self action:@selector(MyAlert:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:btn10];
    btn11 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn11.frame = CGRectMake(col2X, line4Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn11 setBackgroundImage:[UIImage imageNamed:@"directory"] forState:UIControlStateNormal];
    [btn11 setBackgroundImage:[UIImage imageNamed:@"directory_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn11 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    //[self.view addSubview:btn11];
    btn12 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn12.frame = CGRectMake(col2X, line4Y, buttonW, buttonH); // position in the parent view and set the size of the button
    [btn12 setBackgroundImage:[UIImage imageNamed:@"messages"] forState:UIControlStateNormal];
    [btn12 setBackgroundImage:[UIImage imageNamed:@"messages_hover"] forState:UIControlStateHighlighted];
    // add targets and actions
    [btn12 addTarget:self action:@selector(Message:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:btn12];
    
}
-(void)buttonClicked:(id) sender
{
    NSLog(@"button clicked");
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) GetMyListSuccessful : (NSArray*) objects
{
	NSMutableArray * propertyAray = [[NSMutableArray alloc] init];
	NSMutableArray * titles = [[NSMutableArray alloc] init];
	NSLog(@"GetMyListSuccessful %i", [objects count]);
	for (PFObject *object in objects) {
		[propertyAray addObject:object];
		[titles addObject:[object objectForKey:@"propertyName"]];
	}
	
	[[DataEngine sharedInstance] SetPropertyDataArray:propertyAray];
	[[DataEngine sharedInstance] SetPropertyNameList:titles];
	
	ProPortfolioViewController *pushview = [self.storyboard instantiateViewControllerWithIdentifier:@"proPortfolioView"];
	[self presentViewController:pushview animated:YES completion:nil];
	
}

- (void) GetMyListFailed : (NSString*) errorString
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:errorString
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
	//NSLog(@"%@", errorString);
}
- (IBAction)testpush:(id)sender {
    
    
    [[NotificationEngine sharedInstance] simulatePush];
}

@end
