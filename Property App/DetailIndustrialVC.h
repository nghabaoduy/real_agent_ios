//
//  DetailHDBVC.h
//  Property App
//
//  Created by Brian on 7/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface DetailIndustrialVC : UITableViewController <MFMailComposeViewControllerDelegate>
{

    
    IBOutlet UITableView *tv;
    
     NSString * type;
    //display
    IBOutlet UIImageView * icon;
    IBOutlet UILabel * propertyName;
    IBOutlet UILabel * propertyType;
    IBOutlet UILabel * adress;
    IBOutlet UITextView *addressTV;
    IBOutlet UILabel * postalCode;
    
    //Detail
    IBOutlet UITextField * price;
    IBOutlet UITextField * district;
    IBOutlet UITextField * tenure;
    IBOutlet UITextField * floorSize;
    
    //Status
    IBOutlet UITextField * status;
    IBOutlet UITextField * rentedPrice;
    
    //Project Information
    IBOutlet UITextField * projectName;
    IBOutlet UITextField * psf;
    IBOutlet UITextField * topYear;
    IBOutlet UITextField * rentalYield;

    
    IBOutlet UITextField * industrialType;
    //industrical extra
    IBOutlet UITextField * gst;
    IBOutlet UITextField * condition;
    IBOutlet UITextField * electricalLoad;
    IBOutlet UITextField * amps;
    IBOutlet UITextField * floorLoad;
    IBOutlet UITextField * noOfCargoLift;
    IBOutlet UITextField * flattedRampUp;
    IBOutlet UITextField * ceilingHeight;
    
    IBOutlet UITextField * level;
    IBOutlet UITextField * vehicalAccess;
    
    IBOutlet UITextField * landSize;
    IBOutlet UITextField * noOfStorey;
    IBOutlet UITextField * landType;
    
    //Contact Agen
    IBOutlet UIImageView * agentIcon;
    IBOutlet UILabel * agentName;
    IBOutlet UILabel * agentMobile;
    IBOutlet UILabel * agentEmail;
    IBOutlet UITableViewCell * schedule;
    User * curUser;
    
    IBOutlet UITableViewCell * addToShare;
}

@property (nonatomic, retain) Property* property;
@property (nonatomic) BOOL isMyProperty;


@property (nonatomic) BOOL isSharing;
@property (nonatomic) BOOL allowUnitNo;
@property (nonatomic, retain) PFObject * curGroup;
@property (nonatomic, retain) ProPortfolioViewController * proportView;

- (void) hideSchedule;
- (void) showDialog : (NSString *) message;

@end
