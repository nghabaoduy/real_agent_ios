//
//  EditPartnerVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 1/19/14.
//  Copyright (c) 2014 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Partner.h"

@interface EditPartnerVC : UITableViewController<UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    IBOutlet UITableViewCell * bankCell;
    IBOutlet UITextField * name;
    IBOutlet UITextField * email;
    IBOutlet UITextField * phone;
    IBOutlet UITextField * bankTF;
    
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
    
    NSArray * bankList;
    
    User * curUser;
}


@property (nonatomic, retain) Partner * curPartner;

- (IBAction)saveData:(id)sender;

@end
