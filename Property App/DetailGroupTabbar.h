//
//  DetailGroupTabbar.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface DetailGroupTabbar : UITabBarController

@property (nonatomic, strong) PFObject * curGroup;
-(void) initTabbarWithGroupObj:(PFObject*) inputGroupObj :(UIViewController *)curView :(UINavigationController *)destination;
@end
