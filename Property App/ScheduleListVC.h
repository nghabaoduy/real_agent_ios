//
//  ScheduleListVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/13/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUITableViewController.h"
#import "User.h"

@interface ScheduleListVC : MyUITableViewController
{
    
    User * curUser;
}

@property (nonatomic, retain) NSMutableArray * myRequestList;
@property (nonatomic, retain) NSMutableArray * otherRequestList;

@end
