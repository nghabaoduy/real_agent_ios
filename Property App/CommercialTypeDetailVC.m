//
//  CommercialTypeDetailVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/3/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "CommercialTypeDetailVC.h"
#import "ImagePickVC.h"

@interface CommercialTypeDetailVC ()

@end

@implementation CommercialTypeDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    levelList = [[NSArray alloc]initWithObjects:
                 @"[Select]",
                 @"Ground",
                 @"Low",
                 @"Mid",
                 @"High", nil];
    gstList = [[NSArray alloc]initWithObjects:
               @"[Select]",
               @"Yes",
               @"No", nil];
    
    conditionList = [[NSArray alloc]initWithObjects:
                     @"Bare",
                     @"Partial",
                     @"Full Fitted", nil];
    
    airconList = [[NSArray alloc]initWithObjects:
                  @"Central",
                  @"Individual", nil];
    
    ownPantryList = [[NSArray alloc]initWithObjects:
                     @"[Select]",
                     @"Yes",
                     @"No", nil];
    
    gradeList = [[NSArray alloc]initWithObjects:
                 @"[select]",
                 @"A",
                 @"B",
                 @"Others", nil];
    
    serverRoomList = [[NSArray alloc]initWithObjects:
                      @"[select]",
                      @"Yes",
                      @"No", nil];
    
    fAndBTypeList = [[NSArray alloc]initWithObjects:
                     @"[select]",
                     @"HDB ShopHse",
                     @"Mall",
                     @"Private Shophse",
                     @"Others", nil];
    
    greaseTrapList = [[NSArray alloc]initWithObjects:
                      @"[select]",
                      @"Yes",
                      @"No", nil];
    
    exhaustList = [[NSArray alloc]initWithObjects:
                   @"[select]",
                   @"Yes",
                   @"No", nil];
    
    retailTypeList = [[NSArray alloc]initWithObjects:
                      @"[select]",
                      @"HDB ShopHse",
                      @"Mall",
                      @"Private ShopHse",
                      @"Others", nil];
    
    
    electricalLoadList = [[NSArray alloc] initWithObjects:
                          @"[select]",
                          @"Single",
                          @"3 phase", nil];
    
    [self initTextfieldPlaceHolder];
}

- (void) initTextfieldPlaceHolder
{
    
    levelTF.placeholder = @"Level: ";
    gstTF.placeholder = @"GST: ";
    retailTypeTF.placeholder = @"Retail Type: ";
    conditionTF.placeholder = @"Condition: ";
    electricalLoadTF.placeholder = @"Electrical Load: ";
    ampsTF.placeholder = @"Amps: ";
    electricalLoadTF.text = @"Single";
    airConTF.placeholder = @"Aircon: ";
    ownPantryTF.placeholder = @"Own Pantry: ";
    gradeTF.placeholder = @"Grade: ";
    floorLoadTF.placeholder = @"Floor Load: ";
    serverRoomTF.placeholder = @"Server Room: ";
    fAndBTypeTF.placeholder = @"F&A Type: ";
    greaseTrapTF.placeholder = @"Grease Trap: ";
    exhaustTF.placeholder = @"Exhaust: ";
    builtUpTF.placeholder = @"Built Up: ";
    noOfStoreyTF.placeholder = @"No of Storeys: ";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
}

-(void)categoryDoneButtonPressed{
    //Add for wheel
    //categoryLable.text = selectedCategory;
    if (activeTextField == levelTF) {
        [levelTF setText:selectedCategory];
    } else if (activeTextField == gstTF)
    {
        [gstTF setText:selectedCategory];
    } else if (activeTextField == conditionTF)
    {
        [conditionTF setText:selectedCategory];
    } else if (activeTextField == airConTF)
    {
        [airConTF setText:selectedCategory];
    } else if (activeTextField == ownPantryTF)
    {
        [ownPantryTF setText:selectedCategory];
    }
    else if (activeTextField == gradeTF)
    {
        [gradeTF setText:selectedCategory];
    }
    else if (activeTextField == serverRoomTF)
    {
        [serverRoomTF setText:selectedCategory];
    }
    else if (activeTextField == fAndBTypeTF)
    {
        [fAndBTypeTF setText:selectedCategory];
    }
    else if (activeTextField == greaseTrapTF)
    {
        [greaseTrapTF setText:selectedCategory];
    }
    else if (activeTextField == exhaustTF)
    {
        [exhaustTF setText:selectedCategory];
    }
    else if (activeTextField == retailTypeTF)
    {
        [retailTypeTF setText:selectedCategory];
    }
    else if (activeTextField == electricalLoadTF)
    {
        [electricalLoadTF setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
    
}


-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    //Add Wheel
    // Handle the selection
    if (activeTextField == levelTF) {
        selectedCategory = [NSString stringWithFormat:@"%@",[levelList objectAtIndex:row]];
    } else if (activeTextField == gstTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[gstList objectAtIndex:row]];
    } else if (activeTextField == conditionTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[conditionList objectAtIndex:row]];
    } else if (activeTextField == airConTF)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[airconList objectAtIndex:row]];
    } else if (activeTextField == ownPantryTF)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[ownPantryList objectAtIndex:row]];
    } else if (activeTextField == gradeTF)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[gradeList objectAtIndex:row]];
    } else if (activeTextField == serverRoomTF)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[serverRoomList objectAtIndex:row]];
    } else if (activeTextField == fAndBTypeTF)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[fAndBTypeList objectAtIndex:row]];
    } else if (activeTextField == greaseTrapTF)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[greaseTrapList objectAtIndex:row]];
    } else if (activeTextField == exhaustTF)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[exhaustList objectAtIndex:row]];
    } else if (activeTextField == retailTypeTF)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[retailTypeList objectAtIndex:row]];
    }
    else if (activeTextField == electricalLoadTF)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[electricalLoadList objectAtIndex:row]];
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    //Add Wheel
    
    if (activeTextField == levelTF) {
        return [levelList count];
    } else if (activeTextField == gstTF)
    {
        return [gstList count];
    } else if (activeTextField == conditionTF)
    {
        return [conditionList count];
    } else if (activeTextField == airConTF)
    {
        return [airconList count];
    } else if (activeTextField == ownPantryTF)
    {
        return [ownPantryList count];
    } else if (activeTextField == gradeTF)
    {
        return [gradeList count];
    } else if (activeTextField == serverRoomTF)
    {
        return [serverRoomList count];
    } else if (activeTextField == fAndBTypeTF)
    {
        return [fAndBTypeList count];
    } else if (activeTextField == greaseTrapTF)
    {
        return [greaseTrapList count];
    } else if (activeTextField == exhaustTF)
    {
        return [exhaustList count];
    }
    else if (activeTextField == retailTypeTF)
    {
        return [retailTypeList count];
    }
    else if (activeTextField == electricalLoadTF)
    {
        return [electricalLoadList count];
    }
    return 0;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == levelTF) {
        return [levelList objectAtIndex:row];
    } else if (activeTextField == gstTF)
    {
        return [gstList objectAtIndex:row];
    } else if (activeTextField == conditionTF)
    {
        return [conditionList objectAtIndex:row];
    } else if (activeTextField == airConTF)
    {
        return [airconList objectAtIndex:row];
    } else if (activeTextField == ownPantryTF)
    {
        return [ownPantryList objectAtIndex:row];
    } else if (activeTextField == gradeTF)
    {
        return [gradeList objectAtIndex:row];
    } else if (activeTextField == serverRoomTF)
    {
        return [serverRoomList objectAtIndex:row];
    } else if (activeTextField == fAndBTypeTF)
    {
        return [fAndBTypeList objectAtIndex:row];
    } else if (activeTextField == greaseTrapTF)
    {
        return [greaseTrapList objectAtIndex:row];
    } else if (activeTextField == exhaustTF)
    {
        return [exhaustList objectAtIndex:row];
    }
    else if (activeTextField == retailTypeTF)
    {
        return [retailTypeList objectAtIndex:row];
    }
    else if (activeTextField == electricalLoadTF)
    {
        return [electricalLoadList objectAtIndex:row];
    }
    return 0;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //Add Wheel
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:levelTF] || [textField isEqual:gstTF] || [textField isEqual:conditionTF] || [textField isEqual:airConTF] || [textField isEqual:ownPantryTF] || [textField isEqual:gradeTF] || [textField isEqual:serverRoomTF] || [textField isEqual:fAndBTypeTF] || [textField isEqual:greaseTrapTF] || [textField isEqual:exhaustTF] || [textField isEqual:retailTypeTF] || [textField isEqual:electricalLoadTF])
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"next"]) {
        _property.retailType = [NSString stringWithFormat:@"%@", retailTypeTF.text];
        _property.groundLevel = [NSString stringWithFormat:@"%@", levelTF.text];
        _property.gst = [NSString stringWithFormat:@"%@", gstTF.text];
        _property.condition = [NSString stringWithFormat:@"%@", conditionTF.text];
        _property.electricalLoad = [NSString stringWithFormat:@"%@", electricalLoadTF.text];
        _property.amps = [NSString stringWithFormat:@"%@", ampsTF.text];
        _property.aircon = [NSString stringWithFormat:@"%@", airConTF.text];
        _property.ownPantry = [NSString stringWithFormat:@"%@", ownPantryTF.text];
        _property.grade = [NSString stringWithFormat:@"%@", gradeTF.text];
        _property.floorLoad = [NSString stringWithFormat:@"%@", floorLoadTF.text];
        _property.serverRoom = [NSString stringWithFormat:@"%@", serverRoomTF.text];
        _property.fAndBType = [NSString stringWithFormat:@"%@", fAndBTypeTF.text];
        _property.greaseTrap = [NSString stringWithFormat:@"%@", greaseTrapTF.text];
        _property.exhaust = [NSString stringWithFormat:@"%@", exhaustTF.text];
        _property.area = [NSString stringWithFormat:@"%@", builtUpTF.text];
        _property.noOfStorey = [NSString stringWithFormat:@"%@", noOfStoreyTF.text];
        
        _property.retailType = [NSString stringWithFormat:@"%@", retailTypeTF.text];
        
        ImagePickVC * destination = segue.destinationViewController;
        if ([self.navigationItem.title isEqualToString:@"Step 1/3"]) {
            [destination.navigationItem setTitle:@"Step 2/3"];
        }
        destination.property = _property;
    }
}
@end
