//
//  PropertyReviewVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 22/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "PropertyReviewVC.h"
#import "PPLoadingVC.h"
#import "MyUIEngine.h"


@interface PropertyReviewVC ()

@end

@implementation PropertyReviewVC
@synthesize property= _property;


- (void) DisplayDetails
{
    [address setText:_property.address];
    [postalCode setText:_property.postCode];
    [blockNo setText:_property.blkNo];
    [levelNo setText:_property.levelNumber];
    [unitNo setText:_property.unitNo];
    [district setText:_property.district];
    [noOfBedroom setText:_property.bedroom];
    [area setText:_property.area];
    [price setText:_property.price];
    [tenure setText:_property.tenure];
    [adder setText:_property.adder.fullName];
    
    [status setText:_property.status];
    [rentedPrice setText:_property.rentedPrice];
    
    [projectName setText:_property.projectName];
    [PSF setText:_property.PSF];
    [topYear setText:_property.topYear];
    [rentalYield setText:_property.rentalYeild];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    //Vincent removed- self.navigationItem.backBarButtonItem.title = @"";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self initTextfieldPlaceHolder];
    [self DisplayDetails];
}

- (void) initTextfieldPlaceHolder
{
    
    
    
    address.placeholder = @"Address: ";
    postalCode.placeholder = @"Postal Code: ";
    blockNo.placeholder = @"Block: ";
    levelNo.placeholder = @"Level: ";
    unitNo.placeholder = @"Unit: ";
    district.placeholder = @"District: ";
    area.placeholder = @"Floor Size: ";
    price.placeholder = @"Price:* ";
    adder.placeholder = @"Client:* ";
    //adder.text = _property.adder.fullName;
    
    address.enabled = NO;
    postalCode.enabled = NO;
    blockNo.enabled = NO;
    district.enabled = NO;
    adder.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"loadingNext"]) {
        PPLoadingVC * destination = segue.destinationViewController;
        destination.property = _property;
    }
    
}

/*
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
