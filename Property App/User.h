//
//  User.h
//  Property App
//
//  Created by Jonathan Le on 12/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
@interface User : NSObject

@property (nonatomic, strong) PFUser *userObject;

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *agentPhone;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *userType;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *homeEmail;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSNumber *isInstall;
@property (nonatomic, strong) NSMutableArray *propertyList;

- (id) initWithUser : (PFUser*) userOb;
- (PFUser*) getUserObject;
- (void) GetPropertyList;
- (void) GetPropertySuccessful : (NSMutableArray*) propList;
- (void) GetPropertyFailed: (NSString*) errorString;
- (void) InitWithUserInfo:(User *)user;
- (void) loadUserObjectByPhone;
@end
