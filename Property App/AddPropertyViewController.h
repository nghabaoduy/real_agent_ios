  //
//  AddPropertyViewController.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "Property.h"
@interface AddPropertyViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSArray * salesData;
    NSArray * rentsData;
    NSArray * data;
    IBOutlet UISegmentedControl * Segment;
    
}
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) User *adder;
@property (nonatomic, retain) Property *property;
@end
