//
//  NewAddGroupVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewGroupListVC.h"

@interface NewAddGroupVC : MyUIViewController
{
    IBOutlet UITextField * groupNameTxF;
}


@property (nonatomic, strong) NewGroupListVC * groupListView;


- (IBAction)add:(id)sender;
- (IBAction)cancel:(id)sender;
- (void)validateTitle;
- (void)addToDatabase;
- (void)showDialog: (NSString*) message;

@end
