//
//  ImagePickCell.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 22/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIImageView * patternImageView;
@property (nonatomic, strong) UIImage * curImage;
@end
