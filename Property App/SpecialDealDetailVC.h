//
//  SpecialDealDetailVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/19/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialDealDetailVC : UITableViewController
{
    IBOutlet UILabel * districLB;
    IBOutlet UILabel * freeHoldLB;
    IBOutlet UILabel * unitsLB;
    IBOutlet UILabel * typeLB;
    IBOutlet UILabel * developementNameLB;
    IBOutlet UILabel * propertyTypeLB;
    IBOutlet UILabel * developerLB;
    IBOutlet UILabel * countryLB;
    IBOutlet UILabel * descriptionLB;
    IBOutlet UITextView *descriptionTV;
}

@property (nonatomic, retain) NSDictionary * curDic;
@property (nonatomic, retain) IBOutlet UIImageView * background;
@property (nonatomic, retain) UIImage * image;


@end
