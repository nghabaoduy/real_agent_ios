//
//  CommercialCalVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/26/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommercialCalVC : UIViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    double purchasePrice;
    double stampDuty;
    UITextField * activeTextField;
    
}


@property (nonatomic, retain) IBOutlet UITextField * purchasePriceTF;
@property (nonatomic, retain) IBOutlet UILabel * value;

@end
