//
//  ChangeUserInfoViewController.h
//  Property App
//
//  Created by Jonathan Le on 15/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "User.h"
#import "MyUIViewController.h"
#import "PersonInfoViewController.h"
@interface ChangeUserInfoViewController : MyUIViewController <UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIScrollView *theScrollView;
    
}
@property (nonatomic, assign) UITextField *activeTextField;

- (IBAction)dismissKeyboard:(id)sender;
@property (nonatomic, strong) IBOutlet UITextField * phone;
@property (nonatomic, strong) IBOutlet UITextField * email;
@property (nonatomic, strong) IBOutlet UITextField * firstname;
@property (nonatomic, strong) IBOutlet UITextField * lastname;
@property (nonatomic, retain) User* updatingUser;
@property (nonatomic, retain) PersonInfoViewController* personInfoView;

@end
