//
//  RequestMeetupClientVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/13/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "RequestMeetupClientVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"

@interface RequestMeetupClientVC ()

@end

@implementation RequestMeetupClientVC
@synthesize property, isEvent, event;


- (void) hideCell
{
    if (!event.isReschedule) {
        NSLog(@"hi hide");
        acceptCell.hidden = YES;
        rescheduleCell.hidden = YES;
    }
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"sdadasdsa");
    if (isEvent) {
        
        NSLog(@"isEvent");
        NSLog(@"isEvent2");
        [thumbnailTV setImage:event.picture];
        NSLog(@"isEvent");
        [projectTypeLB setText:event.projectType];
        NSLog(@"isEvent3");
        [projectyNameLB setText:event.projectName];
        NSLog(@"isEvent4");
        [idealBuyingPriceTF setText:event.idealBuyingPrice];
        
        [event.receiverAgent fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                User *propAgent = [[User alloc] initWithUser:event.receiverAgent];
               [propertyAgentLB setText:[NSString stringWithFormat:@"Agent: %@",propAgent.fullName]];
            }
        }];
        
        
        
        [addressLB setText:event.address];
        [addressTV setText:event.address];
        
        NSDate * date = event.eventSchedule;
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"dd/MM/yyyy"];
        
        NSString *title = [outputFormatter stringFromDate:date];
        [proposedMeetupDateTF setText:title];
        NSLog(@"isEvent5");
        [outputFormatter setDateFormat:@"HH:mm a"];
        NSString *title1 = [outputFormatter stringFromDate:date];
        
        [proposedMeetupTimeTF setText:title1];
        curDate = event.eventSchedule;
        
        proposedMeetupDateTF.enabled = NO;
        proposedMeetupTimeTF.enabled= NO;
        idealBuyingPriceTF.enabled = NO;
        NSLog(@"isEvent6");
        [self hideCell];
    }
    else
    {
        NSLog(@"isEvent2");
        thumbnailTV.image = property.iconImage;
        [projectyNameLB setText:property.propertyName];
        [projectTypeLB setText:property.propertyType];
        [addressLB setText:property.address];
        
        
        NSDate * now = [NSDate date];
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"dd/MM/yyyy"];
        
        NSString *title = [outputFormatter stringFromDate:now];
        
        [proposedMeetupDateTF setText:title];
        curDate = now;
        
        [outputFormatter setDateFormat:@"HH:mm a"];
        NSString *title1 = [outputFormatter stringFromDate:now];
        
        [proposedMeetupTimeTF setText:title1];
    }
    
    
    curUser = [[DataEngine sharedInstance] GetCurUser];
    
    [self initTextfieldPlaceHolder];
    
    
    
}

- (void) initTextfieldPlaceHolder
{
    idealBuyingPriceTF.placeholder = @"Ideal Buying Price: ";
    proposedMeetupDateTF.placeholder = @"Proposed Viewing Date: ";
    proposedMeetupTimeTF.placeholder = @"Proposed Viewing Time: ";

    
    //address.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showDatePicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    datePick = [[UIDatePicker alloc] init];
    datePick.datePickerMode = YES;
    [datePick setDate:curDate];
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    
    
    
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    [datePick addTarget:self
                 action:@selector(SetDatePickerTime:)
       forControlEvents:UIControlEventValueChanged];
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:datePick];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    CGRect pickerRect = datePick.bounds;
    pickerRect.origin.y = 0;
    datePick.bounds = pickerRect;
    
}

-(void)SetDatePickerTime:(id)sender
{
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *title = [outputFormatter stringFromDate:datePick.date];
    
    [proposedMeetupDateTF setText:title];
    curDate = datePick.date;
}


- (void)showTimePicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    timePick = [[UIDatePicker alloc] init];
    timePick.datePickerMode = UIDatePickerModeTime;
    [timePick setDate:curDate];
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    
    
    
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    [timePick addTarget:self
                 action:@selector(setTimePicker:)
       forControlEvents:UIControlEventValueChanged];
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:timePick];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    CGRect pickerRect = timePick.bounds;
    pickerRect.origin.y = 0;
    timePick.bounds = pickerRect;
    
}

-(void)setTimePicker:(id)sender
{
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm a"];
    
    NSString *title = [outputFormatter stringFromDate:timePick.date];
    
    NSLog(@"time : %@", curDate);
    NSLog(@"time2 : %@", timePick.date);
    [proposedMeetupTimeTF setText:title];
    curDate = timePick.date;
}



-(void)categoryDoneButtonPressed
{
    
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    
    
    return 0;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [NSString stringWithFormat:@"%d", row+1];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:proposedMeetupDateTF] )
    {
        [textField resignFirstResponder];
        [self showDatePicker];
    }
    else if ([textField isEqual:proposedMeetupTimeTF])
    {
        [textField resignFirstResponder];
        [self showTimePicker];
    }
}





- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    float keyboardHeight = 216.0f;
    if (textField.frame.origin.y + textField.frame.size.height > self.view.frame.size.height - keyboardHeight) {
        const int movementDistance = 100; // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

-(void)dismissKeyboard {
    [activeTextField resignFirstResponder];
}

- (IBAction)addSchedule:(id)sender
{
    if (isEvent) {
        
    }
    else
    {
        acceptBtn.hidden = YES;
        indicator.hidden = NO;
        
        ScheduleEvent * newEvent = [[ScheduleEvent alloc]init];
        
        newEvent.requester = curUser;
        newEvent.receiverAgent = [property.agent getUserObject];
        newEvent.receiverCreator = property.adder.userObject;
        newEvent.eventSchedule = curDate;
        newEvent.idealBuyingPrice = idealBuyingPriceTF.text;
        newEvent.picture = thumbnailTV.image;
        newEvent.projectName = projectyNameLB.text;
        newEvent.projectType = projectTypeLB.text;
        newEvent.address = addressLB.text;
        newEvent.requesterAccept = YES;
        
        [[DatabaseEngine sharedInstance] requestMeetup:self :newEvent];
    }
    
    
}
- (IBAction)accept:(id)sender
{
    if (isReschedule) {
        event.eventSchedule = curDate;
        [[DatabaseEngine sharedInstance] acceptRequester:self :event];
    }
    else
    {
        event.isReschedule = NO;
        [[DatabaseEngine sharedInstance] acceptRequester:self :event];
    }
    
}

- (IBAction)reschedule:(id)sender
{
    NSLog(@"reschedule");
    proposedMeetupTimeTF.enabled = YES;
    [proposedMeetupTimeTF setPlaceholder:@"Proposed Meetup Time"];
    proposedMeetupDateTF.enabled = YES;
    [proposedMeetupDateTF setPlaceholder:@"Proposed Meetup Date"];
    event.isReschedule = YES;
    isReschedule = YES;
    
    [acceptBtn setTitle:@"Done" forState:UIControlStateNormal];
    [rescheduleCell setHidden:YES];
}


@end
