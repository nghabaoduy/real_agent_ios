//
//  SideBarViewController.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 14/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
@interface SideBarViewController : UITableViewController <MFMailComposeViewControllerDelegate>
@property (nonatomic, strong) NSArray *menuItems;


@end
