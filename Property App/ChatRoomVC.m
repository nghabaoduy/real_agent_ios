//
//  ChatRoomVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//
#define TABBAR_HEIGHT 49.0f
#define TEXTFIELD_HEIGHT 70.0f
#define MAX_ENTRIES_LOADED 25

#import "ChatRoomVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "ChatCell.h"
#import "User.h"
#import "Property.h"
#import "DetailPropertyNavVC.h"
#import "DetailPropertyTabBarVC.h"
#import "DetailPropertyVC.h"
#import "GalleryPropertyVC.h"
#import "LocationPropertyVC.h"
#import "DetailComercialVC.h"

/*
 PFObject * chatInfo = [chatList objectAtIndex:indexPath.row];
 
 NSString * content = [chatInfo objectForKey:@"content"];
 
 PFUser * sender = [chatInfo objectForKey:@"userId"];
 
 
 NSString * fullText = @"";
 
 if ([sender.objectId isEqualToString:curUser.userObject.objectId]) {
 fullText = [NSString stringWithFormat:@"%@: %@", curUser.username, content];
 }
 else
 {
 fullText = [NSString stringWithFormat:@"%@: %@", messageInfo.connector.userObject.username, content];
 }
 */

@interface ChatRoomVC ()

@end

@implementation ChatRoomVC
@synthesize chatRoomInfo;
@synthesize chatList;
@synthesize userInfoList;
@synthesize chatDisplayArray;
@synthesize userDisplayArray;
@synthesize propArray;

- (void)actionRun
{
    [super actionRun];
    [[DatabaseEngine sharedInstance] LoadMessage:chatRoomInfo :self];
    
}

- (void)actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    [self reloadChatDisplayArray];
    [tv reloadData];

    if ([tv numberOfSections] > 0) {
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [((NSMutableArray *)[chatDisplayArray lastObject]) count]-1 inSection: userDisplayArray.count-1];
        [tv scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    }
    
    [self timerStart];
    
}

- (void)timerStart
{
    
    myTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                               target:self
                                             selector:@selector(refreshingStart)
                                             userInfo:nil
                                              repeats:YES];
}

- (void)timerStop
{
    [myTimer invalidate];
    myTimer = nil;
}

-(void)actionFail:(NSString *)message
{
    [super actionFail:message];
}

-(void) reloadChatDisplayArray
{
    NSLog(@"chatList = %@", chatList);
    NSLog(@"userInfoList = %@", userInfoList);
    chatDisplayArray = [[NSMutableArray alloc] init];
    userDisplayArray = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i< chatList.count; i++) {
        if (i == 0) {
            //NSLog(@"userInfoList = %@",userInfoList);
            NSLog(@"chatList = %@",chatList);
            [userDisplayArray addObject:[userInfoList objectAtIndex:0]];
            NSMutableArray *chatArray = [[NSMutableArray alloc] init];
            [chatArray addObject:[chatList objectAtIndex:0]];
            [chatDisplayArray addObject:chatArray];
        }
        else
        {
            PFUser * indexUser =[userInfoList objectAtIndex:i];
            NSString * indexUserName = [NSString stringWithFormat:@"%@", indexUser.username];
            PFUser * lastUser = [userDisplayArray lastObject];
            NSString * lastUserName = [NSString stringWithFormat:@"%@", lastUser.username];
            
            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
            timeFormatter.dateFormat = @"dd-MM-yyyy";
            
            NSMutableArray *lastChatLast = [chatDisplayArray lastObject];
            PFObject *chatBefore = [lastChatLast lastObject];
            NSString *timeBefore = [timeFormatter stringFromDate: chatBefore.createdAt];
            
            PFObject *chatNew = [chatList objectAtIndex:i];
            NSString *timeNew = [timeFormatter stringFromDate: chatNew.createdAt];
            
            if ([timeBefore isEqualToString:timeNew]) {
                if ([indexUserName isEqualToString:lastUserName]) {
                    [lastChatLast addObject:[chatList objectAtIndex:i]];
                }
                else
                {
                    [userDisplayArray addObject:indexUser];
                    NSMutableArray *newChatArray = [[NSMutableArray alloc] init];
                    [newChatArray addObject:[chatList objectAtIndex:i]];
                    [chatDisplayArray addObject:newChatArray];
                }
            }
            else
            {
                [userDisplayArray addObject:indexUser];
                NSMutableArray *newChatArray = [[NSMutableArray alloc] init];
                [newChatArray addObject:[chatList objectAtIndex:i]];
                [chatDisplayArray addObject:newChatArray];
            }
        }
    }
    //NSLog(@"userDisplayArray = %@",userDisplayArray);
    NSLog(@"chatDisplayArray = %@",chatDisplayArray);
}

- (IBAction)refresh:(id)sender
{
    PFObject * newest = [chatList lastObject];
    NSLog(@"%@ date", newest.createdAt);
    
    [self refreshingStart];
}


- (void) refreshingStart
{
    NSLog(@".");
    [[DatabaseEngine sharedInstance] RefreshMessage:chatRoomInfo :chatList :userInfoList :self];
}

- (void) refreshingDone
{
    NSLog(@"refreshDone");
    [self reloadChatDisplayArray];
    [tv reloadData];
    if ([tv numberOfSections] > 0) {
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [((NSMutableArray *)[chatDisplayArray lastObject]) count]-1 inSection: userDisplayArray.count-1];
        [tv scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    }
}

- (void)sendMessage:(id)sender
{
    if (![self.content.text isEqualToString:@""] && self.content.text != nil) {
        NSLog(@"user.userObject = %@",user.userObject);
        NSLog(@"content.text = %@",self.content.text);
        NSLog(@"chatRoomInfo = %@",chatRoomInfo);
        [self timerStop];
        [[DatabaseEngine sharedInstance] sendMessage: user.userObject: self.content.text :chatRoomInfo :self];
        
        [self.content setText:@""];
        send.enabled = NO;
    }
    
    
}

- (void) sendDone : (PFObject*) chatContent
{
    
    [[DatabaseEngine sharedInstance] sendNotificationToGroup:self.chatRoomInfo :chatContent];
    
    NSLog(@"sendDone %i", [chatList count]);
    [self refreshingStart];
    send.enabled = YES;
    
    
    //Configure tableview
    [self reloadChatDisplayArray];
    [tv reloadData];
    if ([tv numberOfSections] > 0) {
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [((NSMutableArray *)[chatDisplayArray lastObject]) count]-1 inSection: userDisplayArray.count-1];
        [tv scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    }
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    tv.separatorStyle = UITableViewCellSeparatorStyleNone;
    userInfoList = [[NSMutableArray alloc] init];
    chatList = [[NSMutableArray alloc] init];
    propArray = [[NSMutableArray alloc] init];
    user = [[DataEngine sharedInstance] GetCurUser];
    [self registerForKeyboardNotifications];
    NSLog(@"chatRoomInfo.objectId = %@", chatRoomInfo.objectId);
    //send.enabled = NO;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self actionRun];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self timerStop];
}

- (void) dismissKeyboard{
    [self.content resignFirstResponder];
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//custom section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20)];
    //sectionView.backgroundColor = [UIColor blueColor];
    UILabel *dateTxt = [[UILabel alloc] initWithFrame:sectionView.frame];
    [sectionView addSubview:dateTxt];
    dateTxt.textAlignment = NSTextAlignmentCenter;
    dateTxt.center = sectionView.center;
    
    //text
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
    timeFormatter.dateFormat = @"dd-MM-yyyy";
    
    NSMutableArray *chatArray = [chatDisplayArray objectAtIndex:section];
    PFObject * thisChatInfo = [chatArray firstObject];
    NSString *dateString = [timeFormatter stringFromDate: thisChatInfo.createdAt];
    if (section == 0) {
        [dateTxt setText:dateString];
    }
    else
    {
        NSMutableArray *preChatArray = [chatDisplayArray objectAtIndex:section-1];
        PFObject * preChatInfo = [preChatArray lastObject];
        NSString *preDateString = [timeFormatter stringFromDate: preChatInfo.createdAt];
        
        if (![dateString isEqualToString:preDateString]) {
            [dateTxt setText:dateString];
        }
    }
   
    
    
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
    [dateTxt setFont:font];
    [dateTxt setTextColor:[UIColor grayColor]];
    
    
    return sectionView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return userDisplayArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"%i ting ting", [chatList count]);
    NSMutableArray *chatArray = [chatDisplayArray objectAtIndex:section];
    return [chatArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"chatCell";
    
    ChatCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    NSMutableArray *chatArray = [chatDisplayArray objectAtIndex:indexPath.section];
    
    PFObject * chatInfo = [chatArray objectAtIndex:indexPath.row];
    
    
    PFUser * theUSer =[userDisplayArray objectAtIndex:indexPath.section];
    NSLog(@"Chat User = %@",theUSer);
    User *curUser = [[User alloc] initWithUser:theUSer];
    NSString * theContent = [NSString stringWithFormat:@"%@", [chatInfo objectForKey:@"content"]];
    
    //username
    if (indexPath.row == 0) {
        cell.usernameTF.text = curUser.fullName;
    }
    else
    {
        cell.usernameTF.text = @"";
    }
    //time
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
    timeFormatter.dateFormat = @"hh:mm a";
    NSString *dateString = [timeFormatter stringFromDate: chatInfo.createdAt];
    cell.timeTF.text = dateString;
    
    //content
    cell.contentTF.userInteractionEnabled = NO;
    cell.propAddress.userInteractionEnabled = NO;
    
    if ([self checkNormalContent:cell:theContent]) {
        cell.chatView.hidden = NO;
        cell.propView.hidden = YES;
        cell.contentTF.text = theContent;
    }
    else
    {
        cell.chatView.hidden = YES;
        cell.propView.hidden = NO;
    }
    

    //custom cell
    [self customCell:cell :indexPath : tv : curUser.userObject.objectId];
    
    return cell;
}



-(BOOL) checkNormalContent:(ChatCell *) cCell : (NSString *)theContent
{
    if ([theContent hasPrefix:@"share Property:"]) {
        NSLog(@"has prefix share prop");
        NSString *propID = [[theContent componentsSeparatedByString:@": "] lastObject];
        BOOL foundProp = NO;
        for (Property *prop in self.propArray) {
            if ([prop.propID isEqualToString:propID] && !foundProp) {
                
                cCell.propIcon.image = prop.iconImage;
                
                [cCell.propAddress setText:prop.address];
                [cCell.propPrice setText:[NSString stringWithFormat:@"Price: %@",prop.price]];
                foundProp = YES;
                
            }
        }
        if (foundProp == NO) {
            [cCell.propAddress setText:@"Loading..."];
            UIImage *cellImage = [UIImage imageNamed:@"chatDefaultProperty"];
            cCell.propIcon.image = cellImage;
            [cCell setCellPropertyId:propID:self.propArray];
        }
        return NO;
    }
    return YES;
}


-(void) customCell:(ChatCell *) cCell :(NSIndexPath *)indexPath :(UITableView *) tableView : (NSString *)userId
{
    NSString *yourId = [[DataEngine sharedInstance] curUser].userObject.objectId;
    
    if ([userId isEqualToString:yourId]) {
        if ([tableView numberOfRowsInSection:indexPath.section] > 1) {
            if (indexPath.row == 0) {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_top"]];
            }
            else if(indexPath.row == [tableView numberOfRowsInSection:indexPath.section] -1)
            {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_bot"]];
            }
            else
            {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_mid"]];
            }
        }
        else
        {
            cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_one"]];
        }
    }
    else
    {
        if ([tableView numberOfRowsInSection:indexPath.section] > 1) {
            if (indexPath.row == 0) {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_top2"]];
            }
            else if(indexPath.row == [tableView numberOfRowsInSection:indexPath.section] -1)
            {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_bot2"]];
            }
            else
            {
                cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_mid2"]];
            }
        }
        else
        {
            cCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatCell_one2"]];
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{

    NSMutableArray *chatArray = [chatDisplayArray objectAtIndex:indexPath.section];
    if (chatArray.count > 0) {
        if (indexPath.row == chatArray.count-1) {
            return 91;
        }
    }
    return 83;
    
}

-(void) shareProperty: (Property*) prop
{
    shareProp = prop;
    [propArray addObject:prop];
    NSLog(@"shareProperty: %@", prop.propertyName);
    [self.content setText:[NSString stringWithFormat:@"share Property: %@",prop.propertyObject.objectId]];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray *chatArray = [chatDisplayArray objectAtIndex:indexPath.section];
    PFObject * chatInfo = [chatArray objectAtIndex:indexPath.row];
    NSString * theContent = [NSString stringWithFormat:@"%@", [chatInfo objectForKey:@"content"]];
    
    if ([theContent hasPrefix:@"share Property:"]) {
        NSLog(@"has prefix share prop");
        NSString *propID = [[theContent componentsSeparatedByString:@": "] lastObject];
        
        
        BOOL foundProp = NO;
        for (Property *prop in self.propArray) {
            if ([prop.propID isEqualToString:propID] && !foundProp) {
                
                //found prop in proparray
                foundProp = YES;
                [self loadDetailViewWithProp:prop];
            }
        }
        
        
        
        //search in parse
        if (foundProp == NO) {
            PFQuery *query = [PFQuery queryWithClassName:@"Property"];
            [query whereKey:@"objectId" equalTo:propID];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    if (objects.count > 0) {
                        Property *prop = [[Property alloc] initWithProperty:[objects firstObject]];
                        [self loadDetailViewWithProp:prop];
                    }
                    else
                    {
                        NSLog(@"cant find prop");
                    }
                }
                else
                {
                    NSLog(@"error while search prop");
                }
            }];
        }
        
    }
}

-(void) loadDetailViewWithProp: (Property *)curPro
{
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"DetailStoryboard" bundle:nil];
    
    
    
    DetailPropertyNavVC * navView =[newStoryboard instantiateViewControllerWithIdentifier:@"detailProperty"];
    
    DetailPropertyTabBarVC * tabBar = (DetailPropertyTabBarVC*) navView.topViewController;
    [tabBar initTabByProperty:curPro :newStoryboard ];
    
    [self presentViewController:navView animated:YES completion:nil];
}



// keyboard function
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 120) ? NO : YES;
}

-(void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


-(void) freeKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


-(void) keyboardWasShown:(NSNotification*)aNotification
{
    NSLog(@"Keyboard was shown");
    NSDictionary* info = [aNotification userInfo];
    
    // Get animation info from userInfo
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardFrame;
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&keyboardFrame];
    
    // Move
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    NSLog(@"frame..%f..%f..%f..%f",self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    NSLog(@"keyboard..%f..%f..%f..%f",keyboardFrame.origin.x, keyboardFrame.origin.y, keyboardFrame.size.width, keyboardFrame.size.height);
    self.view.autoresizesSubviews = NO;
    
    [self.view setFrame:CGRectMake(0, 0 , self.view.frame.size.width, self.view.frame.size.height- keyboardFrame.size.height+TABBAR_HEIGHT)];
   // [chatTable setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+ keyboardFrame.size.height+TABBAR_HEIGHT+TEXTFIELD_HEIGHT, self.view.frame.size.width, self.view.frame.size.height-keyboardFrame.size.height)];
    //[chatTable scrollsToTop];
    
    [UIView commitAnimations];
    
}
-(void) keyboardWillHide:(NSNotification*)aNotification
{
    NSLog(@"Keyboard will hide");
    NSDictionary* info = [aNotification userInfo];
    
    // Get animation info from userInfo
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardFrame;
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&keyboardFrame];
    
    // Move
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    [self.view setFrame:CGRectMake(0, 0 , self.view.frame.size.width, self.view.frame.size.height+ keyboardFrame.size.height-TABBAR_HEIGHT)];
    //[chatTable setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-TABBAR_HEIGHT)];
    [UIView commitAnimations];
}
@end
