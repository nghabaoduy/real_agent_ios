//
//  LoadingViewController.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 15/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "LoadingViewController.h"
#import "MainViewController.h"
#import "SWRevealViewController.h"
#import "DataEngine.h"

@interface LoadingViewController ()

@end

@implementation LoadingViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [DataEngine sharedInstance].loadingView = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadMainMenu
{
    NSLog(@"loading view ::: Login successful!");
    
	SWRevealViewController * mainView = [self.storyboard instantiateViewControllerWithIdentifier:@"sw"];
	[self presentViewController:mainView animated:NO completion:nil];
    
}


@end
