//
//  PersonInfoViewController.h
//  PhoneBookController
//
//  Created by Brian on 9/9/13.
//  Copyright (c) 2013 Brian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "Property.h"
#import <MessageUI/MFMailComposeViewController.h>
@interface PersonInfoViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>

@property (nonatomic, retain) IBOutlet UILabel *nameLbl;
@property (nonatomic, retain) IBOutlet UILabel *phoneLbl;
@property (nonatomic, retain) IBOutlet UILabel *emailLbl;
@property (nonatomic, retain) IBOutlet UILabel * addressLbl1;
@property (nonatomic, retain) IBOutlet UILabel * addressLbl2;

@property (nonatomic, retain) IBOutlet UIBarButtonItem *backBtn;

@property (nonatomic, retain) User *user;
@property (nonatomic, retain) NSMutableArray* propertyList;
@property (nonatomic, retain) NSMutableArray* marketingList;
@property (nonatomic, retain) NSMutableArray* nonMarketingList;
@property (nonatomic, retain) Property *addedProperty;

@property (nonatomic, retain) IBOutlet UITableView *tableView;

-(void) refreshPersonInfo;

@end
