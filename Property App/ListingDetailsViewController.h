//
//  ListingDetailsViewController.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Property.h"
@interface ListingDetailsViewController : UIViewController
{
    
}

@property (nonatomic, retain) IBOutlet UINavigationBar *navBar;

@property (strong, nonatomic) Property* property;

@property (strong, nonatomic) IBOutlet UILabel * propertyName;
@property (strong, nonatomic) IBOutlet UILabel * propertyType;

@property (strong, nonatomic) IBOutlet UILabel * addressLbl;
@property (strong, nonatomic) IBOutlet UILabel * postcodeLbl;
@property (strong, nonatomic) IBOutlet UITextField * blkNoTextF;
@property (strong, nonatomic) IBOutlet UITextField * levelTextF;
@property (strong, nonatomic) IBOutlet UITextField * unitTextF;
@property (strong, nonatomic) IBOutlet UITextField * districtTextF;
@property (strong, nonatomic) IBOutlet UITextField * bedroomTextF;
@property (strong, nonatomic) IBOutlet UITextField * areaTextF;
@property (strong, nonatomic) IBOutlet UITextField * priceTextF;
@property (strong, nonatomic) IBOutlet UITextField * tenureTextF;
//status
@property (strong, nonatomic) IBOutlet UITextField * statusTextF;
@property (strong, nonatomic) IBOutlet UITextField * rentedPriceTextF;

//project info
@property (strong, nonatomic) IBOutlet UITextField * projectTextF;
@property (strong, nonatomic) IBOutlet UITextField * psfTextF;
@property (strong, nonatomic) IBOutlet UITextField * topYearTextF;
@property (strong, nonatomic) IBOutlet UITextField * rentalYeildTextF;

@property (strong, nonatomic) IBOutlet UILabel *agentName;
@property (strong, nonatomic) IBOutlet UILabel *agentPhone;
@property (strong, nonatomic) IBOutlet UILabel *agentEmail;
@property (strong, nonatomic) IBOutlet UIImageView *agentIcon;

@property (nonatomic, strong) IBOutlet UIBarButtonItem * doneBtn;






-(void) LoadInformation;

@end
