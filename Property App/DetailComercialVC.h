//
//  DetailComercialVC.h
//  Property App
//
//  Created by Brian on 4/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface DetailComercialVC : UITableViewController <MFMailComposeViewControllerDelegate>
{
    
    IBOutlet UITableView *tv;
    
     NSString * type;
    //display
    IBOutlet UIImageView * icon;
    IBOutlet UILabel * propertyName;
    IBOutlet UILabel * propertyType;
    IBOutlet UILabel * adress;
    IBOutlet UITextView *addressTV;
    IBOutlet UILabel * postalCode;
    
    //Detail
    IBOutlet UITextField * comType;
    IBOutlet UITextField * price;
    IBOutlet UITextField * district;
    IBOutlet UITextField * floorSize;
    IBOutlet UITextField * tenure;
    
    
    //Status
    IBOutlet UITextField * status;
    IBOutlet UITextField * rentedPrice;
    
    //Project Information
    IBOutlet UITextField * projectName;
    IBOutlet UITextField * psf;
    IBOutlet UITextField * topYear;
    IBOutlet UITextField * rentalYield;
    
    //Contact Agen
    IBOutlet UIImageView * agentIcon;
    IBOutlet UILabel * agentName;
    IBOutlet UILabel * agentMobile;
    IBOutlet UILabel * agentEmail;
    
    //Office extra field
    IBOutlet UITextField * OFFICElevelTF;
    IBOutlet UITextField * OFFICEgstTF;
    IBOutlet UITextField * OFFICEelectLoadTF;
    IBOutlet UITextField *OFFICEamps;
    IBOutlet UITextField * OFFICEairconTF;
    IBOutlet UITextField * OFFICEownPantryTF;
    IBOutlet UITextField * OFFICEgradeTF;
    IBOutlet UITextField * OFFICEfloorLoadTF;
    IBOutlet UITextField * OFFICEserverRoomTF;
    
    //retail extra field
    IBOutlet UITextField * RETAILlevelTF;
    IBOutlet UITextField * RETAILgstTF;
    IBOutlet UITextField * RETAILelectLoadTF;
    IBOutlet UITextField * RETAILamps;
    IBOutlet UITextField * RETAILretailTypeTF;
    IBOutlet UITextField * RETAILconditionTF;
    
    //F&B extra field
    IBOutlet UITextField * FABlevelTF;
    IBOutlet UITextField * FABgstTF;
    IBOutlet UITextField * FABelectLoadTF;
    IBOutlet UITextField * FABampsTF;
    IBOutlet UITextField * FABfabTypeTF;
    IBOutlet UITextField * FABconditionTF;
    IBOutlet UITextField * FABgreaseTrapTF;
    IBOutlet UITextField * FABexhaustTF;

    //land extra field
    IBOutlet UITextField * LANDgstTF;
    IBOutlet UITextField * LANDelectLoadTF;
    IBOutlet UITextField * LANDampsTF;
    IBOutlet UITextField * LANDbuiltUpTF;
    IBOutlet UITextField * LANDnoOfStoreysTF;
    
    IBOutlet UITableViewCell * schedule;
    User * curUser;
    
    IBOutlet UITableViewCell * addToShare;
    
}

@property (nonatomic, retain) Property* property;


@property (nonatomic) BOOL isSharing;
@property (nonatomic) BOOL allowUnitNo;
@property (nonatomic, retain) ProPortfolioViewController * proportView;


@property (nonatomic, retain) PFObject * curGroup;
@property (nonatomic) BOOL isMyProperty;
- (void) hideSchedule;

- (void) showDialog : (NSString *) message;
@end
