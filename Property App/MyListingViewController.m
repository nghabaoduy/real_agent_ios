//
//  MyListingViewController.m
//  Property App
//
//  Created by Brian on 9/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyListingViewController.h"
#import "SalesDetailsViewController.h"
#import "ListingDetailsViewController.h"
#import "ListingGalleryViewController.h"
#import "ListingLocationViewController.h"
#import "AddPropertyViewController.h"
#import "MyUIEngine.h"
@implementation MyListingViewController

@synthesize data = _data;
@synthesize tableView = _tableView;
@synthesize Titles = _Titles;

- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction) AddPro:(id)sender
{
    AddPropertyViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"addPropertyView"];
    [self presentViewController:view animated:YES completion:nil];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SalesDetailsViewController * tabview = [self.storyboard instantiateViewControllerWithIdentifier:@"salesDetailsView"];
    ListingDetailsViewController * tab01 = [self.storyboard instantiateViewControllerWithIdentifier:@"listingDetailsView"];
    ListingGalleryViewController * tab02 = [self.storyboard instantiateViewControllerWithIdentifier:@"listingGalleryView"];
    ListingLocationViewController * tab03 = [self.storyboard instantiateViewControllerWithIdentifier:@"listingLocationView"];
    
    tab01.property = [_data objectAtIndex:indexPath.row];
    //tab02.property = [_data objectAtIndex:indexPath.row];
    
    tabview.viewControllers = [NSArray arrayWithObjects:tab01, tab02, tab03, nil];
    [self presentViewController:tabview animated:YES completion:nil];
    
    
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_Titles count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * simpleTableIndentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [[MyUIEngine sharedUIEngine] getCell:self.tableView :simpleTableIndentifier :indexPath.row :_Titles.count :@""];
    
    Property *prop = [_data objectAtIndex:indexPath.row];
    NSLog(@"%@", [prop description]);
    
    cell.textLabel.text = [prop description];
    cell.detailTextLabel.text = @"line\nline\nhaha";
    cell.detailTextLabel.numberOfLines = 3;
    cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.font = [UIFont fontWithName:@"TimesNewRoman" size:10.0 ];
    return cell;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :self.tableView];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
