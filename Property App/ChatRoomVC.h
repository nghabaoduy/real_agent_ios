//
//  ChatRoomVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUIViewController.h"
#import <Parse/Parse.h>
#import "User.h"
#import "Property.h"

@interface ChatRoomVC : MyUIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    User * user;
    IBOutlet UITextField * content;
    IBOutlet UITableView * tv;
    NSTimer * myTimer;
    IBOutlet UIButton * send;
    BOOL sending;
    Property *shareProp;
    
    
}
@property (nonatomic, retain) PFObject * chatRoomInfo;
@property (nonatomic, retain) NSMutableArray * chatList;
@property (nonatomic, retain) NSMutableArray * userInfoList;
@property (nonatomic, retain) NSMutableArray * chatDisplayArray;
@property (nonatomic, retain) NSMutableArray * userDisplayArray;

@property (nonatomic, retain) NSMutableArray * propArray;

@property (nonatomic, retain) IBOutlet UITextField * content;

- (IBAction)sendMessage:(id)sender;
- (void) sendDone : (PFObject*) chatContent;

-(void) registerForKeyboardNotifications;
-(void) freeKeyboardNotifications;
-(void) keyboardWasShown:(NSNotification*)aNotification;
-(void) keyboardWillHide:(NSNotification*)aNotification;
- (void) refreshingStart;
- (void) refreshingDone;
- (void) timerStop;
- (void) timerStart;

-(void) shareProperty: (Property*) prop;

@end
