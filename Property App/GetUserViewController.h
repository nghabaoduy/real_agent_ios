//
//  GetUserViewController.h
//  Property App
//
//  Created by Brian on 12/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUIViewController.h"

@interface GetUserViewController :MyUIViewController

@property (nonatomic, retain) NSMutableArray* agentClientList;
@property (nonatomic, retain) NSMutableArray* agentClientPropertyList;

- (void) LoadPropertyListDone;

@end
