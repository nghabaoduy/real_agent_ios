//
//  DetailGroupListingVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "MyUITableViewController.h"
#import "Property.h"
#import "ProPortfolioViewController.h"


@interface DetailGroupListingVC : MyUITableViewController


@property (nonatomic, strong) PFObject * curGroup;


@property (nonatomic, strong) NSArray * propertyList;

@end
