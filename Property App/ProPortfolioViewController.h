//
//  ProPortfolioViewController.h
//  Property App
//
//  Created by Brian on 12/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUIViewController.h"
#import "User.h"
#import <Parse/Parse.h>



@interface ProPortfolioViewController : MyUIViewController <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, retain) IBOutlet UITableView  *tableView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *addBtn;
@property (nonatomic, retain) NSMutableArray *contentArray;
@property (nonatomic, retain) NSMutableArray* agentClientList;
@property (nonatomic, retain) NSMutableArray* agentClientPropertyList;
@property (nonatomic, retain) NSMutableArray* allPropertyList;

@property (nonatomic) BOOL isSharing;
@property (nonatomic, retain) PFObject * curGroup;


-(void) addClientProperty: (User *)user;

@end
