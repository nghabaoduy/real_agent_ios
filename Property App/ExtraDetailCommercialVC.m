//
//  ExtraDetailCommercialVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/1/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ExtraDetailCommercialVC.h"
#import "ImagePickVC.h"
#import "MyUIEngine.h"
#import "MyCustomBack.h"
#import "DataEngine.h"
#import "Agent.h"
#import "Client.h"
#import "CommercialTypeDetailVC.h"

@interface ExtraDetailCommercialVC ()

@end

@implementation ExtraDetailCommercialVC
@synthesize property = _property;


Agent* curAgent;

- (void) DisplayAddress
{
    [address setText:_property.address];
    [postalCode setText:_property.postCode];
    
    [blockNo setText:_property.blkNo];
    [levelNo setText:_property.levelNumber];
    [unitNo setText:_property.unitNo];
    [district setText:_property.district];
    //[area setText:_property.area];
    //[price setText:_property.price];
    [tenure setText:_property.tenure];
    
    [status setText:_property.status];
    [rentedPrice setText:_property.rentedPrice];
    
    
    
    [projectName setText:_property.projectName];
    [topYear setText:_property.topYear];
    [PSF setText:_property.PSF];
    [rentalYield setText:_property.rentalYeild];
    
    
}
-(void) addBackBtn
{
    NSLog(@"addBackBtn runs");
    UIButton *Btn =[[UIButton alloc] initWithFrame:CGRectMake(0.0f,0.0f,68.0f,30.0f)];
    
    [Btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [Btn setBackgroundImage:[UIImage imageNamed:@"button_back2"] forState:UIControlStateNormal];
    [Btn setTitle:@"   Back" forState:UIControlStateNormal];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithCustomView:Btn];
    //[addButton setBackgroundImage:[UIImage imageNamed:@"button_back2"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [addButton setTitle:@"   Back"];
    [self.navigationItem setLeftBarButtonItem:addButton];
}


-(void) back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    
    [price addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    [rentedPrice addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    
    

    [self DisplayAddress];
    
    rentalYieldList = [[NSArray alloc] initWithObjects:
                       @"",
                       @"3% & Below",
                       @"3-5%",@"5-10%,",
                       @"10% & Above",
                       nil];
    
    districtList = [[DataEngine sharedInstance] getDistrictList];
    tenureList = [[NSArray alloc] initWithObjects:
                  @"",
                  @"FH",
                  @"999",
                  @"99",
                  @"60",
                  @"30",
                  @"<30yrs",
                  nil];
    statusList = [[NSArray alloc] initWithObjects:
                  @"",
                  @"Vacant",
                  @"Rented",
                  nil];
    
    commercialTypeList = [[NSArray alloc]initWithObjects:
                          @"[select]",
                          @"Office",
                          @"Retail",
                          @"F&B",
                          @"Land/Building", nil];
    
    curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
    adderList = [[NSMutableArray alloc] init];
	//[adderList addObject:curAgent];
	for (Client* client in curAgent.myClientList) {
		[adderList addObject:client];
	}
    NSLog(@"adderList = %@",adderList);
    adderNameList = [[NSMutableArray alloc] init];
    [adderNameList addObject:@""];
	for (Client* client in curAgent.myClientList) {
		[adderNameList addObject:client.fullName];
	}
    NSLog(@"adderNameList = %@",adderNameList);
    
    [self initTextfieldPlaceHolder];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_property.listingType isEqualToString:@"Sales"]) {
        
        if (indexPath.section == 1 && indexPath.row == 1) {
            return 0.0;
        }
        else
        {
            return 44.0;
        }
    }
    else
    {
        if (indexPath.section == 1 && indexPath.row == 0) {
            return 0.0;
        }
        if (indexPath.section == 2 && indexPath.row == 3) {
            return 0.0;
        }
        return 44.0;

        
    }
    
    
}

-(void)dismissKeyboard {
    
    [activeTextField resignFirstResponder];
}

-(void) textFieldDidChange{
    int rentP = 0;
    int P = 0;
    
    if (![price isEqual:@""]) {
        P = [price.text intValue];
    }
    
    if (![rentedPrice isEqual:@""]) {
        rentP = [rentedPrice.text intValue];
    }
    
    
    [self CalculateRentedYield:rentP :P];
    
}


- (void) initTextfieldPlaceHolder
{
    
    
    
    address.placeholder = @"Address: ";
    postalCode.placeholder = @"Postal Code: ";
    blockNo.placeholder = @"Block: ";
    levelNo.placeholder = @"Level: ";
    unitNo.placeholder = @"Unit: ";
    district.placeholder = @"District: ";
    area.placeholder = @"Floor Size: ";
    price.placeholder = @"Price:* ";
    tenure.placeholder = @"Tenure:* ";
    adder.placeholder = @"Client:* ";
    commercialType.placeholder = @"Commercial Type:* ";
    adder.text = _property.adder.fullName;
    
    status.placeholder = @"Status: ";
    rentedPrice.placeholder = @"Rental Price: ";
    
    
    
    projectName.placeholder = @"Project Name: ";
    topYear.placeholder = @"Top Year: ";
    PSF.placeholder = @"PSF: ";
    rentalYield.placeholder = @"Rental Yield: ";
    
    
    
    address.enabled = NO;
    postalCode.enabled = NO;
    blockNo.enabled = NO;
    district.enabled = NO;
    adder.enabled = NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) ValidateBlock
{
    if (![blockNo.text isEqualToString:@""]) {
        return YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:@"Block No cannot be blank"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    return NO;
}

- (BOOL) ValidateDistrict
{
    if (![district.text isEqualToString:@""]) {
        return YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:@"District cannot be blank"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    return NO;
}



- (BOOL) ValidatePrice
{
    if (![price.text isEqualToString:@""]) {
        return YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:@"Price cannot be blank"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    return NO;
    
}

- (BOOL) ValidateTenure
{
    if (![tenure.text isEqualToString:@""]) {
        return YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:@"Tenure cannot be blank"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    return NO;
}

- (BOOL) ValidateAdder
{
    if (![adder.text isEqualToString:@""]) {
        return YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:@"Adder cannot be blank"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    return NO;
}


- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
}

-(void)categoryDoneButtonPressed{
    //Add for wheel
    //categoryLable.text = selectedCategory;
    if (activeTextField == district) {
        [district setText:selectedCategory];
    } else if (activeTextField == tenure)
    {
        [tenure setText:selectedCategory];
    } else if (activeTextField == rentalYield)
    {
        [rentalYield setText:selectedCategory];
    } else if (activeTextField == adder)
    {
        [adder setText:selectedCategory];
    } else if (activeTextField == status)
    {
        [status setText:selectedCategory];
        if ([selectedCategory isEqualToString:@"Rented"]) {
            rentedPrice.enabled = YES;
        } else
        {
            rentedPrice.enabled = NO;
            [rentedPrice setText:@""];
            [rentalYield setText:@"0"];
        }
    }
    else if (activeTextField == commercialType)
    {
        [commercialType setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
    
}

- (void)CalculateRentedYield: (int) rentedP : (int) P
{
    if(P>0)
    {
        int value = ((rentedP *12)/P)*100;
        
        [rentalYield setText:[NSString stringWithFormat:@"%i", value]];
    }
    
}

-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    //Add Wheel
    // Handle the selection
    if (activeTextField == district) {
        selectedCategory = [NSString stringWithFormat:@"%@",[districtList objectAtIndex:row]];
    } else if (activeTextField == tenure)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[tenureList objectAtIndex:row]];
    } else if (activeTextField == rentalYield)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[rentalYieldList objectAtIndex:row]];
    } else if (activeTextField == adder)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[adderNameList objectAtIndex:row]];
    } else if (activeTextField == status)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[statusList objectAtIndex:row]];
    } else if (activeTextField == commercialType)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[commercialTypeList objectAtIndex:row]];
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    //Add Wheel
    
    if (activeTextField == district) {
        return [districtList count];
    } else if (activeTextField == tenure)
    {
        return [tenureList count];
    } else if (activeTextField == rentalYield)
    {
        return [rentalYieldList count];
    } else if (activeTextField == adder)
    {
        return [adderNameList count];
    } else if (activeTextField == status)
    {
        return [statusList count];
    } else if (activeTextField == commercialType)
    {
        return [commercialTypeList count];
    }
    
    return 0;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == district) {
        return [districtList objectAtIndex:row];
    } else if (activeTextField == tenure)
    {
        return [tenureList objectAtIndex:row];
    } else if (activeTextField == rentalYield)
    {
        return [rentalYieldList objectAtIndex:row];
    } else if (activeTextField == adder)
    {
        return [adderNameList objectAtIndex:row];
    } else if (activeTextField == status)
    {
        return [statusList objectAtIndex:row];
    } else if (activeTextField == commercialType)
    {
        return [commercialTypeList objectAtIndex:row];
    }
    
    
    return 0;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //Add Wheel
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:district] || [textField isEqual:tenure] || [textField isEqual:rentalYield] || [textField isEqual:adder] || [textField isEqual:status] || [textField isEqual:commercialType])
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    _property.address = address.text;
    _property.postCode = postalCode.text;
    _property.blkNo = blockNo.text;
    _property.levelNumber = levelNo.text;
    _property.unitNo = unitNo.text;
    _property.district = district.text;
    _property.area = area.text;
    _property.price = price.text;
    _property.tenure = tenure.text;
    _property.adder = [adderList objectAtIndex:[adderNameList indexOfObject:adder.text]-1];
    NSLog(@"extra detailCom::adder name = %@",_property.adder.fullName);
    _property.status = status.text;
    _property.rentedPrice = rentedPrice.text;
    
    _property.propertyName = projectName.text;
    _property.projectName = projectName.text;
    _property.PSF = PSF.text;
    _property.topYear = topYear.text;
    _property.rentalYeild = rentalYield.text;
    _property.commercialType = [NSString stringWithFormat:@"%@", commercialType.text];
    _property.tenantedPrice = [NSString stringWithFormat:@"%@", tenantedPrice.text];
    if ([[segue identifier] isEqualToString:@"extraDetailNext"]) {
        
        
        ImagePickVC * destination = segue.destinationViewController;
        if ([self.navigationItem.title isEqualToString:@"Step 1/3"]) {
            [destination.navigationItem setTitle:@"Step 2/3"];
        }
        destination.property = _property;
    }
    else 
    {
        
        
        CommercialTypeDetailVC * destination = segue.destinationViewController;
        [destination.navigationItem setTitle:self.navigationItem.title];
        destination.property = _property;
    }
    
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"extraDetailNext"]) {
        if ([self ValidateBlock] &&
            [self ValidateDistrict] &&
            [self ValidatePrice] &&
            [self ValidateTenure] &&
            [self ValidateAdder] )
        {
            return YES;
        }
        else
        {
            //Show log
            return NO;
        }
    }
    return NO;
    
}

- (IBAction)next:(id)sender
{
    if ([self ValidateBlock] &&
        [self ValidateDistrict] &&
        [self ValidatePrice] &&
        [self ValidateTenure] &&
        [self ValidateAdder] )
    {
        if ([commercialType.text isEqualToString:@"Office"]) {
            [self performSegueWithIdentifier:@"goOffice" sender:self];
        }
        else if ([commercialType.text isEqualToString:@"Retail"])
        {
            [self performSegueWithIdentifier:@"goRental" sender:self];
        }
        else if ([commercialType.text isEqualToString:@"F&B"])
        {
            [self performSegueWithIdentifier:@"goF&B" sender:self];
        }
        else if ([commercialType.text isEqualToString:@"Land/Building"])
        {
            [self performSegueWithIdentifier:@"goLand" sender:self];
        }
        else
        {
            //err
            
        }
    }
    else
    {
        //Show log
        
    }
    
}
@end
