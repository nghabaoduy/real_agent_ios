//
//  SearchHDBVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/24/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "SearchHDBVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"

@interface SearchHDBVC ()

@end

@implementation SearchHDBVC
- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self LoadDefault];
    levelList = [[NSArray alloc]initWithObjects:
                 @"[Select]",
                 @"Ground",
                 @"Low",
                 @"Mid",
                 @"High", nil];
    
    conditionList = [[NSArray alloc] initWithObjects:
                     @"[select]",
                     @"Bare",
                     @"Partial",
                     @"Full Fitted",
                     nil];
    roomPositionList = [[NSArray alloc] initWithObjects:
                         @"[select]",
                         @"Conner",
                         @"Intermediate",
                         @"Door to Door",
                         nil];
    
    mainDoorList = [[NSArray alloc] initWithObjects:
                    @"[select]",
                    @"West",
                    @"East",
                    @"North",
                    @"South",
                    nil];
    
    districtList = [[DataEngine sharedInstance] getDistrictList];
    
    [self initTextfieldPlaceHolder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) LoadDefault
{
    [propertyTypeTF setText: _property.propertyType];
    [listingTypeTF setText:_property.listingType];
}

-(void)dismissKeyboard {
    [activeTextField resignFirstResponder];
}



- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
}

-(void)categoryDoneButtonPressed{
    //categoryLable.text = selectedCategory;
    if (activeTextField == districtTF) {
        [districtTF setText:selectedCategory];
    } else if (activeTextField == hdbTypeTF)
    {
        [hdbTypeTF setText:selectedCategory];
    } else if (activeTextField == levelTF)
    {
        [levelTF setText:selectedCategory];
    } else if (activeTextField == conditionTF)
    {
        [conditionTF setText:selectedCategory];
    } else if (activeTextField == roomPositionTF)
    {
        [roomPositionTF setText:selectedCategory];
    } else if (activeTextField == mainDoorDirectionTF)
    {
        [mainDoorDirectionTF setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if (activeTextField == districtTF) {
        selectedCategory = [NSString stringWithFormat:@"%@",[districtList objectAtIndex:row]];
    } else if (activeTextField == hdbTypeTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[hdbTypeList objectAtIndex:row]];
    } else if (activeTextField == levelTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[levelList objectAtIndex:row]];
    } else if (activeTextField == conditionTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[conditionList objectAtIndex:row]];
    } else if (activeTextField == roomPositionTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[roomPositionList objectAtIndex:row]];
    } else if (activeTextField == mainDoorDirectionTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[mainDoorList objectAtIndex:row]];
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (activeTextField == districtTF) {
        return [districtList count];
    } else if (activeTextField == hdbTypeTF)
    {
        return [hdbTypeList count];
    } else if (activeTextField == levelTF)
    {
        return [levelList count];
    } else if (activeTextField == conditionTF)
    {
        return [conditionList count];
    }
    return [districtList count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == districtTF) {
        return [districtList objectAtIndex:row];
    } else if (activeTextField == hdbTypeTF)
    {
        return [hdbTypeList objectAtIndex:row];
    } else if (activeTextField == levelTF)
    {
        return [levelList objectAtIndex:row];
    } else if (activeTextField == conditionTF)
    {
        return [conditionList objectAtIndex:row];
    } else if (activeTextField == roomPositionTF)
    {
        return [roomPositionList objectAtIndex:row];
    } else if (activeTextField == mainDoorDirectionTF)
    {
        return [mainDoorList objectAtIndex:row];
    }
    
    return [NSString stringWithFormat:@"%d", row+1];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:districtTF] || [textField isEqual:hdbTypeTF] || [textField isEqual:levelTF] || [textField isEqual:conditionTF] || [textField isEqual:roomPositionTF] || [textField isEqual:mainDoorDirectionTF])
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];

    
}

- (void) initTextfieldPlaceHolder
{
    districtTF.placeholder = @"District: ";
    areaTF.placeholder = @"Floor Size: ";
    priceTF.placeholder = @"Price: ";
    
    levelTF.placeholder = @"Level: ";
    
    
    
    hdbTypeTF.placeholder = @"HDB Type: ";
    leftLevelTF.placeholder = @"Left Level: ";
    mainDoorDirectionTF.placeholder = @"Main Door Direction: ";
    townTF.placeholder = @"Town: ";
    valuationTF.placeholder = @"Valuation";
    conditionTF.placeholder = @"Condition";
    roomPositionTF.placeholder = @"Room Position";
    
}


- (IBAction)search:(id)sender
{
    _property.district = districtTF.text;
    _property.area = areaTF.text;
    _property.price = priceTF.text;
    _property.groundLevel = levelTF.text;
    _property.hdbType = hdbTypeTF.text;
    _property.liftLevel= leftLevelTF.text;
    _property.mainDoorDirection = mainDoorDirectionTF.text;
    _property.roomPosition = roomPositionTF.text;
    _property.town = townTF.text;
    _property.valuation = valuationTF.text;
    _property.condition = conditionTF.text;
    
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NSLog(@"can run3");
    SalesListViewController *pushview = [newStoryboard instantiateViewControllerWithIdentifier:@"salesListView"];
    NSLog(@"can run4");
	//pushview.navBar.topItem.title = @"Search Result";
    pushview.propertySearch = _property;
    NSLog(@"can run5");
	[self presentViewController:pushview animated:YES completion:nil];
}

- (void) SearchSuccessfully : (NSArray*) objects
{
	NSMutableArray * propertyAray = [[NSMutableArray alloc] init];
	NSMutableArray * titles = [[NSMutableArray alloc] init];
	//NSLog(@"%i", [objects count]);
	for (PFObject *object in objects) {
		[propertyAray addObject:object];
		[titles addObject:[object objectForKey:@"propertyName"]];
	}
	[[DataEngine sharedInstance] SetPropertyDataArray:propertyAray];
	[[DataEngine sharedInstance] SetPropertyNameList:titles];
    
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    SalesListViewController *pushview = [newStoryboard instantiateViewControllerWithIdentifier:@"salesListView"];
	pushview.navBar.topItem.title = @"Search Result";
	[self presentViewController:pushview animated:YES completion:nil];
}

- (void) SearchFailed : (NSString*) errorString
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:errorString
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}


@end
