//
//  DatabaseEngine.h
//  PropertyV1
//
//  Created by Jonathan Le on 27/8/13.
//  Copyright (c) 2013 Jonathan Le. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "ViewController.h"
#import "SignUpViewController.h"
#import "SalesRowViewController.h"
#import "MainViewController.h"
#import "ListingGalleryViewController.h"
#import "ChangeUserInfoViewController.h"
#import "ResetPasswordViewController.h"
#import "Property.h"
#import "User.h"
#import "Agent.h"
#import "Client.h"
#import "SearchPropertyVC.h"
#import "SalesListViewController.h"
#import "MyGroupVC.h"
#import "ChatCell.h"
#import "PhoneBookVC.h"
#import "AddGroupVC.h"
#import "ChatRoomVC.h"
#import "GroupInfoVC.h"
#import "ParticipantCell.h"
#import "MessageListVC.h"
#import "UserListVC.h"
#import "MessageInfo.h"
#import "MessageChatVC.h"
#import "DetailPropertyTabBarVC.h"
#import "MyFavoriteVC.h"
#import "AddNewAlertVC.h"
#import "MyAlertsVC.h"
#import "AlertInfo.h"
#import "AlertPropertyListVC.h"
#import "Partner.h"
#import "AddPartnerVC.h"
#import "PartnersVC.h"
#import "RequestMeetupClientVC.h"
#import "RequestMeetupAgentVC.h"
#import "ScheduleEvent.h"
#import "ScheduleListVC.h"
#import "ScheduleCell.h"
#import "CalendarDisplayCV.h"
#import "SpecialDealListVC.h"
#import "SearchCommercialVC.h"
#import "SearchHDBVC.h"
#import "SearchIndustryVC.h"
#import "NewGroupListVC.h"
#import "NewAddGroupVC.h"
#import "DetailGroupMemberVC.h"
#import "DetailGroupListingVC.h"
#import "DetailPropertyVC.h"
#import "DetailComercialVC.h"
#import "DetailHDBVC.h"
#import "DetailIndustrialVC.h"
#import "DetailLandVC.h"
#import "EditPartnerVC.h"




@interface DatabaseEngine : NSObject

@property (nonatomic, retain) NSMutableArray* propertyAray;


+ (DatabaseEngine*)sharedInstance;

//Authentication function
-(void)UserSignUp:(NSDictionary*)  userInfoDict userType:(NSString*) userType signupView: (UIViewController*) signupView;
- (void) UserLogin: (NSString*) username : (NSString*) password : (MyUIViewController*) loginView;
- (void) UserLogout;
- (void) ResetPasswordRequest : (NSString*) email : (ResetPasswordViewController*) view;
- (void) ChangeNameByPhone: (NSString*) phone Firstname: (NSString*) fname Lastname: (NSString*) lname email:(NSString *)email address:(NSString *)address view:(MyUIViewController *)view;
- (void) ChangeUserInfo: (User*) client
			  firstname:(NSString*) firstName lastName:(NSString*) lastName phone: (NSString*) phone email: (NSString*) email changeUserView: (MyUIViewController*) view;

//property function
- (void) AddProperty: (Property*) propertyIn;

- (void) SearchProperty: (Property*) propertyIn : (SalesListViewController*) salesRowView;
- (void) deleteProperty: (Property*) propertyIn;


//Duy Implementation Load ICon
- (void) LoadIcon: (PFObject*) proper : (SalesListViewController*) sales : (Property*) curProper;
- (void) LoadIconPro: (PFObject*) proper : (ProPortfolioViewController*) proView : (Property*) curProper;


- (void) uploadImage: (UIImage*) image : (Property*) propertyInstance;
//- (void)downloadImages : (PFObject*) property : (AddPropertyImageViewController*) addPropertyImageView;
- (void)downloadImages: (ListingGalleryViewController*) galleryView :(Property *)propertyInstance;
- (void) GetMyListing : (MyUIViewController*) portView;
- (void) GetUserPropertyList : (User*) user;
- (void) GetAgentClientPropertyList : (Agent*) agent : (Client*) client;
- (void) SetPropertyViewed: (PFObject*) property;

//favorite property function
- (void) SetFavoriteProperty: (PFObject*) property;
- (void) DeleteFavoriteProperty: (PFObject*) property;
- (void) GetFavoriteProperty;

//Watchlist function
- (void) AddWatchlist: (PFObject*) user : (NSString*) subject : (NSString*) transaction :
(NSString*) type : (NSString*) project :(NSString*) district :
(NSString*) blkUnitNo : (NSString*) location : (NSString*) tenure : (NSString*) price :
(NSString*) area : (NSString*) level : (NSString*) bedroom : (NSString*) remarks;
- (void) DeteleWatchlist: (PFObject*) watchlist;

//Group function
- (void) CreateGroup: (NSString*) groupName;
- (void) GroupShareProperty: (PFObject*) property : (PFObject*) group;
- (void) GetGroupSharingList: (PFObject*) group;
- (void) AddGroupMember: (PFObject*) user : (PFObject*) group;
- (void) GroupMemberStatus: (NSString*) status : (PFObject*) user : (PFObject*) group;
-(void) sendNotificationToGroup:(PFObject *) groupInfo : (PFObject *) contentObject;

//Agent, client link
-(NSString *) removeClientString:(NSString *) input;
-(void) addOrUpdateClient:(User *)user processingArray: (NSMutableArray *)processingArray view:(MyUIViewController *)view;
- (BOOL) addNewClientToDatabase:(User *)user processingArray: (NSMutableArray *)processingArray view:(MyUIViewController *)view;
-(void)ClientSignUp:(NSMutableDictionary*) userInfoDict view: (MyUIViewController*) view;
-(BOOL) updateUser:(User *)user view:(MyUIViewController *)view;

- (void) AddAgentClientLink: (PFObject*) agent : (PFObject*) client : (MyUIViewController *) view;
- (void) GetMyAgentList : (Client*) clientUser;
- (void) GetMyClientList : (Agent*) agentUser;


//MyGroup
- (void) AddGroupInfo : (PFObject*) user : (NSString*) Title : (NSArray*) phoneList : (AddGroupVC*) addgroupView;
- (void) LoadGroupInfo : (PFObject*) user : (MyGroupVC*) myGroupView;
- (void) LoadPhoneBookList : (PhoneBookVC*) phoneBookView;
- (void) loadPhoneBookList2 : (PhoneBookVC*) phoneBookView : (NSArray*) curList;
- (void) loadAllAgent : (PhoneBookVC*) phoneBookView : (NSMutableArray *) unneededArray;

- (void) sendMessage : (PFObject*) user : (NSString *) content : (PFObject*) chatRoomInfo : (ChatRoomVC*) chatRoomView;

- (void) LoadMessage : (PFObject*) chatRoomInfo : (ChatRoomVC*) chatRoomView;

- (void) RefreshMessage : (PFObject*) chatRoomInfo : (NSArray*) curChatList : (NSArray*) curUserList : (ChatRoomVC*) chatRoomView;

- (void) LoadParticipant : (PFObject *) groupInfo : (GroupInfoVC*) groupInfoView;

- (void) AddParticipants : (NSMutableArray*) participants : (PFObject*) chatRoomInfo : (PhoneBookVC*) phoneBookView;
- (void) DeleteParticipants : (PFObject*) groupInfo : (PFUser*) curUser : (ParticipantCell*) cell;
-(void) getPropertyWithChatCell: (ChatCell *) cCell;


//Message
- (void) getMessageListByUser : (PFUser*) curUser : (MessageListVC *) messageListView;
- (void) getAdditionalMessageListByUser : (PFUser *) curUser : (MessageListVC*) messageListView : (NSMutableArray*) curList;


- (void) addMessageConnector : (PFUser*) curUser : (PFUser *) endUser : (UserListVC*) userListView;
- (void) validateMessageConnector : (PFUser*) curUser : (PFUser*) endUser : (UserListVC*) userListView;
- (void) validateMessageConnector2 : (PFUser*) curUser : (PFUser*) endUser : (UserListVC*) userListView;
- (void) loadAllUser : (UserListVC*) userListView : (PFUser *) curUser;
- (void) chat : (MessageInfo*) messageInfo : (User*) curUser : (MessageChatVC*) messageChatView : (NSString*) content;

- (void) loadMessageList : (MessageInfo*) messageInfo : (MessageChatVC*) messageChatView;

- (void) refreshMessageList : (MessageInfo*) messageInfo : (NSMutableArray*) curList : (MessageChatVC*) messageChatView;

//My shortlisted

- (void) addFavorite : (User*) curUser : (Property*) curProperty : (DetailPropertyTabBarVC*) detailTab;
- (void) validateFavorite : (User*) curUser : (Property*) curProperty : (DetailPropertyTabBarVC*) detailTab;

- (void) unaddFavorite : (PFObject*) favoriteInfo : (DetailPropertyTabBarVC*) detailTab;

- (void) loadFavorite : (User*) curUser : (MyFavoriteVC*) myFavoriteView;


//My alert
- (void) addAlert : (AddNewAlertVC*) addNewAlertView : (NSString*) subject : (NSString*) transaction : (NSString*) type : (NSString*) project : (NSString*) district : (NSString*) blockUnitNo : (NSString*) location : (NSString*) tenure : (NSString*) price : (NSString*) area : (NSString*) level : (NSString*) bedroom : (NSString*) remarks : (User *) curUser;

- (void) addNewAlert : (AddNewAlertVC*) addNewAlertView : (AlertInfo*) newAlert : (User*) curUser;

- (void) loadAlerts : (MyAlertsVC*) myAlertsView : (User*) curUser;

- (void) searchAlert: (AlertPropertyListVC*) alertPropertyList : (AlertInfo*) alertInfo;

- (void) editAlert: (AddNewAlertVC *) addNewAlertView : (AlertInfo*) alertInfo;

- (void) getAllAlertList : (Property*) curProperty;


// parner

- (void) addPartner : (AddPartnerVC *) addPartnerView : (Partner*) curParner;

- (void) getPartners : (PartnersVC*) partnersView : (NSString*) type : (User*) curUser;

- (void) editPartners : (EditPartnerVC *) editView : (Partner*) curParner;

// Calendar

- (void)requestMeetup:(RequestMeetupClientVC *)requestMeetupView :(ScheduleEvent *)scheduleEvent;
- (void)getRequestList:(ScheduleListVC*) scheduleListView : (User *) curUser;
- (void)getRequestListForCalendar:(CalendarDisplayCV*) scheduleListView : (User *) curUser;
- (void)getUserRequest:(PFUser*) user : (ScheduleCell*) scheduleCell;
- (void)acceptAgent: (RequestMeetupAgentVC *)requestMeetupView :(ScheduleEvent *)scheduleEvent;

- (void)acceptRequester: (RequestMeetupClientVC *)requestMeetupView :(ScheduleEvent *)scheduleEvent;
//Special Deal

- (void)getSpecialDeal : (SpecialDealListVC * ) specialDealView;



//New Group List
- (void) getGroupList: (NewGroupListVC * ) groupListView :  (User *) curUser;
- (void)getGroupByMember:(NewGroupListVC *)groupListView :(User *)curUser : (NSMutableArray *) curGroupList;
- (void) validateTitle: (NewAddGroupVC * ) aGroupListView: (NSString *) title;
- (void) addNewGroup: (NewAddGroupVC * ) aGroupListView: (NSMutableDictionary *) group ;
- autoAddPropertiesToNewAcceptGroup: (PFObject *)curGroup;
- (void) getMembers: (DetailGroupMemberVC *) groupMemberView : (PFObject *) curGroup;
- (void) getGroupSharing : (DetailGroupListingVC *) groupTabbarView : (PFObject *) curGroup;
- (void) addToGroupShareIndustry:(DetailIndustrialVC *)groupListingVIew;
- (void) valiadteIndustry: (DetailIndustrialVC *) groupListingView;
- (void)valiadteLand:(DetailLandVC *)groupListingView;
- (void) addToGroupShareHDB:(DetailHDBVC *)groupListingVIew;
- (void) valiadteHDB: (DetailHDBVC *) groupListingView;
- (void) addToGroupShareCommercial:(DetailComercialVC *)groupListingVIew;
- (void) valiadteCommercial: (DetailComercialVC *) groupListingView;
- (void) addToGroupSharePR:(DetailPropertyVC *)groupListingVIew;
- (void) valiadtePR: (DetailPropertyVC *) groupListingView;
- (void) addMembers: (PhoneBookVC *) phoneBookVC : (NSMutableArray *) selectedMember;
- (void) deleteMember: (DetailGroupMemberVC *) groupMemberVC : (PFUser *) curUser;

@end
