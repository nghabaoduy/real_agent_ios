//
//  SearchCommercialVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/24/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"


@interface SearchCommercialVC : UITableViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    IBOutlet UITextField * propertyTypeTF;
    IBOutlet UITextField * listingTypeTF;
    IBOutlet UITextField * districtTF;
    IBOutlet UITextField * commercialTypeTF;
    IBOutlet UITextField * areaTF;
    IBOutlet UITextField * priceTF;
    IBOutlet UITextField * tenureTF;
    IBOutlet UITextField * rentedPriceTF;
    IBOutlet UITextField * projectNameTF;
    IBOutlet UITextField * psfTF;
    IBOutlet UITextField * topYearTF;
    IBOutlet UITextField * rentalYieldTF;
    
    NSArray * commercialTypeList;
    NSArray * rentalYieldList;
    NSArray * districtList;
    NSArray * tenureList;
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
    
}


@property (nonatomic, strong) Property * property;
@end
