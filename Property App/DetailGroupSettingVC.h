//
//  DetailGroupSettingVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>

@interface DetailGroupSettingVC : UITableViewController{
    
    IBOutlet UISwitch *switch1;
    IBOutlet UISwitch *switch2;
    IBOutlet UISwitch *switch3;
}


@property (nonatomic, strong) PFObject * curGroup;
@end
