//
//  MyClientViewController.m
//  PhoneBookController
//
//  Created by Brian on 8/9/13.
//  Copyright (c) 2013 Brian. All rights reserved.
//

#import <AddressBook/AddressBook.h>

#import "MyClientViewController.h"
#import "ClientSearchViewController.h"
#import "PersonInfoViewController.h"
#import "User.h"

#import "MyUIEngine.h"
#import "DataEngine.h"
#import "Agent.h"
#import "DatabaseEngine.h"

@interface MyClientViewController ()

@end

@implementation MyClientViewController


@synthesize tableView;
@synthesize addBtn;
@synthesize contentArray = _contentArray;
@synthesize barSeperatorIndex, agentClientNameList, objectInEachSection, searchBar;

Agent* curAgent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //float currentVersion = 7.0;
    //if ([[[UIDevice currentDevice] systemVersion] floatValue] >= currentVersion) {
        // iOS 7
        self.navBar.frame = CGRectMake(self.navBar.frame.origin.x, self.navBar.frame.origin.y, self.navBar.frame.size.width, 64);
    //}
    
    //[[MyUIEngine sharedUIEngine] customizeViewSetting:self :self.tableView];
    [addBtn setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"HelveticaNeue-Bold" size:25.0], UITextAttributeFont,nil] forState:UIControlStateNormal];
    
    _contentArray = [[NSMutableArray alloc] init];
	curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
    self.agentClientList = [[NSMutableArray alloc] init];
	self.agentClientPropertyList = [[NSMutableArray alloc] init];
	
	//get user array
	for (Client* client in curAgent.myClientList) {
		[self.agentClientList addObject:client];
	}
	NSLog(@"Array of users %@", self.agentClientList);
	

	[self generateDisplayTableList];
	
	
}
-(void) generateDisplayTableList
{
    //Array of user name and sort
	agentClientNameList = [[NSMutableArray alloc] init];
	for (User* u in self.agentClientList) {
		[agentClientNameList addObject:u.fullName];
        //NSLog(@"u.fullName = %@",u.fullName);
	}
	[agentClientNameList sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	
	//NSLog(@"agentClientNameList = %@",agentClientNameList);
	//---create the index---
    barSeperatorIndex = [[NSMutableArray alloc] init];
    objectInEachSection = [[NSMutableArray alloc] init];
    for (int i=0; i<[agentClientNameList count]; i++){
        //---get the first char of each state---
        char alphabet = [[agentClientNameList objectAtIndex:i] characterAtIndex:0];
        NSString *uniChar = [NSString stringWithFormat:@"%c", alphabet];
        
        //---add each letter to the index array---
        if (![barSeperatorIndex containsObject:uniChar])
        {
            [barSeperatorIndex addObject:uniChar];
            // NSLog(@"uniChar = %@",uniChar);
        }
    }
	
	for (int i = 0; i < barSeperatorIndex.count; i++) {
		NSString *alphabet = [barSeperatorIndex objectAtIndex:i];
		//NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", alphabet];
		//NSArray *users = [agentClientNameList filteredArrayUsingPredicate:predicate];
        NSMutableArray *sortedNameArray =[[NSMutableArray alloc] init];
        for (NSString *name in self.agentClientNameList) {
            if ([name hasPrefix:alphabet]) {
                [sortedNameArray addObject:name];
            }
        }
        [objectInEachSection addObject:sortedNameArray];
		
		
	}
	//NSLog(@"objectInEachSection = %@",objectInEachSection);
	//search bar
	//---set the correction type for the search bar---
    searchBar.autocorrectionType = UITextAutocorrectionTypeYes;
    
    //---used for storing the search result---
    searchResult = [[NSMutableArray alloc] init];
    
    isSearchOn = NO;
    canSelectRow = YES;
    
    [tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	if (isSearchOn)
        return 1;
    else
		return [barSeperatorIndex count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (isSearchOn) {
        return [searchResult count];
    } else
		return [[objectInEachSection objectAtIndex:section] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//NSLog(@"blh blah %@",[[objectInEachSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]);
    static NSString *CellIdentifier = @"ClientVCCell";
    
	UITableViewCell *cell = [[MyUIEngine sharedUIEngine] getCell:self.tableView :CellIdentifier :indexPath.row :self.contentArray.count :@""];
	
	if (isSearchOn) {
        NSString *cellValue = [searchResult objectAtIndex:indexPath.row];
        cell.textLabel.text = cellValue;
       // NSLog(@"cellValue = %@",cellValue);
    } else {
		cell.textLabel.text = [[objectInEachSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
       // NSLog(@"cellValue = %@",cell.textLabel.text);
	}
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if (isSearchOn)
        return nil;
	else
		return [barSeperatorIndex objectAtIndex:section];
	
}

- (NSArray*) sectionIndexTitlesForTableView:(UITableView *)tableView {
	if (isSearchOn)
        return nil;
	else
		return barSeperatorIndex;
}


- (IBAction)AddClient:(id)sender
{
    ClientSearchViewController* view = [self.storyboard instantiateViewControllerWithIdentifier:@"clientSearchView"];
    view.clientView = self;
    [self presentViewController:view animated:YES completion:nil];
    
}



- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {

}

//---fired when the user types something into the Search Bar---
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText length] == 0) {
        isSearchOn = NO;
        canSelectRow = NO;
        self.tableView.scrollEnabled = NO;
        [self.tableView reloadData];
        
    }

}
-(void) searchBarSearchButtonClicked:(UISearchBar *) searchBar{
    
    NSLog(@"searchBarSearchButtonClicked");
    [searchResult removeAllObjects];
	
    //---if there is something to search for---
    if ([searchBar.text length] > 0) {
        isSearchOn = YES;
        canSelectRow = YES;
        self.tableView.scrollEnabled = YES;
        [self searchTableView];
    }
    else {
        //---nothing to search---
        isSearchOn = NO;
        canSelectRow = NO;
        self.tableView.scrollEnabled = NO;
    }
	[self.tableView reloadData];
    [searchBar resignFirstResponder];
}

//---performs the searching using the array of states---
- (void) searchTableView {
    //---clears the search result---
    [searchResult removeAllObjects];
    
    for (NSString *str in agentClientNameList)
    {
        NSRange titleResultsRange =
		[str rangeOfString:searchBar.text options:NSCaseInsensitiveSearch];
        if (titleResultsRange.length > 0)
            [searchResult addObject:str];
    }
    [self searchPropertyAddress];
    
}

-(void) searchPropertyAddress
{
    for (NSMutableArray *proArray in curAgent.myClientPropertyList)
    {
        
        for (Property *prop in proArray)
        {
            NSRange titleResultsRange =
            [prop.address rangeOfString:searchBar.text options:NSCaseInsensitiveSearch];
            if (titleResultsRange.length > 0)
            {
                if (![self isUserAlreadyInSearch:prop.adder]) {
                    [searchResult addObject:prop.adder.fullName];
                    NSLog(@"%@ - %@ - %@",searchBar.text,prop.adder.fullName, prop.address);
                }
            }
        }
    }
}
-(BOOL) isUserAlreadyInSearch:(User *) user
{
    BOOL hasInSearch = NO;
    for (NSString* fullname in searchResult) {
        if ([user.fullName isEqualToString:fullname]) {
            hasInSearch = YES;
            return hasInSearch;
        }
    }
    return hasInSearch;
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
	self.searchBar.text = @"";
	
    isSearchOn = NO;
    canSelectRow = YES;
	
    self.tableView.scrollEnabled = YES;
    self.navigationItem.rightBarButtonItem = nil;
    
    //---hides the keyboard---
    [self.searchBar resignFirstResponder];
	
    //---refresh the Table View---
    [self.tableView reloadData];
}
-(void) refreshData
{
    [self viewDidLoad];
}
-(void) refreshView
{
    [self viewDidLoad];
}
-(void) addClientTemporary:(NSArray *)newUsers
{
    NSLog(@"newUsers = %@",newUsers);
    for (User *newUser in newUsers) {
        if (![self.agentClientList containsObject:newUser]) {
            [self.agentClientList addObject:newUser];
        }
    }
    [self generateDisplayTableList];
    NSLog(@"addClientTemporary = %@",self.agentClientList);
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"user cell selected");
    if ([segue.identifier isEqualToString:@"showInfo"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        PersonInfoViewController *destViewController = segue.destinationViewController;
        
        NSString *name;
        if (isSearchOn) {
            name = [searchResult objectAtIndex:indexPath.row];
            // NSLog(@"cellValue = %@",cellValue);
        } else {
            name =[NSString stringWithFormat:@"%@",[[objectInEachSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]];
        }
        
        
        NSLog(@"name = %@",name);
        destViewController.user = [self getUserByName:name];
        NSLog(@"user = %@", destViewController.user.fullName);
    }
}
-(User *) getUserByName:(NSString *) name
{
    for (User *user in self.agentClientList) {
        if ([user.fullName isEqualToString:name]) {
            return user;
        }
    }
    return nil;
}
- (IBAction)importSyncClients:(id)sender {
    [self startLoading];
    [self loadAddressBook];
    
}
- (void) analyzeImportedClients
{
    [self startLoading];
    
    for (User *user in self.addBookArray) {
        [[DatabaseEngine sharedInstance] addNewClientToDatabase:user processingArray:nil view:self];
    }
    if (self.addBookArray.count == 0) {
        [self finishLoading];
    }
}

-(void) actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
}
-(void) actionFail:(NSString *)message
{
    [super actionFail:message];
}
            
            
- (void)getPersonOutOfAddressBook
{
    self.addBookArray = [[NSMutableArray alloc] init] ;
    CFErrorRef error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    if (addressBook != nil)
    {
        NSLog(@"AddressBook loads Succesful.");
        
        NSArray *allContacts = (__bridge_transfer NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
        NSLog(@"contact count = %i",allContacts.count);
        NSUInteger i = 0;
        for (i = 0; i < [allContacts count]; i++)
        {
            User *user = [[User alloc] init];
            //name
            ABRecordRef contactPerson = (__bridge ABRecordRef)allContacts[i];
            
            NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty);
            NSString *lastName =  (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonLastNameProperty);
            NSString *fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];

            
            NSString *keyword = @"client";
            NSRange titleResultsRange =
            [fullName rangeOfString:keyword options:NSCaseInsensitiveSearch];
            if(titleResultsRange.length > 0)
            {
                NSLog(@"user.fullname = %@",user.fullName);

                user.firstName = [[DatabaseEngine sharedInstance] removeClientString:firstName];
                user.lastName = [[DatabaseEngine sharedInstance] removeClientString:lastName];
                user.fullName = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
                NSLog(@"user.fullname = %@",user.fullName);
                
                
                //address
                
                
                ABMultiValueRef address = ABRecordCopyValue(contactPerson, kABPersonAddressProperty);
                
                NSArray * theArray  = (__bridge_transfer NSArray*)ABMultiValueCopyArrayOfAllValues(address);
                
                NSLog(@"total %i", [theArray count]);
                
                // check if array count = 0;
                NSDictionary *theDict = [theArray objectAtIndex:0];
                
                const NSUInteger theCount = [theDict count];
                id __unsafe_unretained objects[theCount];
                id __unsafe_unretained keys[theCount];
                
                [theDict getObjects:objects andKeys:keys];
                
                NSString *theAddress;
                
                theAddress = [NSString stringWithFormat:@" %@, %@ %@",
                              [theDict objectForKey:(NSString *)kABPersonAddressStreetKey] == nil?@"":[theDict objectForKey:(NSString *)kABPersonAddressStreetKey],
                              [theDict objectForKey:(NSString *)kABPersonAddressCityKey] == nil?@"":[theDict objectForKey:(NSString *)kABPersonAddressCityKey],
                              [theDict objectForKey:(NSString *)kABPersonAddressStateKey] == nil?@"":[theDict objectForKey:(NSString *)kABPersonAddressStateKey]
                              //[theDict objectForKey:(NSString *)kABPersonAddressZIPKey] == nil?@"":[theDict objectForKey:(NSString *)kABPersonAddressZIPKey],
                              // [theDict objectForKey:(NSString *)kABPersonAddressCountryKey] == nil?@"":[theDict objectForKey:(NSString *)kABPersonAddressCountryKey]
                              ];
                
                
                user.address = theAddress;
                NSLog(@"address = %@", user.address);
                //email
                
                ABMultiValueRef emails = ABRecordCopyValue(contactPerson, kABPersonEmailProperty);
                
                NSUInteger j = 0;
                for (j = 0; j < ABMultiValueGetCount(emails); j++)
                {
                    NSString *email = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emails, j);
                    NSLog(@"email [%i] = %@",j,email);
                    if (j == 0)
                    {
                        user.email = email;
                       // NSLog(@"person.homeEmail = %@ ", person.homeEmail);
                    }
                    else if (j==1)
                        user.email = email;
                }
                //phone no
                NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
                ABMultiValueRef multiPhones = ABRecordCopyValue(contactPerson,kABPersonPhoneProperty);
                for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);++i) {
                    CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                    NSString *phoneNumber = (__bridge_transfer NSString *) phoneNumberRef;
                    
                    [phoneNumbers addObject:phoneNumber];
                }
                if ([phoneNumbers count]>0) {
                    NSString* phoneNumber = [[DataEngine sharedInstance] cleanPhoneNo:[phoneNumbers objectAtIndex:0]];
                    user.phone = phoneNumber;
                    NSLog(@"phone no = %@",[phoneNumbers objectAtIndex:0]);
                }
                //finish
                NSLog(@"user.phone = %@",user.phone);
                if (user.phone != nil) {
                    [self.addBookArray addObject:user];
                }
                NSLog(@"self.addBookArray = %@",self.addBookArray);
                
            }
        }
        for (User *contact in self.addBookArray) {
            [[DatabaseEngine sharedInstance] removeClientString:contact.firstName];
            [[DatabaseEngine sharedInstance] removeClientString:contact.lastName];
            [[DatabaseEngine sharedInstance] removeClientString:contact.fullName];
            NSLog(@"addressbook user = %@",contact);
        }
        
        NSLog(@"Client address Array = %@",self.addBookArray);
        
        
        
        [self finishLoading];
        [self analyzeImportedClients];
        
        //8
        CFRelease(addressBook);
    }
    else
    {
        //9
        NSLog(@"Error reading Address Book");
    }
}
-(void)loadAddressBook
{
    // Request authorization to Address Book
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            // First time access has been granted, add the contact
            [self getPersonOutOfAddressBook];
            [self.tableView reloadData];
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        // The user has previously given access, add the contact
        [self getPersonOutOfAddressBook];
    }
    else {
        // The user has previously denied access
        // Send an alert telling user to change privacy setting in settings app
    }
    
}



@end
