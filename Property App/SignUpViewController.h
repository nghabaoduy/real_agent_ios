//
//  SignUpViewController.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "ViewController.h"
@interface SignUpViewController : UIViewController <UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIScrollView *theScrollView;
    
}
@property (nonatomic, assign) UITextField *activeTextField;

- (IBAction)dismissKeyboard:(id)sender;
@property (nonatomic, strong) IBOutlet UITextField * password;
@property (nonatomic, strong) IBOutlet UITextField * repassword;
@property (nonatomic, strong) IBOutlet UITextField * phone;
@property (nonatomic, strong) IBOutlet UITextField * email;
@property (nonatomic, strong) IBOutlet UITextField * agentPhone;
@property (nonatomic, strong) IBOutlet UITextField * firstname;
@property (nonatomic, strong) IBOutlet UITextField * lastname;

@property ViewController *loginView;

- (void) SignUpSuccessful;
- (void) SignUpFail: (NSString*) error;

- (void) SearchAgentFailed : (NSString*) errorString;
@end
