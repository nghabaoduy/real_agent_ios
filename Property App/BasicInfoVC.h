//
//  BasicInfoVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 21/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"

@interface BasicInfoVC : UITableViewController
{
    IBOutlet UITextField * postalCode;
}

@property (nonatomic, strong) Property * property;

@property BOOL test;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, retain) NSString* requestType;
@property (nonatomic, retain) NSMutableArray* data;
@end
