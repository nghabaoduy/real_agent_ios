//
//  MyFavoriteVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/19/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUITableViewController.h"
#import "Property.h"
#import "User.h"

@interface MyFavoriteVC : MyUITableViewController

@property (nonatomic, retain) NSMutableArray * allPropertyList;
@property (nonatomic, retain) User * curUser;
@end
