//
//  SearchLandVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/14/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "SearchLandVC.h"
#import "DataEngine.h"
#import "DatabaseEngine.h"

@interface SearchLandVC ()

@end

@implementation SearchLandVC
- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self LoadDefault];
    rentalYieldList = [[NSArray alloc] initWithObjects:
                       @"",
                       @"3% & Below",
                       @"3-5%,5-10%,",
                       @"10% & Above",
                       nil];
    
    districtList = [[DataEngine sharedInstance] getDistrictList];
    tenureList = [[NSArray alloc] initWithObjects:
                  @"",
                  @"FH",
                  @"999",
                  @"99",
                  @"60",
                  @"30",
                  @"<30yrs",
                  nil];
    landTypeList  = [[NSArray alloc]initWithObjects:
                     @"",
                     @"Terrace",
                     @"Detached",
                     @"Semi-D",
                     @"Conner Terrace",
                     @"Bungalow",
                     @"GCB",
                     @"Town House"
                     @"Conversation House",
                     @"Land/Building", nil];
    
    
    
    funishingList = [[NSArray alloc]initWithObjects:
                     @"",
                     @"Bare",
                     @"Partial",
                     @"Full Fitted", nil];
    
    mainDoorFacingList = [[NSArray alloc]initWithObjects:
                          @"",
                          @"North",
                          @"East",
                          @"West",
                          @"South", nil];
    
    mainGateFacingList = mainDoorFacingList;
    
    carParkList = [[NSArray alloc]initWithObjects:
                   @"No",
                   @"Inside",
                   @"Outside", nil];
    
    
    gardenList = [[NSArray alloc]initWithObjects:
                  @"No",
                  @"Yes", nil];
    
    swimmingPoolList = gardenList;
    basementList = gardenList;
    roofTerranceList = gardenList;
    [self initTextfieldPlaceHolder];
}

- (void) initTextfieldPlaceHolder
{
    districtTF.placeholder = @"District: ";
    landTypeTF.placeholder = @"Land Type: ";
    areaTF.placeholder = @"Floor Size: ";
    priceTF.placeholder = @"Price: ";
    tenureTF.placeholder = @"Tenure: ";
    
    rentedPriceTF.placeholder = @"Rental Price: ";
    
    bedroom.placeholder = @"Bedroom: ";
    
    projectNameTF.placeholder = @"Project Name: ";
    topYearTF.placeholder = @"Top Year: ";
    psfTF.placeholder = @"PSF: ";
    rentalYieldTF.placeholder = @"Rental Yield: ";
    
    funishing.placeholder = @"Furnish: ";
    landSize.placeholder = @"Land Size: ";
    mainDoorFacing.placeholder = @"Main Door Facing: ";
    mainGateFacing.placeholder = @"Main Gate Facing: ";
    carPark.placeholder = @"Car Park: ";
    garden.placeholder = @"Garden: ";
    swimmingPool.placeholder = @"Swimming Pool: ";
    basement.placeholder = @"Base: ";
    roofTerrance.placeholder = @"Roof Terrance: ";
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) LoadDefault
{
    [propertyTypeTF setText: _property.propertyType];
    [listingTypeTF setText:_property.listingType];
}

-(void)dismissKeyboard {
    [activeTextField resignFirstResponder];
}



- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
}

-(void)categoryDoneButtonPressed{
    //categoryLable.text = selectedCategory;
    if (activeTextField == districtTF) {
        [districtTF setText:selectedCategory];
    } else if (activeTextField == tenureTF)
    {
        [tenureTF setText:selectedCategory];
    } else if (activeTextField == rentalYieldTF)
    {
        [rentalYieldTF setText:selectedCategory];
    } else if (activeTextField == landTypeTF)
    {
        [landTypeTF setText:selectedCategory];
    }
    else if (activeTextField == funishing)
    {
        [funishing setText:selectedCategory];
    }
    else if (activeTextField == mainDoorFacing)
    {
        [mainDoorFacing setText:selectedCategory];
    }
    else if (activeTextField == mainGateFacing)
    {
        [mainGateFacing setText:selectedCategory];
    }
    else if (activeTextField == carPark)
    {
        [carPark setText:selectedCategory];
    }
    else if (activeTextField == garden)
    {
        [garden setText:selectedCategory];
    }
    else if (activeTextField == swimmingPool)
    {
        [swimmingPool setText:selectedCategory];
    }
    else if (activeTextField == basement)
    {
        [basement setText:selectedCategory];
    }
    else if (activeTextField == roofTerrance)
    {
        [roofTerrance setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if (activeTextField == districtTF) {
        selectedCategory = [NSString stringWithFormat:@"%@",[districtList objectAtIndex:row]];
    } else if (activeTextField == tenureTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[tenureList objectAtIndex:row]];
    } else if (activeTextField == rentalYieldTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[rentalYieldList objectAtIndex:row]];
    } else if (activeTextField == landTypeTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[landTypeList
                                                             objectAtIndex:row]];
    } else if (activeTextField == funishing)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[funishingList objectAtIndex:row]];
    } else if (activeTextField == mainDoorFacing)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[mainDoorFacingList objectAtIndex:row]];
    } else if (activeTextField == mainGateFacing)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[mainGateFacingList objectAtIndex:row]];
    } else if (activeTextField == carPark)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[carParkList objectAtIndex:row]];
    } else if (activeTextField == garden)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[gardenList objectAtIndex:row]];
    } else if (activeTextField == swimmingPool)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[swimmingPoolList objectAtIndex:row]];
    } else if (activeTextField == basement)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[basementList objectAtIndex:row]];
    } else if (activeTextField == roofTerrance)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[roofTerranceList objectAtIndex:row]];
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (activeTextField == districtTF) {
        return [districtList count];
    } else if (activeTextField == tenureTF)
    {
        return [tenureList count];
    } else if (activeTextField == rentalYieldTF)
    {
        return [rentalYieldList count];
    } else if (activeTextField == landTypeTF)
    {
        return [landTypeList count];
    } else if (activeTextField == funishing)
    {
        return [funishingList count];
    } else if (activeTextField == mainDoorFacing)
    {
        return [mainDoorFacingList count];
    } else if (activeTextField == mainGateFacing)
    {
        return [mainGateFacingList count];
    } else if (activeTextField == carPark)
    {
        return [carParkList count];
    } else if (activeTextField == garden)
    {
        return [gardenList count];
    } else if (activeTextField == swimmingPool)
    {
        return [swimmingPoolList count];
    } else if (activeTextField == basement)
    {
        return [basementList count];
    } else if (activeTextField == roofTerrance)
    {
        return [roofTerranceList count];
    }
    return [districtList count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == districtTF) {
        return [districtList objectAtIndex:row];
    } else if (activeTextField == tenureTF)
    {
        return [tenureList objectAtIndex:row];
    } else if (activeTextField == rentalYieldTF)
    {
        return [rentalYieldList objectAtIndex:row];
    } else if (activeTextField == landTypeTF)
    {
        return [landTypeList objectAtIndex:row];
    } else if (activeTextField == funishing)
    {
        return [funishingList objectAtIndex:row];
    } else if (activeTextField == mainDoorFacing)
    {
        return [mainDoorFacingList objectAtIndex:row];
    } else if (activeTextField == mainGateFacing)
    {
        return [mainGateFacingList objectAtIndex:row];
    } else if (activeTextField == carPark)
    {
        return [carParkList objectAtIndex:row];
    } else if (activeTextField == garden)
    {
        return [gardenList objectAtIndex:row];
    } else if (activeTextField == swimmingPool)
    {
        return [swimmingPoolList objectAtIndex:row];
    } else if (activeTextField == basement)
    {
        return [basementList objectAtIndex:row];
    } else if (activeTextField == roofTerrance)
    {
        return [roofTerranceList objectAtIndex:row];
    }
    
    return [NSString stringWithFormat:@"%d", row+1];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:districtTF] || [textField isEqual:tenureTF] || [textField isEqual:rentalYieldTF] || [textField isEqual:landTypeTF] || [textField isEqual:funishing] || [textField isEqual:mainDoorFacing] || [textField isEqual:mainGateFacing] || [textField isEqual:carPark] || [textField isEqual:garden] || [textField isEqual:swimmingPool] || [textField isEqual:basement] || [textField isEqual:roofTerrance])
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (IBAction)search:(id)sender
{
    _property.district = districtTF.text;
    _property.landType = landTypeTF.text;
    _property.area = areaTF.text;
    _property.price = priceTF.text;
    _property.tenure = tenureTF.text;
    _property.rentedPrice = rentedPriceTF.text;
    _property.projectName = projectNameTF.text;
    _property.PSF = psfTF.text;
    _property.topYear = topYearTF.text;
    _property.rentalYeild = rentalYieldTF.text;
    
    
    _property.bedroom = bedroom.text;
    _property.condition = funishing.text;
    _property.landSize = landSize.text;
    _property.mainDoorDirection = mainDoorFacing.text;
    _property.mainGateFacing = mainGateFacing.text;
    _property.carPark = carPark.text;
    _property.garden = garden.text;
    _property.swimmingPool = swimmingPool.text;
    _property.basement = basement.text;
    _property.roofTerrance = roofTerrance.text;
    
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    SalesListViewController *pushview = [newStoryboard instantiateViewControllerWithIdentifier:@"salesListView"];
    pushview.propertySearch = _property;
	[self presentViewController:pushview animated:YES completion:nil];
}
@end
