//
//  ParentLoadingVC.h
//  Property App
//
//  Created by Brian on 29/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentLoadingVC : UIViewController
-(void) sucess:(NSString*) message;
-(void) fail:(NSString*) message;
@end
