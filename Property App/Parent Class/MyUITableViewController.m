//
//  MyUITableViewController.m
//  Property App
//
//  Created by Brian on 6/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyUITableViewController.h"

@interface MyUITableViewController ()

@end

@implementation MyUITableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self customNavigationTitle];
	// Do any additional setup after loading the view.
}
-(void) customNavigationTitle
{
    if (_customNaviTitle != nil) {
        [self.navigationItem setTitle:_customNaviTitle];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) lateViewDidLoad
{
    isInLoading = NO;
    self.mask.hidden = YES;
    self.mask2.hidden = YES;
    self.indicator.hidden = YES;
}
-(void) startLoading
{
    if (!isInLoading) {
        isInLoading = YES;
        //setup mask
        CGRect frame = self.view.frame;
        self.mask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height - 0)];
        self.mask.backgroundColor = [UIColor blackColor];
        self.mask.alpha = 0.8;
        self.mask.hidden = NO;
        [self.view addSubview:self.mask];
        self.mask2 = [[UIView alloc] initWithFrame:CGRectMake(frame.size.width/2, 0, frame.size.width/2, 64)];
        self.mask2.backgroundColor = [UIColor clearColor];
        self.mask2.alpha = 0.8;
        self.mask2.hidden = NO;
        [self.view addSubview:self.mask2];
        //set up indicator
        self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        self.indicator.hidden = NO;
        self.indicator.alpha = 1.0;
        self.indicator.center = self.view.center;
        self.indicator.hidesWhenStopped = NO;
        [self.view addSubview:self.indicator];
        [self.indicator startAnimating];
        
    }
}
-(void) finishLoading
{
    isInLoading = NO;
    self.mask.hidden = YES;
    self.mask2.hidden = YES;
    self.indicator.hidden = YES;
}
-(void) actionRun
{
    [self startLoading];
}
-(void) actionSuceed:(NSString *)message
{
    [self finishLoading];
}
-(void) actionFail:(NSString *)message
{
    [self finishLoading];
}
@end
