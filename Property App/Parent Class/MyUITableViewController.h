//
//  MyUITableViewController.h
//  Property App
//
//  Created by Brian on 6/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUIViewController.h"
@interface MyUITableViewController : UITableViewController
{
    BOOL isInLoading;
}

@property (nonatomic,retain) UIActivityIndicatorView *indicator;
@property (nonatomic,retain) UIView *mask;
@property (nonatomic,retain) UIView *mask2;
@property (nonatomic,retain) NSString *customNaviTitle;

-(void) lateViewDidLoad;

-(void) refreshView;

-(void) startLoading;
-(void) finishLoading;
-(void) actionRun;
-(void) actionSuceed:(NSString *)message;
-(void) actionFail:(NSString *)message;

-(void) updateView;
@end
