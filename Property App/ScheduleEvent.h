//
//  ScheduleEvent.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/13/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "User.h"

@interface ScheduleEvent : NSObject


@property (nonatomic, retain) PFObject * eventObject;
@property (nonatomic, retain) NSString * eventType;
@property (nonatomic, retain) NSString * projectName;
@property (nonatomic, retain) NSString * projectType;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * meetupVenue;
@property (nonatomic, retain) NSString * idealBuyingPrice;

@property (nonatomic, retain) PFUser * receiverAgent;
@property (nonatomic, retain) PFUser * receiverCreator;
@property (nonatomic, retain) User * requester;
@property (nonatomic, retain) NSDate * eventSchedule;

@property (nonatomic, retain) UIImage * picture;

@property (nonatomic) BOOL agentAccept;
@property (nonatomic) BOOL creatorAccept;
@property (nonatomic) BOOL requesterAccept;

@property (nonatomic) BOOL isReschedule;

- (NSString*)getEventType;
- (NSInteger) getEventMonth;
- (NSInteger) getEventDay;
- (NSInteger) getEventYear;

@end
