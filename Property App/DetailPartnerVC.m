//
//  DetailPartnerVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/30/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailPartnerVC.h"
#import "DataEngine.h"
#import "EditPartnerVC.h"

@interface DetailPartnerVC ()

@end

@implementation DetailPartnerVC
@synthesize curPartner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [adrTV setFont:[UIFont fontWithName:adrTV.font.fontName size:21]];
	// Do any additional setup after loading the view.
    partnerType = curPartner.partnerType;
    if ([partnerType isEqualToString:@"Commercial Loan Bankers"] || [partnerType isEqualToString:@"Mortgage Bankers"] || [partnerType isEqualToString:@"Residential Loan Bankers"]) {
        bank.hidden = NO;
        bankTitle.hidden = NO;
        [bank setText:[NSString stringWithFormat:@"Bank: %@",curPartner.bank]];
        [name setText:curPartner.name];
        [adrTV setText:curPartner.address];
        [self switchBankLogo];
    }
    else
    {
        [name setText:curPartner.name];
        [adrTV setText:curPartner.address];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)call:(id)sender
{
    NSString * call = [NSString stringWithFormat:@"tel:%@", curPartner.contact];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}

- (IBAction)mail:(id)sender
{
    User * curUser = [[DataEngine sharedInstance] GetCurUser];
    NSString * intro = @"Hi, I am interested in your professional services. Please contact me at a convenient time.";
    NSString * temp = [NSString stringWithFormat:@"%@\nMy Phone Number: %@\nMy Email Address: %@", intro, curUser.phone, curUser.email];
    NSString * email = curPartner.email;
    NSArray * recipients = [NSArray arrayWithObject:email];
    if ([MFMailComposeViewController canSendMail]) {
        // Show the composer
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:recipients];
        //[controller setCcRecipients:cRecipients];
        [controller setSubject:@"REAL client business Request"];
        [controller setMessageBody:temp isHTML:NO];
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Handle the error
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    // Notifies users about errors associated with the interface
    if (result == MFMailComposeResultSent)
    {
        NSLog(@"\n\n Email Sent");
        
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)deletePartner:(id)sender
{
    [curPartner.partnerObject deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            [self.navigationController popViewControllerAnimated:YES];
            [_partnerView actionRun];
        }
    }];

}




-(void) switchBankLogo
{
    if ([curPartner.bank isEqualToString:@"Hong Leong"]) {
        [bankLogo setImage:[UIImage imageNamed:@"bank_hongleong.png"]];
        [bankLogo setHidden:NO];
    }
    else if ([curPartner.bank isEqualToString:@"HSBC"]) {
        [bankLogo setImage:[UIImage imageNamed:@"bank_hsbc.png"]];
        [bankLogo setHidden:NO];
    }
    else if ([curPartner.bank isEqualToString:@"OCBC"]) {
        [bankLogo setImage:[UIImage imageNamed:@"bank_ocbc.png"]];
        [bankLogo setHidden:NO];
    }
    else if ([curPartner.bank isEqualToString:@"Standard Chartered"]) {
        [bankLogo setImage:[UIImage imageNamed:@"bank_sc.png"]];
        [bankLogo setHidden:NO];
    }
    else if ([curPartner.bank isEqualToString:@"UOB"]) {
        [bankLogo setImage:[UIImage imageNamed:@"bank_uob.png"]];
        [bankLogo setHidden:NO];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"goEdit"]) {
        EditPartnerVC * destination = segue.destinationViewController;
        destination.curPartner = curPartner;
    }
}

@end
