//
//  AddNewAlertVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/20/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyAlertsVC.h"
#import "User.h"
#import "AlertInfo.h"

@interface AddNewAlertVC : UITableViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    NSArray * transactionList;
    NSArray * typeList;
    NSArray * projectList;
    NSArray * districList;
    NSArray * tenureList;
    NSArray * priceList;
    NSArray * areaList;
    
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
    
    User * curUser;
    
    IBOutlet UIBarButtonItem * submitBtn;
    IBOutlet UITableViewCell * deleteBtn;
}

@property (nonatomic, retain) MyAlertsVC * myAlertsView;
@property (nonatomic, retain) IBOutlet UITextField * subjectTF;
@property (nonatomic, retain) IBOutlet UITextField * transactionTF;
@property (nonatomic, retain) IBOutlet UITextField * typeTF;
@property (nonatomic, retain) IBOutlet UITextField * projectTF;
@property (nonatomic, retain) IBOutlet UITextField * districtTF;
@property (nonatomic, retain) IBOutlet UITextField * blockUnitNoTF;
@property (nonatomic, retain) IBOutlet UITextField * locationTF;
@property (nonatomic, retain) IBOutlet UITextField * tenureTF;

@property (nonatomic, retain) IBOutlet UITextField * priceBeginTF;
@property (nonatomic, retain) IBOutlet UITextField * priceEndTF;

@property (nonatomic, retain) IBOutlet UITextField * areaBeginTF;
@property (nonatomic, retain) IBOutlet UITextField * areaEndTF;

@property (nonatomic, retain) IBOutlet UITextField * bedroomBeginTF;
@property (nonatomic, retain) IBOutlet UITextField * bedroomEndTF;

@property (nonatomic, retain) IBOutlet UITextField * levelFromTF;
@property (nonatomic, retain) IBOutlet UITextField * levelToTF;
@property (nonatomic, retain) IBOutlet UITextField * remarkTF;


@property (nonatomic) BOOL isEdit;
@property (nonatomic, retain) AlertInfo * alertInfo;


- (void) addAlertDone;


@end
