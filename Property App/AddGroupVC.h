//
//  AddGroupVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUITableViewController.h"

@interface AddGroupVC : MyUITableViewController
{
    IBOutlet UILabel * participants;
    NSMutableArray * participantList;
    IBOutlet UITextField * subject;
    IBOutlet UIButton * doneBtn;
}
- (void) addToMain : (NSArray*) data;
@property (nonatomic, retain) NSMutableArray * participantList;
@end
