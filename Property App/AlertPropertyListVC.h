//
//  AlertPropertyListVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/20/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUITableViewController.h"
#import "User.h"
#import "AlertInfo.h"


@interface AlertPropertyListVC : MyUITableViewController
{
    User * curUser;
    
}

@property (nonatomic, retain) AlertInfo * alertInfo;
@property (nonatomic, retain) NSMutableArray * propertyList;
@property (nonatomic, retain) NSMutableArray * statusList;

-(void) refreshStatusNew:(NSString*)statusNew :(BOOL) isRead;
@end
