//
//  SearchHDBVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/24/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"

@interface SearchHDBVC : UITableViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    IBOutlet UITextField * propertyTypeTF;
    IBOutlet UITextField * listingTypeTF;
    IBOutlet UITextField * districtTF;
    IBOutlet UITextField * areaTF;
    IBOutlet UITextField * priceTF;
    IBOutlet UITextField * levelTF;
    IBOutlet UITextField * hdbTypeTF;
    IBOutlet UITextField * leftLevelTF;
    IBOutlet UITextField * mainDoorDirectionTF;
    IBOutlet UITextField * roomPositionTF;
    IBOutlet UITextField * townTF;
    IBOutlet UITextField * valuationTF;
    IBOutlet UITextField * conditionTF;
    
    NSArray * hdbTypeList;
    NSArray * districtList;
    NSArray * levelList;
    NSArray * conditionList;
    NSArray * roomPositionList;
    NSArray * mainDoorList;
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
}


@property (nonatomic, strong) Property * property;
- (void) SearchSuccessfully : (NSArray*) objects;
- (void) SearchFailed : (NSString*) errorString;

@end
