//
//  CommercialCalVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/26/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "CommercialCalVC.h"

@interface CommercialCalVC ()

@end

@implementation CommercialCalVC
@synthesize purchasePriceTF, value;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [value setText:@"S$ 0.00"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)calculate:(id)sender
{
    if (![purchasePriceTF.text isEqualToString:@""]) {
        purchasePrice = [purchasePriceTF.text doubleValue];
        if (purchasePrice <= 180000) {
            stampDuty = purchasePrice * 0.01;
        }
        else if (purchasePrice <= 360000)
        {
            stampDuty = (purchasePrice * 0.02) - 1800;
        }
        else
        {
            stampDuty = (purchasePrice * 0.03) - 5400;
        }
        
        if (isnan(stampDuty)) {
            [value setText:@"$S 0.00"];
            return;
        }
        
        [value setText:[NSString stringWithFormat:@"S$ %.1f0", stampDuty]];
    }
    else
    {
        //errr
        [value setText:@"S$ 0.00"];
    }
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    activeTextField = textField;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    float keyboardHeight = 216.0f;
    if (textField.frame.origin.y + textField.frame.size.height > self.view.frame.size.height - keyboardHeight) {
        const int movementDistance = 100; // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}
-(void)dismissKeyboard {
    [activeTextField resignFirstResponder];
}
@end
