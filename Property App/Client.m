//
//  Client.m
//  Property App
//
//  Created by Jonathan Le on 12/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "Client.h"
#import "DatabaseEngine.h"
@implementation Client
@synthesize myAgentList;

- (PFUser*) getUserObject
{
	return self.userObject;
}
- (NSMutableArray*) GetAgentList
{
	return myAgentList;
}

- (void) GetMyAgentList
{
	myAgentList = [[NSMutableArray alloc] init];
	[[DatabaseEngine sharedInstance] GetMyAgentList:self];
}

- (void) GetAgentSuccessful : (NSMutableArray*) agentList
{
	for (PFUser* agent in agentList) {
		Agent* ag = [[Agent alloc] initWithUser:agent];
		[myAgentList addObject:ag];
	}
	//NSLog(@"Successfully added %i agents", myAgentList.count);
}

- (void) GetAgentFailed : (NSString*) errorString
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:errorString
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}

- (NSString*) description {
	return [NSString stringWithFormat:@"Client - %@",[super description]];
}



@end
