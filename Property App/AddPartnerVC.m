//
//  AddPartnerVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/30/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "AddPartnerVC.h"
#import "Partner.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"

@interface AddPartnerVC ()

@end

@implementation AddPartnerVC
@synthesize partnerType, partnerView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    curUser = [[DataEngine sharedInstance]GetCurUser];
    bankList = [[NSArray alloc]initWithObjects:
                @"[Select]",
                @"Hong Leong",
                @"HSBC",
                @"OCBC",
                @"Standard Chartered",
                @"UOB", nil];

    if ([partnerType isEqualToString:@"Commercial Loan Bankers"] || [partnerType isEqualToString:@"Mortgage Bankers"] || [partnerType isEqualToString:@"Residential Loan Bankers"]) {
        bankCell.hidden = NO;
    }
    [self setPlaceHolder];
}

-(void) setPlaceHolder
{
    [nameTF setPlaceholder:@"Name: "];
    [addressTF setPlaceholder:@"Address: "];
    [contacttF setPlaceholder:@"Contact Number: "];
    [emailTF setPlaceholder:@"Email: "];
    [bankTF setPlaceholder:@"Bank: "];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done:(id)sender
{
    [self startLoading];
    if ([partnerType isEqualToString:@"Commercial Loan Bankers"] || [partnerType isEqualToString:@"Mortgage Bankers"] || [partnerType isEqualToString:@"Residential Loan Bankers"]) {
        
        
        if ([nameTF.text isEqualToString:@""] || [addressTF.text isEqualToString:@""] || [emailTF.text isEqualToString:@""] || [contacttF.text isEqualToString:@""] || [bankTF.text isEqualToString: @""]) {
            //errr
        }
        else
        {
            Partner * newPartner = [[Partner alloc]init];
            
            newPartner.partnerType = partnerType;
            newPartner.name = nameTF.text;
            newPartner.address = addressTF.text;
            newPartner.email = emailTF.text;
            newPartner.contact = contacttF.text;
            newPartner.bank = bankTF.text;
            newPartner.creator = curUser;
            
            [[DatabaseEngine sharedInstance] addPartner:self :newPartner];
            
            
        }
    }
    else
    {
        if ([nameTF.text isEqualToString:@""] || [addressTF.text isEqualToString:@""] || [emailTF.text isEqualToString:@""] || [contacttF.text isEqualToString:@""]) {
            //errr
        }
        else
        {
            Partner * newPartner = [[Partner alloc]init];
            
            newPartner.partnerType = partnerType;
            newPartner.name = nameTF.text;
            newPartner.address = addressTF.text;
            newPartner.email = emailTF.text;
            newPartner.contact = contacttF.text;
            newPartner.bank = @"";
            newPartner.creator = curUser;
            
            [[DatabaseEngine sharedInstance] addPartner:self :newPartner];
            
            
        }
    }
}
- (void)addDone
{
    [self finishLoading];
    [self.navigationController popViewControllerAnimated:YES];
    [partnerView actionRun];
    
}

- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    // Add the picker
    /*
     UIDatePicker *pickerView = [[UIDatePicker alloc] init];
     pickerView.datePickerMode = UIDatePickerModeDate;
     [menu addSubview:pickerView];
     [menu showInView:self.view];
     [menu setBounds:CGRectMake(0,0,320, 500)];*/
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
    
}

-(void)categoryDoneButtonPressed{
    //categoryLable.text = selectedCategory;
    if (activeTextField == bankTF) {
        [bankTF setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if (activeTextField == bankTF) {
        selectedCategory = [NSString stringWithFormat:@"%@",[bankList objectAtIndex:row]];
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (activeTextField == bankTF)
    {
        return [bankList count];
    }
    
    
    return [bankList count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == bankTF)
    {
        return [bankList objectAtIndex:row];
    }
    return [NSString stringWithFormat:@"%d", row+1];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:bankTF] )
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}





- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    float keyboardHeight = 216.0f;
    if (textField.frame.origin.y + textField.frame.size.height > self.view.frame.size.height - keyboardHeight) {
        const int movementDistance = 100; // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
    
    
    
}

-(void)dismissKeyboard {
    [activeTextField resignFirstResponder];
}

@end
