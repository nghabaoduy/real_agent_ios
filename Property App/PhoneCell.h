//
//  PhoneCell.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UISwitch *selectSwitch;
@property (nonatomic, retain) IBOutlet UILabel *mainLabel;

@end
