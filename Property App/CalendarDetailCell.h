//
//  CalendarDetailCell.h
//  Property App
//
//  Created by Brian on 17/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarDetailCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *eventTimeLbl;
@property (strong, nonatomic) IBOutlet UILabel *eventTitleLbl;
@property (strong, nonatomic) IBOutlet UILabel *eventClientLbl;
@property (strong, nonatomic) IBOutlet UILabel *eventAddressLbl;

@end
