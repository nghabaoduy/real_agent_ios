//
//  LoadingCurPropVC.h
//  Property App
//
//  Created by Brian on 1/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ParentLoadingVC.h"
#import "Property.h"
@interface LoadingCurPropVC : ParentLoadingVC
@property (strong, nonatomic) Property * property;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

-(void)startUpload;

@end
