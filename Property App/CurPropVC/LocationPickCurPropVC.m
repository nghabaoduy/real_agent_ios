//
//  LocationPickCurPropVC.m
//  Property App
//
//  Created by Brian on 1/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "LocationPickCurPropVC.h"
#import "ExtraDetailCurPropVC.h"
#import "MyUIEngine.h"
#import "DataEngine.h"

@interface LocationPickCurPropVC ()

@end

@implementation LocationPickCurPropVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"pickLocationNext"])
    {
        ExtraDetailCurPropVC * destination = segue.destinationViewController;
        destination.property = _property;
    }
}
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"pickLocationNext"])
    {
        NSLog(@"_property.address = %@",_property.address);
        if (_property.address!= NULL) {
            if (![_property.address isEqualToString:@""]) {
                return YES;
            }
            return NO;
        }
        return NO;
    }
    return NO;
}

-(void) GetMapLocation : (NSArray*) data {
	NSString* postCode;
	NSLog(@"data %@", data);
	
	for (int i = 0; i < data.count; i++) {
		postCode = [[data objectAtIndex:i] objectForKey:@"POSTALCODE"];
		NSLog(@"post %@", postCode);
		CLGeocoder *geocoder = [[CLGeocoder alloc] init];
		
		
		//NSLog(@"%@", postCode);
		[geocoder geocodeAddressString:postCode
					 completionHandler:^(NSArray *placemarks, NSError *error) {
						 
						 if (error) {
							 NSLog(@"Geocode failed with error: %@", error);
							 return;
						 }
						 
						 if(placemarks && placemarks.count > 0)
						 {
							 CLPlacemark *placemark = placemarks[0];
							 
							 NSDictionary *addressDictionary =
							 placemark.addressDictionary;
							 
							 //NSLog(@"location: %@", [placemark location]);
							 NSLog(@"addressdict: %@", [placemark addressDictionary]);
							 //_property.address = [addressDictionary objectForKey:@"Street"];
							 //_property.blkNo = [[DataEngine sharedInstance] getBlockNumber:_property.postCode];
							 //_property.district = [[DataEngine sharedInstance] getDistrict:_property.postCode];
							 
							 //[placemark ]
							 
							 // NSLog(@"%@ %@ %@ %@", address,city, state, zip);
							 CLLocationCoordinate2D coord = {.latitude =  placemark.location.coordinate.latitude, .longitude = placemark.location.coordinate.longitude};
							 MKCoordinateSpan span = {.latitudeDelta = 0.01, .longitudeDelta =  0.01};
							 MKCoordinateRegion region = {coord, span};
							 
							 MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
							 point.coordinate = coord;
							 point.title = [[data objectAtIndex:i] objectForKey:@"BUILDINGNAME"];
							 point.subtitle = [NSString stringWithFormat:@"Blk %@ %@ %@",[[data objectAtIndex:i] objectForKey:@"BLOCK"] , [[data objectAtIndex:i] objectForKey:@"ROAD"], [[data objectAtIndex:i] objectForKey:@"POSTALCODE"]];
							 [self.mapView addAnnotation:point];
							 
							 NSLog(@"View loading");
							 [self.mapView setRegion:region];
							 NSLog(@"view loaded");
							 [self.mapView selectAnnotation:point animated:YES];
							 
                             _property.projectName =[[data objectAtIndex:i] objectForKey:@"BUILDINGNAME"];
							 _property.postCode = [[data objectAtIndex:i] objectForKey:@"POSTALCODE"];
                             _property.address = [NSString stringWithFormat:@"BLK %@ %@",[[data objectAtIndex:i] objectForKey:@"BLOCK"] , [[data objectAtIndex:i] objectForKey:@"ROAD"]];
                             _property.blkNo = [[data objectAtIndex:i] objectForKey:@"BLOCK"];
                             _property.blkNo = [[data objectAtIndex:i] objectForKey:@"BLOCK"];
                             _property.district = [[DataEngine sharedInstance] getDistrict:_property.postCode];
						 }
					 }
		 ];
	}
}
@end
