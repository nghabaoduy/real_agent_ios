//
//  ExtraDetailCurPropVC.m
//  Property App
//
//  Created by Brian on 1/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ExtraDetailCurPropVC.h"
#import "MyUIEngine.h"

#import "DataEngine.h"
#import "Agent.h"
#import "Client.h"

#import "LoadingCurPropVC.h"

@interface ExtraDetailCurPropVC ()

@end

@implementation ExtraDetailCurPropVC

@synthesize property = _property;


Agent* curAgent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self DisplayAddress];
    

    districtList = [[DataEngine sharedInstance] getDistrictList];
    tenureList = [[NSArray alloc] initWithObjects:
                  @"",
                  @"FH",
                  @"999",
                  @"99",
                  @"60",
                  @"30",
                  @"<30yrs",
                  nil];
    
    
    [self initTextfieldPlaceHolder];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}
-(void)dismissKeyboard {
    [activeTextField resignFirstResponder];
}

- (void) DisplayAddress
{
    [address setText:_property.address];
    [postalCode setText:_property.postCode];
}


- (void) initTextfieldPlaceHolder
{
    
    address.placeholder = @"Address: ";
    postalCode.placeholder = @"Postal Code: ";
    blockNo.placeholder = @"Block: ";
    levelNo.placeholder = @"Level: ";
    unitNo.placeholder = @"Unit: ";
    district.placeholder = @"District: ";
    
    
    
    address.text = _property.address;
    address.enabled = NO;
    [postalCode setText:_property.postCode];
    postalCode.enabled = NO;
    [blockNo setText:_property.blkNo];
    blockNo.enabled = NO;
    [district setText:_property.district];
    district.enabled = NO;
}


- (BOOL) ValidateBlock
{
    if (![blockNo.text isEqualToString:@""]) {
        return YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:@"Block No cannot be blank"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    return NO;
}

- (BOOL) ValidateDistrict
{
    if (![district.text isEqualToString:@""]) {
        return YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:@"District cannot be blank"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    return NO;
}






- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    // Add the picker
    /*
     UIDatePicker *pickerView = [[UIDatePicker alloc] init];
     pickerView.datePickerMode = UIDatePickerModeDate;
     [menu addSubview:pickerView];
     [menu showInView:self.view];
     [menu setBounds:CGRectMake(0,0,320, 500)];*/
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
}

-(void)categoryDoneButtonPressed{
    //categoryLable.text = selectedCategory;
    if (activeTextField == district) {
        [district setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if (activeTextField == district) {
        selectedCategory = [NSString stringWithFormat:@"%@",[districtList objectAtIndex:row]];
    }
    
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (activeTextField == district) {
        return [districtList count];
    }
    
    return [districtList count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == district) {
        return [districtList objectAtIndex:row];
    }
    
    return [NSString stringWithFormat:@"%d", row+1];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:district])
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField{

    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"loadingNext"]) {
        _property.address = address.text;
        _property.postCode = postalCode.text;
        _property.blkNo = blockNo.text;
        _property.levelNumber = levelNo.text;
        _property.unitNo = unitNo.text;
        _property.district = district.text;

        NSLog(@"adder name = %@",_property.adder.fullName);
        
        LoadingCurPropVC * destination = segue.destinationViewController;
        destination.property = _property;
        //[destination startUpload];
    }
}
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"loadingNext"]) {
        if ([self ValidateBlock] &&
            [self ValidateDistrict])
        {
            return YES;
        }
        else
        {
            //Show log
            return NO;
        }
    }
    return NO;
    
}


@end
