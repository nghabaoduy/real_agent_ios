//
//  BasicInfoCurPropVC.h
//  Property App
//
//  Created by Brian on 1/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
@interface BasicInfoCurPropVC : UITableViewController
{
    IBOutlet UITextField * postalCode;
}

@property (nonatomic, strong) Property * property;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, retain) NSString* requestType;
@property (nonatomic, retain) NSMutableArray* data;
@end
