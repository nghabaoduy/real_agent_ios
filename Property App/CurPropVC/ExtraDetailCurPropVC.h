//
//  ExtraDetailCurPropVC.h
//  Property App
//
//  Created by Brian on 1/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
@interface ExtraDetailCurPropVC : UITableViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    //Basic Information
    IBOutlet UITextField * address;
    IBOutlet UITextField * postalCode;
    IBOutlet UITextField * blockNo;
    IBOutlet UITextField * levelNo;
    IBOutlet UITextField * unitNo;
    IBOutlet UITextField * district;

    
    UITextField * activeTextField;
    NSArray * districtList;
    NSArray * tenureList;
    
    UIActionSheet * menu;
    NSString * selectedCategory;
    
}
@property (strong, nonatomic) Property * property;

@end
