//
//  LocationPickCurPropVC.h
//  Property App
//
//  Created by Brian on 1/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Property.h"

@interface LocationPickCurPropVC : UIViewController
@property (strong, nonatomic) Property * property;
@property (nonatomic, weak) IBOutlet MKMapView *mapView;
-(void) GetMapLocation : (NSArray*) data;
@end
