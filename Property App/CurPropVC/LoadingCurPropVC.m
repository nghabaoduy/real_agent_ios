//
//  LoadingCurPropVC.m
//  Property App
//
//  Created by Brian on 1/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "LoadingCurPropVC.h"
#import "DetailPropertyTabBarVC.h"
#import "DetailPropertyVC.h"
#import "GalleryPropertyVC.h"
#import "LocationPropertyVC.h"
#import "DetailPropertyNavVC.h"
#import "SWRevealViewController.h"
#import "PersonInfoViewController.h"

@interface LoadingCurPropVC ()

@end

@implementation LoadingCurPropVC
@synthesize property = _property;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidLoad];
    [self startUpload];
    
	// Do any additional setup after loading the view.
}
- (void) startUpload
{
    if (self.property != NULL) {
        [self upload];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)upload
{
    //Upload function
    [self.property printInfo];
    [self.indicator startAnimating];
	[_property UploadProperty: self];
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"backToPersonalInfo"])
    {
        PersonInfoViewController *dest = [segue destinationViewController];
        //dest
    }
}
-(void) sucess:(NSString *)message
{
    NSLog(@"upload success runs");
    [self performSegueWithIdentifier:@"backToPersonalInfo" sender:self];
    
}
-(void) fail:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Fail"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
    [alert show];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end