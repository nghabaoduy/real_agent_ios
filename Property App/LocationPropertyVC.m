//
//  LocationPropertyVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "LocationPropertyVC.h"

#import "MyUIEngine.h"
#import "SWRevealViewController.h"
#import "DatabaseEngine.h"

@interface LocationPropertyVC ()

@end

@implementation LocationPropertyVC

@synthesize property = curProperty;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
	[self GetMapLocation];
	
	NSLog(@"curProperty = %@",curProperty.propertyName);
	


	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) GetMapLocation {
	NSString* postCode = curProperty.postCode;
	
		NSLog(@"post %@", postCode);
		CLGeocoder *geocoder = [[CLGeocoder alloc] init];
		
		
		//NSLog(@"%@", postCode);
		[geocoder geocodeAddressString:postCode
					 completionHandler:^(NSArray *placemarks, NSError *error) {
						 
						 if (error) {
							 NSLog(@"Geocode failed with error: %@", error);
							 return;
						 }
						 
						 if(placemarks && placemarks.count > 0)
						 {
							 CLPlacemark *placemark = placemarks[0];
							 
							 NSDictionary *addressDictionary =
							 placemark.addressDictionary;
							 
							 //NSLog(@"location: %@", [placemark location]);
							 NSLog(@"addressdict: %@", [placemark addressDictionary]);
							 //_property.address = [addressDictionary objectForKey:@"Street"];
							 //_property.blkNo = [[DataEngine sharedInstance] getBlockNumber:_property.postCode];
							 //_property.district = [[DataEngine sharedInstance] getDistrict:_property.postCode];
							 
							 //[placemark ]
							 
							 // NSLog(@"%@ %@ %@ %@", address,city, state, zip);
							 CLLocationCoordinate2D coord = {.latitude =  placemark.location.coordinate.latitude, .longitude = placemark.location.coordinate.longitude};
							 MKCoordinateSpan span = {.latitudeDelta = 0.01, .longitudeDelta =  0.01};
							 MKCoordinateRegion region = {coord, span};
							 
							 MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
							 point.coordinate = coord;
							 point.title = curProperty.projectName;
							 point.subtitle = [NSString stringWithFormat:@"%@ %@",curProperty.address , curProperty.postCode];
							 [self.mapView addAnnotation:point];
							 
							 NSLog(@"View loading");
							 [self.mapView setRegion:region];
							 NSLog(@"view loaded");
							 [self.mapView selectAnnotation:point animated:YES];
							 
						 }
					 }
        ];
}


@end
