//
//  DetailPartnerVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/30/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Partner.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "PartnersVC.h"

@interface DetailPartnerVC : UIViewController <MFMailComposeViewControllerDelegate>
{
    IBOutlet UILabel * name;
    IBOutlet UILabel * address;
    IBOutlet UILabel * bank;
    IBOutlet UILabel * bankTitle;
    IBOutlet UIImageView *bankLogo;
    IBOutlet UITextView *adrTV;
    
    NSString * partnerType;
    
    
}

@property (nonatomic, retain) Partner * curPartner;
@property (nonatomic, retain) PartnersVC * partnerView;


@end
