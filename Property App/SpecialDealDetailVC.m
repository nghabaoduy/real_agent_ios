//
//  SpecialDealDetailVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/19/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "SpecialDealDetailVC.h"
#import "DataEngine.h"

@interface SpecialDealDetailVC ()

@end

@implementation SpecialDealDetailVC
@synthesize background, image, curDic;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [background setImage:image];
    
    [self displayInformation];
    
    
    
}
- (IBAction)callAgent:(id)sender {
    NSString * call = [NSString stringWithFormat:@"tel:%@", [DataEngine sharedInstance].curUser.username];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}

- (void) displayInformation
{
    NSString * district = [NSString stringWithFormat:@"District: %@", [curDic objectForKey:@"district"]];
    [districLB setText: district];
    
    NSString * freeHold = [NSString stringWithFormat:@"%@", [curDic objectForKey:@"freeHold"]];
    
    [freeHoldLB setText:freeHold];
    
    NSString * units = [NSString stringWithFormat:@"%@", [curDic objectForKey:@"units"]];
    
    [unitsLB setText:units];
    
    NSString * type = [NSString stringWithFormat:@"%@", [curDic objectForKey:@"type"]];
    
    [typeLB setText:type];
    
    NSString * developementName = [NSString stringWithFormat:@"Developement Name: %@", [curDic objectForKey:@"developmentName"]];
    
    [developementNameLB setText:developementName];
    
    NSString * propertyType = [NSString stringWithFormat:@"Property Type: %@", [curDic objectForKey:@"propertyType"]];
    
    [propertyTypeLB setText:propertyType];
    
    NSString * developer = [NSString stringWithFormat:@"Developer: %@", [curDic objectForKey:@"developer"]];
    
    [developerLB setText:developer];
    
    NSString * country = [NSString stringWithFormat:@"Country: %@", [curDic objectForKey:@"country"]];
    
    [countryLB setText:country];
    
    NSString * description = [NSString stringWithFormat:@"%@", [curDic objectForKey:@"description"]];
    
    [descriptionTV setText:description];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
