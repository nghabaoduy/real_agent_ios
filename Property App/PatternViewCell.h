//
//  PatternViewCell.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 1/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PatternViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView * patternImageView;
@property (nonatomic, weak) IBOutlet UILabel * patternImageLabel;
@end
