//
//  GetUserViewController.m
//  Property App
//
//  Created by Brian on 12/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "GetUserViewController.h"

@interface GetUserViewController ()

@end

@implementation GetUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) startLoading
{
    [super startLoading];
}
-(void) finishLoading
{
    [super finishLoading];
}
-(void) actionRun
{
    [super actionRun];
}
-(void) actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
}
-(void) actionFail:(NSString *)message
{
    [super actionFail:message];
}

@end
