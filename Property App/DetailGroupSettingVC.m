//
//  DetailGroupSettingVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailGroupSettingVC.h"
#import "DataEngine.h"

@interface DetailGroupSettingVC ()

@end

@implementation DetailGroupSettingVC
@synthesize curGroup = _curGroup;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"groupobject = %@",_curGroup);
    
    PFUser * user =[_curGroup objectForKey:@"creator"];
    
    
    if (![user.objectId isEqualToString: [[DataEngine sharedInstance]GetCurUser].userObject.objectId]) {
        NSLog(@"abc");
    }
    
    BOOL bool1 = [[_curGroup objectForKey:@"switch1"] intValue]==1;
    BOOL bool2 = [[_curGroup objectForKey:@"switch2"] intValue]==1;
    BOOL bool3 = [[_curGroup objectForKey:@"switch3"] intValue]==1;
    NSLog(@"%@ - %@ - %@",bool1?@"true":@"false",bool2?@"true":@"false",bool3?@"true":@"false");
    [switch1 setOn:bool1 animated:YES];
    [switch2 setOn:bool2 animated:YES];
    [switch3 setOn:bool3 animated:YES];
    
    
}
- (IBAction)switch1Change:(id)sender {
    
    _curGroup[@"switch1"]=[NSNumber numberWithBool:switch1.isOn];
    [_curGroup saveInBackground];
    
}
- (IBAction)switch2change:(id)sender {
    _curGroup[@"switch2"]=[NSNumber numberWithBool:switch2.isOn];
    [_curGroup saveInBackground];
    
}
- (IBAction)switch3change:(id)sender {
    _curGroup[@"switch3"]=[NSNumber numberWithBool:switch3.isOn];
    [_curGroup saveInBackground];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
