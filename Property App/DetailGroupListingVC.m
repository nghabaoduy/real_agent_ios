//
//  DetailGroupListingVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailGroupListingVC.h"
#import "DatabaseEngine.h"
#import "ListPattern.h"
#import "DetailPropertyNavVC.h"
#import "DetailPropertyTabBarVC.h"
#import "MyUIEngine.h"


@interface DetailGroupListingVC ()

@end

@implementation DetailGroupListingVC
@synthesize propertyList = _propertyList;

- (void)actionRun
{
    [super actionRun];
    [[DatabaseEngine sharedInstance] getGroupSharing:self :_curGroup];
}

- (void)actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    
    NSLog(@"number: %i", _propertyList.count);
    [self.tableView reloadData];
    
}

- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.contentInset = UIEdgeInsetsMake(-1.0f, 0.0f, 0.0f, 0.0);
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    NSLog(@"id: %@", _curGroup.objectId);
    
    [self actionRun];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    if (section == 0 || section == 1) {
        return 0;
    }
    if (section == 2) {
        return [_propertyList count];
    }
    else
    {
        return 1;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2) {
        static NSString * simpleTableIndentifier = @"listPattern";
        
        ListPattern *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIndentifier];
        
        //PFObject *prop = [_data objectAtIndex:indexPath.row];
        PFObject* share = [_propertyList objectAtIndex:indexPath.row];
        PFObject* propInstance = [share objectForKey:@"propertyId"];
        Property * curProperty = [[Property alloc]initWithProperty:propInstance];
        
        NSLog(@"id : = %@", [propInstance objectForKey:@"projectName"]);
        
        
        cell.displayer.image = curProperty.iconImage;
        [cell.projectName setText:curProperty.projectName];
        [cell.priceAndBedroom setText:[NSString stringWithFormat:@"S$ %@", curProperty.price]];
        [cell.projectType setText:curProperty.propertyType];
        [cell.areaAndPsf setText:[NSString stringWithFormat:@"%@ sqft - PFS S$ %@", curProperty.area, curProperty.PSF]];
        [cell.location setText:curProperty.postCode];
        NSLog(@"runb");
        return cell;
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"addProperty";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        
        return cell;
    }
    
}

 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2) {
        return 128;
    }
    else
    {
        return 44;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BOOL allowUnitNo = [[_curGroup objectForKey:@"switch1"] intValue]==1;
    
    if (indexPath.section == 2) {
        
        
        PFObject* share = [_propertyList objectAtIndex:indexPath.row];
        PFObject* propInstance = [share objectForKey:@"propertyId"];
        Property * curPro = [[Property
                              alloc]initWithProperty:propInstance];
        
        
        UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"DetailStoryboard" bundle:nil];
        
        
        
        DetailPropertyNavVC * navView =[newStoryboard instantiateViewControllerWithIdentifier:@"detailProperty"];
        
        DetailPropertyTabBarVC * tabBar = (DetailPropertyTabBarVC*) navView.topViewController;
        tabBar.isHide = YES;
        tabBar.allowUnitNo = allowUnitNo;
        [tabBar initTabByProperty:curPro :newStoryboard ];
        
        [self presentViewController:navView animated:YES completion:nil];
    }
}
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.0f;

}

- (NSString*) tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (IBAction)addProperty:(id)sender
{
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    ProPortfolioViewController *pushview = [newStoryboard instantiateViewControllerWithIdentifier:@"proPortfolioView"];
    pushview.isSharing = YES;
    pushview.curGroup = _curGroup;
	[self presentViewController:pushview animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self actionRun];
}


@end
