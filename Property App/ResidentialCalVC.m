//
//  ResidentialCalVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/26/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ResidentialCalVC.h"

@interface ResidentialCalVC ()

@end

@implementation ResidentialCalVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    profileOfBuyerList = [[NSArray alloc]initWithObjects:
                          @"[select]",
                          @"Foreigner or Company",
                          @"Singapore PR",
                          @"Singapore Citizen",
                           nil];
    
    
    currentResidentialList = [[NSArray alloc]initWithObjects:
                              @"[select]",
                              @"1st Residential Property",
                              @"2nd Residential Property",
                              @"3rd Residential Property Onwards"
                              , nil];
    
    [value setText: @"S$ 0.00"];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (double)numberOfProperties : (NSString*) string
{
    if ([string isEqualToString:@"1st Residential Property"]) {
        return 1;
    }
    else if ([string isEqualToString:@"2nd Residential Property"])
    {
        return 2;
    }
    else if ([string isEqualToString:@"3rd Residential Property Onwards"])
    {
        return 3;
    }
    return 0;
}

- (IBAction)calculate:(id)sender
{
    purchasePrice = [purchasePriceTF.text doubleValue];
    if (purchasePrice <= 180000) {
        stampDuty = purchasePrice * 0.01;
    }
    else if (purchasePrice <= 360000)
    {
        stampDuty = (purchasePrice * 0.02) - 1800;
    }
    else
    {
        stampDuty = (purchasePrice * 0.03) - 5400;
    }
    
    numberOfProperty = [self numberOfProperties:currentResidentialTF.text];
    
    if ([profileOfBuyerTF.text isEqualToString:@"Foreigner or Company"]) {
        priceABSD = 0.15;
    }
    else if ([profileOfBuyerTF.text isEqualToString:@"Singapore PR"])
    {
        if (numberOfProperty == 1)
        {
            priceABSD = 0.05;
        }
        else if (numberOfProperty >=2)
        {
            priceABSD = 0.10;
        }
    }
    else if ([profileOfBuyerTF.text isEqualToString:@"Singapore Citizen"])
    {
        if (numberOfProperty == 1) {
            priceABSD = 0;
        }
        else if (numberOfProperty == 2)
        {
            priceABSD = 0.07;
        }
        else if (numberOfProperty == 3)
        {
            priceABSD = 0.10;
        }
    }
    
    double finalABSDRates = purchasePrice * priceABSD;
    double finalStampDuty = stampDuty + finalABSDRates;
    
    if (isnan(finalStampDuty)) {
        [value setText:@"$S 0.00"];
        return;
    }
    
    [value setText:[NSString stringWithFormat:@"S$ %.1f0", finalStampDuty]];
}

- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    // Add the picker
    /*
     UIDatePicker *pickerView = [[UIDatePicker alloc] init];
     pickerView.datePickerMode = UIDatePickerModeDate;
     [menu addSubview:pickerView];
     [menu showInView:self.view];
     [menu setBounds:CGRectMake(0,0,320, 500)];*/
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
    
}

-(void)categoryDoneButtonPressed{
    //categoryLable.text = selectedCategory;
    if (activeTextField == profileOfBuyerTF) {
        [profileOfBuyerTF setText:selectedCategory];
    }
    else if (activeTextField == currentResidentialTF)
    {
        [currentResidentialTF setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if (activeTextField == profileOfBuyerTF) {
        selectedCategory = [NSString stringWithFormat:@"%@",[profileOfBuyerList objectAtIndex:row]];
    }
    else if (activeTextField == currentResidentialTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[currentResidentialList objectAtIndex:row]];
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (activeTextField == profileOfBuyerTF)
    {
        return [profileOfBuyerList count];
    }
    else if (activeTextField == currentResidentialTF)
    {
        return [currentResidentialList count];
    }
    return [profileOfBuyerList count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == profileOfBuyerTF)
    {
        return [profileOfBuyerList objectAtIndex:row];
    }
    else if (activeTextField == currentResidentialTF)
    {
        return [currentResidentialList objectAtIndex:row];
    }
    return [NSString stringWithFormat:@"%d", row+1];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:profileOfBuyerTF] || [textField isEqual:currentResidentialTF] )
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    float keyboardHeight = 216.0f;
    if (textField.frame.origin.y + textField.frame.size.height > self.view.frame.size.height - keyboardHeight) {
        const int movementDistance = 100; // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

-(void)dismissKeyboard {
    [activeTextField resignFirstResponder];
}


@end
