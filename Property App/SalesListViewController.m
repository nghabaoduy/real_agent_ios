//
//  SalesListViewController.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "SalesListViewController.h"
#import "SalesDetailsViewController.h"
#import "ListingDetailsViewController.h"
#import "ListingGalleryViewController.h"
#import "ListingLocationViewController.h"
#import "MyUIEngine.h"
#import "DataEngine.h"
#include <unistd.h>


#import "DetailPropertyTabBarVC.h"
#import "DetailPropertyVC.h"
#import "GalleryPropertyVC.h"
#import "LocationPropertyVC.h"
#import "DetailPropertyNavVC.h"
#import "DetailComercialVC.h"
#import "ListPattern.h"
#import "DatabaseEngine.h"

@interface SalesListViewController ()

@end

@implementation SalesListViewController
@synthesize data = _data;
@synthesize tableView = _tableView;
@synthesize Titles = _Titles;
@synthesize toolbarButtons;
@synthesize propertySearch;

- (void)actionRun
{
    [super actionRun];
    [[DatabaseEngine sharedInstance] SearchProperty:propertySearch : self];
}

- (void) actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    [_tableView reloadData];
}

- (void) actionFail:(NSString *)message
{
    [super actionFail:message];
}



- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction) AddPro:(id)sender
{
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView reloadData];
    //PFObject *prop = [_data objectAtIndex:indexPath.row];
    Property* propInstance = [_data objectAtIndex:indexPath.row];
    
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"DetailStoryboard" bundle:nil];
    
    
    
    DetailPropertyNavVC * navView =[newStoryboard instantiateViewControllerWithIdentifier:@"detailProperty"];
    
    DetailPropertyTabBarVC * tabBar = (DetailPropertyTabBarVC*) navView.topViewController;
    [tabBar initTabByProperty:propInstance :newStoryboard ];
    
    
    [self presentViewController:navView animated:YES completion:nil];
    
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 128;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_data count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * simpleTableIndentifier = @"listPattern1";
    
    ListPattern *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIndentifier];
    
    //PFObject *prop = [_data objectAtIndex:indexPath.row];
    Property* propInstance = [_data objectAtIndex:indexPath.row];
    
    
    cell.displayer.image = propInstance.iconImage;
    [cell.projectName setText:propInstance.projectName];
    [cell.priceAndBedroom setText:[NSString stringWithFormat:@"S$ %@", propInstance.price]];
    [cell.projectType setText:propInstance.propertyType];
    [cell.areaAndPsf setText:[NSString stringWithFormat:@"%@ sqft - PFS S$ %@", propInstance.area, propInstance.PSF]];
    [cell.location setText:propInstance.postCode];
    NSLog(@"runb");
    return cell;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
         toolbarButtons = [self.toolbarItems mutableCopy];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :self.tableView];
    
    [self actionRun];
}

- (void) ReloadData
{
    total = [_data count];
    count = 0;
    for (PFObject * object in _data) {
        Property * cur = [[Property alloc] initWithPropertyWithNoIcon:object];
        [[DatabaseEngine sharedInstance] LoadIcon:object :self :cur ];
    }
}

- (void) AddToPropertyList: (Property*) cur
{
    [propertyArray addObject:cur];
    count++;
    if (count == total) {
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) hideAddBtn {
    [toolbarButtons removeObject:self.addBtn];
    [self setToolbarItems:toolbarButtons animated:YES];
}
-(void) showAddBtn {
    if (![toolbarButtons containsObject:self.addBtn]) {
        
        [toolbarButtons addObject:self.addBtn];
        [self setToolbarItems:toolbarButtons animated:YES];
    }
}



@end
