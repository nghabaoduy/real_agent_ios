//
//  DeleteCell.h
//  Property App
//
//  Created by Brian on 23/12/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface DeleteCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *myText;
@property (strong, nonatomic) IBOutlet UIButton *deleteBtn;
@property (nonatomic, strong) PFQuery *deleteQuery;

-(void) enableDeleteBtn;
-(void) disableDeleteBtn;
@end
