//
//  SearchPropertyViewController.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "SearchPropertyViewController.h"
#import "SalesRowViewController.h"
#import "SearchPropertyNavVC.h"
#import "SearchPropertyVC.h"
#import "MyUIEngine.h"
#import "SearchCommercialVC.h"
#import "SearchHDBVC.h"
#import "SearchIndustryVC.h"
#import "SearchLandVC.h"

@interface SearchPropertyViewController ()

@end


@implementation SearchPropertyViewController

@synthesize tableView = _tableView;
@synthesize property = _property;

- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([[data objectAtIndex:indexPath.section] isEqualToString:@"Private Residential"])
    {
        Property *newPro = [[Property alloc] init];
		newPro.propertyType = [data objectAtIndex:indexPath.section];
        if (Segment.selectedSegmentIndex == 0) {
            newPro.listingType = @"Sales";
        } else
        {
            newPro.listingType = @"Rent";
        }
        UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"SearchStoryboard" bundle:nil];
        
        UINavigationController * navView = [newStoryboard instantiateViewControllerWithIdentifier:@"goPrivate"];
        
        SearchPropertyVC * view = (SearchPropertyVC*) navView.topViewController;
        
        
        view.property = newPro;
        
        [self presentViewController:navView animated:YES completion:nil];
    }
    else if ([[data objectAtIndex:indexPath.section] isEqualToString:@"Industrial"])
    {
        Property *newPro = [[Property alloc] init];
		newPro.propertyType = [data objectAtIndex:indexPath.section];
        if (Segment.selectedSegmentIndex == 0) {
            newPro.listingType = @"Sales";
        } else
        {
            newPro.listingType = @"Rent";
        }
        UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"SearchStoryboard" bundle:nil];
        
        UINavigationController * navView = [newStoryboard instantiateViewControllerWithIdentifier:@"goIndustry"];
        
        SearchIndustryVC * view = (SearchIndustryVC*) navView.topViewController;
        
        
        view.property = newPro;
        
        [self presentViewController:navView animated:YES completion:nil];
    }
    else if ([[data objectAtIndex:indexPath.section] isEqualToString:@"HDB"])
    {
        Property *newPro = [[Property alloc] init];
		newPro.propertyType = [data objectAtIndex:indexPath.section];
        if (Segment.selectedSegmentIndex == 0) {
            newPro.listingType = @"Sales";
        } else
        {
            newPro.listingType = @"Rent";
        }
        UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"SearchStoryboard" bundle:nil];
        
        UINavigationController * navView = [newStoryboard instantiateViewControllerWithIdentifier:@"goHDB"];
        
        SearchHDBVC * view = (SearchHDBVC*) navView.topViewController;
        
        
        view.property = newPro;
        
        [self presentViewController:navView animated:YES completion:nil];
    }
    else if ([[data objectAtIndex:indexPath.section] isEqualToString:@"Commercial"])
    {
        Property *newPro = [[Property alloc] init];
		newPro.propertyType = [data objectAtIndex:indexPath.section];
        if (Segment.selectedSegmentIndex == 0) {
            newPro.listingType = @"Sales";
        } else
        {
            newPro.listingType = @"Rent";
        }
        UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"SearchStoryboard" bundle:nil];
        
        UINavigationController * navView = [newStoryboard instantiateViewControllerWithIdentifier:@"goCommercial"];
        
        SearchCommercialVC * view = (SearchCommercialVC*) navView.topViewController;
        
        
        view.property = newPro;
        
        [self presentViewController:navView animated:YES completion:nil];
    }
    else if ([[data objectAtIndex:indexPath.section] isEqualToString:@"Land"])
    {
        Property *newPro = [[Property alloc] init];
		newPro.propertyType = [data objectAtIndex:indexPath.section];
        if (Segment.selectedSegmentIndex == 0) {
            newPro.listingType = @"Sales";
        } else
        {
            newPro.listingType = @"Rent";
        }
        UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"SearchStoryboard" bundle:nil];
        
        UINavigationController * navView = [newStoryboard instantiateViewControllerWithIdentifier:@"goLand"];
        
        SearchLandVC * view = (SearchLandVC*) navView.topViewController;
        
        
        view.property = newPro;
        
        [self presentViewController:navView animated:YES completion:nil];
    }
    
}

- (IBAction)SegmentedChange:(id)sender
{
    if(Segment.selectedSegmentIndex == 0)
    {
        data = salesData;
        [_tableView reloadData];
        
    }
    if (Segment.selectedSegmentIndex == 1) {
        data = rentsData;
        [_tableView reloadData];
        
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [data count];
}


- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	
    return 1;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 3.0;
}
- (UITableViewCell *) tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * simpleTableIndentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [[MyUIEngine sharedUIEngine] getButtonLongCell:tableView1 :simpleTableIndentifier :indexPath.row :data.count :@""];
    
    cell.textLabel.text = [data objectAtIndex:indexPath.section];
	//NSLog(@"data here %@ and index %i", [data objectAtIndex:indexPath.section], indexPath.section);
    return cell;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :self.tableView];
	// Do any additional setup after loading the view.
    salesData = [NSArray arrayWithObjects:@"Private Residential", @"Industrial", @"HDB", @"Commercial", @"Land", nil];
    
    rentsData = [NSArray arrayWithObjects:@"Private Residential", @"Industrial", @"HDB", @"Commercial", @"Land", nil];
    data = salesData;
    self.tableView.backgroundColor = [UIColor clearColor];
    [_tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
