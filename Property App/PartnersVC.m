//
//  PartnersVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/30/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "PartnersVC.h"
#import "AddPartnerVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "Partner.h"
#import "PartnerCell.h"
#import "DetailPartnerVC.h"

@interface PartnersVC ()

@end

@implementation PartnersVC
@synthesize partnerType, partnerList, callActive, emailActive;
- (void)actionRun
{
    [super actionRun];
    curUser = [[DataEngine sharedInstance] GetCurUser];
    [[DatabaseEngine sharedInstance] getPartners:self :partnerType : curUser];
}

- (void)actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    [self.tableView reloadData];
    
}

- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.title = partnerType;
    [self actionRun];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([partnerType isEqualToString:@"Commercial Loan Bankers"] || [partnerType isEqualToString:@"Mortgage Bankers"] || [partnerType isEqualToString:@"Residential Loan Bankers"]) {
    return 145.0f;
    }
    return 110.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [partnerList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    
    if ([partnerType isEqualToString:@"Commercial Loan Bankers"] || [partnerType isEqualToString:@"Mortgage Bankers"] || [partnerType isEqualToString:@"Residential Loan Bankers"]) {
        MyIdentifier = @"type02";
        PartnerCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            cell = [[PartnerCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
        }
        
        // Configure the cell...
        
        Partner * curPartner = [partnerList objectAtIndex:indexPath.row];
        
        [cell.nameLB setText:curPartner.name];
        [cell.bankLB setText:[NSString stringWithFormat:@"Bank: %@",curPartner.bank]];
        [cell switchBankLogo:curPartner.bank];
        [cell.AddressLB setText:curPartner.address];
        [cell.AdrTextView setText:curPartner.address];
        cell.curPartner = curPartner;
        cell.partnerView = self;
        return cell;
    }
    else
    {
        MyIdentifier = @"type01";
        PartnerCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            cell = [[PartnerCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
        }
        
        // Configure the cell...
        
        Partner * curPartner = [partnerList objectAtIndex:indexPath.row];
        
        [cell.AddressLB setText:curPartner.address];
        [cell.AdrTextView setText:curPartner.address];
        [cell.nameLB setText:curPartner.name];
        cell.curPartner = curPartner;
        cell.partnerView = self;
        return cell;
    }
}

- (void)call
{
    NSString * call = [NSString stringWithFormat:@"tel:%@", callActive];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}

- (void)mail
{
    NSString * email = emailActive;
    NSArray * recipients = [NSArray arrayWithObject:email];
    NSArray * cRecipients = [NSArray arrayWithObject:curUser.email];
    NSString * intro = @"Hi, I am interested in your professional services. Please contact me at a convenient time.";
    NSString * temp = [NSString stringWithFormat:@"%@\nMy Phone Number: %@\nMy Email Address: %@", intro, curUser.phone, curUser.email];
    if ([MFMailComposeViewController canSendMail]) {
        // Show the composer
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:recipients];
        //[controller setCcRecipients:cRecipients];
        [controller setSubject:@"REAL client business Request"];
        [controller setMessageBody:temp isHTML:NO];
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Handle the error
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    // Notifies users about errors associated with the interface
    if (result == MFMailComposeResultSent)
    {
        NSLog(@"\n\n Email Sent");
        
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"addPartner"]) {
        AddPartnerVC * destination = segue.destinationViewController;
        destination.partnerType = partnerType;
        destination.partnerView = self;
    }
    else if ([[segue identifier] isEqualToString:@"goDetail"])
    {
        DetailPartnerVC * destination = segue.destinationViewController;
        destination.curPartner = [partnerList objectAtIndex: self.tableView.indexPathForSelectedRow.row];
        destination.partnerView = self;
    }
}


@end
