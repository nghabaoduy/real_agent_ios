//
//  AppDelegate.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>


#import "MyUIEngine.h"
#import "NotificationEngine.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [Parse setApplicationId:@"LR4TsTOkCmqJa4InWUkN2KOvVjEcCJQj9KoXsqy9"
                  clientKey:@"tywFRmv8gKxBI2pAjINd0rIT2TAme0BMxZymORyk"];
    
    // ****************************************************************************
    
    // Override point for customization after application launch.
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound];
    
    [[MyUIEngine sharedUIEngine] appDelegateUICustomzation];
    return YES;
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    [[NotificationEngine sharedInstance] registerPush:deviceToken];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code == 3010) {
        NSLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSDictionary *aps = [userInfo valueForKey:@"aps"];
    NSString *pushMessage = [aps valueForKey:@"alert"];
    [PFPush handlePush:userInfo];
    NSLog(@"pushDict = %@", userInfo);
    NSLog(@"pushMessage = %@",pushMessage);
    
    [[NotificationEngine sharedInstance] handleNotification:userInfo];
    
    
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    // Create empty photo object
    /*NSString *photoId = [userInfo objectForKey:@"p"];
     PFObject *targetPhoto = [PFObject objectWithoutDataWithClassName:@"Photo"
     objectId:photoId];
     
     // Fetch photo object
     [targetPhoto fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
     // Show photo view controller
     if (error) {
     handler(UIBackgroundFetchResultFailed);
     } else if ([PFUser currentUser]) {
     PhotoVC *viewController = [[PhotoVC alloc] initWithPhoto:object];
     [self.navController pushViewController:viewController animated:YES];
     handler(UIBackgroundFetchResultNewData);
     } else {
     handler(UIBackgroundModeNoData);
     }
     }];*/
    NSLog(@"app Open With push dict = %@", userInfo);
    [[NotificationEngine sharedInstance] handleNotification:userInfo];
    
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if (!url) {  return NO; }
    
    NSString *URLString = [url absoluteString];
    NSArray *passArray = [URLString componentsSeparatedByString:@"://"];
    NSLog(@"Opening URL = %@",passArray);
    if (passArray.count >=2) {
        
        [NotificationEngine sharedInstance].passInfo = [passArray objectAtIndex:1];
        if ([NotificationEngine sharedInstance].mainView != nil) {
            [[NotificationEngine sharedInstance] analyzePassingInfo];
        }
        
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:URLString forKey:@"url"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return YES;
}


@end
