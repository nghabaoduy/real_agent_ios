//
//  ChatCell.h
//  Property App
//
//  Created by Brian on 11/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
@interface ChatCell : UITableViewCell

@property (nonatomic, retain)  IBOutlet UIView *chatView;
@property (nonatomic, retain) IBOutlet UILabel *timeTF;
@property (nonatomic, retain) IBOutlet UITextView *contentTF;
@property (nonatomic, retain) IBOutlet UILabel *usernameTF;

@property (nonatomic, retain)  IBOutlet UIView *propView;
@property (nonatomic, retain)  IBOutlet UIImageView *propIcon;
@property (nonatomic, retain)  IBOutlet UITextView *propAddress;
@property (nonatomic, retain)  IBOutlet UILabel *propPrice;

@property (nonatomic, retain) Property *prop;
@property (nonatomic, retain) NSString *propId;

@property (nonatomic, retain) NSMutableArray *parentPropArray;

-(void) setCellPropertyId:(NSString *) propId : (NSMutableArray *)parentPropArray;
-(void) setCellProperty:(Property *)prop;
-(void) refreshCell;
@end
