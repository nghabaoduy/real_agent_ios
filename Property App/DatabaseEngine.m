//
//  DatabaseEngine.m
//  PropertyV1
//
//  Created by Jonathan Le on 27/8/13.
//  Copyright (c) 2013 Jonathan Le. All rights reserved.
//

#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "MyClientViewController.h"
#import "SearchPropertyVC.h"
#import "MessageInfo.h"
#import "NotificationEngine.h"
#import "ScheduleListVC.h"
#import "CalendarDisplayCV.h"
#import "SalesListViewController.h"


@implementation DatabaseEngine
@synthesize propertyAray;

static DatabaseEngine *sharedObject;
static PFUser *curUser;


+ (DatabaseEngine*)sharedInstance
{
	if (sharedObject == nil) {
		sharedObject = [[super allocWithZone:NULL] init];
	}
	return sharedObject;
}

-(id)init {
	self = [super init];
	if(self) {

	}
	return self;
}

/**
 * User sign up
 * @param username: user's chosen username
 * @param password: user's chosen password
 * @param email:
 * @param phone:
 * Add more user's attribute down here
 */
-(NSString *)checkNULLStr:(NSString *)str
{
    if (str == nil) {
        str = @"N/A";
    }
    NSLog(@"checkNULLStr :'%@'",str);
    return str;
    
}

-(void)UserSignUp:(NSMutableDictionary*)  userInfoDict userType:(NSString*) userType signupView: (UIViewController*) signupView
{
    NSString *phone = [userInfoDict objectForKey:@"phone"];
    NSString *agentPhone = [userInfoDict objectForKey:@"agentPhone"];
    NSString *firstName = [userInfoDict objectForKey:@"firstName"];
    NSString *lastName = [userInfoDict objectForKey:@"lastName"];
    NSString *password = [userInfoDict objectForKey:@"password"];
    NSString *email = [userInfoDict objectForKey:@"email"];
    NSString *address = [userInfoDict objectForKey:@"address"];
    

    
    address = [self checkNULLStr:address];
    
    SignUpViewController* view = (SignUpViewController*) signupView;
    PFUser *user = [PFUser user];
    user.username = phone;
    user.password = password;
    user.email = email;
    
    // other fields can be set just like with PFObject
    //[user setObject:email forKey:@"email"];
	[user setObject:phone forKey:@"phone"];
    [user setObject:userType forKey:@"userType"];
    [user setObject:agentPhone forKey:@"agentPhone"];
    [user setObject:firstName forKey:@"firstName"];
	[user setObject:lastName forKey:@"lastName"];
    [user setObject:address forKey:@"address"];
    
    
    
    if (![agentPhone isEqual: @""] && agentPhone!= nil) {
        //client register
        [self SearchAndLinkAgentByPhone:agentPhone Client:user SignupView:view];
    }
    else
    {
        //agent register
        
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
         {
			 if (!error)
			 {
				 // Register successfully, do something after this
				 if(signupView!= nil)
				 {
					 [view SignUpSuccessful];
				 }
			 } else {
				 // Show the errorString somewhere and let the user try again.
				 NSString *errorString = [[error userInfo] objectForKey:@"error"];
				 if(signupView!= nil)
				 {
					 [view SignUpFail:errorString];
				 }
				 
			 }
		 }];
    }
    
    
}


- (void) UserLogin: (NSString*) username : (NSString*) password : (MyUIViewController*) loginView
{
    NSLog(@"user login runs");
	MyUIViewController* view = (MyUIViewController*) loginView;
	
	[PFUser logInWithUsernameInBackground:username password:password
									block:^(PFUser *user, NSError *error) {
										if (user) {
                                            [user fetchIfNeeded];
											// Do stuff after successful login.
											NSLog(@"user = %@", user.username);
											NSString* userType = [user objectForKey:@"userType"];
											//NSLog(@"type %@", userType);
											if ([userType isEqualToString:@"agent"]) {
												Agent* ag = [[Agent alloc] initWithUser:user];
												[ag GetPropertyList];
												[ag GetMyClientList];
												[[DataEngine sharedInstance] SetCurUser:ag];
                                                [[NotificationEngine sharedInstance] setPushUser:user];
											} else {
												Client* cli = [[Client alloc] initWithUser:user];
												[[DataEngine sharedInstance] SetCurUser:cli];
												[[DataEngine sharedInstance] setIsAgent:true];
											}
											//User *userInstance = [[User alloc] initWithUser:user];
											
											[user setObject: [NSNumber numberWithBool:YES] forKey:@"isInstall"];
											[user saveInBackground];
											
											curUser = [PFUser currentUser];
											
											[view actionSuceed:@""];
										} else {
											// The login failed. Check error to see why (wrong password?)
											NSString *errorString = [[error userInfo] objectForKey:@"error"];
											[view actionFail:errorString];
										}
									}];
}

- (void) UserLogout
{
	[PFUser logOut];
}

- (void) ResetPasswordRequest : (NSString*) email : (ResetPasswordViewController*) view
{
	//[PFUser requestPasswordResetForEmail:@"love.strings.93@gmail.com"];
	[PFUser requestPasswordResetForEmailInBackground:email block:^(BOOL succeeded, NSError *error) {
		if (!error) {
			[view RequestSuccessful];
		} else {
			NSString *errorString = [[error userInfo] objectForKey:@"error"];
			[view RequestFailed:errorString];
		}
	}];
}

- (void) ChangeNameByPhone: (NSString*) phone Firstname: (NSString*) fname Lastname: (NSString*) lname email:(NSString *)email address:(NSString *)address view:(MyUIViewController *)view{
	PFQuery *query = [PFUser query];
	[query whereKey:@"phone" equalTo:phone];
	
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
	 {
		 if(!error) {
			 if (objects.count > 1) {
				 NSLog(@"More than 1 record found, error!");
			 } else {
				 User *u = [[User alloc] initWithUser:[objects objectAtIndex:0]];
				 //[self ChangeUserInfo:u firstname:fname lastName:lname phone:@"" email:email address:address changeUserView:view];
			 }
		 }
	 }];
}


- (void) ChangeUserInfo: (User*) client
			  firstname:(NSString*) firstName lastName:(NSString*) lastName phone: (NSString*) phone email: (NSString*) email address:(NSString *) address changeUserView: (MyUIViewController*) view
{
	Agent* curAgent;
    curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
    [curAgent addRefreshView:view];
	
	[PFCloud callFunctionInBackground:@"editUser" withParameters:@{
                                                                   @"userId": [client getUserObject].objectId,
                                                                   @"firstN": firstName,
                                                                   @"lastN" : lastName,
                                                                   @"phoneNo" : @"",
                                                                   @"emailAdd" : email == nil? @"":email,
                                                                   @"add": address == nil? @"":address
                                                                   } block:^(id object, NSError *error) {
                                                                       if (!error) {
                                                                           NSLog(@"updateSucessful");
                                                                           [(Agent*) [[DataEngine sharedInstance] GetCurUser] RefreshData];
                                                                           
                                                                       } else {
                                                                           NSString *errorString = [[error userInfo] objectForKey:@"error"];
                                                                           [view actionFail:errorString];
                                                                       }
                                                                   }];
}



- (void) SearchAndLinkAgentByPhone : (NSString*) agentPhoneNo Client: (PFUser *)client SignupView:(SignUpViewController*) signupView
{
    
	PFQuery *query = [PFUser query];
	
	NSLog(@"phone no: %@", agentPhoneNo);
	
	[query whereKey:@"phone" equalTo:agentPhoneNo];
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
	 {
		 if (!error)
		 {
			 //no agent found
			 if(objects.count == 0)
			 {
				 NSString *errorString = [[error userInfo] objectForKey:@"No agent found."];
				 if(signupView != nil)
				 {
					 [signupView SignUpFail:errorString];
				 }
				 return;
			 }
			 // Find agent succeeded.
			 NSLog(@"Successfully retrieved %d agents.", objects.count);
			 //create client
			 [client signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
			  {
				  if (!error)
				  {
					  // Register successfully, do something after this
					  //Link Data
					  PFUser *agentLink = [objects objectAtIndex:0];
					  [[DatabaseEngine sharedInstance] AddAgentClientLink:agentLink :client : nil];
					  if(signupView != nil)
					  {
						  [signupView SignUpSuccessful];
					  }
                      else{
                          
                      }
				  } else {
					  // Show the errorString somewhere and let the user try again.
                      NSLog(@"Create client failed - error: %@",error.localizedDescription);
					  NSString *errorString = [[error userInfo] objectForKey:@"Create Client fail"];
					  if(signupView != nil)
					  {
						  [signupView SignUpFail:errorString];
					  }
                      else
                      {
                          [[[DataEngine sharedInstance] clientSearchView] actionFail:error.localizedDescription];
                          //[[[DataEngine sharedInstance] clientSearchView] actionFail:[NSString stringWithFormat:@"%i",[error code]]];
                      }
				  }
			  }];
			 
			 
			 
			 
		 } else {
			 //Find agent fail.
			 if(signupView != nil)
			 {
                 NSString *errorString = [[error userInfo] objectForKey:@"Find agent fail."];
				 [signupView SignUpFail:errorString];
			 }
			 
		 }
	 }];
}

-(void) addOrUpdateClient:(User *)user processingArray: (NSMutableArray *)processingArray view:(MyUIViewController *)view
{
    PFQuery *query = [PFUser query];
    [query whereKey:@"phone" equalTo:user.phone];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         if (!error) {
             if (objects.count > 0) {
                 NSLog(@"user is already there - %@",user.email);
                 
                 [[DatabaseEngine sharedInstance] ChangeNameByPhone:user.phone Firstname:user.firstName Lastname:user.lastName email:user.email address:user.address view:view];
             }
             else
             {
                 [[DatabaseEngine sharedInstance] addNewClientToDatabase:user processingArray:nil view:view];
             }
         }
         else
         {
             NSLog(@"%@",error.debugDescription);
         }
     }];
}

-(BOOL) addNewClientToDatabase:(User *)user processingArray: (NSMutableArray *)processingArray view:(MyUIViewController *)view
{
    //cur agent user
    Agent* curAgent;
    curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
    [curAgent addRefreshView:view];
    
    if(user!= nil)
    {
        if (user.phone == nil || user.phone == NULL || [user.phone isEqualToString:@""]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:[NSString stringWithFormat:@"%@ %@'s phone no. is blank",user.firstName,user.lastName]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return NO;
        }
        //  Client *client = [[Client alloc] init];
        // [client InitWithUserInfo:user];
        
        NSString *unknownEmail = @"";
        if (user.firstName == nil) {
            user.firstName = @"";
        }
        if (user.lastName == nil) {
            user.lastName = @"";
        }
        
        if (user.email == nil) {
            unknownEmail = [NSString stringWithFormat:@"%@@unknow.com",user.phone];
        }
        else
        {
            unknownEmail = user.email;
        }

        NSLog(@"pass in Client : %@ : %@ : %@ : %@",user.phone,user.firstName,user.lastName,user.email);
        
        NSLog(@"curAgent phone = %@",[curAgent phone]);
        
        if (processingArray != nil) {
            [processingArray addObject:user];
        }
        
        NSMutableDictionary *userInfoDict = [[NSMutableDictionary alloc] init];
        [userInfoDict setObject:user.phone forKey:@"phone"];
        [userInfoDict setObject:user.phone forKey:@"password"];
        [userInfoDict setObject:unknownEmail forKey:@"email"];
        [userInfoDict setObject:[curAgent phone] forKey:@"agentPhone"];
        [userInfoDict setObject:user.firstName forKey:@"firstName"];
        [userInfoDict setObject:user.lastName forKey:@"lastName"];
        [userInfoDict setObject:@"client" forKey:@"userType"];
        [userInfoDict setObject:user.address forKey:@"address"];
        NSLog(@"userInfoDict = %@",userInfoDict);
        [[DatabaseEngine sharedInstance] ClientSignUp:userInfoDict view:view];
    }
    
    
    return user != nil;
    
    
}

-(NSString *) removeClientString:(NSString *) input
{
    NSLog(@"input str = %@",input);
    input = [input stringByReplacingOccurrencesOfString:@"client " withString:@""];
    input = [input stringByReplacingOccurrencesOfString:@" client" withString:@""];
    input = [input stringByReplacingOccurrencesOfString:@" client " withString:@""];
    input = [input stringByReplacingOccurrencesOfString:@"client" withString:@""];
    
    input = [input stringByReplacingOccurrencesOfString:@"Client " withString:@""];
    input = [input stringByReplacingOccurrencesOfString:@" Client" withString:@""];
    input = [input stringByReplacingOccurrencesOfString:@" Client " withString:@""];
    input = [input stringByReplacingOccurrencesOfString:@"Client" withString:@""];
    
    return input;
    NSLog(@"output str = %@",input);
}

-(BOOL) checkAgentClientLinkAvailable:(PFObject*) agent : (PFObject*) client : (MyUIViewController *) view
{
	
}
-(void)ClientSignUp:(NSMutableDictionary*) userInfoDict view: (MyUIViewController*) view
{
    
    NSLog(@"Client SignUp runs");
    NSString *phone = [userInfoDict objectForKey:@"phone"];
    NSString *agentPhone = [userInfoDict objectForKey:@"agentPhone"];
    NSString *firstName = [userInfoDict objectForKey:@"firstName"];
    NSString *lastName = [userInfoDict objectForKey:@"lastName"];
    NSString *password = [userInfoDict objectForKey:@"password"];
    NSString *email = [userInfoDict objectForKey:@"email"];
    NSString *address = [userInfoDict objectForKey:@"address"];
    
    address = [self checkNULLStr:address];
    
    PFUser *user = [PFUser user];
    user.username = phone;
    user.password = password;
    user.email = email;
    
	[user setObject:phone forKey:@"phone"];
    [user setObject:@"client" forKey:@"userType"];
    [user setObject:agentPhone forKey:@"agentPhone"];
    [user setObject:firstName forKey:@"firstName"];
	[user setObject:lastName forKey:@"lastName"];
    [user setObject:address forKey:@"address"];
    
    PFQuery *query = [PFUser query];
	
	NSLog(@"phone no: %@", agentPhone);
	
	[query whereKey:@"phone" equalTo:agentPhone];
	[query findObjectsInBackgroundWithBlock:^(NSArray *agents, NSError *error) {
        if (!error)
		 {
			 //no agent found
			 if(agents.count == 0)
			 {
				 NSString *errorString = [[error userInfo] objectForKey:@"No agent found."];
				 if(view != nil)
				 {
					 [view actionFail:errorString];
				 }
				 return;
			 }
             
			 // Find agent succeeded.
             PFObject *agent = [agents objectAtIndex:0];
			 NSLog(@"ClientSignUp - Successfully retrieved agent.");
			 //create client
             
             PFQuery *getClient = [PFUser query];
             [getClient whereKey:@"phone" equalTo:[userInfoDict objectForKey:@"phone"]];
             NSLog(@"search client with phone %@",[userInfoDict objectForKey:@"phone"]);
             [getClient findObjectsInBackgroundWithBlock:^(NSArray *clients, NSError *error) {
                 NSLog(@"getClientquery - client: %@ - error:%@",clients,error);
                 if (!error) {
                     //client has registered
                     if (clients.count > 0) {
                         PFObject *client = [clients objectAtIndex:0];
                         //updateClient
                         [[DatabaseEngine sharedInstance] ChangeNameByPhone:phone Firstname:firstName Lastname:lastName email:email address:address view:view];
                         
                         
                         //check link avai
                         PFQuery *linkQuery = [PFQuery queryWithClassName:@"AgentClientRecord"];
                         [linkQuery whereKey:@"agent" equalTo:agent];
                         [linkQuery whereKey:@"client" equalTo:client];
                         [linkQuery findObjectsInBackgroundWithBlock:^(NSArray *links, NSError *error) {
                             NSLog(@"linkQuery - client: %@ - error:%@",links,error);
                             if (!error)
                             {
                                 //link avai
                                 
                                 if (links.count == 0) {
                                     NSLog(@"Link available");
                                     [self AddAgentClientLink:[[[DataEngine sharedInstance] curUser] userObject] :client:view];
                                 }
                                 //link not avai
                                 else
                                     NSLog(@"Link not available");
                                 {
                                     [view actionSuceed:@""];
                                 }
                             }
                             else
                             {
                                 NSString *errorString = [error debugDescription];
                                 if(view != nil)
                                 {
                                     [view actionFail:errorString];
                                 }
                             }
                         }];
                         //create link
                     }
                     //client has not registered
                     else
                     {
                         NSLog(@"client has not registered");
                         [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
                          {
                              if (!error)
                              {
                                  // Register successfully, do something after this
                                  //Link Data
                                  //create link
                                  PFObject *link = [PFObject objectWithClassName:@"AgentClientRecord"];
                                  
                                  [link setObject:agent forKey:@"agent"];
                                  [link setObject:user forKey:@"client"];
                                  [link saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                      if(!error)
                                      {
                                          [(Agent*) [[DataEngine sharedInstance] GetCurUser] RefreshData];
                                          [view actionSuceed:@"sucess create client"];
                                      }
                                  }];
                                  
                              } else {
                                  // Show the errorString somewhere and let the user try again.
                                  NSString *errorString = [error debugDescription];
                                  if(view != nil)
                                  {
                                      [view actionFail:errorString];
                                  }
                              }
                          }];
                     }
                 }
                 else
                 {
                     NSString *errorString = [error debugDescription];
                     if(view != nil)
                     {
                         [view actionFail:errorString];
                     }
                 }
                
             }];
		 } else {
			 //Find agent fail.
             NSString *errorString = [error debugDescription];
             if(view != nil)
             {
                 [view actionFail:errorString];
             }
		 }
	 }];
    
    
}

-(BOOL) updateUser:(User *)user view:(MyUIViewController *)view
{
    //add refresh view
    Agent* curAgent;
    curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
    [curAgent addRefreshView:view];
    
    
    NSLog(@"update user: %@",user);
    if(user!= nil)
    {
        if (user.phone == nil || user.phone == NULL || [user.phone isEqualToString:@""]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:[NSString stringWithFormat:@"%@ %@'s phone no. is blank",user.firstName,user.lastName]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return NO;
        }
        
        NSString *unknownEmail = @"";
        if (user.firstName == nil) {
            user.firstName = @"";
        }
        if (user.lastName == nil) {
            user.lastName = @"";
        }
        
        if (user.email == nil) {
            unknownEmail = @"";
			//unknownEmail = [NSString stringWithFormat:@"%@%@client@unknow.com",user.firstName,user.lastName];
			//unknownEmail = [unknownEmail stringByReplacingOccurrencesOfString:@" " withString:@""];
        }
        else
        {
            unknownEmail = user.email;
        }
        //cur agent user
        
        
        NSLog(@"pass in Client : %@ : %@ : %@ : %@",user.phone,user.firstName,user.lastName,user.email);
        
        NSLog(@"curAgent phone = %@",[curAgent phone]);

        NSMutableDictionary *userInfoDict = [[NSMutableDictionary alloc] init];
        [userInfoDict setObject:user.phone forKey:@"phone"];
        [userInfoDict setObject:user.phone forKey:@"password"];
        [userInfoDict setObject:unknownEmail forKey:@"email"];
        [userInfoDict setObject:[curAgent phone] forKey:@"agentPhone"];
        [userInfoDict setObject:user.firstName forKey:@"firstName"];
        [userInfoDict setObject:user.lastName forKey:@"lastName"];
        [userInfoDict setObject:@"client" forKey:@"userType"];
        [userInfoDict setObject:user.address forKey:@"address"];
        NSLog(@"userInfoDict = %@",userInfoDict);
        [[DatabaseEngine sharedInstance] updateUserToDB:userInfoDict view:view];
    }
    return user != nil;
}

-(void)updateUserToDB:(NSMutableDictionary*) userInfoDict view: (MyUIViewController*) view
{
    NSString *phone = [userInfoDict objectForKey:@"phone"];
    NSString *agentPhone = [userInfoDict objectForKey:@"agentPhone"];
    NSString *firstName = [userInfoDict objectForKey:@"firstName"];
    NSString *lastName = [userInfoDict objectForKey:@"lastName"];
    NSString *password = [userInfoDict objectForKey:@"password"];
    NSString *email = [userInfoDict objectForKey:@"email"];
    NSString *address = [userInfoDict objectForKey:@"address"];
    
    
    
    address = [self checkNULLStr:address];
    
    PFQuery *query = [PFQuery queryWithClassName:@"User"];
    [query whereKey:@"phone" equalTo:phone];
    [query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error)
    {
        if (users.count > 0) {
            PFObject *user = [users objectAtIndex:0];
            [user setObject:firstName forKey:@"firstName"];
            [user setObject:lastName forKey:@"lastName"];
            [user setObject:address forKey:@"address"];
            
            [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
             {
                 if (!error) {
                     Agent* curAgent;
                     curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
                     [curAgent RefreshData];
                 }
             }];
        }
        else
        {
            NSLog(@"%@",error.debugDescription);
        }
    }];
    
    
}



/**
 * Add a property record to the database
 * @param name: property name
 */
//Question: need to fill up every detail or not
- (void) AddProperty: (Property*) propertyIn
{
    if (propertyIn.propertyObject != NULL) {
        [propertyIn.propertyObject deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if(!error) {
                NSLog(@"success remove curObject");
                [self startUploadProperty:propertyIn];
            } else {
                NSString *errorString = [[error userInfo] objectForKey:@"error"];
                [propertyIn AddPropertyFailed:errorString];
            }
        } ];
        
    }
    else
    {
        [self startUploadProperty:propertyIn];
    }
}
- (void) startUploadProperty: (Property*) propertyIn
{

	NSLog(@"Start adding");
	PFObject *property = [PFObject objectWithClassName:@"Property"];
    NSLog(@"AddProperty Log : %@ - %@ - %@",propertyIn.propertyName,propertyIn.propertyType,propertyIn.listingType);
    [property setObject:propertyIn.propertyName forKey:@"propertyName"];
    [property setObject:propertyIn.propertyType forKey:@"propertyType"];
    [property setObject:propertyIn.listingType forKey:@"listingType"];
    
    NSLog(@"adding 1");
    [property setObject:propertyIn.blkNo forKey:@"blkNo"];
	[property setObject:propertyIn.unitNo forKey:@"unitNo"];
	[property setObject:propertyIn.levelNumber forKey:@"levelNo"];
	[property setObject:propertyIn.address forKey:@"address"];
	[property setObject:propertyIn.district forKey:@"district"];
	[property setObject:propertyIn.postCode forKey:@"postcode"];
    //----
	[property setObject:[NSNumber numberWithInt:[propertyIn.price intValue]] forKey:@"price"];
	[property setObject:[NSNumber numberWithInt:[propertyIn.area intValue]] forKey:@"area"];
    [property setObject:propertyIn.tenure forKey:@"tenure"];
    if (propertyIn.bedroom != nil) {
        [property setObject:[NSNumber numberWithInt:[propertyIn.bedroom intValue]] forKey:@"bedroom"];
    }
    NSLog(@"adding 2");
    //----
    [property setObject:propertyIn.status forKey:@"status"];
    [property setObject:[NSNumber numberWithInt:[propertyIn.rentedPrice intValue]] forKey:@"rentedPrice"];
    //----
    [property setObject:propertyIn.adder.userObject forKey:@"userAdded"];
    [property setObject:propertyIn.isSubmited forKey:@"submit"];
    
    //----
    //condo field
    [property setObject:propertyIn.propertyName forKey:@"project"];
    [property setObject:[NSNumber numberWithInt:[propertyIn.PSF intValue]] forKey:@"psf"];
    [property setObject:propertyIn.rentalYeild forKey:@"rentalYield"];
    [property setObject:propertyIn.topYear forKey:@"topYear"];
    NSLog(@"adding 3");
    //---Commercial
    
    if (propertyIn.prType != nil) {
        [property setObject:propertyIn.prType forKey:@"prType"];
    }
    
    if (propertyIn.commercialType != nil) {
        [property setObject:propertyIn.commercialType forKey:@"commercialType"];
    }
    
    [property setObject:[NSNumber numberWithInt:[propertyIn.tenantedPrice intValue]] forKey:@"tenantedPrice"];
    
    if (propertyIn.gst != nil) {
        [property setObject:propertyIn.gst forKey:@"gst"];
    }
    
    if (propertyIn.groundLevel != nil) {
        [property setObject:propertyIn.groundLevel forKey:@"groundLevel"];
    }
    
    if (propertyIn.condition) {
        [property setObject:propertyIn.condition forKey:@"condition"];
    }
    NSLog(@"adding 4");
    if (propertyIn.electricalLoad != nil) {
        [property setObject:propertyIn.electricalLoad forKey:@"electricalLoad"];
    }
    if (propertyIn.amps != nil) {
        [property setObject:propertyIn.amps forKey:@"amps"];
    }
    if (propertyIn.aircon != nil) {
        [property setObject:propertyIn.aircon forKey:@"aircon"];
    }
    
    if (propertyIn.ownPantry != nil) {
        [property setObject:propertyIn.ownPantry forKey:@"ownPantry"];
    }
    
    if (propertyIn.grade != nil) {
        [property setObject:propertyIn.grade forKey:@"grade"];
    }
    
    if (propertyIn.floorLoad != nil) {
        [property setObject:propertyIn.floorLoad forKey:@"floorLoad"];
    }
    
    if (propertyIn.serverRoom != nil) {
        [property setObject:propertyIn.serverRoom forKey:@"serverRoom"];
    }
    
    if (propertyIn.retailType != nil) {
        [property setObject:propertyIn.retailType forKey:@"retailType"];
    }
    
    if (propertyIn.fAndBType != nil) {
        [property setObject:propertyIn.fAndBType forKey:@"fAndBType"];
    }
    
    if (propertyIn.greaseTrap != nil) {
        [property setObject:propertyIn.greaseTrap forKey:@"greaseTrap"];
    }
    
    if (propertyIn.exhaust != nil) {
        [property setObject:propertyIn.exhaust forKey:@"exhaust"];
    }
    NSLog(@"adding 5");
    [property setObject:[NSNumber numberWithInt:[propertyIn.landSize intValue]] forKey:@"landSize"];
    //[property setObject:[NSNumber numberWithInt:[propertyIn.area intValue]]  forKey:@"builtUp"];
    [property setObject:[NSNumber numberWithInt:[propertyIn.noOfStorey intValue]] forKey:@"noOfStorey"];
    
    if (propertyIn.hdbType != Nil) {
        [property setObject:propertyIn.hdbType forKey:@"hdbType"];
    }
    
    if (propertyIn.liftLevel != nil) {
        [property setObject:propertyIn.liftLevel forKey:@"liftLevel"];
    }
    if (propertyIn.mainDoorDirection != nil) {
        [property setObject:propertyIn.mainDoorDirection forKey:@"mainDoorDirection"];
    }
    if (propertyIn.roomPosition != nil) {
        [property setObject:propertyIn.roomPosition forKey:@"roomPosition"];
    }
    if (propertyIn.town != nil) {
        [property setObject:propertyIn.town forKey:@"town"];
    }
    if (propertyIn.valuation != nil) {
        [property setObject:propertyIn.valuation forKey:@"valuation"];
    }
    
    
    if (propertyIn.industryType != Nil) {
        [property setObject:propertyIn.industryType forKey:@"industryType"];
    }
    
    if (propertyIn.vehicleAccess != Nil) {
        [property setObject:propertyIn.vehicleAccess forKey:@"vehicleAccess"];
    }
    if (propertyIn.noOfStorey != Nil) {
        [property setObject:[NSNumber numberWithInt:[propertyIn.noOfStorey intValue]] forKey:@"noOfStorey"];
    }
    
    if (propertyIn.noOfCargoLift != Nil) {
        [property setObject:[NSNumber numberWithInt:[propertyIn.noOfCargoLift intValue]] forKey:@"noOfCargoLift"];
    }
    
    if (propertyIn.flatted != Nil) {
        [property setObject:propertyIn.flatted forKey:@"flatted"];
    }
    
    if (propertyIn.ceilingHeight != Nil) {
        [property setObject:[NSNumber numberWithInt:[propertyIn.ceilingHeight intValue]]  forKey:@"ceilingHeight"];
    }
    
    if (propertyIn.type != Nil) {
        [property setObject:propertyIn.type forKey:@"type"];
    }
    
    if (propertyIn.iconImage != Nil) {
        
        NSData* imageData = UIImageJPEGRepresentation(propertyIn.iconImage, 0.001f);
        PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:imageData];
        [property setObject:imageFile forKey:@"picture"];
    }
    
    if (propertyIn.landType != Nil) {
        [property setObject:propertyIn.landType forKey:@"landType"];
    }
    if (propertyIn.furnishing != Nil) {
        [property setObject:propertyIn.furnishing forKey:@"furnishing"];
    }

    
    if (propertyIn.mainGateFacing != Nil) {
        [property setObject:propertyIn.mainGateFacing forKey:@"mainGateFacing"];
    }

    if (propertyIn.carPark != Nil) {
        [property setObject:propertyIn.carPark forKey:@"carPark"];
    }

    if (propertyIn.garden != Nil) {
        [property setObject:propertyIn.garden forKey:@"garden"];
    }

    if (propertyIn.swimmingPool != Nil) {
        [property setObject:propertyIn.swimmingPool forKey:@"swimmingPool"];
    }

    if (propertyIn.basement != Nil) {
        [property setObject:propertyIn.basement forKey:@"basement"];
    }

    if (propertyIn.roofTerrance != Nil) {
        [property setObject:propertyIn.roofTerrance forKey:@"roofTerrance"];
    }

    
    
    //----
    NSLog(@"saveAgent");
    propertyIn.agent = (Agent*)[[DataEngine sharedInstance] GetCurUser];
    [property setObject:propertyIn.agent.userObject forKey:@"agent"];
    
	[property saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		if(!error) {
			//No error at adding
			//this is the object after creating
            NSLog(@"Done Upload and Download Infomation");
            //Property* prop = [[Property alloc] initWithProperty:property];
			
            [propertyIn AddPropertySuccessfully:property];
            
            Agent *curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
            [curAgent GetAgentClientPropertyList];
            //[curAgent.myClientPropertyList addObject:propertyIn];
			
		} else {
			//Adding error. Show the error message
			NSString *errorString = [[error userInfo] objectForKey:@"error"];
			[propertyIn AddPropertyFailed:errorString];
		}
	} ];
}





- (void)LoadIcon:(PFObject *)proper :(SalesListViewController *)sales :(Property *)curProper
{
    NSLog(@"Start getting Image 1");
    PFQuery *query = [PFQuery queryWithClassName:@"Photo"];
    [query whereKey:@"property" equalTo:proper];
	NSLog(@"Start getting Image 2");
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		// If there are photos, we start extracting the data
		// Save a list of object IDs while extracting this data
		if (!error) {
            if ([objects count] == 0) {
                curProper.iconImage = [UIImage imageNamed:@"condo2.jpg"];
                [sales AddToPropertyList:curProper];
            }
            else
            {
                PFFile *theImage = [[objects objectAtIndex:0] objectForKey:@"photo"];
                NSData *imageData = [theImage getData];
                UIImage *image = [UIImage imageWithData:imageData];
                
                curProper.iconImage = image;
                [sales AddToPropertyList:curProper];
            }
		} else {
			
		}
	}];
    
}



- (void) SearchProperty: (Property*) propertyIn : (SalesListViewController*) view {
	propertyAray = [[NSMutableArray alloc] init];
	PFQuery *query = [PFQuery queryWithClassName:@"Property"];
	
	//Search criteria. Can use equalTo, lessThan, nearGeopoint....
	
	if ([propertyIn.listingType length] != 0) {
		[query whereKey:@"listingType" equalTo:propertyIn.listingType];
	}
	
	//query for type
	if ([propertyIn.propertyType length] != 0) {
		[query whereKey:@"propertyType" equalTo:propertyIn.propertyType];
	}
    
    
    if ([propertyIn.propertyType isEqualToString:@"Private Residential"]) {
        NSLog(@"search PR");
        //query for district
        if ([propertyIn.district length] != 0) {
            NSLog(@"district: %@", propertyIn.district);
            [query whereKey:@"district" equalTo:propertyIn.district];
        }
        
        //query for bedroom
        if ([propertyIn.bedroom length] !=0) {
            NSLog(@"bedroom: %@", propertyIn.bedroom);
            [query whereKey:@"bedroom" lessThan:[NSNumber numberWithInt:[propertyIn.bedroom intValue]]];
        }
        
        //query for area
        if ([propertyIn.area length] != 0) {
            NSLog(@"area: %@", propertyIn.area);
            [query whereKey:@"area" lessThan:[NSNumber numberWithInt:[propertyIn.area intValue]]];
        }
        
        //query for price
        if ([propertyIn.price length] != 0) {
            NSLog(@"price: %@", propertyIn.price);
            [query whereKey:@"price" lessThan:[NSNumber numberWithInt:[propertyIn.price intValue]]];
        }
        
        //query for tenure
        if ([propertyIn.tenure length] != 0) {
            NSLog(@"tenure: %@", propertyIn.tenure);
            [query whereKey:@"tenure" equalTo:propertyIn.tenure];
        }
        
        //query for tenure
        if ([propertyIn.prType length] != 0) {
            NSLog(@"pr type: %@", propertyIn.prType);
            [query whereKey:@"prType" equalTo:propertyIn.prType];
        }
        
        //query for rented price
        if ([propertyIn.rentedPrice length] != 0) {
            NSLog(@"rented price: %@", propertyIn.rentedPrice);
            [query whereKey:@"rentedPrice" lessThan:[NSNumber numberWithInt:[propertyIn.rentedPrice intValue]]];
        }
        
        //query for project name
        if ([propertyIn.projectName length] != 0) {
            NSLog(@"project name: %@", propertyIn.projectName);
            [query whereKey:@"projectName" equalTo:propertyIn.projectName];
        }
        
        //query for psf
        if ([propertyIn.PSF length] != 0) {
            NSLog(@"psf: %@", propertyIn.PSF);
            [query whereKey:@"psf" lessThan:[NSNumber numberWithInt:[propertyIn.PSF intValue]]];
        }
        
        //query for top year
        if ([propertyIn.topYear length] != 0) {
            NSLog(@"top year: %@", propertyIn.topYear);
            [query whereKey:@"topYear" equalTo:propertyIn.topYear];
        }
        
        //query for rental Yield
        if ([propertyIn.rentalYeild length] != 0) {
            NSLog(@"rental yield: %@", propertyIn.rentalYeild);
            [query whereKey:@"rentalYield" equalTo:propertyIn.rentalYeild];
        }
    }
    else if ([propertyIn.propertyType isEqualToString:@"HDB"])
    {
        NSLog(@"search HDB");
        //query for district
        if ([propertyIn.district length] != 0) {
            NSLog(@"district: %@", propertyIn.district);
            [query whereKey:@"district" equalTo:propertyIn.district];
        }
        
        //query for area
        if ([propertyIn.area length] != 0) {
            NSLog(@"area: %@", propertyIn.area);
            [query whereKey:@"area" lessThan:[NSNumber numberWithInt:[propertyIn.area intValue]]];
        }
        
        //query for rented price
        if ([propertyIn.rentedPrice length] != 0) {
            NSLog(@"rented price: %@", propertyIn.rentedPrice);
            [query whereKey:@"rentedPrice" lessThan:[NSNumber numberWithInt:[propertyIn.rentedPrice intValue]]];
        }
        
        //query for level
        if ([propertyIn.groundLevel length] != 0) {
            NSLog(@"level: %@", propertyIn.district);
            [query whereKey:@"groundLevel" equalTo:propertyIn.groundLevel];
        }
        
        //query for hdb type
        if ([propertyIn.hdbType length] != 0) {
            NSLog(@"hdb type: %@", propertyIn.hdbType);
            [query whereKey:@"hdbType" equalTo:propertyIn.hdbType];
        }
        
        //query for lift level
        if ([propertyIn.hdbType length] != 0) {
            NSLog(@"hdb type: %@", propertyIn.liftLevel);
            [query whereKey:@"liftLevel" equalTo:propertyIn.liftLevel];
        }
        
        //query for main door direction
        if ([propertyIn.mainDoorDirection length] != 0) {
            NSLog(@"main door direction: %@", propertyIn.mainDoorDirection);
            [query whereKey:@"mainDoorDirection" equalTo:propertyIn.mainDoorDirection];
        }
        
        //query for room position
        if ([propertyIn.roomPosition length] != 0) {
            NSLog(@"room position: %@", propertyIn.roomPosition);
            [query whereKey:@"roomPosition" equalTo:propertyIn.roomPosition];
        }
        
        
        //query for town
        if ([propertyIn.town length] != 0) {
            NSLog(@"town: %@", propertyIn.town);
            [query whereKey:@"town" equalTo:propertyIn.town];
        }
        
        //query for valuation
        if ([propertyIn.valuation length] != 0) {
            NSLog(@"valuation: %@", propertyIn.valuation);
            [query whereKey:@"valuation" equalTo:propertyIn.valuation];
        }
        
        //query for condition
        if ([propertyIn.condition length] != 0) {
            NSLog(@"condition: %@", propertyIn.condition);
            [query whereKey:@"condition" equalTo:propertyIn.condition];
        }
        
        
    }
    else if ([propertyIn.propertyType isEqualToString:@"Industrial"])
    {
        //query for commercial type
        if ([propertyIn.industryType length] != 0) {
            NSLog(@"industry Type: %@", propertyIn.industryType);
            [query whereKey:@"industryType" equalTo:propertyIn.industryType];
        }
        
        //query for district
        if ([propertyIn.district length] != 0) {
            NSLog(@"district: %@", propertyIn.district);
            [query whereKey:@"district" equalTo:propertyIn.district];
        }
        
        //query for area
        if ([propertyIn.area length] != 0) {
            NSLog(@"area: %@", propertyIn.area);
            [query whereKey:@"area" lessThan:[NSNumber numberWithInt:[propertyIn.area intValue]]];
        }
        
        //query for price
        if ([propertyIn.price length] != 0) {
            NSLog(@"price: %@", propertyIn.price);
            [query whereKey:@"price" lessThan:[NSNumber numberWithInt:[propertyIn.price intValue]]];
        }
        
        //query for tenure
        if ([propertyIn.tenure length] != 0) {
            NSLog(@"tenure: %@", propertyIn.tenure);
            [query whereKey:@"tenure" equalTo:propertyIn.tenure];
        }
        
        //query for rented price
        if ([propertyIn.rentedPrice length] != 0) {
            NSLog(@"rented price: %@", propertyIn.rentedPrice);
            [query whereKey:@"rentedPrice" lessThan:[NSNumber numberWithInt:[propertyIn.rentedPrice intValue]]];
        }
        
        //query for project name
        if ([propertyIn.projectName length] != 0) {
            NSLog(@"project name: %@", propertyIn.projectName);
            [query whereKey:@"projectName" equalTo:propertyIn.projectName];
        }
        
        //query for psf
        if ([propertyIn.PSF length] != 0) {
            NSLog(@"psf: %@", propertyIn.PSF);
            [query whereKey:@"psf" lessThan:[NSNumber numberWithInt:[propertyIn.PSF intValue]]];
        }
        
        //query for top year
        if ([propertyIn.topYear length] != 0) {
            NSLog(@"top year: %@", propertyIn.topYear);
            [query whereKey:@"topYear" equalTo:propertyIn.topYear];
        }
        
        //query for rental Yield
        if ([propertyIn.rentalYeild length] != 0) {
            NSLog(@"rental yield: %@", propertyIn.rentalYeild);
            [query whereKey:@"rentalYield" equalTo:propertyIn.rentalYeild];
        }
    }
    else if ([propertyIn.propertyType isEqualToString:@"Commercial"])
    {
        //query for commercial type
        if ([propertyIn.commercialType length] != 0) {
            NSLog(@"commercial Type: %@", propertyIn.commercialType);
            [query whereKey:@"commercialType" equalTo:propertyIn.commercialType];
        }
        
        //query for district
        if ([propertyIn.district length] != 0) {
            NSLog(@"district: %@", propertyIn.district);
            [query whereKey:@"district" equalTo:propertyIn.district];
        }
        
        //query for area
        if ([propertyIn.area length] != 0) {
            NSLog(@"area: %@", propertyIn.area);
            [query whereKey:@"area" lessThan:[NSNumber numberWithInt:[propertyIn.area intValue]]];
        }
        
        //query for price
        if ([propertyIn.price length] != 0) {
            NSLog(@"price: %@", propertyIn.price);
            [query whereKey:@"price" lessThan:[NSNumber numberWithInt:[propertyIn.price intValue]]];
        }
        
        //query for tenure
        if ([propertyIn.tenure length] != 0) {
            NSLog(@"tenure: %@", propertyIn.tenure);
            [query whereKey:@"tenure" equalTo:propertyIn.tenure];
        }
        
        //query for rented price
        if ([propertyIn.rentedPrice length] != 0) {
            NSLog(@"rented price: %@", propertyIn.rentedPrice);
            [query whereKey:@"rentedPrice" lessThan:[NSNumber numberWithInt:[propertyIn.rentedPrice intValue]]];
        }
        
        //query for project name
        if ([propertyIn.projectName length] != 0) {
            NSLog(@"project name: %@", propertyIn.projectName);
            [query whereKey:@"projectName" equalTo:propertyIn.projectName];
        }
        
        //query for psf
        if ([propertyIn.PSF length] != 0) {
            NSLog(@"psf: %@", propertyIn.PSF);
            [query whereKey:@"psf" lessThan:[NSNumber numberWithInt:[propertyIn.PSF intValue]]];
        }
        
        //query for top year
        if ([propertyIn.topYear length] != 0) {
            NSLog(@"top year: %@", propertyIn.topYear);
            [query whereKey:@"topYear" equalTo:propertyIn.topYear];
        }
        
        //query for rental Yield
        if ([propertyIn.rentalYeild length] != 0) {
            NSLog(@"rental yield: %@", propertyIn.rentalYeild);
            [query whereKey:@"rentalYield" equalTo:propertyIn.rentalYeild];
        }
    }else if ([propertyIn.propertyType isEqualToString:@"Land"])
    {
        //query for commercial type
        if ([propertyIn.commercialType length] != 0) {
            NSLog(@"land Type: %@", propertyIn.landType);
            [query whereKey:@"landType" equalTo:propertyIn.landType];
        }
        
        //query for district
        if ([propertyIn.district length] != 0) {
            NSLog(@"district: %@", propertyIn.district);
            [query whereKey:@"district" equalTo:propertyIn.district];
        }
        
        //query for area
        if ([propertyIn.area length] != 0) {
            NSLog(@"area: %@", propertyIn.area);
            [query whereKey:@"area" lessThan:[NSNumber numberWithInt:[propertyIn.area intValue]]];
        }
        
        //query for price
        if ([propertyIn.price length] != 0) {
            NSLog(@"price: %@", propertyIn.price);
            [query whereKey:@"price" lessThan:[NSNumber numberWithInt:[propertyIn.price intValue]]];
        }
        
        //query for tenure
        if ([propertyIn.tenure length] != 0) {
            NSLog(@"tenure: %@", propertyIn.tenure);
            [query whereKey:@"tenure" equalTo:propertyIn.tenure];
        }
        
        //query for rented price
        if ([propertyIn.rentedPrice length] != 0) {
            NSLog(@"rented price: %@", propertyIn.rentedPrice);
            [query whereKey:@"rentedPrice" lessThan:[NSNumber numberWithInt:[propertyIn.rentedPrice intValue]]];
        }
        
        //query for project name
        if ([propertyIn.projectName length] != 0) {
            NSLog(@"project name: %@", propertyIn.projectName);
            [query whereKey:@"projectName" equalTo:propertyIn.projectName];
        }
        
        //query for psf
        if ([propertyIn.PSF length] != 0) {
            NSLog(@"psf: %@", propertyIn.PSF);
            [query whereKey:@"psf" lessThan:[NSNumber numberWithInt:[propertyIn.PSF intValue]]];
        }
        
        //query for top year
        if ([propertyIn.topYear length] != 0) {
            NSLog(@"top year: %@", propertyIn.topYear);
            [query whereKey:@"topYear" equalTo:propertyIn.topYear];
        }
        
        //query for rental Yield
        if ([propertyIn.rentalYeild length] != 0) {
            NSLog(@"rental yield: %@", propertyIn.rentalYeild);
            [query whereKey:@"rentalYield" equalTo:propertyIn.rentalYeild];
        }
        
        
        //query for room
        if ([propertyIn.bedroom length] != 0) {
            NSLog(@"bedroom: %@", propertyIn.bedroom);
            [query whereKey:@"bedroom" lessThan:[NSNumber numberWithInt:[propertyIn.bedroom intValue]]];
        }
        
        //query for condition
        if ([propertyIn.condition length] != 0) {
            NSLog(@"condition: %@", propertyIn.condition);
            [query whereKey:@"condition" equalTo:propertyIn.condition];
        }
        
        //query for land Size
        if ([propertyIn.landSize length] != 0) {
            NSLog(@"land Size: %@", propertyIn.landSize);
            [query whereKey:@"landSize" lessThan:[NSNumber numberWithInt:[propertyIn.landSize intValue]]];
        }
        
        
        //query for main gate
        if ([propertyIn.mainGateFacing length] != 0) {
            NSLog(@"main gate facing: %@", propertyIn.mainGateFacing);
            [query whereKey:@"mainGateFacing" equalTo:propertyIn.mainGateFacing];
        }
        
        //query for car Park
        if ([propertyIn.carPark length] != 0) {
            NSLog(@"main door facing: %@", propertyIn.carPark);
            [query whereKey:@"carPark" equalTo:propertyIn.carPark];
        }
        
        //query for garden
        if ([propertyIn.garden length] != 0) {
            NSLog(@"garden: %@", propertyIn.garden);
            [query whereKey:@"carPark" equalTo:propertyIn.garden];
        }
        
        //query for swimming pool
        if ([propertyIn.swimmingPool length] != 0) {
            NSLog(@"swimming pool %@", propertyIn.swimmingPool);
            [query whereKey:@"swimmingPool" equalTo:propertyIn.swimmingPool];
        }
        
        
        //query for basement
        if ([propertyIn.basement length] != 0) {
            NSLog(@"basement: %@", propertyIn.basement);
            [query whereKey:@"basement" equalTo:propertyIn.basement];
        }


        //query for roof terrace
        if ([propertyIn.roofTerrance length] != 0) {
            NSLog(@"Roof terrace: %@", propertyIn.roofTerrance);
            [query whereKey:@"roofTerrace" equalTo:propertyIn.roofTerrance];
        }

    }
    
	
	
	//[query whereKey:@"propertyType" nearGeoPoint:@"Some geo point"];
	
    //Query for submit
    [query whereKey:@"submit" equalTo:@"YES"];
    
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
			
            NSMutableArray * pArray = [[NSMutableArray alloc] init];
            for (PFObject *object in objects) {
                Property * newProperty = [[Property alloc]initWithProperty:object];
                [pArray addObject:newProperty];
            }
            view.data = pArray;
            [view actionSuceed:@""];
		} else {
			[view actionFail:@"fail"];
		}
	}];
}

- (void)SearchPropertyHDB:(Property *)propertyIn :(SearchHDBVC *)salesRowView
{
    propertyAray = [[NSMutableArray alloc] init];
	PFQuery *query = [PFQuery queryWithClassName:@"Property"];
	
	//Search criteria. Can use equalTo, lessThan, nearGeopoint....
	
	if ([propertyIn.listingType length] != 0) {
		[query whereKey:@"listingType" equalTo:propertyIn.listingType];
	}
	
	//query for type
	if ([propertyIn.propertyType length] != 0) {
		[query whereKey:@"propertyType" equalTo:propertyIn.propertyType];
	}
    
    
    if ([propertyIn.propertyType isEqualToString:@"HDB"])
    {
        NSLog(@"search HDB");
        //query for district
        if ([propertyIn.district length] != 0) {
            NSLog(@"district: %@", propertyIn.district);
            [query whereKey:@"district" equalTo:propertyIn.district];
        }
        
        //query for area
        if ([propertyIn.area length] != 0) {
            NSLog(@"area: %@", propertyIn.area);
            [query whereKey:@"area" lessThan:[NSNumber numberWithInt:[propertyIn.area intValue]]];
        }
        
        //query for area
        if ([propertyIn.price length] != 0) {
            NSLog(@"price: %@", propertyIn.price);
            [query whereKey:@"price" lessThan:[NSNumber numberWithInt:[propertyIn.price intValue]]];
        }
        
        //query for rented price
        if ([propertyIn.rentedPrice length] != 0) {
            NSLog(@"rented price: %@", propertyIn.rentedPrice);
            [query whereKey:@"rentedPrice" lessThan:[NSNumber numberWithInt:[propertyIn.rentedPrice intValue]]];
        }
        
        //query for level
        if ([propertyIn.groundLevel length] != 0) {
            NSLog(@"level: %@", propertyIn.district);
            [query whereKey:@"groundLevel" equalTo:propertyIn.groundLevel];
        }
        
        //query for hdb type
        if ([propertyIn.hdbType length] != 0) {
            NSLog(@"hdb type: %@", propertyIn.hdbType);
            [query whereKey:@"hdbType" equalTo:propertyIn.hdbType];
        }
        
        //query for lift level
        if ([propertyIn.hdbType length] != 0) {
            NSLog(@"hdb type: %@", propertyIn.liftLevel);
            [query whereKey:@"liftLevel" equalTo:propertyIn.liftLevel];
        }
        
        //query for main door direction
        if ([propertyIn.mainDoorDirection length] != 0) {
            NSLog(@"main door direction: %@", propertyIn.mainDoorDirection);
            [query whereKey:@"mainDoorDirection" equalTo:propertyIn.mainDoorDirection];
        }
        
        //query for room position
        if ([propertyIn.roomPosition length] != 0) {
            NSLog(@"room position: %@", propertyIn.roomPosition);
            [query whereKey:@"roomPosition" equalTo:propertyIn.roomPosition];
        }
        
        
        //query for town
        if ([propertyIn.town length] != 0) {
            NSLog(@"town: %@", propertyIn.town);
            [query whereKey:@"town" equalTo:propertyIn.town];
        }
        
        //query for valuation
        if ([propertyIn.valuation length] != 0) {
            NSLog(@"valuation: %@", propertyIn.valuation);
            [query whereKey:@"valuation" equalTo:propertyIn.valuation];
        }
        
        //query for condition
        if ([propertyIn.condition length] != 0) {
            NSLog(@"condition: %@", propertyIn.condition);
            [query whereKey:@"condition" equalTo:propertyIn.condition];
        }
        
        
    }
    
	
	
	//[query whereKey:@"propertyType" nearGeoPoint:@"Some geo point"];
	
    //Query for submit
    [query whereKey:@"submit" equalTo:@"YES"];
    
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
			// The find succeeded.
			NSLog(@"Successfully retrieved %d scores.", objects.count);
			// Do something with the found objects
            [salesRowView SearchSuccessfully:objects];
		} else {
			// Log details of the failure
			NSString *errorString = [[error userInfo] objectForKey:@"error"];
			[salesRowView SearchFailed:errorString];
		}
	}];
}


- (void) GetMyListing : (MyUIViewController*) portView
{
	PFQuery *query = [PFQuery queryWithClassName:@"Property"];
    [query whereKey:@"userAdded" equalTo:[PFUser currentUser]];
    [query includeKey:@"agent"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSMutableArray * properties = [[NSMutableArray alloc] init];
            NSMutableArray * titles = [[NSMutableArray alloc] init];
            NSLog(@"GetMyListSuccessful %i", [objects count]);
            for (PFObject *object in objects) {
                [properties addObject:object];
                [titles addObject:[object objectForKey:@"propertyName"]];
                //NSLog(@"properties = %@",properties);
            }
            
            [[DataEngine sharedInstance] SetPropertyDataArray:properties];
            [[DataEngine sharedInstance] SetPropertyNameList:titles];
            [portView actionSuceed:@""];
            
        } else {
            // Log details of the failure
            NSString *errorString = [[error userInfo] objectForKey:@"error"];
			[portView actionFail:errorString];
        }
    }];
}

- (void) GetUserPropertyList : (User*) user
{
	PFQuery *query = [PFQuery queryWithClassName:@"Property"];
    [query whereKey:@"userAdded" equalTo:[user getUserObject]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
			NSLog(@"found %i properties", objects.count);
			NSMutableArray* propertyList = [[NSMutableArray alloc] init];
			for (PFObject* property in objects) {
				Property* propInstance = [[Property alloc] initWithProperty:property];
				[propertyList addObject:propInstance];
			}
			[user GetPropertySuccessful:propertyList];
            
            
        } else {
            // Log details of the failure
            NSString *errorString = [[error userInfo] objectForKey:@"error"];
			[user GetPropertyFailed:errorString];
        }
    }];
}

- (void) GetAgentClientPropertyList : (Agent*) agent : (Client*) client
{
	PFQuery *query = [PFQuery queryWithClassName:@"Property"];
    [query whereKey:@"agent" equalTo:[agent getUserObject]];
	[query whereKey:@"userAdded" equalTo:[client getUserObject]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
			//NSLog(@"find %i common property", objects.count);
			NSMutableArray* propertyList = [[NSMutableArray alloc] init];
			for (PFObject* property in objects) {
				Property* propInstance = [[Property alloc] initWithProperty:property];
				[propertyList addObject:propInstance];
			}
			[agent GetAgentClientPropertySuccessful:propertyList];
            
            
        } else {
            // Log details of the failure
            NSString *errorString = [[error userInfo] objectForKey:@"error"];
			[agent GetAgentClientPropertyFailed:errorString];
        }
    }];
}

/**
 * Record when client view a property
 * @param property: parse object representation for property
 */
- (void) SetPropertyViewed: (PFObject*) property
{
	PFObject *propertyView = [PFObject objectWithClassName:@"PropertyViewed"];
	
	//NSLog(@"%@",curUser);
	
	[propertyView setObject:curUser forKey:@"user"];
	[propertyView setObject:property forKey:@"property"];
	[propertyView saveInBackground];
}

/**
 * Set a property as favorite
 * @param property: parse object representation for property
 */
- (void) SetFavoriteProperty: (PFObject*) property
{
	PFObject *favorite = [PFObject objectWithClassName:@"UserFavorite"];
	
	[favorite setObject:curUser forKey:@"user"];
	[favorite setObject:property forKey:@"property"];
	[favorite saveInBackground];
}

/**
 * Delete a property from the favorite list
 * @param property: parse object representation for property
 */
- (void) DeleteFavoriteProperty: (PFObject*) property
{
	PFQuery *query = [PFQuery queryWithClassName:@"UserFavorite"];
	[query whereKey:@"user" equalTo:curUser];
	[query whereKey:@"property" equalTo:property];
	
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
			// The find succeeded.
			NSLog(@"Successfully retrieved %d scores.", objects.count);
			// Do something with the found objects
			for (PFObject *object in objects) {
				[object deleteInBackground];
			}
		} else {
			// Log details of the failure
			//NSString *errorString = [[error userInfo] objectForKey:@"error"];
			
		}
	}];
}

/**
 * Get the favorite list
 *
 */
- (void) GetFavoriteProperty
{
	PFQuery *query = [PFQuery queryWithClassName:@"UserFavorite"];
	[query whereKey:@"user" equalTo:curUser];
	
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
			// The find succeeded.
			NSLog(@"Successfully retrieved %d scores.", objects.count);
			// Do something with the found objects
			for (PFObject *object in objects) {
				NSLog(@"%@", object.objectId);
			}
		} else {
			// Log details of the failure
			//NSString *errorString = [[error userInfo] objectForKey:@"error"];
		}
	}];
}

/**
 * Creating group
 * @param groupName: name of group
 */
- (void) CreateGroup: (NSString*) groupName
{
	PFObject *group = [PFObject objectWithClassName:@"Group"];
	
	[group setObject:curUser forKey:@"GroupCreator"];
	[group setObject:groupName forKey:@"GroupName"];
	[group saveInBackground];
	
	[self AddGroupMember:curUser :group];
	[self GroupMemberStatus:@"accept" :curUser :group];
	
}

/**
 * Add property to group sharing
 * @param property: parse object representation for property
 * @param group: parse object representation for group
 */
- (void) GroupShareProperty: (PFObject*) property : (PFObject*) group
{
	PFObject *groupSharing = [PFObject objectWithClassName:@"GroupSharingInfo"];
	
	[groupSharing setObject:group forKey:@"group"];
	[groupSharing setObject:property forKey:@"property"];
	[groupSharing saveInBackground];
}

/**
 * Get list of property shared by group
 * @param group: parse object representation for group
 */
- (void) GetGroupSharingList: (PFObject*) group
{
	PFQuery *query = [PFQuery queryWithClassName:@"GroupSharingInfo"];
	[query whereKey:@"group" equalTo:group];
	
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
			// The find succeeded.
			NSLog(@"Successfully retrieved %d scores.", objects.count);
			// Do something with the found objects
			for (PFObject *object in objects) {
				NSLog(@"%@", object.objectId);
			}
		} else {
			// Log details of the failure
			//NSString *errorString = [[error userInfo] objectForKey:@"error"];
		}
	}];
}

/**
 * Add member to the group
 * @param user: parse object representation for user
 * @param group: parse object representation for group
 */
- (void) AddGroupMember: (PFObject*) user : (PFObject*) group
{
	PFObject *groupMember = [PFObject objectWithClassName:@"GroupMemberInfo"];
	
	[groupMember setObject:group forKey:@"group"];
	[groupMember setObject:user forKey:@"user"];
	[groupMember setObject:@"pending" forKey:@"status"];
	[groupMember saveInBackground];
	
}

/**
 * Set member status permission
 * @param status: accept/reject
 * @param user: parse object representation for user
 * @param group: parse object representation for group
 */
- (void) GroupMemberStatus: (NSString*) status : (PFObject*) user : (PFObject*) group
{
	PFQuery* query = [PFQuery queryWithClassName:@"GroupMemberInfo"];
	[query whereKey:@"user" equalTo:user];
	[query whereKey:@"group" equalTo:group];
	
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
			// The find succeeded.
			NSLog(@"Successfully retrieved %d scores.", objects.count);
			// Do something with the found objects
			for (PFObject *object in objects) {
				[object setObject:status forKey:@"status"];
			}
			
		} else {
			// Log details of the failure
			//NSString *errorString = [[error userInfo] objectForKey:@"error"];
		}
	}];
	
}

/**
 * Add watchlist base on certain criteria
 * @param user: parse object represent for the user
 * @param property: parse object representation for property
 * ...
 */
- (void) AddWatchlist: (PFObject*) user : (NSString*) subject : (NSString*) transaction :
(NSString*) type : (NSString*) project :(NSString*) district :
(NSString*) blkUnitNo : (NSString*) location : (NSString*) tenure : (NSString*) price :
(NSString*) area : (NSString*) level : (NSString*) bedroom : (NSString*) remarks
{
	PFObject *watchlist = [PFObject objectWithClassName:@"Property"];
	[watchlist setObject:user forKey:@"user"];
	[watchlist setObject:subject forKey:@"Subject"];
	[watchlist setObject:transaction forKey:@"Transaction"];
	[watchlist setObject:type forKey:@"Type"];
	[watchlist setObject:project forKey:@"Project"];
	[watchlist setObject:district forKey:@"District"];
	[watchlist setObject:blkUnitNo forKey:@"BlkUnitNo"];
	[watchlist setObject:location forKey:@"location"];
	[watchlist setObject:tenure forKey:@"enure"];
	[watchlist setObject:price forKey:@"price"];
	[watchlist setObject:area forKey:@"area"];
	[watchlist setObject:level forKey:@"level"];
	[watchlist setObject:bedroom forKey:@"bedroom"];
	[watchlist setObject:remarks forKey:@"remarks"];
	[watchlist saveInBackground];
}

- (void) DeteleWatchlist: (PFObject*) watchlist
{
	[watchlist deleteInBackground];
}

/**
 * Insert data that associate with a property
 * @param imageData: NSData representation type of the image
 * @param property: parse object representation for property
 */
- (void) uploadImage: (UIImage*) image : (Property*) propertyInstance
{
	NSData* imageData = UIImageJPEGRepresentation(image, 0.001f);
	PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:imageData];
	
	//HUD creation here (see example for code)
	
	// Save PFFile
	[imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		if (!error) {
			// Hide old HUD, show completed HUD (see example for code)
			
			// Create a PFObject around a PFFile and associate it with the current user
			PFObject *userPhoto = [PFObject objectWithClassName:@"Photo"];
			[userPhoto setObject:imageFile forKey:@"photo"];
			[userPhoto setObject:[[DataEngine sharedInstance] GetCurPropertyInstace].propertyObject forKey:@"property"];
			[userPhoto setObject:curUser forKey:@"user"];
			
			// Set the access control list to current user for security purposes
			//userPhoto.ACL = [PFACL ACLWithUser:[PFUser currentUser]];
			
			[userPhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
				if (!error) {
					//[self refresh:nil];
					[propertyInstance ImageUploadSuccessfully];
				}
				else{
					// Log details of the failure
					NSString *errorString = [[error userInfo] objectForKey:@"error"];
					[propertyInstance ImageUploadFailed:errorString];
				}
			}];
		}
		else{
			//[HUD hide:YES];
			// Log details of the failure
            //NSString *errorString = [[error userInfo] objectForKey:@"error"];
        }
	} progressBlock:^(int percentDone) {
		// Update your progress spinner here. percentDone will be between 0 and 100.
		//HUD.progress = (float)percentDone/100;
        if (percentDone == 100)
        {
            NSLog(@"Done up load 1 picture");
            //addPropertyImageView.temp ++;
            /*if ([addPropertyImageView ImageValidation]) {
			 NSLog(@"Done Upload and Download Image");
			 [addPropertyImageView AddDone];
			 }*/
        }
	}];
}
/*
 - (void)downloadImages : (PFObject*) property : (AddPropertyImageViewController*) addPropertyImageView
 {
 NSLog(@"Start getting Image");
 PFQuery *query = [PFQuery queryWithClassName:@"Photo"];
 [query whereKey:@"property" equalTo:property];
 
 [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
 // If there are photos, we start extracting the data
 // Save a list of object IDs while extracting this data
 if (!error) {
 // The find succeeded.
 NSMutableArray *imageDataArray = [NSMutableArray array];
 NSLog(@"Successfully retrieved %d images.", objects.count);
 
 for (PFObject *eachObject in objects) {
 PFFile *theImage = [eachObject objectForKey:@"photo"];
 NSData *imageData = [theImage getData];
 UIImage *image = [UIImage imageWithData:imageData];
 [imageDataArray addObject:image];
 }
 
 [addPropertyImageView GetImageSuccessfully:imageDataArray];
 NSLog(@"%@", imageDataArray);
 } else {
 // Log details of the failure
 NSString *errorString = [[error userInfo] objectForKey:@"error"];
 NSLog(@"%@", errorString);
 [addPropertyImageView GetImageFailed:errorString];
 }
 }];
 }*/

- (void)downloadImages: (ListingGalleryViewController*) galleryView :(Property *)propertyInstance
{
	NSLog(@"Start getting Image 1");
    PFQuery *query = [PFQuery queryWithClassName:@"Photo"];
    [query whereKey:@"property" equalTo:propertyInstance.propertyObject];
	NSLog(@"Start getting Image 2");
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		// If there are photos, we start extracting the data
		// Save a list of object IDs while extracting this data
		if (!error) {
            NSLog(@"Start getting Image 3");
			// The find succeeded.
			NSMutableArray *imageDataArray = [NSMutableArray array];
			NSLog(@"Successfully retrieved %d images.", objects.count);
			
			for (PFObject *eachObject in objects) {
				PFFile *theImage = [eachObject objectForKey:@"photo"];
				NSData *imageData = [theImage getData];
				UIImage *image = [UIImage imageWithData:imageData];
				[imageDataArray addObject:image];
			}
			[galleryView DownloadImageSuccessfully:imageDataArray];
			NSLog(@"%@", imageDataArray);
		} else {
			// Log details of the failure
			NSString *errorString = [[error userInfo] objectForKey:@"error"];
			NSLog(@"%@", errorString);
			[galleryView DownloadImageFail:errorString];
		}
	}];
}

/**
 * Add agent/client record. One param is curUser
 * @param agent: parse object representation for agent
 * @param client: parse object representation for client
 */
- (void) AddAgentClientLink: (PFObject*) agent : (PFObject*) client : (MyUIViewController *) view
{
	PFObject *link = [PFObject objectWithClassName:@"AgentClientRecord"];
	
	[link setObject:agent forKey:@"agent"];
	[link setObject:client forKey:@"client"];
	[link saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(!error)
        {
            [(Agent*) [[DataEngine sharedInstance] GetCurUser] RefreshData];
            //[view actionSuceed:@""];
        }
        else
        {
            [view actionFail:[error debugDescription]];
        }
    }];
}

/**
 * get the list of user's client (if curuser is agent) or vice versa
 * @param agentClient: String "agent" or "client"
 *
 */
int agentNumber;
- (void) GetMyAgentList : (Client*) clientUser {
	PFQuery *query = [PFQuery queryWithClassName:@"AgentClientRecord"];
	
	//Search criteria. Can use equalTo, lessThan, nearGeopoint....
	[query whereKey:@"client" equalTo:[clientUser getUserObject]];
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
			agentNumber = objects.count;
			// The find succeeded.
			NSLog(@"Successfully retrieved %d agents.", objects.count);
			NSMutableArray* agentList = [[NSMutableArray alloc] init];
			// Do something with the found objects
			for (PFObject *object in objects) {
				PFUser* ag = [object objectForKey:@"agent"];
				[agentList addObject:ag];
				//NSLog(@"%@", cli.username);
			}
			for (PFUser* ag in agentList) {
				[ag fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
					if (!error) {
						clientNumber--;
						if (clientNumber == 0) {
							NSLog(@"%@", agentList);
							[clientUser GetAgentSuccessful:agentList];
						}
					} else {
						
					}
				}];
			}
		} else {
			// Log details of the failure
			NSString *errorString = [[error userInfo] objectForKey:@"error"];
			[clientUser GetAgentFailed:errorString];
		}
	}];
}

int clientNumber;
- (void) GetMyClientList : (Agent*) agentUser
{
	
	
	PFQuery *query = [PFQuery queryWithClassName:@"AgentClientRecord"];
	
	//Search criteria. Can use equalTo, lessThan, nearGeopoint....
	[query whereKey:@"agent" equalTo:[agentUser getUserObject]];
    [query includeKey:@"pfUser.username"];
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
			clientNumber = objects.count;
			// The find succeeded.
			NSLog(@"Successfully retrieved %d clients.", objects.count);
			NSMutableArray* clientList = [[NSMutableArray alloc] init];
			// Do something with the found objects
			for (PFObject *object in objects) {
				PFUser* cli = [object objectForKey:@"client"];
				[clientList addObject:cli];
				//NSLog(@"%@", cli.username);
			}
            if (clientList.count == 0) {
                [[DataEngine sharedInstance].loadingView loadMainMenu];
                return;
            }
            
			for (PFUser* cli in clientList) {
				[cli fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
					if (!error) {
						clientNumber--;
						if (clientNumber == 0) {
							NSLog(@"%@", clientList);
							[agentUser GetClientSuccessful:clientList];
						}
					} else {
						
					}
				}];
			}
		} else {
			// Log details of the failure
			NSString *errorString = [[error userInfo] objectForKey:@"error"];
			[agentUser GetClientFailed:errorString];
		}
	}];
    
}
//---------------------My Group---------------------

- (void)LoadGroupInfo:(PFObject *)user :(MyGroupVC *)myGroupView
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupParticipants"];
	
	//Search criteria. Can use equalTo, lessThan, nearGeopoint....
    
	[query whereKey:@"userID" equalTo:user];
    [query orderByAscending:@"createAt"];
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            if (objects.count == 0) {
                [myGroupView actionSuceed:@""];
                return ;
            }
            NSMutableArray * chatList = [[NSMutableArray alloc] init];
            for(int i = 0; i < objects.count; i++) {
                [chatList addObject:[NSNull null]];
            }
            NSLog(@"retrieved chatList = %@",chatList);
            for (int i = 0; i<objects.count; i++) {
                PFObject* object = [objects objectAtIndex:i];
                PFObject * groupInfo = [object objectForKey:@"groupID"];
                
                
                
                PFQuery *query = [PFQuery queryWithClassName:@"GroupInfo"];
                [query getObjectInBackgroundWithId:groupInfo.objectId block:^(PFObject *obj, NSError *error) {
                    if (!error) {

                        [chatList replaceObjectAtIndex:i withObject:obj];
                        NSLog(@"[%i]chatList = %@",i,chatList);
                        
                        int nullTotal = 0;
                        for (NSObject *component in chatList) {
                            if (component == [NSNull null]) {
                                nullTotal++;
                            }
                        }
                        if (nullTotal == 0) {
                            myGroupView.ChatList = chatList;
                            [myGroupView actionSuceed:@"Hell Yeah"];
                        }
                    }
                    else
                    {
                        NSLog(@"%@",error.localizedDescription);
                    }
                    
                }];
            }
		} else {
			[myGroupView actionFail:@"Failed"];
		}
	}];
}

- (void)LoadPhoneBookList:(PhoneBookVC *)phoneBookView
{
    PFQuery *query = [PFUser query];
    [query whereKey:@"userType" equalTo:@"agent"];
    
    NSArray * data = [query findObjects];
    NSMutableArray * list = [[NSMutableArray alloc] init];
    
    for (PFUser * user in data) {
        [list addObject:user];
    }
    
    NSLog(@"new number = %i", data.count);
    
    phoneBookView.agentList = list;
    [phoneBookView actionSuceed:@""];
    
}

- (void)loadAllAgent:(PhoneBookVC *)phoneBookView :(NSMutableArray *)unneededArray
{
    PFQuery *query = [PFUser query];
    [query whereKey:@"userType" equalTo:@"agent"];
    [query whereKey:@"objectId" notContainedIn:unneededArray];
    
    NSArray * data = [query findObjects];
    NSMutableArray * list = [[NSMutableArray alloc] init];
    
    for (PFUser * user in data) {
        NSLog(@"username = %@", user.username);
        [list addObject:user];
    }
    
    NSLog(@"new number = %i", data.count);
    
    phoneBookView.agentList = list;
    [phoneBookView actionSuceed:@""];
}


- (void)loadPhoneBookList2:(PhoneBookVC *)phoneBookView :(NSArray *)curList
{
    PFQuery *query = [PFUser query];
    [query whereKey:@"userType" equalTo:@"agent"];
    [query whereKey:@"objectId" notContainedIn:curList];
	
	//Search criteria. Can use equalTo, lessThan, nearGeopoint....
    
    NSArray * data = [query findObjects];
    NSMutableArray * list = [[NSMutableArray alloc] init];
    
    for (PFUser * user in data) {
        [list addObject:user];
    }
    
    
    phoneBookView.agentList = list;
    [phoneBookView actionSuceed:@""];
}

- (void)AddGroupInfo:(PFObject *)user :(NSString *)Title :(NSArray *)phoneList :(AddGroupVC *)addgroupView
{
    
    PFQuery *query = [PFQuery queryWithClassName:@"GroupInfo"];
	
	
	[query whereKey:@"groupTitle" equalTo:Title];
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            if ([objects count] == 0) {
                PFObject *groupChat = [PFObject objectWithClassName:@"GroupInfo"];
                
                [groupChat setObject:user forKey:@"creator"];
                [groupChat setObject:Title forKey:@"groupTitle"];
                [groupChat saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if(!error)
                    {
                        NSMutableArray * loader = [[NSMutableArray alloc]init];
                        
                        for (PFObject * member in phoneList) {
                            PFObject *phone = [PFObject objectWithClassName:@"GroupParticipants"];
                            
                            [phone setObject:groupChat forKey:@"groupID"];
                            [phone setObject:member forKey:@"userID"];
                            [phone saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                if(!error)
                                {
                                    [loader addObject:phone];
                                    if ([loader count] == [phoneList count]) {
                                        [addgroupView actionSuceed:@"success"];
                                    }
                                    
                                }
                            }];
                        }
                    }
                }];
            }
            else
            {
                //Error
                NSLog(@"%@",error.debugDescription);
                [addgroupView actionFail:@"Group title is not available"];
            }
            
		} else {
			
		}
	}];
    
    
    
    
}

- (void)sendMessage:(PFObject *)user :(NSString *)content :(PFObject *)chatRoomInfo :(ChatRoomVC *)chatRoomView
{
    NSLog(@"content = %@",content);
    PFObject *groupChat = [PFObject objectWithClassName:@"GroupChat"];
	
	[groupChat setObject:user forKey:@"userID"];
    [groupChat setObject:chatRoomInfo forKey:@"groupID"];
	[groupChat setObject:content forKey:@"content"];
	[groupChat saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(!error)
        {
            [chatRoomView sendDone:groupChat];
            [chatRoomView timerStart];
        }
        else
        {
            
        }
    }];
}



- (void)LoadMessage :(PFObject *)chatRoomInfo :(ChatRoomVC *)chatRoomView
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupChat"];
	
	//Search criteria. Can use equalTo, lessThan, nearGeopoint....
    query.limit = 20;
	[query whereKey:@"groupID" equalTo:chatRoomInfo];
    [query orderByDescending:@"createdAt"];
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            if (objects.count == 0) {
                [chatRoomView actionSuceed:@""];
                return ;
            }
            NSMutableArray * chatList = [[NSMutableArray alloc]init];
            for(int i = 0; i < objects.count; i++) {
                [chatList addObject:[NSNull null]];
            }
            NSMutableArray * userList = [[NSMutableArray alloc]init];
            for(int i = 0; i < objects.count; i++) {
                [userList addObject:[NSNull null]];
            }
            
            for (int i = 0; i<objects.count; i++) {
                PFObject * chatRow = [objects objectAtIndex:i];
                [chatList replaceObjectAtIndex:objects.count - i -1 withObject:chatRow];
                
                PFUser * user = [chatRow objectForKey:@"userID"];
                PFQuery * query = [PFUser query];
                [query whereKey:@"objectId" equalTo:user.objectId];
                [query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error) {
                    if (!error) {
                        PFUser * curUser = [users objectAtIndex:0];
                        [userList replaceObjectAtIndex:objects.count - i -1 withObject:curUser];

                        int nullTotal = 0;
                        for (NSObject *component in userList) {
                            if (component == [NSNull null]) {
                                nullTotal++;
                            }
                        }
                        if (nullTotal == 0) {
                            chatRoomView.chatList = chatList;
                            chatRoomView.userInfoList = userList;
                            NSLog(@"chatRoomView.userInfoList = %@",chatRoomView.userInfoList);
                            [chatRoomView actionSuceed:@""];
                        }
                        
                    }
                    else
                    {
                        [chatRoomView actionFail:@"LoadMessage Fail"];
                    }
                
                }];
            NSLog(@"%@ chatID %i", chatRow.objectId, [chatList count]);
                
            }
		} else {
            [chatRoomView actionFail:@"LoadMessage Fail"];
			
		}
	}];
    
}


- (void)RefreshMessage:(PFObject *)chatRoomInfo :(NSMutableArray *)curChatList :(NSMutableArray *)curUserList :(ChatRoomVC *)chatRoomView
{

    PFQuery *query = [PFQuery queryWithClassName:@"GroupChat"];
    
    if (curChatList.count > 0) {
        PFObject * newestChat = [curChatList lastObject];
        [query whereKey:@"createdAt" greaterThan: newestChat.createdAt];
    }
        //Search criteria. Can use equalTo, lessThan, nearGeopoint....
        [query whereKey:@"groupID" equalTo:chatRoomInfo];
    
        [query orderByAscending:@"createdAt"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                if (objects.count == 0) {
                    NSLog(@"Refresh Done1");
                }
                else
                {
                    NSMutableArray * chatList = [[NSMutableArray alloc]init];
                    for(int i = 0; i < objects.count; i++) {
                        [chatList addObject:[NSNull null]];
                    }
                    NSMutableArray * userList = [[NSMutableArray alloc]init];
                    for(int i = 0; i < objects.count; i++) {
                        [userList addObject:[NSNull null]];
                    }
                    
                    for (int i = 0; i<objects.count; i++) {
                        PFObject * chatRow = [objects objectAtIndex:i];
                        [chatList replaceObjectAtIndex:i withObject:chatRow];
                        
                        PFUser * user = [chatRow objectForKey:@"userID"];
                        PFQuery * query = [PFUser query];
                        [query whereKey:@"objectId" equalTo:user.objectId];
                        [query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error) {
                            if (!error) {
                                PFUser * curUser = [users objectAtIndex:0];
                                [userList replaceObjectAtIndex:i withObject:curUser];
                                
                                int nullTotal = 0;
                                for (NSObject *component in userList) {
                                    if (component == [NSNull null]) {
                                        nullTotal++;
                                    }
                                }
                                if (nullTotal == 0) {
                                    [curChatList addObjectsFromArray:chatList];
                                    [curUserList addObjectsFromArray:userList];
                                    
                                    
                                    chatRoomView.chatList = curChatList;
                                    chatRoomView.userInfoList = curUserList;
                        
                                    [chatRoomView refreshingDone];
                                    
                                    
                                    NSLog(@"%i number of external chat list", [curChatList count]);
                                    
                                }
                                
                            }
                            else
                            {
                                //err
                            }
                            
                        }];
                        
                        
                    }
                }
            } else {
                //err
                
            }
        }];
  //  }
}



- (void) LoadParticipant : (PFObject *) groupInfo : (GroupInfoVC*) groupInfoView
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupParticipants"];
    
    [query whereKey:@"groupID" equalTo:groupInfo];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            NSLog(@"%i number of participant", [objects count]);
            NSMutableArray * userList = [[NSMutableArray alloc]init];
            for(int i = 0; i < objects.count; i++) {
                [userList addObject:[NSNull null]];
            }
            
            for (int i = 0; i<objects.count; i++) {
                
                PFObject * curParticipant = [objects objectAtIndex:i];
                PFUser * curUser = [curParticipant objectForKey:@"userID"];
                
                
                PFQuery * query = [PFUser query];
                [query whereKey:@"objectId" equalTo:curUser.objectId];
                [query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error) {
                    if (!error) {
                        PFUser * curUser = [users objectAtIndex:0];
                        [userList replaceObjectAtIndex:i withObject:curUser];
                        
                        int nullTotal = 0;
                        for (NSObject *component in userList) {
                            if (component == [NSNull null]) {
                                nullTotal++;
                            }
                        }
                        if (nullTotal == 0) {
                            groupInfoView.userList = userList;
                            [groupInfoView actionSuceed:@""];
                        }
                    }
                    else
                    {
                        
                    }
                            
                }];
                
            }
            
        }
        else
        {
            [groupInfoView actionFail:@"fail"];
        }
    }];
}


- (void)AddParticipants:(NSMutableArray *)participants :(PFObject *)chatRoomInfo :(PhoneBookVC *)phoneBookView
{
    
    NSMutableArray * temp  = [[NSMutableArray alloc]init];
    for (int i = 0; i < [participants count]; i++) {
        PFUser *  curUser = [participants objectAtIndex:i];
        
        PFObject *phone = [PFObject objectWithClassName:@"GroupParticipants"];
        
        [phone setObject:chatRoomInfo forKey:@"groupID"];
        [phone setObject:curUser forKey:@"userID"];
        [phone saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if(!error)
            {
                [temp addObject:phone];
                if ([temp count] == [participants count]) {
                    [phoneBookView addFinished];
                }
            }
        }];
    }
}

- (void)DeleteParticipants:(PFObject *)groupInfo :(PFUser *)curUser :(ParticipantCell *)cell
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupParticipants"];
	
	//Search criteria. Can use equalTo, lessThan, nearGeopoint....
    
	[query whereKey:@"userID" equalTo:curUser];
    [query whereKey:@"groupID" equalTo:groupInfo];
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            PFObject * cur = [objects objectAtIndex:0];
            [cur deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                [cell deleteDone];
            }];
		} else {
			
		}
	}];
}

-(void) getPropertyWithChatCell: (ChatCell *) cCell
{
    PFQuery *query = [PFQuery queryWithClassName:@"Property"];
    [query whereKey:@"objectId" equalTo:cCell.propId];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            if (objects.count > 0) {
                Property *prop = [[Property alloc] initWithProperty:[objects firstObject]];
                cCell.prop = prop;
                [cCell refreshCell];
            }
            else
            {
                [cCell.contentTF setText:@"cannot find property"];
            }
        }
        else
        {
            [cCell.contentTF setText:@"cannot find property"];
        }
    }];
}

-(void) sendNotificationToGroup:(PFObject *) groupInfo : (PFObject *) contentObject
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupParticipants"];
    
    [query whereKey:@"groupID" equalTo:groupInfo];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSLog(@"%i number of participant", [objects count]);
            for (int i = 0; i<objects.count; i++) {
                
                PFObject * curParticipant = [objects objectAtIndex:i];
                PFUser * curUser = [curParticipant objectForKey:@"userID"];
                
                
                PFQuery * query = [PFUser query];
                [query whereKey:@"objectId" equalTo:curUser.objectId];
                [query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error) {
                    if (!error) {
                        PFUser * curUser = [users objectAtIndex:0];
                        if (curUser.objectId != [DataEngine sharedInstance].curUser.userObject.objectId) {
                            
                            NSString *chatContent = [NSString stringWithFormat:@"%@: %@",[DataEngine sharedInstance].curUser.fullName, [contentObject objectForKey:@"content"]];
                            NSMutableDictionary *pushDict = [[NSMutableDictionary alloc] init];
                            [pushDict setObject:chatContent forKey:@"alert"];
                            [pushDict setObject:groupInfo forKey:@"pushPFObject"];
                            [pushDict setObject:@"group" forKey:@"pushType"];
                            [[NotificationEngine sharedInstance] pushToUserObj:curUser :pushDict];
                        }
                        ;
                        
                    }
                }];
            }
        }
    }];
}


//---------------------Message---------------------

- (void)getMessageListByUser:(PFUser *)curUser :(MessageListVC *)messageListView
{
    PFQuery *query = [PFQuery queryWithClassName:@"MessageInfo"];
    [query whereKey:@"sender" equalTo:curUser];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            
            NSLog(@"start check");
            
            if (objects.count == 0) {
                NSMutableArray * list = [[NSMutableArray alloc]init];
                [self getAdditionalMessageListByUser:curUser :messageListView :list];
                NSLog(@"there are no object in sender");
                
            }
            else
            {
                NSMutableArray * list = [[NSMutableArray alloc]init];
                for (int i = 0; i < [objects count]; i++) {
                    [list addObject:[NSNull null]];
                }
                
                for (int i = 0; i<objects.count; i++) {
                    
                    PFObject * link = [objects objectAtIndex:i];
                    PFUser * receiver = [link objectForKey:@"receiver"];
                    
                    
                    PFQuery * query = [PFUser query];
                    [query whereKey:@"objectId" equalTo:receiver.objectId];
                    [query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error) {
                        if (!error) {
                            if ([users count] == 0) {
                                //err
                            }
                            else
                            {
                                NSLog(@"check connector");
                                PFUser * curReceiver = [users objectAtIndex:0];
                                User * userReceiver = [[User alloc]initWithUser:curReceiver];
                                
                                MessageInfo * message = [[MessageInfo alloc]init];
                                message.messageObject = link;
                                message.connector = userReceiver;
                                
                                NSLog(@"userphone = %@", message.connector.phone);
                                
                                [list replaceObjectAtIndex:i withObject:message];
                            }
                            
                            
                            int nullTotal = 0;
                            for (NSObject *component in list) {
                                if (component == [NSNull null]) {
                                    nullTotal++;
                                }
                            }
                            if (nullTotal == 0) {
                                [self getAdditionalMessageListByUser:curUser :messageListView :list];
                                NSLog(@"check done");
                            }
                        }
                        else
                        {
                            
                        }
                        
                    }];
                    
                }
            }
            
            
            
            
        }
        else
        {
            
        }
    }];
}


- (void)getAdditionalMessageListByUser:(PFUser *)curUser :(MessageListVC *)messageListView :(NSMutableArray *)curList
{
    PFQuery *query = [PFQuery queryWithClassName:@"MessageInfo"];
    [query whereKey:@"receiver" equalTo:curUser];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            if (objects.count == 0) {
                messageListView.messageList = curList;
                [messageListView actionSuceed:@""];
                
                NSLog(@"there are no object in receiver");
            }
            else
            {
                NSMutableArray * list = [[NSMutableArray alloc]init];
                for (int i = 0; i < [objects count]; i++) {
                    [list addObject:[NSNull null]];
                }
                
                
                for (int i = 0; i<objects.count; i++) {
                    
                    PFObject * link = [objects objectAtIndex:i];
                    PFUser * receiver = [link objectForKey:@"sender"];
                    
                    
                    PFQuery * query = [PFUser query];
                    [query whereKey:@"objectId" equalTo:receiver.objectId];
                    [query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error) {
                        if (!error) {
                            if ([users count] == 0) {
                                //err
                            }
                            else
                            {
                                
                                NSLog(@"check connector");
                                PFUser * curReceiver = [users objectAtIndex:0];
                                User * userReceiver = [[User alloc]initWithUser:curReceiver];
                                
                                MessageInfo * message = [[MessageInfo alloc]init];
                                message.messageObject = link;
                                message.connector = userReceiver;
                                
                                NSLog(@"userphone = %@", message.connector.phone);
                                
                                [list replaceObjectAtIndex:i withObject:message];
                                
                                
                            }
                            
                            
                            int nullTotal = 0;
                            for (NSObject *component in list) {
                                if (component == [NSNull null]) {
                                    nullTotal++;
                                }
                            }
                            if (nullTotal == 0) {
                                [curList addObjectsFromArray:list];
                                messageListView.messageList = curList;
                                [messageListView actionSuceed:@""];
                            }
                        }
                        else
                        {
                            
                        }
                        
                    }];
                    
                }
            }
            
        }
        else
        {
            
        }
    }];
}

- (void)validateMessageConnector:(PFUser *)curUser :(PFUser *)endUser :(UserListVC *)userListView
{
    NSLog(@"validation 1 start");
    PFQuery *query = [PFQuery queryWithClassName:@"MessageInfo"];
    [query whereKey:@"sender" equalTo:curUser];
    [query whereKey:@"receiver" equalTo:endUser];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            if (objects.count == 0) {
                NSLog(@"validation 1: Pass");
                [self validateMessageConnector2:curUser :endUser :userListView];
            }
            else
            {
                [userListView selectionFinish];
            }
        }
        else
        {
            
        }
    }];
}

- (void) validateMessageConnector2:(PFUser *)curUser :(PFUser *)endUser :(UserListVC *)userListView
{
    PFQuery *query = [PFQuery queryWithClassName:@"MessageInfo"];
    [query whereKey:@"receiver" equalTo:curUser];
    [query whereKey:@"sender" equalTo:endUser];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            if (objects.count == 0) {
                NSLog(@"validation 2: Pass");
                [self addMessageConnector:curUser :endUser :userListView];
            }
            else
            {
                [userListView selectionFinish];
            }
        }
        else
        {
            
        }
    }];
}

- (void) addMessageConnector:(PFUser *)curUser :(PFUser *)endUser :(UserListVC *)userListView
{
    PFObject *phone = [PFObject objectWithClassName:@"MessageInfo"];
    
    [phone setObject:curUser forKey:@"sender"];
    [phone setObject:endUser forKey:@"receiver"];
    [phone saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(!error)
        {
            [userListView selectionFinish];
        }
    }];
}

- (void) loadAllUser:(UserListVC *)userListView :  (PFUser *) curUser
{
    PFQuery * query = [PFUser query];
    query.limit = 1000;
    [query whereKey:@"objectId" notEqualTo:curUser.objectId];
    [query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error) {
        if (!error) {
            
            
            
            NSMutableArray * userList = [[NSMutableArray alloc] init];
            
            for (PFUser * user in users) {
                User * newUser = [[User alloc]initWithUser:user];
                [userList addObject:newUser];
            }
            
            userListView.userList = userList;
            NSLog(@"number of user list %i", [userList count]);
            [userListView actionSuceed:@""];
        }
        else
        {
            
        }
        
    }];

}

- (void)chat:(MessageInfo *)messageInfo :(User *)curUser :(MessageChatVC *)messageChatView :(NSString *)content
{
    NSLog(@"send start");
    PFObject *chatInfo = [PFObject objectWithClassName:@"MessageChat"];
    
    [chatInfo setObject:curUser.userObject forKey:@"userId"];
     NSLog(@"check 12");
    [chatInfo setObject:content forKey:@"content"];
     NSLog(@"check 123");
    [chatInfo setObject:messageInfo.messageObject forKey:@"connectorId"];
    [chatInfo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(!error)
        {
            NSLog(@"check 1");
            [messageChatView sendDone:chatInfo];
            [messageChatView timerStart];
        }
    }];
}

- (void)loadMessageList:(MessageInfo *)messageInfo :(MessageChatVC *)messageChatView
{
    PFQuery *query = [PFQuery queryWithClassName:@"MessageChat"];
	
	//Search criteria. Can use equalTo, lessThan, nearGeopoint....
    query.limit = 20;
	[query whereKey:@"connectorId" equalTo:messageInfo.messageObject];
    [query orderByDescending:@"createdAt"];
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            NSArray* reversedArray = [[objects reverseObjectEnumerator] allObjects];
            NSLog(@"Number of chat %i", [objects count]);
            if (objects.count == 0) {
                [messageChatView actionSuceed:@""];
            }
            else
            {
                NSMutableArray * list = [[NSMutableArray alloc]init]
                ;
                
                for (PFObject * object in reversedArray) {
                    [list addObject:object];
                    
                    if ([list count] == [objects count]) {
                        messageChatView.chatList = list;
                        [messageChatView actionSuceed:@""];
                    }
                }
            }
            
                
            
		} else {
            [messageChatView actionFail:@"LoadMessage Fail"];
			
		}
	}];
}

- (void)refreshMessageList:(MessageInfo *)messageInfo :(NSMutableArray *)curList :(MessageChatVC *)messageChatView
{
    PFQuery *query = [PFQuery queryWithClassName:@"MessageChat"];
    
    if (curList.count > 0) {
        PFObject * newestChat = [curList lastObject];
        [query whereKey:@"createdAt" greaterThan: newestChat.createdAt];
    }
    //Search criteria. Can use equalTo, lessThan, nearGeopoint....
    [query whereKey:@"connectorId" equalTo:messageInfo.messageObject];
    
    [query orderByAscending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if (objects.count == 0) {
                NSLog(@"No more update");
            }
            else
            {
                NSArray* reversedArray = [[objects reverseObjectEnumerator] allObjects];
                NSLog(@"Number of  additional chat %i", [objects count]);
                NSMutableArray * list = [[NSMutableArray alloc]init]
                ;
                
                for (PFObject * object in reversedArray) {
                    [list addObject:object];
                    
                    if ([list count] == [objects count]) {
                        [curList addObjectsFromArray:list];
                        NSLog(@"%i", [curList count]);
                        messageChatView.chatList = curList;
                        [messageChatView refreshDone];
                    }
                }
            }
        } else {
            //err
            
        }
    }];

}


//---------------------My Shortlisted---------------------
- (void)addFavorite:(User *)curUser :(Property *)curProperty : (DetailPropertyTabBarVC*) detailTab
{
    NSLog(@"send start");
    PFObject *favorite = [PFObject objectWithClassName:@"UserFavorite"];
    
    [favorite setObject:curUser.userObject forKey:@"user"];
    [favorite setObject:curProperty.propertyObject forKey:@"property"];
    [favorite saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(!error)
        {
            detailTab.isFavorited = YES;
            detailTab.favoriteInfo = favorite;
            [detailTab finishCheckFavorite];
        }
    }];
}

- (void)validateFavorite:(User *)curUser :(Property *)curProperty : (DetailPropertyTabBarVC*) detailTab
{
    PFQuery *query = [PFQuery queryWithClassName:@"UserFavorite"];
	
	
	[query whereKey:@"user" equalTo:curUser.userObject];
    [query whereKey:@"property" equalTo:curProperty.propertyObject];
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            if ([objects count] == 0) {
                //havent add
                detailTab.isFavorited = NO;
            }
            else
            {
                //added
                detailTab.favoriteInfo = [objects objectAtIndex:0];
                detailTab.isFavorited = YES;
            }
            [detailTab finishCheckFavorite];
            
		} else {
            
			
		}
	}];

}

- (void)unaddFavorite:(PFObject *)favoriteInfo :(DetailPropertyTabBarVC *)detailTab
{
    [favoriteInfo deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        detailTab.isFavorited = NO;
        [detailTab finishCheckFavorite];
    }];
}


- (void)loadFavorite:(User *)curUser :(MyFavoriteVC *)myFavoriteView
{
    PFQuery *query1 = [PFQuery queryWithClassName:@"UserFavorite"];
	
	
	[query1 whereKey:@"user" equalTo:curUser.userObject];
	[query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            if (objects.count == 0) {
                [myFavoriteView actionSuceed:@""];
                return ;
            }
            
            NSMutableArray * allpro = [[NSMutableArray alloc]init];
            
            for (PFObject * object in objects) {
                [allpro addObject:[NSNull null]];
            }
            
            
            for (int i = 0; i < objects.count; i ++) {
                
                NSLog(@"%i load object", i);
                PFObject * object = [objects objectAtIndex:i];
                
                PFObject * property = [object objectForKey:@"property"];
                
                //Load property
                PFQuery *query2 = [PFQuery queryWithClassName:@"Property"];
                [query2 getObjectInBackgroundWithId:property.objectId block:^(PFObject *newObject, NSError *error) {
                    if (!error) {
                        
                        //Load Image
                        PFQuery *query3 = [PFQuery queryWithClassName:@"Photo"];
                        [query3 whereKey:@"property" equalTo:newObject];
                        query3.limit = 1;
                        [query3 findObjectsInBackgroundWithBlock:^(NSArray *ImageObjects, NSError *error) {
                            if (!error) {
                                // The find succeeded.
                                NSLog(@"Object count = %i",[ImageObjects count]);
                                if ([ImageObjects count] == 0) {
                                    UIImage * iconImage = [UIImage imageNamed:@"condo2.jpg"];
                                    
                                    
                                    
                                    Property * curProperty = [[Property alloc]initWithPropertyWithNoIcon:newObject];
                                    curProperty.iconImage = iconImage;
                                    [allpro replaceObjectAtIndex:i withObject:curProperty];
                                    NSLog(@"load pic at %i done", i);
                                    
                                }
                                else
                                {
                                    PFFile *theImage = [[ImageObjects objectAtIndex:0] objectForKey:@"photo"];
                                    NSData *imageData = [theImage getData];
                                    UIImage *image = [UIImage imageWithData:imageData];
                                    
                                    UIImage * iconImage = image;
                                    
                                    
                                    
                                    Property * curProperty = [[Property alloc]initWithPropertyWithNoIcon:newObject];
                                    curProperty.iconImage = iconImage;
                                    
                                    [allpro replaceObjectAtIndex:i withObject:curProperty];
                                    NSLog(@"load pic at %i done", i);
                                }
                                
                                int nullTotal = 0;
                                for (NSObject *component in allpro) {
                                    if (component == [NSNull null]) {
                                        nullTotal++;
                                    }
                                }
                                if (nullTotal == 0) {
                                    myFavoriteView.allPropertyList = allpro;
                                    [myFavoriteView actionSuceed:@""];
                                }
                                
                            } else {
                                // Log details of the failure
                            }
                        }];
                        
                    }
                }];
                
            }
		} else {
            
			
		}
	}];
}
//---------------------My Alert---------------------
- (void)addAlert:(AddNewAlertVC *)addNewAlertView :(NSString *)subject :(NSString *)transaction :(NSString *)type :(NSString *)project :(NSString *)district :(NSString *)blockUnitNo :(NSString *)location :(NSString *)tenure :(NSString *)price :(NSString *)area :(NSString *)level :(NSString *)bedroom :(NSString *)remarks :(User *)curUser
{
    PFObject *alert = [PFObject objectWithClassName:@"Alert"];
    
    [alert setObject:subject forKey:@"subject"];
    [alert setObject:transaction forKey:@"transaction"];
    [alert setObject:type forKey:@"type"];
    [alert setObject:project forKey:@"project"];
    [alert setObject:district forKey:@"district"];
    [alert setObject:blockUnitNo forKey:@"blockUnitNo"];
    [alert setObject:location forKey:@"location"];
    [alert setObject:tenure forKey:@"tenure"];
    [alert setObject:price forKey:@"price"];
    [alert setObject:area forKey:@"area"];
    //[alert setObject:level forKey:@"level"];
    [alert setObject:bedroom forKey:@"bedroom"];
    [alert setObject:remarks forKey:@"remarks"];
    [alert setObject:curUser.userObject forKey:@"creator"];
    [alert saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(!error)
        {
            [addNewAlertView addAlertDone];
        }
        else
        {
            NSLog(@"%@",error);
        }
    }];
}

- (void)loadAlerts:(MyAlertsVC *)myAlertsView :(User *)curUser
{
    PFQuery *query = [PFQuery queryWithClassName:@"Alert"];
	
	
	[query whereKey:@"creator" equalTo:curUser.userObject];
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            
            NSMutableArray * list = [[NSMutableArray alloc]init];
            myAlertsView.statusList = [[NSMutableArray alloc]init];
            for (int i = 0; i < objects.count; i ++) {
                AlertInfo * alert = [[AlertInfo alloc]initWithAlert:[objects objectAtIndex:i]];
                [list addObject:alert];
                NSString *statusNew = @"";
                
                [myAlertsView.statusList addObject:statusNew];
                [self loadAlertStatus:myAlertsView: statusNew :alert :curUser];
                
            }
            
            
            myAlertsView.alertList = list;
            [myAlertsView actionSuceed:@""];
            
		} else {
            
			
		}
	}];
}

-(void) loadAlertStatus:(MyAlertsVC *)myAlertsView :(NSString*) isNew :(AlertInfo *) alertInfo :(User *)curUser
{
    PFQuery *query = [PFQuery queryWithClassName:@"Property"];
    [self modifyQueryWithAlert: query: alertInfo];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if ([objects count] != 0) {
                __block int totalNew = 0;
                for (int i = 0; i < objects.count; i ++) {
                    PFQuery *query2 = [PFQuery queryWithClassName:@"AlertRead"];
                    [query2 whereKey:@"property" equalTo:[objects objectAtIndex:i]];
                    [query2 whereKey:@"user" equalTo:curUser.userObject];
                    [query2 findObjectsInBackgroundWithBlock:^(NSArray *alertReads, NSError *error) {
                        if (!error) {
                            if (alertReads.count == 0) {
                                totalNew++;
                                NSLog(@"totalNew %i",totalNew);
                                [myAlertsView refreshStatusNew:isNew];
                            }
                        }
                    }];
                    
                }
            }
        }
    }];
    
}

-(void) modifyQueryWithAlert:(PFQuery *)query : (AlertInfo *) alertInfo
{
    NSLog(@"modifyQueryWithAlert %@", alertInfo.subject);
    if ([alertInfo.transaction length] != 0) {
        NSLog(@"run transaction");
        [query whereKey:@"listingType" equalTo:alertInfo.transaction];
    }
    
    if ([alertInfo.type length] != 0) {
        NSLog(@"run type");
        [query whereKey:@"propertyType" equalTo:alertInfo.type];
    }
    
    if ([alertInfo.district length] != 0) {
        NSLog(@"run district");
        [query whereKey:@"district" equalTo:alertInfo.district];
    }
    
    if ([alertInfo.blockUnitNo length] != 0) {
        NSLog(@"run blkNo");
        [query whereKey:@"blkNo" equalTo:alertInfo.blockUnitNo];
    }
    
    if ([alertInfo.tenure length] != 0) {
        NSLog(@"run tenure");
        [query whereKey:@"tenure" equalTo:alertInfo.tenure];
    }
    
    if ([alertInfo.location length] != 0) {
        NSLog(@"run location");
        [query whereKey:@"address" containsString:alertInfo.location];
    }
    if ([alertInfo.project length] != 0) {
        NSLog(@"run project");
        [query whereKey:@"projectName" containsString:alertInfo.project];
    }
    
    
    
    if ([alertInfo.price length] != 0 && ![alertInfo.price isEqualToString:@"0 to 0"]) {
        NSLog(@"run price");
        NSArray *priceSplit = [alertInfo.price componentsSeparatedByString:@" to "];
        if (priceSplit.count > 1) {
            [query whereKey:@"price" greaterThanOrEqualTo:
             [NSNumber numberWithInt:[[priceSplit objectAtIndex:0] intValue]]];
            [query whereKey:@"price" lessThanOrEqualTo:
             [NSNumber numberWithInt:[[priceSplit objectAtIndex:1] intValue]]];
        }
    }
    if ([alertInfo.bedroom length] != 0 && ![alertInfo.bedroom isEqualToString:@"0 to 0"]) {
        NSLog(@"run bedroom");
        NSArray *bedSplit = [alertInfo.bedroom componentsSeparatedByString:@" to "];
        if (bedSplit.count > 1) {
            [query whereKey:@"bedroom" greaterThanOrEqualTo:
             [NSNumber numberWithInt:[[bedSplit objectAtIndex:0] intValue]]];
            [query whereKey:@"bedroom" lessThanOrEqualTo:
             [NSNumber numberWithInt:[[bedSplit objectAtIndex:1] intValue]]];
        }
    }
    if ([alertInfo.area length] != 0 && ![alertInfo.area isEqualToString:@"0 to 0"]) {
        NSLog(@"run area");
        NSArray *areaSplit = [alertInfo.area componentsSeparatedByString:@" to "];
        if (areaSplit.count > 1) {
            [query whereKey:@"area" greaterThanOrEqualTo:
             [NSNumber numberWithInt:[[areaSplit objectAtIndex:0] intValue]]];
            [query whereKey:@"area" lessThanOrEqualTo:
             [NSNumber numberWithInt:[[areaSplit objectAtIndex:1] intValue]]];
        }
    }
}
-(void) loadAlertPropStatus:(AlertPropertyListVC *)alertPropertyList :(NSString*) isNew :(PFObject *) property
{
    PFQuery *query2 = [PFQuery queryWithClassName:@"AlertRead"];
    [query2 whereKey:@"property" equalTo:property];
    [query2 whereKey:@"user" equalTo:[[DataEngine sharedInstance]curUser].userObject];
    [query2 findObjectsInBackgroundWithBlock:^(NSArray *alertReads, NSError *error) {
        if (!error) {
            if (alertReads.count == 0) {
                NSLog(@"%@ is new",property.objectId);
                [alertPropertyList refreshStatusNew:isNew:NO];
                return ;
            }
            else
            {
                [alertPropertyList refreshStatusNew:isNew:YES];
                NSLog(@"%@ is readed",property.objectId);
            }
        }
    }];
    
}

- (void)searchAlert:(AlertPropertyListVC *)alertPropertyList :(AlertInfo *)alertInfo
{
    PFQuery *query = [PFQuery queryWithClassName:@"Property"];
    [self modifyQueryWithAlert: query: alertInfo];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
            if ([objects count] == 0) {
                [alertPropertyList actionSuceed:@""];
                return ;
            }
            
            
			// The find succeeded.
			NSLog(@"Successfully retrieved %d objects.", objects.count);
			// Do something with the found objects
            NSMutableArray * allpro = [[NSMutableArray alloc]init];
            alertPropertyList.statusList = [[NSMutableArray alloc]init];
            //NSLog(@"prop array = %@",objects);
            for (int i = 0; i < objects.count; i ++) {
                [allpro addObject:[NSNull null]];
                
                //search Status
                PFObject * property = [objects objectAtIndex:i];
                NSString *status = @"Checking";
                [alertPropertyList.statusList addObject:status];
                
            }
            
            
            
            //search Image
            for (int i = 0; i < objects.count; i ++) {
                
                NSLog(@"%i loop", i);
                
                NSLog(@"a");
                PFObject * property = [objects objectAtIndex:i];
                
                //search Status
                NSLog(@"alertPropertyList.statusList = %@",alertPropertyList.statusList);
                [self loadAlertPropStatus:alertPropertyList :[alertPropertyList.statusList objectAtIndex:i] :property];
                
                NSLog(@"b property id %@", property.objectId );
                //Load property
                PFQuery *query2 = [PFQuery queryWithClassName:@"Property"];
                NSLog(@"c");
                [query2 getObjectInBackgroundWithId:property.objectId block:^(PFObject *newObject, NSError *error) {
                    if (!error) {
                        
                        //Load Image
                        
                        NSLog(@"load picture %i" , i);
                        PFQuery *query3 = [PFQuery queryWithClassName:@"Photo"];
                        [query3 whereKey:@"property" equalTo:newObject];
                        query3.limit = 1;
                        [query3 findObjectsInBackgroundWithBlock:^(NSArray *ImageObjects, NSError *error) {
                            if (!error) {
                                if ([ImageObjects count] == 0) {
                                    UIImage * iconImage = [UIImage imageNamed:@"condo2.jpg"];
                                    
                                    
                                    
                                    Property * curProperty = [[Property alloc]initWithPropertyWithNoIcon:newObject];
                                    curProperty.iconImage = iconImage;
                                    [allpro replaceObjectAtIndex:i withObject:curProperty];
                                    
                                    NSLog(@"load pic done %i", i);
                                    
                                }
                                else
                                {
                                    PFFile *theImage = [[ImageObjects objectAtIndex:0] objectForKey:@"photo"];
                                    NSData *imageData = [theImage getData];
                                    UIImage *image = [UIImage imageWithData:imageData];
                                    
                                    UIImage * iconImage = image;
                                    
                                    
                                    
                                    Property * curProperty = [[Property alloc]initWithPropertyWithNoIcon:newObject];
                                    curProperty.iconImage = iconImage;
                                    
                                    [allpro replaceObjectAtIndex:i withObject:curProperty];
                                    NSLog(@"load pic done %i", i);
                                }
                                
                                int nullTotal = 0;
                                for (NSObject *component in allpro) {
                                    if (component == [NSNull null]) {
                                        nullTotal++;
                                    }
                                }
                                if (nullTotal == 0) {
                                    alertPropertyList.propertyList = allpro;
                                    [alertPropertyList actionSuceed:@""];
                                }
                                
                            }
                            else
                            {
                                // Log details of the failure
                                [alertPropertyList actionFail:@"fail"];                            }
                        }];
                        
                    }
                }];
                
            }
            
		} else {
			// Log details of the failure
			
		}
	}];
    
}


- (void)editAlert:(AddNewAlertVC *)addNewAlertView :(AlertInfo *)alertInfo
{
    PFObject * alert = alertInfo.alert;
    
    alert[@"subject"] = alertInfo.subject == nil ? @"" : alertInfo.subject;
    alert[@"transaction"] = alertInfo.transaction == nil ? @"" : alertInfo.transaction;
    
    alert[@"type"] = alertInfo.type == nil ? @"" : alertInfo.type;
    
    alert[@"project"] = alertInfo.project == nil ? @"" : alertInfo.project;
    
    alert[@"district"] = alertInfo.district == nil ? @"" : alertInfo.district;
    
    alert[@"blockUnitNo"] = alertInfo.blockUnitNo == nil ? @"" : alertInfo.blockUnitNo;
    
    alert[@"location"] = alertInfo.location == nil ? @"" : alertInfo.location;
    
    alert[@"tenure"] = alertInfo.tenure == nil ? @"" : alertInfo.tenure;
    
    alert[@"price"] = alertInfo.price == nil ? @"" : alertInfo.price;
    
    alert[@"area"] = alertInfo.area == nil ? @"" : alertInfo.area;
    
    alert[@"bedroom"] = alertInfo.bedroom == nil ? @"" : alertInfo.bedroom;
    
    alert[@"remarks"] = alertInfo.remark == nil ? @"" : alertInfo.remark;
    
    
    [alert saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        //
        [addNewAlertView addAlertDone];
    }];
}

- (void)addNewAlert:(AddNewAlertVC *)addNewAlertView :(AlertInfo *)newAlert : (User*) curUser
{
    PFObject *alert = [PFObject objectWithClassName:@"Alert"];
    
    alert[@"subject"] = newAlert.subject;
    alert[@"transaction"] = newAlert.transaction;
    alert[@"type"] = newAlert.type;
    alert[@"project"] = newAlert.project;
    alert[@"district"] = newAlert.district;
    alert[@"blockUnitNo"] = newAlert.blockUnitNo;
    alert[@"location"] = newAlert.location;
    alert[@"tenure"] = newAlert.tenure;
    alert[@"price"] = newAlert.price;
    alert[@"area"] = newAlert.area;
    alert[@"bedroom"] = newAlert.bedroom;
    alert[@"remarks"] = newAlert.remark;
    alert[@"creator"] = curUser.userObject;
    
    [alert saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        //
        
        [addNewAlertView addAlertDone];
    }];
}

- (void)getAllAlertList : (Property*) curProperty
{
    NSLog(@"getall alert list runs");
    PFQuery *query = [PFQuery queryWithClassName:@"Alert"];
    query.limit = 1000;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject * object in objects) {
                AlertInfo * alert = [[AlertInfo alloc]initWithAlert:object];
                
                
                PFQuery *query = [PFQuery queryWithClassName:@"Property"];
                [self modifyQueryWithAlert: query: alert];
                
                [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    if (!error) {
                        if ([objects count] == 0) {
                        }
                        else
                        {
                            NSLog(@"suitable %@",objects);
                            for (PFObject * object in objects) {
                                if ([object.objectId isEqualToString:curProperty.propertyObject.objectId]) {
                                    NSLog(@"print this alert %@", alert.subject);
                                    PFUser * creator = [alert.alert objectForKey:@"creator"];
                                    //creator
                                    NSString *message = [NSString stringWithFormat:@"Alert %@ got new property: %@",alert.subject,curProperty.address];
                                    NSMutableDictionary *pushDict = [[NSMutableDictionary alloc] init];
                                    [pushDict setObject:message forKey:@"alert"];
                                    [pushDict setObject:alert.alert forKey:@"pushPFObject"];
                                    [pushDict setObject:@"alert" forKey:@"pushType"];
                                    [[NotificationEngine sharedInstance] pushToUserObj:creator :pushDict];
                                }
                            }
                        }
                    } else {
                        // Log details of the failure
                        
                    }
                }];
            }
        }
    }];
    
}
//------partner
- (void)addPartner:(AddPartnerVC *)addPartnerView :(Partner *)curParner
{
    PFObject *partner = [PFObject objectWithClassName:@"Partner"];
    
    partner[@"type"] = curParner.partnerType == nil ? @"" : curParner.partnerType;
    
    partner[@"name"] = curParner.name == nil ? @"" : curParner.name;
    
    partner[@"address"] = curParner.address == nil ? @"" : curParner.address;
    
    partner[@"contact"] = curParner.contact == nil ? @"" : curParner.contact;
    
    partner[@"email"] = curParner.email == nil ? @"" : curParner.email;
    
    partner[@"bank"] = curParner.bank == nil ? @"" : curParner.bank;
    
    partner[@"creator"] = curParner.creator.userObject;

    
    
    [partner saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        //
        
        [addPartnerView addDone];
    }];
}

- (void)getPartners:(PartnersVC *)partnersView :(NSString *)type : (User*) curUser
{
    PFQuery *query = [PFQuery queryWithClassName:@"Partner"];
    
    [query whereKey:@"type" equalTo:type];
    [query whereKey:@"creator" equalTo:curUser.userObject];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            NSMutableArray * list = [[NSMutableArray alloc]init];
            for (PFObject * object in objects) {
                
                //User * curUser = [[User alloc]initWithUser:object[@"creator"]];
                Partner * newPartner = [[Partner alloc]init];
                newPartner.partnerType = type;
                newPartner.name = object[@"name"];
                newPartner.address = object[@"address"];
                newPartner.contact = object[@"contact"];
                newPartner.email = object[@"email"];
                newPartner.bank = object[@"bank"];
                newPartner.partnerObject = object;
                //newPartner.creator = curUser;
                [list addObject:newPartner];
            }
            
            partnersView.partnerList = list;
            
            [partnersView actionSuceed:@""];
        }
    }];
}

- (void)editPartners:(EditPartnerVC *)editView :(Partner *)curParner
{
    PFObject * partner = curParner.partnerObject;
    
    partner[@"name"] = curParner.name;
    partner[@"contact"] = curParner.contact;
    partner[@"email"] = curParner.email;
    partner[@"bank"] = curParner.bank;
    
    
    [partner saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        //
        
        
        if ([curParner.partnerType isEqualToString:@"Commercial Loan Bankers"] || [curParner.partnerType isEqualToString:@"Mortgage Bankers"] || [curParner.partnerType isEqualToString:@"Residential Loan Bankers"])
        {
            PartnersVC *prevVC = [editView.navigationController.viewControllers objectAtIndex:2];
            [editView.navigationController popToViewController:prevVC animated:YES];
            [prevVC actionRun];
        }
        else
        {
            NSLog(@"hahahahah");
            PartnersVC *prevVC = [editView.navigationController.viewControllers objectAtIndex:1];
            [editView.navigationController popToViewController:prevVC animated:YES];
            [prevVC actionRun];
        }
        
    }];
}

//Calendar

- (void)requestMeetup:(RequestMeetupClientVC *)requestMeetupView :(ScheduleEvent *)scheduleEvent
{
    
    PFObject * event = [PFObject objectWithClassName:@"ScheduleEvent"];
    if (scheduleEvent.picture != nil) {
        NSData* imageData = UIImageJPEGRepresentation(scheduleEvent.picture, 0.05f);
        PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:imageData];
        event[@"picture"] = imageFile;
    }
    
    if (scheduleEvent.projectName != nil) {
        event[@"projectName"] = scheduleEvent.projectName;
    }
    
    if (scheduleEvent.projectType != nil) {
        event[@"projectType"] = scheduleEvent.projectType;
    }
    
    if (scheduleEvent.address != nil) {
        event[@"address"] = scheduleEvent.address;
    }
    
    if (scheduleEvent.eventSchedule != nil) {
        event[@"dateTime"] = scheduleEvent.eventSchedule;
    }
    
    if (scheduleEvent.meetupVenue != nil) {
        event[@"meetupVenue"] = [NSNumber numberWithInt:[scheduleEvent.meetupVenue intValue]];
    }
    
    if (scheduleEvent.idealBuyingPrice != nil) {
        event[@"idealBuyingPrice"] = scheduleEvent.idealBuyingPrice;
    }
    
    if (scheduleEvent.requester != nil) {
        event[@"requester"] = scheduleEvent.requester.userObject;
    }
    
    if (scheduleEvent.receiverAgent != nil) {
        event[@"receiverAgent"] = scheduleEvent.receiverAgent;
    }
    
    /*
    if (scheduleEvent.receiverCreator != nil) {
        event[@"receiverCreator"] = scheduleEvent.receiverCreator;
    }*/
    
    
    event[@"agentAccept"] = [NSNumber numberWithBool:scheduleEvent.agentAccept];
    event[@"requesterAccept"] = [NSNumber numberWithBool:scheduleEvent.requesterAccept];
    
    [event saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            [requestMeetupView.navigationController popViewControllerAnimated:YES];
        }
    }];
}


- (void)getRequestList:(ScheduleListVC*) scheduleListView : (User *) curUser
{
    PFQuery *query = [PFQuery queryWithClassName:@"ScheduleEvent"];
    
    [query whereKey:@"requester" equalTo:curUser.userObject];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            NSLog(@"myRequest List %i", [objects count]);
            NSMutableArray * list = [[NSMutableArray alloc]init];
            
            for (PFObject * object in objects) {
                ScheduleEvent * newEvent = [[ScheduleEvent alloc]init];
                
                newEvent.eventObject = object;
                newEvent.projectName = [object objectForKey:@"projectName"];
                newEvent.address = [object objectForKey:@"address"];
                newEvent.idealBuyingPrice = [object objectForKey:@"idealBuyingPrice"];
                newEvent.requester = curUser;
                newEvent.eventSchedule = [object objectForKey:@"dateTime"];
                
             
                newEvent.receiverAgent = [object objectForKey:@"receiverAgent"];
                newEvent.receiverCreator = [object objectForKey:@"receiverCreator"];
                
                newEvent.projectType = [object objectForKey:@"projectType"];
                if ([[object objectForKey:@"agentAccept"] isEqual: @YES]) {
                    newEvent.agentAccept = YES;
                }
                else
                {
                    newEvent.agentAccept = NO;
                }
                
                
                if ([[object objectForKey:@"requesterAccept"] isEqual: @YES]) {
                    newEvent.requesterAccept = YES;
                }
                else
                {
                    newEvent.requesterAccept = NO;
                };
                
                if ([[object objectForKey:@"isReschedule"] isEqual: @YES]) {
                    newEvent.isReschedule = YES;
                }
                else
                {
                    newEvent.isReschedule = NO;
                }
                
                newEvent.meetupVenue = [object objectForKey:@"meetupVenue"];
                
                
                PFFile *theImage = [object objectForKey:@"picture"];
				NSData *imageData = [theImage getData];
				UIImage *image = [UIImage imageWithData:imageData];
                
                NSLog(@"agent %hhd", newEvent.agentAccept);
                NSLog(@"requester %hhd", newEvent.requesterAccept);
                newEvent.picture = image;
                [newEvent getEventType];
                
                if (!(newEvent.requesterAccept && newEvent.agentAccept)) {
                    [list addObject:newEvent];
                }
            }
            
            scheduleListView.myRequestList = list;
            [self getReceiverAgent:scheduleListView :curUser];
        }
    }];
}

- (void)getRequestListForCalendar:(CalendarDisplayCV*) scheduleListView : (User *) curUser
{
    PFQuery *query = [PFQuery queryWithClassName:@"ScheduleEvent"];
    
    [query whereKey:@"requester" equalTo:curUser.userObject];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            NSLog(@"myRequest List %i", [objects count]);
            NSMutableArray * list = [[NSMutableArray alloc]init];
            
            for (PFObject * object in objects) {
                ScheduleEvent * newEvent = [[ScheduleEvent alloc]init];
                
                newEvent.eventObject = object;
                newEvent.projectName = [object objectForKey:@"projectName"];
                newEvent.address = [object objectForKey:@"address"];
                newEvent.idealBuyingPrice = [object objectForKey:@"idealBuyingPrice"];
                newEvent.requester = curUser;
                newEvent.eventSchedule = [object objectForKey:@"dateTime"];
                
                
                newEvent.receiverAgent = [object objectForKey:@"receiverAgent"];
                newEvent.receiverCreator = [object objectForKey:@"receiverCreator"];
                
                newEvent.projectType = [object objectForKey:@"projectType"];
                
                if ([[object objectForKey:@"agentAccept"] isEqual: @YES]) {
                    newEvent.agentAccept = YES;
                }
                else
                {
                    newEvent.agentAccept = NO;
                }
                
                
                if ([[object objectForKey:@"requesterAccept"] isEqual: @YES]) {
                    newEvent.requesterAccept = YES;
                }
                else
                {
                    newEvent.requesterAccept = NO;
                }
                
                if ([[object objectForKey:@"isReschedule"] isEqual: @YES]) {
                    newEvent.isReschedule = YES;
                }
                else
                {
                    newEvent.isReschedule = NO;
                }
                
                newEvent.meetupVenue = [object objectForKey:@"meetupVenue"];
                NSLog(@"agent %hhd", newEvent.agentAccept);
                NSLog(@"requester %hhd", newEvent.requesterAccept);
                
                PFFile *theImage = [object objectForKey:@"picture"];
				NSData *imageData = [theImage getData];
				UIImage *image = [UIImage imageWithData:imageData];
                
                newEvent.picture = image;
                [newEvent getEventType];
                
                if ((newEvent.requesterAccept && newEvent.agentAccept)) {
                    [list addObject:newEvent];
                }
            }
            
            scheduleListView.myRequestList = list;
            [self getReceiverAgentForCalendar:scheduleListView :curUser];
        }
    }];
}

- (void) getReceiverAgent :(ScheduleListVC*) scheduleListView : (User *) curUser;
{
    PFQuery *query = [PFQuery queryWithClassName:@"ScheduleEvent"];
    
    [query whereKey:@"receiverAgent" equalTo:curUser.userObject];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            NSLog(@"other request 1 List %i", [objects count]);
            NSMutableArray * list = [[NSMutableArray alloc]init];
            
            for (PFObject * object in objects) {
                ScheduleEvent * newEvent = [[ScheduleEvent alloc]init];
                
                newEvent.eventObject = object;
                newEvent.projectName = [object objectForKey:@"projectName"];
                newEvent.address = [object objectForKey:@"address"];
                newEvent.idealBuyingPrice = [object objectForKey:@"idealBuyingPrice"];
                newEvent.requester = curUser;
                newEvent.eventSchedule = [object objectForKey:@"dateTime"];
                
                newEvent.receiverAgent = [object objectForKey:@"requester"];
                newEvent.receiverCreator = [object objectForKey:@"receiverCreator"];
                
                newEvent.projectType = [object objectForKey:@"projectType"];
                newEvent.agentAccept = [[object objectForKey:@"agentAccept"] boolValue];
                newEvent.requesterAccept =  [[object objectForKey:@"requesterAccept"] boolValue];
                newEvent.meetupVenue = [object objectForKey:@"meetupVenue"];
                
                
                PFFile *theImage = [object objectForKey:@"picture"];
				NSData *imageData = [theImage getData];
				UIImage *image = [UIImage imageWithData:imageData];
                
                newEvent.picture = image;
                [newEvent getEventType];
                
                if (!(newEvent.requesterAccept && newEvent.agentAccept)) {
                    [list addObject:newEvent];
                }
            }
            
            scheduleListView.otherRequestList = list;
            [scheduleListView actionSuceed:@""];
            
        }
    }];
}
- (void) getReceiverAgentForCalendar :(CalendarDisplayCV*) scheduleListView : (User *) curUser;
{
    PFQuery *query = [PFQuery queryWithClassName:@"ScheduleEvent"];
    
    [query whereKey:@"receiverAgent" equalTo:curUser.userObject];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            NSLog(@"other request 1 List %i", [objects count]);
            NSMutableArray * list = [[NSMutableArray alloc]init];
            
            for (PFObject * object in objects) {
                ScheduleEvent * newEvent = [[ScheduleEvent alloc]init];
                
                newEvent.eventObject = object;
                newEvent.projectName = [object objectForKey:@"projectName"];
                newEvent.address = [object objectForKey:@"address"];
                newEvent.idealBuyingPrice = [object objectForKey:@"idealBuyingPrice"];
                newEvent.requester = curUser;
                newEvent.eventSchedule = [object objectForKey:@"dateTime"];
                
                newEvent.receiverAgent = [object objectForKey:@"requester"];
                newEvent.receiverCreator = [object objectForKey:@"receiverCreator"];
                
                newEvent.projectType = [object objectForKey:@"projectType"];
                newEvent.agentAccept = [[object objectForKey:@"agentAccept"] boolValue];
                newEvent.requesterAccept =  [[object objectForKey:@"requesterAccept"] boolValue];
                newEvent.meetupVenue = [object objectForKey:@"meetupVenue"];
                
                
                PFFile *theImage = [object objectForKey:@"picture"];
				NSData *imageData = [theImage getData];
				UIImage *image = [UIImage imageWithData:imageData];
                
                newEvent.picture = image;
                [newEvent getEventType];
                [list addObject:newEvent];
            }
            
            scheduleListView.otherRequestList = list;
            [scheduleListView actionSuceed:@""];
            
        }
    }];
}


- (void)acceptAgent:(RequestMeetupAgentVC *)requestMeetupView :(ScheduleEvent *)scheduleEvent
{
    PFQuery *query = [PFQuery queryWithClassName:@"ScheduleEvent"];
    
    // Retrieve the object by id
    [query getObjectInBackgroundWithId:scheduleEvent.eventObject.objectId block:^(PFObject *event, NSError *error) {
        

        
        
        
        if (scheduleEvent.isReschedule) {
            event[@"dateTime"] = scheduleEvent.eventSchedule;
            event[@"requesterAccept"] = @NO;
            event[@"agentAccept"] = @YES;
            event[@"isReschedule"] = [NSNumber numberWithBool:scheduleEvent.isReschedule];
        }
        else
        {
            event[@"agentAccept"] = @YES;
            event[@"isReschedule"] = @NO;
            event[@"meetupVenue"] = scheduleEvent.meetupVenue;
        }
        
        [event saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                
                [requestMeetupView.navigationController popViewControllerAnimated:YES];
                [requestMeetupView.scheduleList actionRun];
            }
        }];
        
    }];
}
-(void)acceptRequester:(RequestMeetupClientVC *)requestMeetupView :(ScheduleEvent *)scheduleEvent
{
    PFQuery *query = [PFQuery queryWithClassName:@"ScheduleEvent"];
    
    [query getObjectInBackgroundWithId:scheduleEvent.eventObject.objectId block:^(PFObject *event, NSError *error) {
        
        
        
        if (scheduleEvent.isReschedule) {
            NSLog(@"Not accc");
            event[@"requesterAccept"] = @YES;
            event[@"agentAccept"] = @NO;
            event[@"isReschedule"] = @YES;
            event[@"dateTime"] = scheduleEvent.eventSchedule;
            event[@"idealBuyingPrice"] = scheduleEvent.idealBuyingPrice;
        }
        else
        {
            NSLog(@"acceptasdasda");
            event[@"requesterAccept"] = @YES;
            event[@"isReschedule"] = @NO;
        }
        
        
        event[@"isReschedule"] = [NSNumber numberWithBool:scheduleEvent.isReschedule];
        
        [event saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [requestMeetupView.navigationController popViewControllerAnimated:YES];
            [requestMeetupView.scheduleList actionRun];
        }];
        
    }];
}

//special Deal
- (void)getSpecialDeal:(SpecialDealListVC *)specialDealView
{
    PFQuery *query = [PFQuery queryWithClassName:@"SpecialDeal"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSLog(@"%i count", [objects count]);
            if (objects.count == 1) {
                PFFile * file = [[objects objectAtIndex:0] objectForKey:@"deal"];
                NSData * xmlData = [file getData];
                
                
                specialDealView.fileData = xmlData;
                
                [specialDealView actionSuceed:@""];
            }
        }
        
    }];
}

//------------New Group List----------
- (void)getGroupList:(NewGroupListVC *)groupListView :(User *)curUser
{
    PFQuery *query = [PFQuery queryWithClassName:@"Group"];
    
    
    [query whereKey:@"creator" equalTo:curUser.userObject];
    
    [query includeKey:@"creator"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSMutableArray * list = [[NSMutableArray alloc]init];
            
            for (PFObject * object in objects) {
                [list addObject:object];
            }
            [self getGroupByMember:groupListView :curUser : list];
        }
        else
        {
            [groupListView actionFail:@"fail"];
        }
        
    }];
}

- (void)getGroupByMember:(NewGroupListVC *)groupListView :(User *)curUser : (NSMutableArray *) curGroupList;
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupParticipants"];
    
    
    [query whereKey:@"userId" equalTo:curUser.userObject];
    
    [query includeKey:@"groupId"];
    [query includeKey:@"groupId.creator"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            NSMutableArray * invite = [[NSMutableArray alloc]init];
            NSMutableArray * participantList = [[NSMutableArray alloc]init];
            for (PFObject * object in objects) {
                if ([[object objectForKey:@"isAccept"]  isEqual: @NO]) {
                    [invite addObject:object];
                }
                else
                {
                    PFObject * group = [object objectForKey:@"groupId"];
                    if ([group isEqual:Nil]) {
                        
                    }
                    else
                    {
                        [curGroupList addObject:[object objectForKey:@"groupId"]];
                        [participantList addObject:object];
                    }
                }
            }
            
            
            groupListView.groupList = curGroupList;
            groupListView.inviteGroupList = invite;
            groupListView.participantList = participantList;
            [groupListView actionSuceed:@""];
        }
        else
        {
            [groupListView actionFail:@"fail"];
        }
        
    }];
}

- (void)validateTitle:(NewAddGroupVC *)aGroupListView :(NSString *)title
{
    PFQuery *query = [PFQuery queryWithClassName:@"Group"];
    
    
    [query whereKey:@"title" equalTo:title];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if (objects.count > 0) {
                [aGroupListView showDialog:@"Name is not available"];
            }
            else
            {
                [aGroupListView addToDatabase];
            }
        }
        
    }];
}

- (void)addNewGroup:(NewAddGroupVC *)aGroupListView :(NSMutableDictionary *)group
{
    PFObject * newGroup = [PFObject objectWithClassName:@"Group"];
    
    newGroup[@"title"] = [group objectForKey:@"title"];
    newGroup[@"creator"] = [group objectForKey:@"creator"];
    
    [newGroup saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            [aGroupListView cancel:aGroupListView];
            
            [aGroupListView.groupListView actionRun];
        }
    }];
}


- (void)getMembers:(DetailGroupMemberVC *)groupMemberView :(PFObject *)curGroup
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupParticipants"];
    
    NSLog(@"curGroup.objectId = %@", curGroup.objectId);
    [query whereKey:@"groupId" equalTo:curGroup];
    
    [query includeKey:@"userId"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSMutableArray * pending = [[NSMutableArray alloc]init];
            NSMutableArray * accept = [[NSMutableArray alloc]init];
            NSLog(@"Found %i in %@ group",objects.count, curGroup.objectId);
            for (PFObject * object in objects) {
                NSLog(@"%@", [object objectForKey:@"isAccept"]);
                if ([[object objectForKey:@"isAccept"] isEqual: @NO]) {
                    
                    [pending addObject:object];
                    NSLog(@"add");
                    
                }
                else
                {
                    [accept addObject:object];
                }
            }
            
            groupMemberView.pendingMember = pending;
            groupMemberView.acceptedMember = accept;
            
            NSLog(@"groupMemberView.pendingMember = %@", groupMemberView.pendingMember);
            NSLog(@"groupMemberView.acceptedMember = %@", groupMemberView.acceptedMember);
            
            [groupMemberView actionSuceed:@""];
        }
        else
        {
            
        }
    }];
}


- (void)getGroupSharing:(DetailGroupListingVC *)groupTabbarView :(PFObject *)curGroup
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupSharingInfo"];
    
    
    [query whereKey:@"groupId" equalTo:curGroup];
    
    [query includeKey:@"propertyId"];
    [query includeKey:@"propertyId.agent"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error)
        {
            NSLog(@"number %i", objects.count);
            groupTabbarView.propertyList = objects;
            [groupTabbarView actionSuceed:@""];
        }
        else
        {
            [groupTabbarView actionFail:@""];
        }
        
    }];
}

- autoAddPropertiesToNewAcceptGroup: (PFObject *)curGroup
{
    NSLog(@"autoAddPropertiesToNewAcceptGroup runs");
    PFQuery *query = [PFQuery queryWithClassName:@"Property"];
    [query whereKey:@"agent" equalTo:[PFUser currentUser]];
    [query whereKey:@"submit" equalTo:@"YES"];
    [query includeKey:@"agent"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"property = %i",objects.count);
            for (PFObject *property in objects) {
                PFObject * newShare = [PFObject objectWithClassName:@"GroupSharingInfo"];
                
                newShare[@"groupId"] = curGroup;
                newShare[@"propertyId"] = property;
                [newShare saveInBackground];
                [self sendNotificationToGroupWithNewProperty:curGroup :property];
            }
        } else {
            
        }
    }];

}

/*
- (void)addToGroupShare:(DetailGroupListingVC *)groupListingVIew :(Property *)curPorpety
{
    PFObject * newShare = [PFObject objectWithClassName:@"GroupSharingInfo"];
    
    newShare[@"groupId"] = groupListingVIew.curGroup;
    newShare[@"propertyId"] = curPorpety.propertyObject;
    
    [newShare saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            [groupListingVIew actionRun];
        }
    }];
}*/


- (void)addToGroupShareIndustry:(DetailIndustrialVC *)groupListingVIew
{
    PFObject * newShare = [PFObject objectWithClassName:@"GroupSharingInfo"];
    
    newShare[@"groupId"] = groupListingVIew.curGroup;
    newShare[@"propertyId"] = groupListingVIew.property.propertyObject;
    
    
    
    
    [newShare saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            
            [groupListingVIew dismissViewControllerAnimated:YES completion:^{
                [self sendNotificationToGroupWithNewProperty:groupListingVIew.curGroup :groupListingVIew.property.propertyObject];
                
                [groupListingVIew.proportView dismissViewControllerAnimated:YES completion:nil];
            }];
        }
        //send notification
        
        
    }];
}

-(void) sendNewGroupShareNotification:(PFObject *) curGroup :(PFObject *) propertyObj
{
    BOOL bool2 = [[curGroup objectForKey:@"switch2"] intValue]==1;
    if (bool2) {
        PFQuery *query = [PFQuery queryWithClassName:@"GroupParticipants"];
        
        
        [query whereKey:@"groupId" equalTo:curGroup];
        NSLog(@"%@", curGroup.objectId);
        [query includeKey:@"userId"];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                for (PFObject * curUser in objects) {
                    NSLog(@"%@", [curUser objectForKey:@"isAccept"]);
                    if ([[curUser objectForKey:@"isAccept"] isEqual: @YES]) {
                        NSMutableDictionary *pushDict = [[NSMutableDictionary alloc] init];
                        
                        
                        [curGroup allKeys];
                        NSString *groupName = [curGroup objectForKey:@"groupTitle"];
                        NSString *chatContent = [NSString stringWithFormat:@"Group %@ has new property sharing",groupName];
                        
                        [pushDict setObject:chatContent forKey:@"alert"];
                        [pushDict setObject:curGroup forKey:@"pushPFObject"];
                        [pushDict setObject:@"group" forKey:@"pushType"];
                        [[NotificationEngine sharedInstance] pushToUserObj:curUser :pushDict];
                    }
                }
            }
        }];
    }
}

- (void)addToGroupShareLand:(DetailLandVC *)groupListingVIew
{
    PFObject * newShare = [PFObject objectWithClassName:@"GroupSharingInfo"];
    
    newShare[@"groupId"] = groupListingVIew.curGroup;
    newShare[@"propertyId"] = groupListingVIew.property.propertyObject;
    
    [newShare saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            
            [groupListingVIew dismissViewControllerAnimated:YES completion:^{
                [self sendNotificationToGroupWithNewProperty:groupListingVIew.curGroup :groupListingVIew.property.propertyObject];
                [groupListingVIew.proportView dismissViewControllerAnimated:YES completion:nil];
            }];
            
        }
    }];
}

- (void)valiadteIndustry:(DetailIndustrialVC *)groupListingView
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupSharingInfo"];
    
    
    [query whereKey:@"groupId" equalTo:groupListingView.curGroup];
    [query whereKey:@"propertyId" equalTo:groupListingView.property.propertyObject];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error)
        {
            if (objects.count > 0) {
                [groupListingView showDialog:@"Current Property Already Shared!!!"];
            }
            else
            {
                [self addToGroupShareIndustry:groupListingView ];
            }
        }
        
    }];
}

- (void)valiadteLand:(DetailLandVC *)groupListingView
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupSharingInfo"];
    
    
    [query whereKey:@"groupId" equalTo:groupListingView.curGroup];
    [query whereKey:@"propertyId" equalTo:groupListingView.property.propertyObject];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error)
        {
            if (objects.count > 0) {
                [groupListingView showDialog:@"Current Property Already Shared!!!"];
            }
            else
            {
                [self addToGroupShareLand:groupListingView ];
            }
        }
        
    }];
}

- (void)addToGroupShareHDB:(DetailHDBVC *)groupListingVIew
{
    PFObject * newShare = [PFObject objectWithClassName:@"GroupSharingInfo"];
    
    newShare[@"groupId"] = groupListingVIew.curGroup;
    newShare[@"propertyId"] = groupListingVIew.property.propertyObject;
    
    [newShare saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            
            [groupListingVIew dismissViewControllerAnimated:YES completion:^{
                [self sendNotificationToGroupWithNewProperty:groupListingVIew.curGroup :groupListingVIew.property.propertyObject];
                [groupListingVIew.proportView dismissViewControllerAnimated:YES completion:nil];
            }];
            
        }
    }];
}

- (void)valiadteHDB:(DetailHDBVC *)groupListingView
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupSharingInfo"];
    
    
    [query whereKey:@"groupId" equalTo:groupListingView.curGroup];
    [query whereKey:@"propertyId" equalTo:groupListingView.property.propertyObject];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error)
        {
            if (objects.count > 0) {
                [groupListingView showDialog:@"Current Property Already Shared!!!"];
            }
            else
            {
                [self addToGroupShareHDB:groupListingView ];
            }
        }
        
    }];
}

- (void)addToGroupShareCommercial:(DetailComercialVC *)groupListingVIew
{
    PFObject * newShare = [PFObject objectWithClassName:@"GroupSharingInfo"];
    
    newShare[@"groupId"] = groupListingVIew.curGroup;
    newShare[@"propertyId"] = groupListingVIew.property.propertyObject;
    
    [newShare saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            
            [groupListingVIew dismissViewControllerAnimated:YES completion:^{
                [self sendNotificationToGroupWithNewProperty:groupListingVIew.curGroup :groupListingVIew.property.propertyObject];
                [groupListingVIew.proportView dismissViewControllerAnimated:YES completion:nil];
            }];
            
        }
    }];
}

- (void)valiadteCommercial:(DetailComercialVC *)groupListingView
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupSharingInfo"];
    
    
    [query whereKey:@"groupId" equalTo:groupListingView.curGroup];
    [query whereKey:@"propertyId" equalTo:groupListingView.property.propertyObject];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error)
        {
            if (objects.count > 0) {
                [groupListingView showDialog:@"Current Property Already Shared!!!"];
            }
            else
            {
                [self addToGroupShareCommercial:groupListingView ];
            }
        }
        
    }];
}

- (void)addToGroupSharePR:(DetailPropertyVC *)groupListingVIew
{
    PFObject * newShare = [PFObject objectWithClassName:@"GroupSharingInfo"];
    
    newShare[@"groupId"] = groupListingVIew.curGroup;
    newShare[@"propertyId"] = groupListingVIew.property.propertyObject;
    
    [newShare saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            
            [groupListingVIew dismissViewControllerAnimated:YES completion:^{
                [self sendNotificationToGroupWithNewProperty:groupListingVIew.curGroup :groupListingVIew.property.propertyObject];
                [groupListingVIew.proportView dismissViewControllerAnimated:YES completion:nil];
            }];
            
        }
    }];
    
}

- (void)valiadtePR:(DetailPropertyVC *)groupListingView
{
    PFQuery *query = [PFQuery queryWithClassName:@"GroupSharingInfo"];
    
    
    [query whereKey:@"groupId" equalTo:groupListingView.curGroup];
    [query whereKey:@"propertyId" equalTo:groupListingView.property.propertyObject];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error)
        {
            if (objects.count > 0) {
                [groupListingView showDialog:@"Current Property Already Shared!!!"];
            }
            else
            {
                [self addToGroupSharePR:groupListingView ];
            }
        }
        
    }];
}


- (void)addMembers:(PhoneBookVC *)phoneBookVC :(NSMutableArray *)selectedMember
{
    
    
    __block int count = 0;
    for (PFUser * member in selectedMember) {
        PFObject *newMem = [PFObject objectWithClassName:@"GroupParticipants"];
        newMem[@"groupId"] = phoneBookVC.curGroup;
        newMem[@"userId"] = member;
        newMem[@"isAccept"] = @NO;
        
        [newMem saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                count++;
                if (count >= selectedMember.count) {
                    NSLog(@"done %i", count);
                    [phoneBookVC addFinished];
                }
                
            }
        }];
        
    }
    
}

- (void)deleteMember:(DetailGroupMemberVC *)groupMemberVC :(PFUser *)curUser
{
    NSLog(@"G %@, U %@", groupMemberVC.curGroup.objectId, curUser.objectId);
    PFQuery *query = [PFQuery queryWithClassName:@"GroupParticipants"];
    
    
    [query whereKey:@"groupId" equalTo:groupMemberVC.curGroup];
    [query whereKey:@"userId" equalTo:curUser];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error)
        {
            PFObject * user = [objects objectAtIndex:0];
            [user deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    if ([groupMemberVC.pendingMember containsObject:user]) {
                        [groupMemberVC.pendingMember removeObject:user];
                    }
                    else
                    {
                        [groupMemberVC.acceptedMember removeObject:user];
                    }
                    [groupMemberVC.tableView reloadData];
                }
            }];
        }
        
    }];
}
-(void) sendNotificationToGroupWithNewProperty:(PFObject *) groupInfo : (PFObject *) propertyId
{
    NSLog(@"sendNotificationToGroup: %@ WithNewProperty: %@",groupInfo.objectId,propertyId.objectId);
    PFQuery *query = [PFQuery queryWithClassName:@"GroupParticipants"];
    
    [query whereKey:@"groupId" equalTo:groupInfo];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            NSMutableArray * groupMembers = [NSMutableArray arrayWithArray:objects];
           
            NSLog(@"%i number of participant", [groupMembers count]);
            for (int i = 0; i<groupMembers.count; i++) {
                
                PFObject * curParticipant = [groupMembers objectAtIndex:i];
                PFUser * curUser = [curParticipant objectForKey:@"userId"];
                
                
                PFQuery * query = [PFUser query];
                [query whereKey:@"objectId" equalTo:curUser.objectId];
                [query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error) {
                    if (!error) {
                        PFUser * curUser = [users objectAtIndex:0];
                        
                        //NSLog(@"push prepare sending to: %@",curUser.objectId);
                            NSString *chatContent = [NSString stringWithFormat:@"%@ group got new sharing property",[groupInfo objectForKey:@"title"]];
                            NSMutableDictionary *pushDict = [[NSMutableDictionary alloc] init];
                            [pushDict setObject:chatContent forKey:@"alert"];
                            [pushDict setObject:groupInfo forKey:@"pushPFObject"];
                            [pushDict setObject:@"group" forKey:@"pushType"];
                            [[NotificationEngine sharedInstance] pushToUserObj:curUser :pushDict];
                        
                    }
                }];
            }
        }
    }];
}


@end
