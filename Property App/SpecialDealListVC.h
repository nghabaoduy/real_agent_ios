//
//  SpecialDealListVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/19/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyUITableViewController.h"

@interface SpecialDealListVC : MyUITableViewController <NSXMLParserDelegate>
{
    NSMutableArray * dealList;
    NSXMLParser *xmlParser;
    NSMutableArray * imagelist;
}

@property (nonatomic, retain) NSData * fileData;

@end
