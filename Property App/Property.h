//
//  Property.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 31/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "User.h"
#import "Agent.h"
#import "ParentLoadingVC.h"

@interface Property : NSObject
{
    BOOL downloadIcon;
}
@property (nonatomic, strong) NSString *propID;

@property (strong, nonatomic) PFObject * propertyObject;
@property (strong, nonatomic) NSString * propertyName;
@property (strong, nonatomic) NSString * propertyType;
@property (strong, nonatomic) NSString * listingType;
@property (nonatomic, strong) User * adder;
@property (nonatomic, strong) Agent * agent;

@property (strong, nonatomic) NSString * blkNo;
@property (strong, nonatomic) NSString * unitNo;
@property (strong, nonatomic) NSString * levelNumber;
@property (strong, nonatomic) NSString * address;
@property (strong, nonatomic) NSString * district;
@property (strong, nonatomic) NSString * postCode;


@property (strong, nonatomic) NSString * area;
@property (strong, nonatomic) NSString * tenure;
@property (strong, nonatomic) NSString * price;
@property (strong, nonatomic) NSString * bedroom;

@property (strong, nonatomic) NSString * status;
@property (strong, nonatomic) NSString * rentedPrice;
@property (strong, nonatomic) NSString * prType;
//condo extra field
@property (strong, nonatomic) NSString * projectName;
@property (strong, nonatomic) NSString * PSF;
@property (strong, nonatomic) NSString * rentalYeild;
@property (strong, nonatomic) NSString * topYear;

//Commercial
@property (strong, nonatomic) NSString * commercialType;
@property (strong, nonatomic) NSString * tenantedPrice;
@property (strong, nonatomic) NSString * gst;
@property (strong, nonatomic) NSString * groundLevel;
@property (strong, nonatomic) NSString * condition;
@property (strong, nonatomic) NSString * electricalLoad;
@property (strong, nonatomic) NSString * amps;

//Commercial Office
@property (strong, nonatomic) NSString * aircon;
@property (strong, nonatomic) NSString * ownPantry;
@property (strong, nonatomic) NSString * grade;
@property (strong, nonatomic) NSString * floorLoad;
@property (strong, nonatomic) NSString * serverRoom;

//Commercial Retail
@property (strong, nonatomic) NSString * retailType;

//Commercial F&B
@property (strong, nonatomic) NSString * fAndBType;
@property (strong, nonatomic) NSString * greaseTrap;
@property (strong, nonatomic) NSString * exhaust;

//Commercial Land
@property (strong, nonatomic) NSString * landSize;
@property (strong, nonatomic) NSString * noOfStorey;

//HDB
@property (strong, nonatomic) NSString * hdbType;
@property (strong, nonatomic) NSString * liftLevel;
@property (strong, nonatomic) NSString * mainDoorDirection;
@property (strong, nonatomic) NSString * roomPosition; ///Change name
@property (strong, nonatomic) NSString * town;
@property (strong, nonatomic) NSString * valuation;

//Industry
@property (strong, nonatomic) NSString * industryType;
@property (strong, nonatomic) NSString * vehicleAccess;
@property (strong, nonatomic) NSString * noOfCargoLift;
@property (strong, nonatomic) NSString * flatted;
@property (strong, nonatomic) NSString * ceilingHeight;
@property (strong, nonatomic) NSString * type;


//Land
@property (strong, nonatomic) NSString * landType;
@property (strong, nonatomic) NSString * furnishing;
@property (strong, nonatomic) NSString * mainGateFacing;
@property (strong, nonatomic) NSString * carPark;
@property (strong, nonatomic) NSString * garden;
@property (strong, nonatomic) NSString * swimmingPool;
@property (strong, nonatomic) NSString * basement;
@property (strong, nonatomic) NSString * roofTerrance;








@property int imageCount;
@property (strong, nonatomic) NSMutableArray * imageList;
@property (strong, nonatomic) ParentLoadingVC * loadingView;

@property (strong, nonatomic) NSString * isSubmited;


//unused yet
@property (strong, nonatomic) UIImage * iconImage;

//@property (strong, nonatomic) MyImageView * iconImageView;




//- (id) initWithInformation: (NSString *) propertyName : (NSString *) propertyType : (NSString *) roomType : (NSString*) unitNo : (NSString *) address : (NSString *) project : (NSString *) tenure : (NSString *) price : (NSString *) bedroom : (NSString *) floorSize : (NSString *) PSF : (NSString *) age : (NSString *) sortBy : (PFUser *) adder;
- (id) initWithProperty : (PFObject*) proper;
- (id) initWithPropertyWithNoIcon : (PFObject*) proper;
- (void) loadAllData;
- (void) loadAllPropInfo;
- (void) DeleteObject;
- (NSString *) ToString;
-(void) removeNullData;

- (void) AddImage : (UIImage*) image;
//- (void) AddProperty: (Property*) propertyIn;
- (void) AddPropertySuccessfully : (PFObject*) propertyOb;
- (void) AddPropertyFailed : (NSString*) errorString;
- (void) loadIconImage;
-(void) loadIconForIV: (UIImageView *) imageView;
- (void) ImageUploadSuccessfully;
- (void) ImageUploadFailed : (NSString*) errorString;

- (void) UploadProperty : (ParentLoadingVC*) parentV;

//- (BOOL) DownloadIcon;

-(void) printInfo;

@end
