//
//  ScheduleCell.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/13/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScheduleEvent.h"
#import "ScheduleListVC.h"

@interface ScheduleCell : UITableViewCell
{
    
}
@property (nonatomic, retain) IBOutlet UIImageView * displayer;
@property (nonatomic, retain) IBOutlet UILabel * eventTypeLB;
@property (nonatomic, retain) IBOutlet UILabel * projectNameLB;
@property (nonatomic, retain) IBOutlet UILabel * projectTypeLB;
@property (nonatomic, retain) IBOutlet UILabel * addressLB;
@property (nonatomic, retain) IBOutlet UILabel * receiverLB;
@property (nonatomic, retain) IBOutlet UILabel * dateLB;
@property (nonatomic, retain) IBOutlet UILabel * timeLB;

@property (nonatomic) BOOL isLoaded;


@property (nonatomic, retain) ScheduleEvent * event;
@property (nonatomic, retain) ScheduleListVC * scheduleListView;


- (void) reloadUser;

@end
