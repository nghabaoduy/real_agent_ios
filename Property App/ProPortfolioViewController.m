//
//  ProPortfolioViewController.m
//  Property App
//
//  Created by Brian on 12/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ProPortfolioViewController.h"


#import "DetailPropertyTabBarVC.h"
#import "DetailPropertyVC.h"
#import "GalleryPropertyVC.h"
#import "LocationPropertyVC.h"
#import "DetailPropertyNavVC.h"

#import "DetailComercialVC.h"

#import "DatabaseEngine.h"
#import "MyUIEngine.h"
#import "DataEngine.h"
#import "Client.h"
#import "Agent.h"
#import "User.h"

#import "AddPropertyViewController.h"
#import "ListPattern.h"
#import "PortfolioSection.h"



@interface ProPortfolioViewController ()

@end

@implementation ProPortfolioViewController

@synthesize tableView = _tableView;
@synthesize addBtn;
@synthesize contentArray, agentClientList, agentClientPropertyList;

@synthesize allPropertyList;

Agent* curAgent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
	
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [addBtn setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"HelveticaNeue-Bold" size:25.0], UITextAttributeFont,nil] forState:UIControlStateNormal];
	// Do any additional setup after loading the view.
}
- (void) initData
{
    [self actionRun];
    [[DatabaseEngine sharedInstance] GetMyListing:self];
}
-(void) actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    
    //refresh local data
    curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
    
    
    agentClientList = [[NSMutableArray alloc] init];
	agentClientPropertyList = [[NSMutableArray alloc] init];
	[self.agentClientList addObject:curAgent];
	for (Client* client in curAgent.myClientList) {
		[self.agentClientList addObject:client];
	}
    allPropertyList = [[NSMutableArray alloc] init];
    
	
	[self.agentClientPropertyList addObject:curAgent.propertyList];
	for (NSMutableArray* propList in curAgent.myClientPropertyList) {
		[self.agentClientPropertyList addObject:propList];
        for (Property *prop in propList) {
            if ([prop.isSubmited isEqualToString:@"YES"]) {
                //[propList removeObject:prop];
                [self.allPropertyList addObject:prop];
            }
        }
	}
    NSLog(@"allPropertyList %@", self.allPropertyList);

    [self.tableView reloadData];
    
}
-(void) actionFail:(NSString *)message
{
    [super actionFail:message];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];

    
}

- (void) LoadAllIcon
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return allPropertyList.count;
}



-(void) addClientProperty: (User *)user
{
    if(user!= NULL)
    {
        AddPropertyViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"addPropertyView"];
        view.adder = user;
        [self presentViewController:view animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Can't find user"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"indexPath = %i",indexPath.row);
    static NSString *CellIdentifier = @"listPattern";
    

    Property *curPro = [allPropertyList objectAtIndex:indexPath.row];
    NSLog(@"property line %i-%i = %@",indexPath.section,indexPath.row,curPro);
    
    
    ListPattern * cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell1 == nil)
    {
        NSLog(@"cell is nil");
        cell1 = [[ListPattern alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UIImage *background = [UIImage imageNamed:@"cell_odd"];
    
    
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cell1.backgroundView = cellBackgroundView;
    if (curPro.iconImage != nil) {
        cell1.displayer.image = curPro.iconImage;
    }
    if (curPro.propertyName.length > 0) {
         [cell1.projectName setText:curPro.projectName];
    }
    else
    {
         [cell1.projectName setText:curPro.address];
    }
    NSString *listingType = [curPro.listingType isEqualToString:@"Sales"]?@"Sales":@"Rent";
    [cell1.priceAndBedroom setText:[NSString stringWithFormat:@"%@ S$ %@",listingType, curPro.price]];
    [cell1.projectType setText:curPro.propertyType];
    [cell1.areaAndPsf setText:[NSString stringWithFormat:@"%@ sqft  - PFS S$ %@", curPro.area, curPro.PSF]];
    [cell1.location setText:[NSString stringWithFormat:@"Unit No:%@ - Adder:%@",curPro.unitNo, curPro.adder.fullName]];
    
    [cell1.contentView addSubview:cell1.displayer];
    [cell1.contentView addSubview:cell1.priceAndBedroom];
    [cell1.contentView addSubview:cell1.projectType];
    [cell1.contentView addSubview:cell1.areaAndPsf];
    [cell1.contentView addSubview:cell1.location];
    
    return cell1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Property *curPro = [allPropertyList objectAtIndex:indexPath.row];
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"DetailStoryboard" bundle:nil];
    
    
    
    DetailPropertyNavVC * navView =[newStoryboard instantiateViewControllerWithIdentifier:@"detailProperty"];
    
    DetailPropertyTabBarVC * tabBar = (DetailPropertyTabBarVC*) navView.topViewController;
    tabBar.isHide = YES;
    
    tabBar.isShare = _isSharing;
    tabBar.curGroup = _curGroup;
    tabBar.proportView = self;
    [tabBar initTabByProperty:curPro :newStoryboard ];
    
    [self presentViewController:navView animated:YES completion:nil];
    
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.000001f;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.000001f;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] ;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}


- (IBAction)AddProperty:(id)sender
{
    [self addClientProperty:[DataEngine sharedInstance].curUser];
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Property * curProp;

    curProp = [allPropertyList objectAtIndex:indexPath.row];
    
    [curProp.propertyObject deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            [allPropertyList removeObject:curProp];
            
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView endUpdates];
            for (NSMutableArray* propList in curAgent.myClientPropertyList) {
                if ([propList containsObject:curProp]) {
                    [propList removeObject:curProp];
                }
            }
        }
    }];
}




@end
