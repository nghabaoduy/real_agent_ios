//
//  PatternViewCell2.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 9/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PatternViewCell2 : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIImageView * patternImageView;

@end
