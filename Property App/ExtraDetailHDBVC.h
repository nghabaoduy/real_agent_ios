//
//  ExtraDetailHDBVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/5/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"


@interface ExtraDetailHDBVC : UITableViewController<UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    //Basic Information
    IBOutlet UITextField * address;
    IBOutlet UITextField * postalCode;
    IBOutlet UITextField * blockNo;
    IBOutlet UITextField * levelNo;
    IBOutlet UITextField * unitNo;
    IBOutlet UITextField * district;
    IBOutlet UITextField * area;
    IBOutlet UITextField * price;
    IBOutlet UITextField * adder;
    
    
    IBOutlet UITextField * level;
    IBOutlet UITextField * hdbType;
    IBOutlet UITextField * liftLevel;
    IBOutlet UITextField * mainDoorDirection;
    IBOutlet UITextField * roomPosition;
    IBOutlet UITextField * town;
    IBOutlet UITextField * valuation;
    IBOutlet UITextField * condition;
    
    UITextField * activeTextField;
    NSMutableArray * adderList;
    NSMutableArray * adderNameList;
    
    UIActionSheet * menu;
    NSString * selectedCategory;
    
    
    NSArray * liftLevelList;
    NSArray * levelList;
    NSArray * conditionList;
    NSArray * roomPostitionList;
    NSArray * mainDoorList;
    
    
    IBOutlet UITableViewCell * conditionCell;
    IBOutlet UITableViewCell * valuationCell;
    
}
@property (strong, nonatomic) Property * property;
-(void) addBackBtn;


@end
