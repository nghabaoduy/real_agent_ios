//
//  IndustrialType2VC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "IndustrialType2VC.h"
#import "ImagePickVC.h"
#import "MyUIEngine.h"
#import "MyCustomBack.h"
#import "DataEngine.h"
#import "Agent.h"
#import "Client.h"

@interface IndustrialType2VC ()

@end

@implementation IndustrialType2VC
@synthesize property = _property;




-(void) addBackBtn
{
    NSLog(@"addBackBtn runs");
    UIButton *Btn =[[UIButton alloc] initWithFrame:CGRectMake(0.0f,0.0f,68.0f,30.0f)];
    
    [Btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [Btn setBackgroundImage:[UIImage imageNamed:@"button_back2"] forState:UIControlStateNormal];
    [Btn setTitle:@"   Back" forState:UIControlStateNormal];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithCustomView:Btn];
    //[addButton setBackgroundImage:[UIImage imageNamed:@"button_back2"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [addButton setTitle:@"   Back"];
    [self.navigationItem setLeftBarButtonItem:addButton];
}

-(void) back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    
    
    levelList = [[NSArray alloc]initWithObjects:
                 @"[Select]",
                 @"Ground",
                 @"Low",
                 @"Mid",
                 @"High", nil];
    
    //districtList = [[DataEngine sharedInstance] getDistrictList];
    conditionList = [[NSArray alloc] initWithObjects:
                     @"[select]",
                     @"Bare",
                     @"Partial",
                     @"Full Fitted",
                     nil];
    
    gstList = [[NSArray alloc]initWithObjects:
               @"[Select]",
               @"Yes",
               @"No", nil];
    
    flattedList = [[NSArray alloc]initWithObjects:
                   @"[select]",
                   @"Yes",
                   @"No", nil];
    
    typeList = [[NSArray alloc]initWithObjects:
                @"[select]",
                @"B1",
                @"B2",
                @"Open Yard",
                @"Others", nil];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self initTextfieldPlaceHolder];
}

- (void) initTextfieldPlaceHolder
{
    gstTF.placeholder = @"GST: ";
    conditionTF.placeholder = @"Condition: ";
    electricalLoadTF.placeholder = @"Electrical Load: ";
    electricalLoadTF.text = @"Single";
    ampsTF.placeholder = @"Amps: ";
    floorLoadTF.placeholder = @"Floor Load: ";
    noOfCargoLiftTF.placeholder = @"No of Cargo Lift: ";
    flattedTF.placeholder = @"Flatted/Ramp up: ";
    ceilingHeightTF.placeholder = @"Ceiling Height: ";
    landSizeTF.placeholder = @"Land Size: ";
    noOfStoreyTF.placeholder = @"No of Storey: ";
    typeTF.placeholder = @"Type: ";
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
}

-(void)categoryDoneButtonPressed{
    //Add for wheel
    //categoryLable.text = selectedCategory;
    if (activeTextField == conditionTF)
    {
        [conditionTF setText:selectedCategory];
    } else if (activeTextField == gstTF)
    {
        [gstTF setText:selectedCategory];
    } else if (activeTextField == flattedTF)
    {
        [flattedTF setText:selectedCategory];
    } else if (activeTextField == typeTF)
    {
        [typeTF setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
    
}



-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    //Add Wheel
    // Handle the selection
    if (activeTextField == gstTF) {
        selectedCategory = [NSString stringWithFormat:@"%@",[gstList objectAtIndex:row]];
    } else if (activeTextField == conditionTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[conditionList objectAtIndex:row]];
    } else if (activeTextField == flattedTF)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[flattedList objectAtIndex:row]];
    } else if (activeTextField == typeTF)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[typeList objectAtIndex:row]];
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    //Add Wheel
    
    if (activeTextField == gstTF)
    {
        return [gstList count];
    } else if (activeTextField == conditionTF)
    {
        return [conditionList count];
    } else if (activeTextField == flattedTF)
    {
        return [flattedList count];
    } else if (activeTextField == typeTF)
    {
        return [typeList count];
    }
    return 0;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == gstTF)
    {
        return [gstList objectAtIndex:row];
    } else if (activeTextField == conditionTF)
    {
        return [conditionList objectAtIndex:row];
    } else if (activeTextField == flattedTF)
    {
        return [flattedList objectAtIndex:row];
    } else if (activeTextField == typeTF)
    {
        return [typeList objectAtIndex:row];
    }
    return 0;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}



-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //Add Wheel
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:gstTF] || [textField isEqual:conditionTF] ||  [textField isEqual:flattedTF] || [textField isEqual:typeTF])
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"extraDetailNext"])
    {
        _property.gst = gstTF.text;
        _property.condition = conditionTF.text;
        _property.electricalLoad = electricalLoadTF.text;
        _property.amps = ampsTF.text;
        _property.floorLoad = floorLoadTF.text;
        _property.noOfCargoLift = noOfCargoLiftTF.text;
        _property.flatted = flattedTF.text;
        _property.ceilingHeight = ceilingHeightTF.text;
        _property.landSize = landSizeTF.text;
        _property.noOfStorey = noOfStoreyTF.text;
        _property.type = typeTF.text;
        
        ImagePickVC * destination = segue.destinationViewController;
        if ([self.navigationItem.title isEqualToString:@"Step 1/3"]) {
            [destination.navigationItem setTitle:@"Step 2/3"];
        }
        destination.property = _property;
        
        
    }
}



@end
