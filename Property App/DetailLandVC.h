//
//  DetailHDBVC.h
//  Property App
//
//  Created by Brian on 7/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface DetailLandVC : UITableViewController <MFMailComposeViewControllerDelegate>
{
    
     NSString * type;
    //display
    IBOutlet UIImageView * icon;
    IBOutlet UILabel * propertyName;
    IBOutlet UILabel * propertyType;
    IBOutlet UILabel * adress;
    IBOutlet UITextView *addressTV;
    IBOutlet UILabel * postalCode;
    
    //Detail
    IBOutlet UITextField * price;
    //
    IBOutlet UITextField *landType;
    IBOutlet UITextField * district;
    IBOutlet UITextField * floorSize;
    IBOutlet UITextField * tenure;
    
    //Status
    IBOutlet UITextField * status;
    IBOutlet UITextField * rentedPrice;
    
    //Project Information
    IBOutlet UITextField * projectName;
    IBOutlet UITextField * psf;
    IBOutlet UITextField * topYear;
    IBOutlet UITextField * rentalYield;
    
    //land extra
    IBOutlet UITextField * noOfBedroom;
    IBOutlet UITextField * furnishing;
    IBOutlet UITextField * landSize;
    IBOutlet UITextField * mainDoorDirection;
    IBOutlet UITextField * mainGateFacing;
    IBOutlet UITextField * carPark;
    IBOutlet UITextField * garden;
    IBOutlet UITextField * swimmingPool;
    IBOutlet UITextField * basement;
    IBOutlet UITextField * roofTerrance;
    
    
    //Contact Agen
    IBOutlet UIImageView * agentIcon;
    IBOutlet UILabel * agentName;
    IBOutlet UILabel * agentMobile;
    IBOutlet UILabel * agentEmail;
    IBOutlet UITableViewCell * schedule;
    User * curUser;
    
    IBOutlet UITableViewCell * addToShare;
}

@property (nonatomic, retain) Property* property;
@property (nonatomic) BOOL isMyProperty;

@property (nonatomic) BOOL isSharing;
@property (nonatomic) BOOL allowUnitNo;
@property (nonatomic, retain) PFObject * curGroup;
@property (nonatomic, retain) ProPortfolioViewController * proportView;

- (void) hideSchedule;
- (void) showDialog : (NSString *) message;


@end
