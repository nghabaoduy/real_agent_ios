//
//  SearchPropertyVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "SearchPropertyVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "SalesListViewController.h"

@interface SearchPropertyVC ()

@end

@implementation SearchPropertyVC
@synthesize property = _property;
- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) LoadDefault
{
    [propertyType setText: _property.propertyType];
    [listingType setText:_property.listingType];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self LoadDefault];
    
    rentalYieldList = [[NSArray alloc] initWithObjects:
                       @"",
                       @"3% & Below",
                       @"3-5%,5-10%,",
                       @"10% & Above",
                       nil];
    
    districtList = [[DataEngine sharedInstance] getDistrictList];
    tenureList = [[NSArray alloc] initWithObjects:
                  @"",
                  @"FH",
                  @"999",
                  @"99",
                  @"60",
                  @"30",
                  @"<30yrs",
                  nil];
    PRTypeList =[[NSArray alloc] initWithObjects:
                 @"",
                 @"Condo",
                 @"EC",
                 @"Apartment",
                 @"Walk Up",
                 @"Cluster House",
                 @"Land Building",
                 @"Room Rental",
                 nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [self initTextfieldPlaceHolder];
}

- (void) initTextfieldPlaceHolder
{
    district.placeholder = @"District: ";
    bedroom.placeholder = @"Bedroom: ";
    area.placeholder = @"Floor Size: ";
    price.placeholder = @"Price: ";
    tenure.placeholder = @"Tenure: ";
    
    rentedPrice.placeholder = @"Rental Price: ";
    
    
    
    projectName.placeholder = @"Project Name: ";
    topYear.placeholder = @"Top Year: ";
    psf.placeholder = @"PSF: ";
    rentalYield.placeholder = @"Rental Yield: ";
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard {
    [activeTextField resignFirstResponder];
}



- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
}

-(void)categoryDoneButtonPressed{
    //categoryLable.text = selectedCategory;
    if (activeTextField == district) {
        [district setText:selectedCategory];
    } else if (activeTextField == tenure)
    {
        [tenure setText:selectedCategory];
    } else if (activeTextField == rentalYield)
    {
        [rentalYield setText:selectedCategory];
    } else if (activeTextField == PRType)
    {
        [PRType setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if (activeTextField == district) {
        selectedCategory = [NSString stringWithFormat:@"%@",[districtList objectAtIndex:row]];
    } else if (activeTextField == tenure)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[tenureList objectAtIndex:row]];
    } else if (activeTextField == rentalYield)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[rentalYieldList objectAtIndex:row]];
    } else if (activeTextField == PRType)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[PRTypeList objectAtIndex:row]];
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (activeTextField == district) {
        return [districtList count];
    } else if (activeTextField == tenure)
    {
        return [tenureList count];
    } else if (activeTextField == rentalYield)
    {
        return [rentalYieldList count];
    } else if (activeTextField == PRType)
    {
        return [PRTypeList count];
    }
    return [districtList count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == district) {
        return [districtList objectAtIndex:row];
    } else if (activeTextField == tenure)
    {
        return [tenureList objectAtIndex:row];
    } else if (activeTextField == rentalYield)
    {
        return [rentalYieldList objectAtIndex:row];
    } else if (activeTextField == PRType)
    {
        return [PRTypeList objectAtIndex:row];
    }
    
    return [NSString stringWithFormat:@"%d", row+1];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:district] || [textField isEqual:tenure] || [textField isEqual:rentalYield]|| [textField isEqual:PRType])
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (IBAction)search:(id)sender
{
    NSLog(@"can run1");
    _property.district = district.text;
    _property.bedroom = bedroom.text;
    _property.area = area.text;
    _property.price = price.text;
    _property.tenure = tenure.text;
    _property.rentedPrice = rentalYield.text;
    _property.projectName = projectName.text;
    _property.PSF = psf.text;
    _property.topYear = topYear.text;
    _property.rentalYeild = rentedPrice.text;
    _property.prType = PRType.text;
    NSLog(@"can run2");
    
    
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NSLog(@"can run3");
    SalesListViewController *pushview = [newStoryboard instantiateViewControllerWithIdentifier:@"salesListView"];
    NSLog(@"can run4");
	//pushview.navBar.topItem.title = @"Search Result";
    pushview.propertySearch = _property;
    NSLog(@"can run5");
	[self presentViewController:pushview animated:YES completion:nil];
    
    
    NSLog(@"can run");
    
}

- (void) SearchSuccessfully : (NSArray*) objects
{
    NSLog(@"start");
	NSMutableArray * propertyAray = [[NSMutableArray alloc] init];
	NSMutableArray * titles = [[NSMutableArray alloc] init];
	//NSLog(@"%i", [objects count]);
	for (PFObject *object in objects) {
		[propertyAray addObject:object];
		[titles addObject:[object objectForKey:@"propertyName"]];
	}
	[[DataEngine sharedInstance] SetPropertyDataArray:propertyAray];
	[[DataEngine sharedInstance] SetPropertyNameList:titles];
    
    NSLog(@"pop");
    
}

- (void) SearchFailed : (NSString*) errorString
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:errorString
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
	//NSLog(@"%@" , errorString);
}
@end
