//
//  CommercialTypeDetailVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/3/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyUITableViewController.h"
#import "Property.h"

@interface CommercialTypeDetailVC : MyUITableViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    IBOutlet UITextField * levelTF;
    IBOutlet UITextField * gstTF;
    IBOutlet UITextField * conditionTF;
    IBOutlet UITextField * electricalLoadTF;
    IBOutlet UITextField * ampsTF;
    IBOutlet UITextField * airConTF;
    IBOutlet UITextField * ownPantryTF;
    IBOutlet UITextField * gradeTF;
    IBOutlet UITextField * floorLoadTF;
    IBOutlet UITextField * serverRoomTF;
    
    IBOutlet UITextField * fAndBTypeTF;
    IBOutlet UITextField * greaseTrapTF;
    IBOutlet UITextField * exhaustTF;
    
    
    IBOutlet UITextField * builtUpTF;
    IBOutlet UITextField * noOfStoreyTF;
    
    IBOutlet UITextField * retailTypeTF;
    
    
    NSArray * levelList;
    NSArray * gstList;
    NSArray * conditionList;
    NSArray * airconList;
    NSArray * ownPantryList;
    NSArray * gradeList;
    NSArray * serverRoomList;
    NSArray * fAndBTypeList;
    NSArray * greaseTrapList;
    NSArray * exhaustList;
    NSArray * retailTypeList;
    NSArray * electricalLoadList;
    
    
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
}
@property (strong, nonatomic) Property * property;

@end
