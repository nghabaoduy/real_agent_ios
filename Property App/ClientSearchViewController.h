//
//  ClientSearchViewController.h
//  PhoneBookController
//
//  Created by Brian on 9/9/13.
//  Copyright (c) 2013 Brian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyClientViewController.h"
#import "MyUIViewController.h"

@interface ClientSearchViewController :MyUIViewController  <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>


@property (nonatomic, retain) IBOutlet UISearchBar  *itemSearchBar;
@property (nonatomic, retain) IBOutlet UITableView  *tableView;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;


@property (nonatomic, retain) NSMutableArray *displayArray;
@property (nonatomic, retain) NSMutableArray *addBookArray;

@property (nonatomic, retain) NSMutableArray *selectedRowsArray;
@property (nonatomic, retain) NSMutableArray *contentArray;

@property (nonatomic, retain) NSMutableArray *processingClients;

@property (nonatomic, retain) MyClientViewController *clientView;


@end
