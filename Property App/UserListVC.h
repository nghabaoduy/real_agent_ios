//
//  UserListVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/17/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUITableViewController.h"
#import <Parse/Parse.h>
#import "User.h"
#import "MessageListVC.h"
#import <MessageUI/MessageUI.h>

@interface UserListVC : MyUITableViewController <MFMessageComposeViewControllerDelegate>

@property (nonatomic, retain) NSArray * userList;
@property (nonatomic, retain) User * curUser;
@property (nonatomic, retain) MessageListVC * messageListView;


- (void) selectionFinish;

@end
