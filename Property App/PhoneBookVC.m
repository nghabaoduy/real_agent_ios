//
//  PhoneBookVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <AddressBook/AddressBook.h>
#import "PhoneBookVC.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "SwitchCell.h"

@interface PhoneBookVC ()

@end

@implementation PhoneBookVC
@synthesize agentList = _agentList;
@synthesize unneededList = _unneededList;
@synthesize displayArray = _displayArray;
@synthesize selectedRowsArray;

- (void)actionRun
{
    [super actionRun];
    NSMutableArray * newUnneed = [[NSMutableArray alloc]init];
    
    for (PFUser * user in _unneededList) {
        [newUnneed addObject: user.objectId];
        NSLog(@"id : %@", user.objectId);
    }
    
    _unneededList = newUnneed;
    
    [[DatabaseEngine sharedInstance] loadAllAgent:self :_unneededList];
    
    
    
}

-(BOOL) InPhonebook :(NSString *)inputPhoneNo
{
    for (NSString *phoneNo in self.phonebookPhoneList) {
        NSRange titleResultsRange =
        [phoneNo rangeOfString:inputPhoneNo options:NSCaseInsensitiveSearch];
        if (titleResultsRange.length > 0) {
            return YES;
        }
    }
    return NO;
}

- (void)actionSuceed:(NSString *)message
{
    NSLog(@"run sucessful");
    [super actionSuceed:message];
    
    self.displayArray = [[NSMutableArray alloc] init];
    
    for (PFUser *user in _agentList) {
        User *curUser = [[User alloc] initWithUser:user];
        NSLog(@"curUser.phone = %@",curUser.phone);
        if ([self InPhonebook:curUser.phone]) {
            [self.displayArray addObject:user];
        
        }
    }
    
    self.displayArray = [NSMutableArray arrayWithArray:[self.displayArray sortedArrayUsingComparator:^NSComparisonResult(PFUser *a, PFUser*  b)
                {
                    NSString *fullNameA = [[NSString stringWithFormat:@"%@ %@",[a objectForKey:@"firstName"],[a objectForKey:@"lastName"]] lowercaseString];
                    NSString *fullNameB = [[NSString stringWithFormat:@"%@ %@",[b objectForKey:@"firstName"],[b objectForKey:@"lastName"]] lowercaseString];
                    
                    return ([fullNameA compare:fullNameB]);
                    
                }]];
    /*
    userList = [userList sortedArrayUsingComparator:^NSComparisonResult(User *a, User*  b)
                {
                    //Default compare, to protect against cast
                    
                    return ([a.fullName compare:b.fullName]);
                    
                }];*/
    
    [tv reloadData];
}

- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"number = %i", _unneededList.count);
    selectedRowsArray = [[NSMutableArray alloc] init];
    [self loadAddressBook];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.displayArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"get cell at index %i",indexPath.row);
    static NSString *CellIdentifier = @"phoneCell";
    SwitchCell *sCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (sCell == nil) {
        sCell = [[SwitchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    PFUser * user = [self.displayArray objectAtIndex:indexPath.row];
    User *curUser = [[User alloc] initWithUser:user];
    
    
    
    
    sCell.selectSwitch.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleChecking:)];
    [sCell.selectSwitch addGestureRecognizer:tap];
    
    CGRect containerFrame = sCell.selectSwitch.frame;
    containerFrame.origin = CGPointMake(0, 0);
    UIView *containerView = [[UIView alloc] initWithFrame:containerFrame];
    containerView.backgroundColor = [UIColor clearColor];
    [sCell.selectSwitch addSubview:containerView];
    
    
    [sCell.mainLabel setText: curUser.fullName];
    [sCell.selectSwitch setOn:[selectedRowsArray containsObject:[self.displayArray objectAtIndex:indexPath.row]] animated:NO];
    
    return sCell;
}

- (void) handleChecking:(UITapGestureRecognizer *)tapRecognizer {
    
    CGPoint tapLocation = [tapRecognizer locationInView:tv];
    NSIndexPath *tappedIndexPath = [tv indexPathForRowAtPoint:tapLocation];
    
    if ([selectedRowsArray containsObject:[self.displayArray objectAtIndex:tappedIndexPath.row]]) {
        [selectedRowsArray removeObject:[self.displayArray objectAtIndex:tappedIndexPath.row]];
    }
    else {
        [selectedRowsArray addObject:[self.displayArray objectAtIndex:tappedIndexPath.row]];
    }
    NSLog(@"tab row = %@",[self.displayArray objectAtIndex:tappedIndexPath.row]);
    NSLog(@"selectedRowsArray = %@",selectedRowsArray);
    SwitchCell *sCell = (SwitchCell*)[tv cellForRowAtIndexPath:tappedIndexPath];
    [sCell.selectSwitch setOn:!sCell.selectSwitch.on animated:YES];
}


- (IBAction)AddAllToGroup:(id)sender
{
    if (self.selectedRowsArray.count > 0) {
        [self startLoading];
        [[DatabaseEngine sharedInstance] addMembers:self :selectedRowsArray ];
    }
    
    NSLog(@"AddAllToGroup runs");
    
}

- (void)addFinished
{
    [self finishLoading];
    [self.navigationController popViewControllerAnimated:YES];
    [_groupMemberVC actionRun];
}

- (IBAction) SelectAll:(id)sender
{
    /*
    UIBarButtonItem *selectBtn = (UIBarButtonItem *)sender;
    if(![selectBtn.title isEqualToString:@" Deselect All              "])
    {
        for(User *user in self.displayArray)
        {
            if (![selectedRowsArray containsObject:user]) {
                [selectedRowsArray addObject:user];
            }
            [selectBtn setTitle:@" Deselect All              "];
        }
    }
    else
    {
        selectedRowsArray = [[NSMutableArray alloc] init];
        [selectBtn setTitle:@" Select All                  "];
    }
    
    [tv reloadData];*/
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)input
{
    
}

-(void) searchBarSearchButtonClicked:(UISearchBar *) searchBar{
    
    NSString *input = itemSearchBar.text;
    NSLog(@"input = %@",input);
    if([input length]==0)
    {
        self.displayArray = [_agentList copy];
    }
    else
    {
        NSLog(@"search text change");
        self.displayArray = [[NSMutableArray alloc] init];
        for (User *user in _agentList) {
            NSString *fullName = [NSString stringWithFormat:@"%@",user.fullName];
            NSLog(@"%@ - %@",fullName,input);
            NSRange range = [fullName rangeOfString:input options:NSCaseInsensitiveSearch];
            NSLog(@"range = %i",range.length);
            if(range.length!=0){
                [self.displayArray addObject:user];
            }
            NSLog(@"displayArray = %@",self.displayArray);
        }
        
    }
    [tv reloadData];
    // NSLog(@"displayArray = %@",self.displayArray);
    NSLog(@"selectedRowsArray = %@",self.selectedRowsArray);
    
    [searchBar resignFirstResponder];
}

- (void)getPersonOutOfAddressBook
{
    self.phonebookPhoneList = [[NSMutableArray alloc] init] ;
    CFErrorRef error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    if (addressBook != nil)
    {
        NSLog(@"AddressBook loads Succesful.");
        
        NSArray *allContacts = (__bridge_transfer NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
        NSLog(@"contact count = %i",allContacts.count);
        NSUInteger i = 0;
        for (i = 0; i < [allContacts count]; i++)
        {
            ABRecordRef contactPerson = (__bridge ABRecordRef)allContacts[i];
            
            
            //phone no
            NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
            ABMultiValueRef multiPhones = ABRecordCopyValue(contactPerson,kABPersonPhoneProperty);
            for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);++i) {
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = (__bridge_transfer NSString *) phoneNumberRef;
                    
                [phoneNumbers addObject:phoneNumber];
            }
            if ([phoneNumbers count]>0) {
                NSString* phoneNumber = [[DataEngine sharedInstance] cleanPhoneNo:[phoneNumbers objectAtIndex:0]];
                [self.phonebookPhoneList addObject:phoneNumber];
                    
            }
            //NSLog(@"phonebookPhoneList = %@",self.phonebookPhoneList);
                
            
        }
       
        //8
        CFRelease(addressBook);
        //start get agents from database
        [self actionRun];
    }
    else
    {
        //9
        NSLog(@"Error reading Address Book");
    }
}
-(void)loadAddressBook
{
    // Request authorization to Address Book
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            // First time access has been granted, add the contact
            [self getPersonOutOfAddressBook];
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        // The user has previously given access, add the contact
        [self getPersonOutOfAddressBook];
    }
    else {
        // The user has previously denied access
        // Send an alert telling user to change privacy setting in settings app
    }
    
}



@end
