//
//  MyListingViewController.h
//  Property App
//
//  Created by Brian on 9/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyListingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    
}

@property (nonatomic, retain) IBOutlet UINavigationBar * navBar;
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) NSMutableArray * data;
@property (nonatomic, retain) NSMutableArray * Titles;

@end


