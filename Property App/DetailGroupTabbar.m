//
//  DetailGroupTabbar.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailGroupTabbar.h"
#import "DetailGroupMemberVC.h"
#import "DetailGroupSettingVC.h"
#import "DetailGroupListingVC.h"
#import "DataEngine.h"
@interface DetailGroupTabbar ()

@end

@implementation DetailGroupTabbar
@synthesize curGroup = _curGroup;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) initTabbarWithGroupObj:(PFObject*) inputGroupObj :(UIViewController *)curView :(UINavigationController *)destination;
{
    NSLog(@"initTabbarWithGroupObj = %@",inputGroupObj);
    _curGroup = inputGroupObj;
    UIViewController *currentView = curView;
    
    self.navigationItem.title = [_curGroup objectForKey:@"title"];
    [self addBackBtn];
    
    DetailGroupMemberVC * tab01 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab01"];
    tab01.curGroup = _curGroup;
    DetailGroupSettingVC * tab02 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab02"];
    tab02.curGroup = _curGroup;
    DetailGroupListingVC * tab03 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab03"];
    tab03.curGroup = _curGroup;
    PFUser * founder = [tab01.curGroup objectForKey:@"creator"];
    [founder fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            if ([[[DataEngine sharedInstance] GetCurUser].userObject.objectId isEqualToString:founder.objectId])
            {
                self.viewControllers = [NSArray arrayWithObjects:tab01, tab02, tab03, nil];
            }
            else
            {
                self.viewControllers = [NSArray arrayWithObjects:tab01,  tab03, nil];
            }
            [currentView presentViewController:destination animated:NO completion:Nil];
            [self setSelectedIndex:self.viewControllers.count-1];
        }
    }];
    
}

-(void) addBackBtn
{
    NSLog(@"addBackBtn runs");
    UIButton *Btn =[[UIButton alloc] initWithFrame:CGRectMake(0.0f,0.0f,68.0f,30.0f)];
    
    [Btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [Btn setBackgroundImage:[UIImage imageNamed:@"button_back2"] forState:UIControlStateNormal];
    [Btn setTitle:@"   Back" forState:UIControlStateNormal];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithCustomView:Btn];
    //[addButton setBackgroundImage:[UIImage imageNamed:@"button_back2"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [addButton setTitle:@"   Back"];
    [self.navigationItem setLeftBarButtonItem:addButton];
}


-(void) back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
