//
//  MyGroupVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//
#import <Parse/Parse.h>
#import "MyGroupVC.h"
#import "GroupChatTabBar.h"
#import "DatabaseEngine.h"
#import "ChatRoomVC.h"
#import "GroupInfoVC.h"
#import "DataEngine.h"
#import "ShareListingVC.h"

@interface MyGroupVC ()

@end

@implementation MyGroupVC
@synthesize ChatList;

- (void)actionRun
{
    [super actionRun];
    
    User * user = [[DataEngine sharedInstance] GetCurUser];
    [[DatabaseEngine sharedInstance] LoadGroupInfo: user.userObject:self];
}

- (void)actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    [self.tableView reloadData];
}


- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
}



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //ChatList = [NSArray arrayWithObjects:@"Chat1", @"chat2", nil];
    ChatList = [[NSMutableArray alloc] init];
    [self actionRun];
}


- (void) viewWillAppear:(BOOL)animated
{
    [self actionRun];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [ChatList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    
    // Configure the cell...
    
    PFObject * groupInfo = [ChatList objectAtIndex:indexPath.row];
    NSLog(@"groupInfo.objectId = %@", groupInfo.objectId);
    NSString * title = [groupInfo objectForKey:@"groupTitle"];
    NSLog(@"title = %@", title);
    cell.textLabel.text = title;
    return cell;
}


- (void)Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"chat"])
    {
        PFObject * groupInfo = [ChatList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
        
        
        
        
        
        GroupChatTabBar * destination = segue.destinationViewController;
        
        
        destination.navigationItem.title = [groupInfo objectForKey:@"groupTitle"];
        NSLog(@"%@ title", [groupInfo objectForKey:@"groupTitle"]);
        
        ChatRoomVC * tab01 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab01"];
        tab01.chatRoomInfo = [ChatList objectAtIndex:self.tableView.indexPathForSelectedRow.row];
    
        //sdadas
        ShareListingVC * tab02 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab02"];
        GroupInfoVC * tab03 = [self.storyboard instantiateViewControllerWithIdentifier:@"tab03"];
        tab03.groupInfo = groupInfo;

        
        destination.viewControllers = [NSArray arrayWithObjects:tab01, tab02, tab03, nil];
    }
}

@end
