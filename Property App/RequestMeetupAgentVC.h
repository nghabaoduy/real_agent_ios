//
//  RequestMeetupAgentVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/13/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
#import "ScheduleEvent.h"
#import "ScheduleListVC.h"

@interface RequestMeetupAgentVC : UITableViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    IBOutlet UIImageView * thumbnailTV;
    IBOutlet UILabel * projectyNameLB;
    IBOutlet UILabel * projectTypeLB;
    IBOutlet UILabel * addressLB;
    IBOutlet UITextView *addressTV;
    IBOutlet UILabel *requesterLB;
    
    
    IBOutlet UITextField * proposedMeetupDateTF;
    IBOutlet UITextField * proposedMeetupTimeTF;
    IBOutlet UITextField * meetupVenueTF;
    
    
    
    IBOutlet UIButton * acceptBtn;  
    
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
    
    NSArray * meetupVenueList;
    
    UIDatePicker * datePick;
    UIDatePicker * timePick;
    NSDate * curDate;
    
    User * curUser;
    BOOL isReschedule;
    
    IBOutlet UITableViewCell * acceptCell;
    IBOutlet UITableViewCell * rescheduleCell;
}

@property (nonatomic) BOOL isEvent;
@property (nonatomic, retain) ScheduleEvent * event;
@property (nonatomic, retain) ScheduleListVC * scheduleList;

@end
