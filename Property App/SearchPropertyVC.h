//
//  SearchPropertyVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"

@interface SearchPropertyVC : UITableViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    //default
    IBOutlet UITextField * propertyType;
    IBOutlet UITextField * listingType;
    
    //basic information
    IBOutlet UITextField * district;
    IBOutlet UITextField * bedroom;
    IBOutlet UITextField * area;
    IBOutlet UITextField * price;
    IBOutlet UITextField * tenure;
    
    //status
    IBOutlet UITextField * rentedPrice;
    
    //Project information
    IBOutlet UITextField * projectName;
    IBOutlet UITextField * psf;
    IBOutlet UITextField * topYear;
    IBOutlet UITextField * rentalYield;
    
    IBOutlet UITextField * PRType;
    
    
    //Others
    
    NSArray * rentalYieldList;
    NSArray * districtList;
    NSArray * PRTypeList;
    NSArray * tenureList;
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
    
}

- (void) SearchSuccessfully : (NSArray*) objects;
- (void) SearchFailed : (NSString*) errorString;

@property (nonatomic, strong) Property * property;

@end
