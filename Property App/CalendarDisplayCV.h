//
//  CalendarDisplayCV.h
//  Property App
//
//  Created by Brian on 17/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUIViewController.h"
#import "VRGCalendarView.h"
#import "User.h"
#import "ScheduleEvent.h"

@interface CalendarDisplayCV : MyUIViewController<VRGCalendarViewDelegate,UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView *tv;
    User * curUser;
    
    NSMutableArray *acceptRequestList;
    NSMutableArray *dailyAcceptList;
    NSDate *selectedDate;
    VRGCalendarView *calendar;
    float calendarTargetHeight;
    
}

@property (nonatomic, retain) NSArray * myRequestList;
@property (nonatomic, retain) NSArray * otherRequestList;
@property (nonatomic, retain) ScheduleEvent* selectedEvent;
@end
