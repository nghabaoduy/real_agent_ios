//
//  SearchListVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 25/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchListVC : UITableViewController

@property (nonatomic, retain) NSMutableArray * data;
@property (nonatomic, retain) NSMutableArray * Titles;

@end
