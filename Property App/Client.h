//
//  Client.h
//  Property App
//
//  Created by Jonathan Le on 12/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "User.h"
#import "Agent.h"
@interface Client : User

@property (nonatomic, retain) NSMutableArray* myAgentList;


- (PFUser*) getUserObject;
- (NSMutableArray*) GetAgentList;

- (void) GetMyAgentList;
- (void) GetAgentSuccessful : (NSMutableArray*) agentList;
- (void) GetAgentFailed : (NSString*) errorString;
@end
