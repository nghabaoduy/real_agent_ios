//
//  ExtraDetailIndustryVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"

@interface ExtraDetailIndustryVC : UITableViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    //Basic Information
    IBOutlet UITextField * address;
    IBOutlet UITextField * postalCode;
    IBOutlet UITextField * blockNo;
    IBOutlet UITextField * levelNo;
    IBOutlet UITextField * unitNo;
    IBOutlet UITextField * district;
    IBOutlet UITextField * industryType;
    IBOutlet UITextField * area;
    IBOutlet UITextField * price;
    IBOutlet UITextField * tenure;
    IBOutlet UITextField * adder;
    
    //Status
    IBOutlet UITextField * status;
    IBOutlet UITextField * rentedPrice;
    IBOutlet UITextField * tenantedPrice;
    
    
    //Project Information
    IBOutlet UITextField * projectName;
    IBOutlet UITextField * PSF;
    IBOutlet UITextField * topYear;
    IBOutlet UITextField * rentalYield;
    
    
    
    NSArray * rentalYieldList;
    NSArray * districtList;
    NSArray * tenureList;
    NSArray * statusList;
    NSArray * industryTypeList;
    NSMutableArray * adderList;
    NSMutableArray * adderNameList;
    
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
}
@property (strong, nonatomic) Property * property;
-(void) addBackBtn;

@end
