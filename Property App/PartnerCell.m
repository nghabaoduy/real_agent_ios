//
//  PartnerCell.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/30/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "PartnerCell.h"

@implementation PartnerCell
@synthesize curPartner, partnerView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)call:(id)sender
{
    partnerView.callActive = curPartner.contact;
    [partnerView call];
}

- (IBAction)mail:(id)sender
{
    partnerView.emailActive = curPartner.email;
    [partnerView mail];
    
}

-(void) switchBankLogo :(NSString *)bankName
{
    if ([bankName isEqualToString:@"Hong Leong"]) {
        [self.bankLogo setImage:[UIImage imageNamed:@"bank_hongleong.png"]];
    }
    else if ([bankName isEqualToString:@"HSBC"]) {
        [self.bankLogo setImage:[UIImage imageNamed:@"bank_hsbc.png"]];
    }
    else if ([bankName isEqualToString:@"OCBC"]) {
        [self.bankLogo setImage:[UIImage imageNamed:@"bank_ocbc.png"]];
    }
    else if ([bankName isEqualToString:@"Standard Chartered"]) {
        [self.bankLogo setImage:[UIImage imageNamed:@"bank_sc.png"]];
    }
    else if ([bankName isEqualToString:@"UOB"]) {
        [self.bankLogo setImage:[UIImage imageNamed:@"bank_uob.png"]];
    }
}

@end
