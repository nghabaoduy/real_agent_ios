//
//  MyGroupVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUITableViewController.h"

@interface MyGroupVC : MyUITableViewController
{

}

@property (nonatomic, retain) NSMutableArray * ChatList;

- (IBAction)Back:(id)sender;

@end
