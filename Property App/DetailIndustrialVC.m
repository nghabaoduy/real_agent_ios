//
//  DetailHDBVC.m
//  Property App
//
//  Created by Brian on 7/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailIndustrialVC.h"
#import "MyUIEngine.h"
#import "RequestMeetupClientVC.h"
#import "RequestMeetupAgentVC.h"
#import "DataEngine.h"
#import "DatabaseEngine.h"

@interface DetailIndustrialVC ()

@end

@implementation DetailIndustrialVC

@synthesize property = _p;
-(void)hideSchedule
{
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId])
    {
        NSLog(@"hide");
        schedule.hidden = YES;
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    [self initTextfieldPlaceHolder];
    [self LoadInformation];
    type = @"Client";
    
    curUser = [[DataEngine sharedInstance]GetCurUser];
    [self hideSchedule];
    if (!_isSharing) {
        addToShare.hidden = YES;
    }
}




- (void)LoadInformation
{
    [_p printInfo];
    [_p removeNullData];
    [_p printInfo];
    [propertyName setText:_p.propertyName];
    [propertyType setText:_p.propertyType];
    if (_p.unitNo!=nil && ![_p.unitNo isEqualToString:@""]) {
        if (self.allowUnitNo || [_p.agent.username isEqualToString:[DataEngine sharedInstance].curUser.username]) {
            addressTV.text= [NSString stringWithFormat:@"#%@-%@ %@ %@",_p.levelNumber,_p.unitNo,_p.address,_p.postCode];
        }
    }
    else
        
    {
        addressTV.text= [NSString stringWithFormat:@"%@ %@",_p.address,_p.postCode];
    }
    NSLog(@"%@",_p.district);
    [price setText:_p.price];
    
    [industrialType setText:_p.industryType];
    //detail
    [price setText:_p.price];
    [district setText:_p.district];
    [tenure setText:_p.tenure];
    [floorSize setText:_p.area];
    
    //Status
    [status setText:_p.status];
    [rentedPrice setText:_p.rentedPrice];
    
    //Project Information
    [projectName setText:_p.projectName];
    [psf setText:_p.PSF];
    [topYear setText:_p.topYear];
    [rentalYield setText:_p.rentalYeild];
    
    //common
    [gst setText:_p.gst];
    [condition setText:_p.condition];
    [electricalLoad setText:_p.electricalLoad];
    [amps setText:_p.amps];
    [floorLoad setText:_p.floorLoad];
    [noOfCargoLift setText:_p.noOfCargoLift];
    [flattedRampUp setText:_p.flatted];
    [ceilingHeight setText:_p.ceilingHeight];
    
    //type 1
    [level setText:_p.levelNumber];
    [vehicalAccess setText:_p.vehicleAccess];
    
    //type 2
    [landSize setText:_p.landSize];
    [noOfStorey setText:_p.noOfStorey];
    [landType setText:_p.type];
    
    Agent* agent = _p.agent;
    [agentName setText:[NSString stringWithFormat:@"%@ %@",agent.firstName, agent.lastName]];
    [agentMobile setText:[NSString stringWithFormat:@"Tel: %@",agent.agentPhone]];
    [agentEmail setText:[NSString stringWithFormat:@"%@",agent.email]];
    
    
    //Detail
    price.enabled = NO;
    district.enabled = NO;
    tenure.enabled = NO;
    floorSize.enabled = NO;
    
    //Status
    status.enabled = NO;
    rentedPrice.enabled = NO;
    
    //Project Information
    projectName.enabled = NO;
    psf.enabled = NO;
    topYear.enabled = NO;
    rentalYield.enabled = NO;
    
    
    industrialType.enabled = NO;
    //industrical extra
    gst.enabled = NO;
    condition.enabled = NO;
    electricalLoad.enabled = NO;
    floorLoad.enabled = NO;
    noOfCargoLift.enabled = NO;
    flattedRampUp.enabled = NO;
    ceilingHeight.enabled = NO;
    
    level.enabled = NO;
    vehicalAccess.enabled = NO;
    
    landSize.enabled = NO;
    noOfStorey.enabled = NO;
    landType.enabled = NO;
    
}

- (void) initTextfieldPlaceHolder
{
    
    //self.blkNoTextF.placeholder = @"Block: ";
    //self.unitTextF.placeholder = @"Unit: ";
    district.placeholder = @"District: ";
    //noOfBedroom.placeholder = @"Bedroom: ";
    floorSize.placeholder = @"Floor Size: ";
    price.placeholder = @"Price: ";
    tenure.placeholder = @"Tenure: ";
    status.placeholder = @"Status: ";
    rentedPrice.placeholder = @"Rental Price: ";
    
    
    
    projectName.placeholder = @"Project Name: ";
    topYear.placeholder = @"Top Year: ";
    psf.placeholder = @"PSF: ";
    rentalYield.placeholder = @"Rental Yield: ";
    
    //hdb
    industrialType.placeholder = @"Industrial Type : ";
    //industrical extra
    gst.placeholder = @"GST : ";
    condition.placeholder = @"Condition : ";
    electricalLoad.placeholder = @"Electrical Load : ";
    electricalLoad.text = @"Single";
    amps.placeholder = @"Amps: ";
    floorLoad.placeholder = @"Floor Load : ";
    noOfCargoLift.placeholder = @"No of Cargo Lift : ";
    flattedRampUp.placeholder = @"Flatted/Ramp Up : ";
    ceilingHeight.placeholder = @"Ceiling Height : ";
    
    level.placeholder = @"Level : ";
    vehicalAccess.placeholder = @"Vehical Access : ";
    
    landSize.placeholder = @"Land Size : ";
    noOfStorey.placeholder = @"No of Storey : ";
    landType.placeholder = @"Land Type : ";
    
}
- (IBAction)callAgent:(id)sender {
    Agent* agent = _p.agent;
    NSLog(@"agentPhone = %@",agent.phone);
    
    NSString * call = [NSString stringWithFormat:@"tel:%@", agent.phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}
- (IBAction)emailAgent:(id)sender {
    Agent* agent = _p.agent;
    NSString * email = agent.email;
    NSArray * recipients = [NSArray arrayWithObject:email];
    
    NSMutableString *body = [NSMutableString string];
    
    [body appendString:@"\n"];
    [body appendString:@"\n"];
    [body appendString:@"<a>Send from property : </a>\n"];
    [body appendString:[NSString stringWithFormat:@"<a href=\"realagent://property-%@\">Track Property</a>\n",self.property.propertyObject.objectId]];
    if ([MFMailComposeViewController canSendMail]) {
        // Show the composer
        NSLog(@"agentEmail = %@",agent.email);
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:recipients];
        [controller setMessageBody:body isHTML:YES];
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Handle the error
    }
    
    
}
#define IDUSTRIAL_TYPE1 5
#define IDUSTRIAL_TYPE2 6

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId] && indexPath.section == tableView.numberOfSections-1)
    {
        return 0.1f;
    }
    if (indexPath.section == 0) {
        return 140;
    }
    if (indexPath.section == tableView.numberOfSections-2) {
        if (indexPath.row==0) {
            
            return 253;
        }
        else
        {
            if (!_isSharing) {
                return 0;
            }
            else
            {
                return 66;
            }
        }
    }
    if (indexPath.section == tableView.numberOfSections-1) {
        return 43;
    }
    if (indexPath.section > 3) {
        if ([_p.industryType isEqualToString:@"B1"] && indexPath.section!=IDUSTRIAL_TYPE1) {
            return 0;
            
        }
        if ([_p.industryType isEqualToString:@"B2"] && indexPath.section!=IDUSTRIAL_TYPE1) {
            return 0;
            
        }
        
        if ([_p.industryType isEqualToString:@"Central Kitch"] && indexPath.section!=IDUSTRIAL_TYPE1) {
            return 0;
            
        }
        
        if ([_p.industryType isEqualToString:@"Land"] && indexPath.section!=IDUSTRIAL_TYPE2) {
            return 0;
            
        }
        if ([_p.industryType isEqualToString:@"Building"] && indexPath.section!=IDUSTRIAL_TYPE2) {
            return 0;
            
        }
        
    }
    
    
    
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId] && section == tableView.numberOfSections-1)
    {
        return 0.1f;
    }
    if (section == 0) {
        return 35.0f;
    }
    if (section == tableView.numberOfSections-1) {
        return 35.0f;
    }
    if (section == tableView.numberOfSections-2) {
        return 35.0f;
    }
    if (section > 4) {
        
        return 0.1f;
        
    }
    return 35.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([curUser.userObject.objectId isEqualToString: _p.agent.userObject.objectId] && section == tableView.numberOfSections-1)
    {
        return 0.1f;
    }
    if (section == 0) {
        return 10.0f;
    }
    if (section == tableView.numberOfSections-1) {
        return 10.0f;
    }
    if (section == tableView.numberOfSections-2) {
        return 10.0f;
    }
    if (section > 4) {
        return 0.1f;
    }
    return 10.0f;
}



- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)schedule:(id)sender
{
    [self performSegueWithIdentifier:@"goMeetupClient" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goMeetupClient"])
    {
        RequestMeetupClientVC * destination = segue.destinationViewController;
        destination.property = _p;
    }
}

- (IBAction)AddToShare:(id)sender
{
    [sender setEnabled:NO];
    [[DatabaseEngine sharedInstance] valiadteIndustry:self];
}

- (void)showDialog:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
    [alert show];
}

@end
