//
//  GroupInfoVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUIViewController.h"
#import <Parse/Parse.h>

@interface GroupInfoVC : MyUIViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView * tableView;
    PFObject * admin;
    IBOutlet UIButton * add;
    BOOL editEnable;
    IBOutlet UIButton * edit;
}


@property (nonatomic, retain) PFObject * groupInfo;
@property (nonatomic, retain) NSMutableArray * userList;
@property (nonatomic, retain) NSMutableArray * groupParticipantList;
@property (nonatomic, retain) NSMutableArray * removeBtnList;


- (void) ReloadData;

@end
