//
//  LocationPickVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 21/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "LocationPickVC.h"
#import "ExtraDetailVC.h"
#import "MyUIEngine.h"
#import "DataEngine.h"
#import "ExtraDetailCommercialVC.h"
#import "ExtraDetailHDBVC.h"
#import "ExtraDetailIndustryVC.h"
#import "ExtraDetailLandVC.h"


@interface LocationPickVC ()

@end

@implementation LocationPickVC

@synthesize property = _property;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    //[self GetLocationByAddress];
    
    
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
-(void) GetLocationByAddress {
	CLGeocoder *geocoder = [[CLGeocoder alloc] init];
	NSString* postCode = _property.postCode;
	
	NSLog(@"%@", postCode);
	[geocoder geocodeAddressString:postCode
				 completionHandler:^(NSArray *placemarks, NSError *error) {
					 
					 if (error) {
						 NSLog(@"Geocode failed with error: %@", error);
						 return;
					 }
					 
					 if(placemarks && placemarks.count > 0)
					 {
						 CLPlacemark* place = [placemarks objectAtIndex:0];
						 
						 CLLocation *location = place.location;
						 CLLocationCoordinate2D coords = location.coordinate;
						 
						 NSLog(@"Latitude = %f, Longitude = %f",coords.latitude, coords.longitude);
						 NSLog(@"%@", place.addressDictionary);
						 [self GetAddressByLatitude:coords.latitude :coords.longitude];
						 
						 
						 
					 }
				 }
	 ];
}
-(void) GetAddressByLatitude: (double) lat : (double) longt {
	CLGeocoder *geocoder = [[CLGeocoder alloc] init];
	
	CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:lat
														longitude:longt];
	
	[geocoder reverseGeocodeLocation:newLocation
				   completionHandler:^(NSArray *placemarks, NSError *error) {
					   
					   if (error) {
						   NSLog(@"Geocode failed with error: %@", error);
						   return;
					   }
					   
					   if (placemarks && placemarks.count > 0)
					   {
						   CLPlacemark *placemark = placemarks[0];
						   
						   NSDictionary *addressDictionary =
                           placemark.addressDictionary;
						   
						   //NSLog(@"location: %@", [placemark location]);
						   NSLog(@"addressdict: %@", [placemark addressDictionary]);
                           _property.address = [addressDictionary objectForKey:@"Street"];
                           _property.blkNo = [[DataEngine sharedInstance] getBlockNumber:_property.postCode];
                           _property.district = [[DataEngine sharedInstance] getDistrict:_property.postCode];
						   
						   //[placemark ]
						   
						   // NSLog(@"%@ %@ %@ %@", address,city, state, zip);
						   CLLocationCoordinate2D coord = {.latitude =  placemark.location.coordinate.latitude, .longitude = placemark.location.coordinate.longitude};
						   MKCoordinateSpan span = {.latitudeDelta = 0.01, .longitudeDelta =  0.01};
						   MKCoordinateRegion region = {coord, span};
						   
						   MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
						   point.coordinate = coord;
						   point.title = placemark.name;
						   point.subtitle = [NSString stringWithFormat:@"%@ %@", placemark.country, placemark.postalCode];
						   [self.mapView addAnnotation:point];
						   
						   NSLog(@"View loading");
						   [self.mapView setRegion:region];
						   NSLog(@"view loaded");
						   [self.mapView selectAnnotation:point animated:YES];
					   }
					   
				   }];
}*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"pickLocationNext"])
    {
        ExtraDetailVC * destination = segue.destinationViewController;
        destination.property = _property;
    }
    else if ([[segue identifier] isEqualToString:@"goCommercial"])
    {
        ExtraDetailCommercialVC * destination = segue.destinationViewController;
        destination.property = _property;
    }
    else if ([[segue identifier] isEqualToString:@"goHDB"])
    {
        ExtraDetailHDBVC * destination = segue.destinationViewController;
        destination.property = _property;
    }
    else if ([[segue identifier] isEqualToString:@"goIndustrial"])
    {
        ExtraDetailIndustryVC * destination = segue.destinationViewController;
        destination.property = _property;
    }else if ([[segue identifier] isEqualToString:@"goLand"])
    {
        ExtraDetailLandVC * destination = segue.destinationViewController;
        destination.property = _property;
    }
}
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"pickLocationNext"])
    {
        NSLog(@"_property.address = %@",_property.address);
        if (_property.address!= NULL) {
            if (![_property.address isEqualToString:@""]) {
                return YES;
            }
            return NO;
        }
        return NO;
    }
    else if ([identifier isEqualToString:@"goCommercial"])
    {
        if (_property.address!= NULL) {
            if (![_property.address isEqualToString:@""]) {
                return YES;
            }
            return NO;
        }
        return NO;
    }
    else if ([identifier isEqualToString:@"goHDB"])
    {
        if (_property.address!= NULL) {
            if (![_property.address isEqualToString:@""]) {
                return YES;
            }
            return NO;
        }
        return NO;
    }
    else if ([identifier isEqualToString:@"goIndustrial"])
    {
        if (_property.address!= NULL) {
            if (![_property.address isEqualToString:@""]) {
                return YES;
            }
            return NO;
        }
        return NO;
    }
    else if ([identifier isEqualToString:@"goLand"])
    {
        if (_property.address!= NULL) {
            if (![_property.address isEqualToString:@""]) {
                return YES;
            }
            return NO;
        }
        return NO;
    }
    return NO;
}

- (IBAction)next:(id)sender
{
    if ([_property.propertyType  isEqualToString: @"Private Residential"]) {
        [self performSegueWithIdentifier:@"pickLocationNext" sender:self];
    }
    else if ([_property.propertyType isEqualToString:@"Commercial"])
    {
        [self performSegueWithIdentifier:@"goCommercial" sender:self];
    }
    else if ([_property.propertyType isEqualToString:@"HDB"])
    {
        [self performSegueWithIdentifier:@"goHDB" sender:self];
    }
    else if ([_property.propertyType isEqualToString:@"Industrial"])
    {
        [self performSegueWithIdentifier:@"goIndustrial" sender:self];
    }
    else if ([_property.propertyType isEqualToString:@"Land"])
    {
        [self performSegueWithIdentifier:@"goLand" sender:self];
    }
}


-(void) GetMapLocation : (NSArray*) data {
	NSString* postCode;
	NSLog(@"data %@", data);
	
	for (int i = 0; i < data.count; i++) {
		postCode = [[data objectAtIndex:i] objectForKey:@"POSTALCODE"];
		NSLog(@"post %@", postCode);
		CLGeocoder *geocoder = [[CLGeocoder alloc] init];
		
		
		//NSLog(@"%@", postCode);
		[geocoder geocodeAddressString:postCode
					 completionHandler:^(NSArray *placemarks, NSError *error) {
						 
						 if (error) {
							 NSLog(@"Geocode failed with error: %@", error);
							 return;
						 }
						 
						 if(placemarks && placemarks.count > 0)
						 {
							 CLPlacemark *placemark = placemarks[0];
							 
							 NSDictionary *addressDictionary =
							 placemark.addressDictionary;
							 
							 //NSLog(@"location: %@", [placemark location]);
							 NSLog(@"addressdict: %@", [placemark addressDictionary]);
							 //_property.address = [addressDictionary objectForKey:@"Street"];
							 //_property.blkNo = [[DataEngine sharedInstance] getBlockNumber:_property.postCode];
							 //_property.district = [[DataEngine sharedInstance] getDistrict:_property.postCode];
							 
							 //[placemark ]
							 
							 // NSLog(@"%@ %@ %@ %@", address,city, state, zip);
							 CLLocationCoordinate2D coord = {.latitude =  placemark.location.coordinate.latitude, .longitude = placemark.location.coordinate.longitude};
							 MKCoordinateSpan span = {.latitudeDelta = 0.01, .longitudeDelta =  0.01};
							 MKCoordinateRegion region = {coord, span};
							 
							 MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
							 point.coordinate = coord;
							 point.title = [[data objectAtIndex:i] objectForKey:@"BUILDINGNAME"];
							 point.subtitle = [NSString stringWithFormat:@"Blk %@ %@ %@",[[data objectAtIndex:i] objectForKey:@"BLOCK"] , [[data objectAtIndex:i] objectForKey:@"ROAD"], [[data objectAtIndex:i] objectForKey:@"POSTALCODE"]];
							 [self.mapView addAnnotation:point];
							 
							 NSLog(@"View loading");
							 [self.mapView setRegion:region];
							 NSLog(@"view loaded");
							 [self.mapView selectAnnotation:point animated:YES];
							 
							 _property.postCode = [[data objectAtIndex:i] objectForKey:@"POSTALCODE"];
                             _property.address = point.title;
						 }
					 }
		 ];
	}
}
@end
