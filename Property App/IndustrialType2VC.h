//
//  IndustrialType2VC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"

@interface IndustrialType2VC : UITableViewController<UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    
    IBOutlet UITextField * gstTF;
    IBOutlet UITextField * conditionTF;
    IBOutlet UITextField * electricalLoadTF;
    IBOutlet UITextField *ampsTF;
    IBOutlet UITextField * floorLoadTF;
    IBOutlet UITextField * noOfCargoLiftTF;
    IBOutlet UITextField * flattedTF;
    IBOutlet UITextField * ceilingHeightTF;
    IBOutlet UITextField * landSizeTF;
    IBOutlet UITextField * noOfStoreyTF;
    IBOutlet UITextField * typeTF;
    
    
    
    NSArray * levelList;
    NSArray * gstList;
    NSArray * conditionList;
    NSArray * flattedList;
    NSArray * typeList;
    
    
    
    
    
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
}
@property (strong, nonatomic) Property * property;
-(void) addBackBtn;

@end
