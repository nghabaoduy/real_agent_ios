//
//  DetailPropertyTabBarVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
#import "User.h"
#import "ProPortfolioViewController.h"

@interface DetailPropertyTabBarVC : UITabBarController
{
    IBOutlet UIBarButtonItem * addBtn;
}

@property (nonatomic, strong) UIViewController *homeVC;
@property (nonatomic, strong) Property * property;
@property (nonatomic, retain) User * curUser;
@property (nonatomic) BOOL isFavorited;
@property (nonatomic, strong) PFObject * favoriteInfo;

@property (nonatomic) BOOL isHide;
@property (nonatomic) BOOL isShare;
@property (nonatomic) BOOL allowUnitNo;


@property (nonatomic, retain) PFObject * curGroup;
@property (nonatomic, retain) ProPortfolioViewController * proportView;


- (IBAction)favorite:(id)sender;
- (void) startCheckFavorite;
- (void) finishCheckFavorite;
-(void) initTabByProperty:(Property*) curPro :(UIStoryboard *) newStoryboard;

@end
