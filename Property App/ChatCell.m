//
//  ChatCell.m
//  Property App
//
//  Created by Brian on 11/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ChatCell.h"
#import "DatabaseEngine.h"


@implementation ChatCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setCellPropertyId:(NSString *) propId : (NSMutableArray *)parentPropArray
{
    self.parentPropArray = parentPropArray;
    
    self.propId = propId;
    [[DatabaseEngine sharedInstance] getPropertyWithChatCell:self];
    
}
-(void) setCellProperty:(Property *)prop
{
 

    
}
-(void) refreshCell
{
    if (self.parentPropArray != nil) {
        if (![self.parentPropArray containsObject:self.prop]) {
            [self.parentPropArray addObject:self.prop];
        }
    }
    [self.propAddress setText:self.prop.address];
    [self.propPrice setText:[NSString stringWithFormat:@"Price: %@",self.prop.price]];
    [self.prop loadIconForIV:self.propIcon];
    
}

@end
