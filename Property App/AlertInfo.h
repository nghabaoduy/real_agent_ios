//
//  AlertInfo.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/21/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface AlertInfo : NSObject

@property (nonatomic, retain) PFObject * alert;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSString * transaction;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * project;
@property (nonatomic, retain) NSString * district;
@property (nonatomic, retain) NSString * blockUnitNo;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * tenure;
@property (nonatomic, retain) NSString * price;
@property (nonatomic, retain) NSString * area;
@property (nonatomic, retain) NSString * level;
@property (nonatomic, retain) NSString * bedroom;
@property (nonatomic, retain) NSString * remark;


- (id) initWithAlert : (PFObject*) newAlert;
@end
