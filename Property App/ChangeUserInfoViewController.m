//
//  ChangeUserInfoViewController.m
//  Property App
//
//  Created by Jonathan Le on 15/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ChangeUserInfoViewController.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "SWRevealViewController.h"
@interface ChangeUserInfoViewController ()

@end

@implementation ChangeUserInfoViewController


@synthesize phone = _phone;
@synthesize email = _email;
@synthesize firstname = _firstname;
@synthesize lastname = _lastname;
@synthesize activeTextField, updatingUser;


- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    
    
    [textField resignFirstResponder];
    
    return YES;
    
}



- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
- (IBAction) Back:(id)sender
{
	//SWRevealViewController * mainView = [self.storyboard instantiateViewControllerWithIdentifier:@"sw"];
	//[self presentViewController:mainView animated:YES completion:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	//updatingUser = [[DataEngine sharedInstance] getViewingUser];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.delegate = self;
	
    [self initTextfieldPlaceHolder];
}

- (void) initTextfieldPlaceHolder
{
    _phone.placeholder = @"Phone No: ";
    _firstname.placeholder = @"Firstname: ";
    _lastname.placeholder = @"Lastname: ";
    _email.placeholder = @"Email: ";
    
    [_phone setEnabled:NO];
    
    [_phone setText:updatingUser.phone];
	[_firstname setText:updatingUser.firstName];
	[_lastname setText:updatingUser.lastName];
    if (![updatingUser.email hasSuffix:@"unknow.com"]) {
        [_email setText:updatingUser.email];
    }
    else
    {
        [_email setText:@""];
    }
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction) UpdateInfo:(id)sender
{
	//this method will generate error: cannot modify other user object. Working on it
    //[[DatabaseEngine sharedInstance] ChangeUserInfo:updatingUser firstname:@"abc" lastName:@"abc" phone:@"234" email:@"brain@mail.com" changeUserView:nil];
	//[[DatabaseEngine sharedInstance] ChangeUserInfo:updatingUser firstname:_firstname.text lastName:_lastname.text phone:_phone.text email:_email.text changeUserView:self];
    
    Agent* curAgent;
    curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
	[curAgent addRefreshView:self];
	[PFCloud callFunctionInBackground:@"editUser" withParameters:@{
                                                                   @"userId": updatingUser.userObject.objectId,
                                                                   @"firstN": _firstname.text,
                                                                   @"lastN" : _lastname.text,
                                                                   @"phoneNo" : @"",
                                                                   @"emailAdd" : _email.text == nil? @"":_email.text,
                                                                  
                                                                   } block:^(id object, NSError *error) {
                                                                       if (!error) {
                                                                           NSLog(@"updateSucessful");
                                                                           [(Agent*) [[DataEngine sharedInstance] GetCurUser] RefreshData];
                                                                           
                                                                       } else {
                                                                           NSString *errorString = [[error userInfo] objectForKey:@"error"];
                                                                           [self actionFail:errorString];
                                                                       }
                                                                   }];

    
    [self actionRun];
}
-(void) actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    updatingUser.firstName = _firstname.text;
    updatingUser.lastName = _lastname.text;
    if (_email.text != nil) {
        updatingUser.email = _email.text;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Status"
													message:@"User Info Edit Successfully"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
    [alert show];
	//[(Agent*)[[DataEngine sharedInstance] GetCurUser] GetMyClientList];
    [self.personInfoView refreshPersonInfo];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void) actionFail:(NSString *)message
{
    [super actionFail:message];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Error"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([touch.view isKindOfClass:[UIButton class]]){
        return NO;
    }
    return YES; // handle the touch
}

- (void) dismissKeyboard{
    [self.activeTextField resignFirstResponder];
}



- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
    
    
    // Step 3: Scroll the target text field into view.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(aRect, self.activeTextField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.activeTextField.frame.origin.y - (keyboardSize.height-15));
        [theScrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
}

// Set activeTextField to the current active textfield

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
}

// Set activeTextField to nil

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextField = nil;
}


// Dismiss the keyboard

- (IBAction)dismissKeyboard:(id)sender
{
    [self.activeTextField resignFirstResponder];
}


@end
