//
//  ResetPasswordViewController.h
//  Property App
//
//  Created by Jonathan Le on 15/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPasswordViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITextField * email;

- (void) RequestSuccessful;
- (void) RequestFailed: (NSString*) errorString;
- (IBAction) Back:(id)sender;
@end
