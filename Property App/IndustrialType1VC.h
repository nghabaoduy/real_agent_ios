//
//  IndustrialType1VC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"


@interface IndustrialType1VC : UITableViewController<UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    IBOutlet UITextField * levelTF;
    IBOutlet UITextField * gstTF;
    IBOutlet UITextField * conditionTF;
    IBOutlet UITextField * vehicleAccessTF;
    IBOutlet UITextField * electricalLoadTF;
    IBOutlet UITextField *ampsTF;
    IBOutlet UITextField * floorLoadTF;
    IBOutlet UITextField * noOfCargoLiftTF;
    IBOutlet UITextField * flattedTF;
    IBOutlet UITextField * ceilingHeightTF;
    
    
    
    NSArray * levelList;
    NSArray * gstList;
    NSArray * conditionList;
    NSArray * vehicleAccessList;
    NSArray * flattedList;
    
    
    
    
    
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
}
@property (strong, nonatomic) Property * property;
-(void) addBackBtn;

@end
