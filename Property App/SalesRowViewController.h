//
//  SalesRowViewController.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
@interface SalesRowViewController : UIViewController <UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    IBOutlet UIScrollView *theScrollView;
    
    
}
@property (nonatomic, assign) UITextField *activeTextField;
@property (nonatomic, retain) Property* property;
- (IBAction)dismissKeyboard:(id)sender;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (nonatomic, retain) IBOutlet UINavigationBar * navBar;
//old
//@property (nonatomic, strong) IBOutlet UITextField * price;
//@property (nonatomic, strong) IBOutlet UITextField * psf;

//new
@property (strong, nonatomic) IBOutlet UITextField * districtTxF;
@property (strong, nonatomic) IBOutlet UITextField * bedroomTxF;
@property (strong, nonatomic) IBOutlet UITextField * areaTxF;
@property (strong, nonatomic) IBOutlet UITextField * priceTxF;
@property (strong, nonatomic) IBOutlet UITextField * tenureTxF;
//status
@property (strong, nonatomic) IBOutlet UITextField * statusTxF;
@property (strong, nonatomic) IBOutlet UITextField * rentedPriceTxF;

//project info
@property (strong, nonatomic) IBOutlet UITextField * projectTxF;
@property (strong, nonatomic) IBOutlet UITextField * psfTxF;
@property (strong, nonatomic) IBOutlet UITextField * topYearTxF;
@property (strong, nonatomic) IBOutlet UITextField * rentalYeildTxF;

@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) IBOutlet UIToolbar*  mypickerToolbar;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *doneBtn;


@property (strong, nonatomic) NSArray *districtChoice;
@property (strong, nonatomic) NSArray *priceRangeChoice;
@property (strong, nonatomic) NSArray *tenureRangeChoice;
@property (strong, nonatomic) NSArray *psfRangeChoice;
@property (strong, nonatomic) NSArray *yieldRangeChoice;

@property BOOL keyBoardIsActive;
- (void) SearchSuccessfully : (NSArray*) objects;
- (void) SearchFailed : (NSString*) errorString;

@end