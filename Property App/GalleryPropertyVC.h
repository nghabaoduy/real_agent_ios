//
//  GalleryPropertyVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Property.h"

@interface GalleryPropertyVC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>
{
    NSMutableArray * imageList;
}

@property (strong, nonatomic) IBOutlet UICollectionView * collectionView;
@property (strong, nonatomic) NSMutableArray * imageList;
@property (nonatomic, retain) Property* property;
- (void) DownloadImageSuccessfully : (NSMutableArray*) imageArray;
- (void) DownloadImageFail : (NSString*) errorString;

@end
