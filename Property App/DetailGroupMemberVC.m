//
//  DetailGroupMemberVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/7/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailGroupMemberVC.h"
#import "FounderCell.h"
#import "DatabaseEngine.h"
#import "DataEngine.h"
#import "PhoneBookVC.h"
#import "AgentProfileVC.h"
#import "MyUIEngine.h"

@interface DetailGroupMemberVC ()

@end

@implementation DetailGroupMemberVC
@synthesize curGroup = _curGroup;
@synthesize acceptedMember = _acceptedMember;
@synthesize pendingMember = _pendingMember;

- (void)actionRun
{
    [super actionRun];
    NSLog(@"DetailGroupMemberVC start getMembers of group: %@",_curGroup.objectId);
    [[DatabaseEngine sharedInstance] getMembers:self :_curGroup];
}

- (void)actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    
    NSLog(@"%i", _acceptedMember.count);
    
    [self.tableView reloadData];
}

- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    _acceptedMember = [[NSMutableArray alloc]init];
    _pendingMember = [[NSMutableArray alloc]init];
    [self actionRun];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    if (section == 0) {
        return 1;
    }
    else if (section == 1)
    {
        return [_acceptedMember count];
    }
    else if (section == 2)
    {
        return [_pendingMember count];
    }
    else
    {
        PFUser * founder = [_curGroup objectForKey:@"creator"];
        if ([[[DataEngine sharedInstance] GetCurUser].userObject.objectId isEqualToString:founder.objectId]) {
            return 2;
        }
        else
        {
            return 1;
        }
            
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"member";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        PFUser * founder = [_curGroup objectForKey:@"creator"];
        User * curUser = [[User alloc]initWithUser:founder];
        
        cell.textLabel.text = curUser.fullName;
        
        
        return cell;
    }
    else if (indexPath.section == 1)
    {
        static NSString *CellIdentifier = @"member";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        
        
        PFObject * member = [_acceptedMember objectAtIndex:indexPath.row];
        User * curUser = [[User alloc]initWithUser: [member objectForKey:@"userId"]];
        cell.textLabel.text = curUser.fullName;
        
        return cell;
    }
    else if (indexPath.section == 2)
    {
        static NSString *CellIdentifier = @"member";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        PFUser * member = [_pendingMember objectAtIndex:indexPath.row];
        User * curUser = [[User alloc]initWithUser:[member objectForKey:@"userId"]];
        cell.textLabel.text = curUser.fullName;
        
        
        return cell;
    }
    else
    {
        
        PFUser * founder = [_curGroup objectForKey:@"creator"];
        if ([[[DataEngine sharedInstance] GetCurUser].userObject.objectId isEqualToString:founder.objectId]) {
            
            if (indexPath.row == 0) {
                static NSString *CellIdentifier = @"addMember";
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                }
                
                
                return cell;
            }
            else
            {
                static NSString *CellIdentifier = @"removeGroup";
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                }
                
                
                return cell;
            }
        }
        else
        {
            static NSString *CellIdentifier = @"leaveGroup";
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            
            
            return cell;
        }
        
        
        
        
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    PFUser * founder = [_curGroup objectForKey:@"creator"];
    if ([[[DataEngine sharedInstance] GetCurUser].userObject.objectId isEqualToString:founder.objectId]) {
        if (indexPath.section == 1) {
            return  YES;
        }
        else if (indexPath.section == 2)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else
    {
        return NO;
    }
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 1) {
        PFObject * curUser  = [_acceptedMember objectAtIndex:indexPath.row];
        [curUser deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [_acceptedMember removeObject:curUser];
                [tableView beginUpdates];
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                 withRowAnimation:UITableViewRowAnimationFade];
                [tableView endUpdates];
            }
        }];
    }
    else if (indexPath.section == 2)
    {
        NSLog(@"inde %i", indexPath.row );
        PFObject * curUser  = [_pendingMember objectAtIndex:indexPath.row];
        [curUser deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [_pendingMember removeObject:curUser];
                [tableView beginUpdates];
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                 withRowAnimation:UITableViewRowAnimationFade];
                [tableView endUpdates];
                NSLog(@"do remove %i", _pendingMember.count);
                
            }
        }];
    }
    
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Group Founder";
    }
    else if (section == 1)
    {
        return @"Member";
    }
    else if (section == 2)
    {
        return @"Pending member";
    }
    else
    {
        return nil;
    }
}


- (IBAction)addMember:(id)sender
{
    [self startLoading];
    UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"PhoneBookNav"];
    [self presentViewController:navController animated:YES completion:nil];
}


- (IBAction)removeGroup:(id)sender
{
    [self startLoading];
    for (PFObject * mem in _acceptedMember) {
        [mem deleteInBackground];
    }
    
    for (PFObject * mem in _pendingMember) {
        [mem deleteInBackground];
    }
    
    [_curGroup deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [self finishLoading];
        }
    }];
    
}


- (IBAction)leaveGroup:(id)sender
{
    for (PFObject * mem in _acceptedMember) {
        
        PFUser * memUser = [mem objectForKey:@"userId"];
        User * cur = [[DataEngine sharedInstance]GetCurUser];
        
        if ([memUser.objectId isEqualToString:cur.userObject.objectId]) {
            [mem deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
        
        
    }
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goMember"]) {
        NSMutableArray * newList  = [[NSMutableArray alloc]init];
        PFUser * founder = [_curGroup objectForKey:@"creator"];
        [newList addObject:founder];
        
        for (PFObject * accept in _acceptedMember) {
            [newList addObject: [accept objectForKey:@"userId"]];
        }
        
        for (PFObject * pending in _pendingMember) {
            [newList addObject: [pending objectForKey:@"userId"]];
        }
        
        
        PhoneBookVC * destination = segue.destinationViewController;
        destination.unneededList = newList;
        destination.curGroup = _curGroup;
        destination.groupMemberVC =  self;
        
    }
    else if ([[segue identifier] isEqualToString:@"goDetail"])
    {
        AgentProfileVC * destionation = segue.destinationViewController;
        
        NSLog(@"abc");
        
        
        if (self.tableView.indexPathForSelectedRow.section == 0)
        {
            PFUser * founder = [_curGroup objectForKey:@"creator"];
            User * curUser = [[User alloc]initWithUser:founder];
            destionation.curUser = curUser;
        }
        
        if (self.tableView.indexPathForSelectedRow.section == 1)
        {
            PFObject * member = [_acceptedMember objectAtIndex:self.tableView.indexPathForSelectedRow.row];
            User * curUser = [[User alloc]initWithUser: [member objectForKey:@"userId"]];
            destionation.curUser = curUser;
        }
        else if (self.tableView.indexPathForSelectedRow.section == 2)
        {
            PFObject * member = [_pendingMember objectAtIndex:self.tableView.indexPathForSelectedRow.row];
            User * curUser = [[User alloc]initWithUser: [member objectForKey:@"userId"]];
            destionation.curUser = curUser;
        }
    }
}


@end
