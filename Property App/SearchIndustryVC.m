//
//  SearchIndustryVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/24/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "SearchIndustryVC.h"
#import "DataEngine.h"
#import "DatabaseEngine.h"

@interface SearchIndustryVC ()

@end

@implementation SearchIndustryVC
- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self LoadDefault];

    industryTypeList = [[NSArray alloc]initWithObjects:@"", nil];
    rentalYieldList = [[NSArray alloc] initWithObjects:
                       @"",
                       @"3% & Below",
                       @"3-5%,5-10%,",
                       @"10% & Above",
                       nil];
    
    districtList = [[DataEngine sharedInstance] getDistrictList];
    tenureList = [[NSArray alloc] initWithObjects:
                  @"",
                  @"FH",
                  @"999",
                  @"99",
                  @"60",
                  @"30",
                  @"<30yrs",
                  nil];
    industryTypeList  = [[NSArray alloc]initWithObjects:
                         @"",
                         @"B1",
                         @"B2",
                         @"Central Kitch",
                         @"Land",
                         @"Building", nil];
    [self initTextfieldPlaceHolder];
}

- (void) initTextfieldPlaceHolder
{
    districtTF.placeholder = @"District: ";
    industryTypeTF.placeholder = @"Industry Type: ";
    areaTF.placeholder = @"Floor Size: ";
    priceTF.placeholder = @"Price: ";
    tenureTF.placeholder = @"Tenure: ";
    
    rentedPriceTF.placeholder = @"Rental Price: ";
    
    
    
    projectNameTF.placeholder = @"Project Name: ";
    topYearTF.placeholder = @"Top Year: ";
    psfTF.placeholder = @"PSF: ";
    rentalYieldTF.placeholder = @"Rental Yield: ";
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) LoadDefault
{
    [propertyTypeTF setText: _property.propertyType];
    [listingTypeTF setText:_property.listingType];
}

-(void)dismissKeyboard {
    [activeTextField resignFirstResponder];
}



- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
}

-(void)categoryDoneButtonPressed{
    //categoryLable.text = selectedCategory;
    if (activeTextField == districtTF) {
        [districtTF setText:selectedCategory];
    } else if (activeTextField == tenureTF)
    {
        [tenureTF setText:selectedCategory];
    } else if (activeTextField == rentalYieldTF)
    {
        [rentalYieldTF setText:selectedCategory];
    } else if (activeTextField == industryTypeTF)
    {
        [industryTypeTF setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if (activeTextField == districtTF) {
        selectedCategory = [NSString stringWithFormat:@"%@",[districtList objectAtIndex:row]];
    } else if (activeTextField == tenureTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[tenureList objectAtIndex:row]];
    } else if (activeTextField == rentalYieldTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[rentalYieldList objectAtIndex:row]];
    } else if (activeTextField == industryTypeTF)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[industryTypeList
                                                             objectAtIndex:row]];
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (activeTextField == districtTF) {
        return [districtList count];
    } else if (activeTextField == tenureTF)
    {
        return [tenureList count];
    } else if (activeTextField == rentalYieldTF)
    {
        return [rentalYieldList count];
    } else if (activeTextField == industryTypeTF)
    {
        return [industryTypeList count];
    }
    return [districtList count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == districtTF) {
        return [districtList objectAtIndex:row];
    } else if (activeTextField == tenureTF)
    {
        return [tenureList objectAtIndex:row];
    } else if (activeTextField == rentalYieldTF)
    {
        return [rentalYieldList objectAtIndex:row];
    } else if (activeTextField == industryTypeTF)
    {
        return [industryTypeList objectAtIndex:row];
    }
    
    return [NSString stringWithFormat:@"%d", row+1];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:districtTF] || [textField isEqual:tenureTF] || [textField isEqual:rentalYieldTF] || [textField isEqual:industryTypeTF])
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (IBAction)search:(id)sender
{
    NSLog(@"can run1");
    _property.district = districtTF.text;
    _property.industryType = industryTypeTF.text;
    _property.area = areaTF.text;
    _property.price = priceTF.text;
    _property.tenure = tenureTF.text;
    _property.rentedPrice = rentedPriceTF.text;
    _property.projectName = projectNameTF.text;
    _property.PSF = psfTF.text;
    _property.topYear = topYearTF.text;
    _property.rentalYeild = rentalYieldTF.text;

    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NSLog(@"can run3");
    SalesListViewController *pushview = [newStoryboard instantiateViewControllerWithIdentifier:@"salesListView"];
    NSLog(@"can run4");
    pushview.propertySearch = _property;
    NSLog(@"can run5");
	[self presentViewController:pushview animated:YES completion:nil];
}








@end
