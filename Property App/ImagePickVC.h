//
//  ImagePickVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 21/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"

@interface ImagePickVC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate>
{
    IBOutlet UICollectionView * cv;
    NSMutableArray * imageList;
    int selectedIndex;
}


@property (strong,nonatomic) Property * property;

- (void) deleteAllContent;


@end
