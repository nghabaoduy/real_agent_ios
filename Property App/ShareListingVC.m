//
//  ShareListingVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ShareListingVC.h"

#import "DetailPropertyTabBarVC.h"
#import "DetailPropertyVC.h"
#import "GalleryPropertyVC.h"
#import "LocationPropertyVC.h"
#import "DetailPropertyNavVC.h"

#import "DatabaseEngine.h"
#import "MyUIEngine.h"
#import "DataEngine.h"
#import "Client.h"
#import "Agent.h"
#import "User.h"

#import "AddPropertyViewController.h"
#import "ListPattern.h"
#import "PortfolioSection.h"

@interface ShareListingVC ()

@end

@implementation ShareListingVC

@synthesize tableView = _tableView;
@synthesize contentArray, agentClientList, agentClientPropertyList;

@synthesize allPropertyList;

Agent* curAgent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
	
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	// Do any additional setup after loading the view.
}
- (void) initData
{
    [self actionRun];
    [[DatabaseEngine sharedInstance] GetMyListing:self];
}
-(void) actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    
    //refresh local data
    curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
    
    
    agentClientList = [[NSMutableArray alloc] init];
	agentClientPropertyList = [[NSMutableArray alloc] init];
	[self.agentClientList addObject:curAgent];
	for (Client* client in curAgent.myClientList) {
		[self.agentClientList addObject:client];
	}
    allPropertyList = [[NSMutableArray alloc] init];
    
	
	[self.agentClientPropertyList addObject:curAgent.propertyList];
	for (NSMutableArray* propList in curAgent.myClientPropertyList) {
		[self.agentClientPropertyList addObject:propList];
        for (Property *prop in propList) {
            if ([prop.isSubmited isEqualToString:@"YES"]) {
                //[propList removeObject:prop];
                [self.allPropertyList addObject:prop];
            }
        }
	}
    NSLog(@"allPropertyList %@", self.allPropertyList);
    
    [self.tableView reloadData];
    
}
-(void) actionFail:(NSString *)message
{
    [super actionFail:message];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    
    
}

- (void) LoadAllIcon
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



- (IBAction) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return allPropertyList.count;
}




-(void) addClientProperty: (User *)user
{
    if(user!= NULL)
    {
        AddPropertyViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"addPropertyView"];
        view.adder = user;
        [self presentViewController:view animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Can't find user"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"indexPath = %i",indexPath.row);
    static NSString *CellIdentifier = @"listPattern1";
    
    
    Property *curPro = [allPropertyList objectAtIndex:indexPath.row];
    NSLog(@"property line %i-%i = %@",indexPath.section,indexPath.row,curPro);
    
    
    ListPattern * cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell1 == nil)
    {
        NSLog(@"cell is nil");
        cell1 = [[ListPattern alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UIImage *background = [UIImage imageNamed:@"cell_odd"];
    
    
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cell1.backgroundView = cellBackgroundView;
    if (curPro.iconImage != nil) {
        cell1.displayer.image = curPro.iconImage;
    }
    
    [cell1.projectName setText:curPro.projectName];
    [cell1.priceAndBedroom setText:[NSString stringWithFormat:@"S$ %@", curPro.price]];
    [cell1.projectType setText:curPro.propertyType];
    [cell1.areaAndPsf setText:[NSString stringWithFormat:@"%@ sqft  - PFS S$ %@", curPro.area, curPro.PSF]];
    [cell1.location setText:curPro.adder.fullName];
    
    
    
    return cell1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 128;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatRoomVC* room =[self.tabBarController.viewControllers objectAtIndex:0];
    
    Property *curPro = [allPropertyList objectAtIndex:indexPath.row];
    
    //Customize text and push to room.content
    self.tabBarController.selectedViewController = room;
    [room shareProperty:curPro];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.000001f;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.000001f;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] ;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}

@end
