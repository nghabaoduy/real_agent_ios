//
//  AddPropertyNavVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 21/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "AddPropertyNavVC.h"
#import "BasicInfoVC.h"
#import "MyUIEngine.h"

@interface AddPropertyNavVC ()

@end

@implementation AddPropertyNavVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"%hhd", self.test);
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    self.navigationItem.backBarButtonItem.title = @"";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"run");
    BasicInfoVC * view = segue.destinationViewController;
    view.test = YES;
}

@end
