//
//  ExtraDetailLandVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 12/13/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"


@interface ExtraDetailLandVC : UITableViewController<UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    //Basic Information
    IBOutlet UITextField * address;
    IBOutlet UITextField * postalCode;
    IBOutlet UITextField * blockNo;
    IBOutlet UITextField * levelNo;
    IBOutlet UITextField * unitNo;
    IBOutlet UITextField * district;
    IBOutlet UITextField * landType;
    IBOutlet UITextField * area;
    IBOutlet UITextField * price;
    IBOutlet UITextField * tenure;
    IBOutlet UITextField * adder;
    
    //Status
    IBOutlet UITextField * status;
    IBOutlet UITextField * rentedPrice;
    IBOutlet UITextField * tenantedPrice;
    
    
    //Project Information
    IBOutlet UITextField * projectName;
    IBOutlet UITextField * PSF;
    IBOutlet UITextField * topYear;
    IBOutlet UITextField * rentalYield;
    
    
    //Extra
    IBOutlet UITextField * bedroom;
    IBOutlet UITextField * funishing;
    IBOutlet UITextField * landSize;
    IBOutlet UITextField * mainDoorFacing;
    IBOutlet UITextField * mainGateFacing;
    IBOutlet UITextField * carPark;
    IBOutlet UITextField * garden;
    IBOutlet UITextField * swimmingPool;
    IBOutlet UITextField * basement;
    IBOutlet UITextField * roofTerrance;
    
    
    
    
    
    NSArray * rentalYieldList;
    NSArray * districtList;
    NSArray * tenureList;
    NSArray * statusList;
    NSArray * landTypeList;
    NSArray * funishingList;
    NSArray * mainDoorFacingList;
    NSArray * mainGateFacingList;
    NSArray * carParkList;
    NSArray * gardenList;
    NSArray * swimmingPoolList;
    NSArray * basementList;
    NSArray * roofTerranceList;
    
    
    
    
    
    NSMutableArray * adderList;
    NSMutableArray * adderNameList;
    
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
}
@property (strong, nonatomic) Property * property;
-(void) addBackBtn;

@end
