//
//  ReviewImageVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 22/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ReviewImageVC.h"
#import "MyUIEngine.h"

@interface ReviewImageVC ()

@end

@implementation ReviewImageVC
@synthesize theView = _theView;
@synthesize curImage = _curImage;
@synthesize test = _test;


- (void) Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    self.navigationItem.backBarButtonItem.title = @"";
	// Do any additional setup after loading the view.
    _theView.image =  _curImage;
    NSLog(@"%@ hahah%i", _curImage, _test);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
