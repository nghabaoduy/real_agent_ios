//
//  DealCell.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/19/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DealCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIImageView * background;
@property (nonatomic, retain) IBOutlet UILabel * status;
@property (nonatomic, retain) IBOutlet UILabel * subject;
@property (nonatomic, retain) IBOutlet UILabel * price;
@property (nonatomic, retain) IBOutlet UILabel * address;
@property (nonatomic, retain) IBOutlet UILabel * createdAt;
@property (nonatomic, retain) IBOutlet UILabel * advertiser;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic) BOOL isLoaded;
@property (nonatomic, retain) IBOutlet UIButton * phoneNumber;

- (void) getButton;
- (void)getImage;

@end
