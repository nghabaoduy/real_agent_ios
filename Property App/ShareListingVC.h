//
//  ShareListingVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/11/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUIViewController.h"

@interface ShareListingVC : MyUIViewController <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, retain) IBOutlet UITableView  *tableView;

@property (nonatomic, retain) NSMutableArray *contentArray;
@property (nonatomic, retain) NSMutableArray* agentClientList;
@property (nonatomic, retain) NSMutableArray* agentClientPropertyList;
@property (nonatomic, retain) NSMutableArray* allPropertyList;

@end
