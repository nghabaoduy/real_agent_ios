//
//  ParticipantCell.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/12/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "GroupInfoVC.h"

@interface ParticipantCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel * username;
@property (nonatomic, retain) IBOutlet UILabel * phoneNumber;
@property (nonatomic, retain) IBOutlet UIButton * deleteBtn;

@property (nonatomic, retain) PFObject * groupInfo;
@property (nonatomic, retain) PFUser * curUser;
@property (nonatomic, retain) GroupInfoVC * groupInfoView;
@property (nonatomic) int index;

- (IBAction)deleteStart:(id)sender ;
- (void) deleteDone;

@end
