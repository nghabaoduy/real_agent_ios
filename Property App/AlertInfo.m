//
//  AlertInfo.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/21/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "AlertInfo.h"

@implementation AlertInfo
@synthesize alert, subject, transaction, type, project, district, blockUnitNo,location, tenure, price, area, level, bedroom, remark;

- (id)initWithAlert:(PFObject *)newAlert
{
    if (self == [super init]) {
		
        alert = newAlert;
        subject = [alert objectForKey:@"subject"];
        transaction = [alert objectForKey:@"transaction"];
        type = [alert objectForKey:@"type"];
        project = [alert objectForKey:@"project"];
        district = [alert objectForKey:@"district"];
        blockUnitNo = [alert objectForKey:@"blockUnitNo"];
        location = [alert objectForKey:@"location"];
        tenure = [alert objectForKey:@"tenure"];
        price = [alert objectForKey:@"price"];
        area  = [alert objectForKey:@"area"];
        level = [alert objectForKey:@"level"];
        bedroom = [alert objectForKey:@"bedroom"];
        remark = [alert objectForKey:@"remarks"];
        
    }
    return self;

}

@end
