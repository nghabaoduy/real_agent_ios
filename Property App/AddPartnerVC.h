//
//  AddPartnerVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/30/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "PartnersVC.h"

@interface AddPartnerVC : MyUITableViewController<UINavigationBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate>
{
    IBOutlet UITableViewCell * bankCell;
    
    IBOutlet UITextField * nameTF;
    IBOutlet UITextField * addressTF;
    IBOutlet UITextField * contacttF;
    IBOutlet UITextField * emailTF;
    IBOutlet UITextField * bankTF;
    
    UITextField * activeTextField;
    UIActionSheet * menu;
    NSString * selectedCategory;
    
    NSArray * bankList;
    
    User * curUser;
}

@property (nonatomic, retain) NSString * partnerType;
@property (nonatomic, retain) PartnersVC * partnerView;
- (void) addDone;
@end
