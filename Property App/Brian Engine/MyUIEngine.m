//
//  MyUIEngine.m
//  PhoneBookController
//
//  Created by Brian on 8/9/13.
//  Copyright (c) 2013 Brian. All rights reserved.
//

#import "MyUIEngine.h"
#import "MyButtonMedium.h"

#import "DetailPropertyTabBarVC.h"
#import "DetailPropertyVC.h"
#import "GalleryPropertyVC.h"
#import "LocationPropertyVC.h"

#import "DetailComercialVC.h"
#import "DetailHDBVC.h"


@implementation MyUIEngine

+ (id)sharedUIEngine {
    static MyUIEngine *sharedUIEngine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUIEngine = [[self alloc] init];
    });
    return sharedUIEngine;
}

- (id)init {
    if (self = [super init]) {
        for(NSString *family in [UIFont familyNames])
        {
            for (NSString *name in [UIFont fontNamesForFamilyName:family]) {
                NSLog(@"%@",name);
            }
        }
    }
    return self;
}

-(void) customSearchBar:(UISearchBar *)searchBar
{
    
}
-(void) customToolbar:(UIToolbar *)toolbar
{
   [toolbar setBackgroundImage:[UIImage imageNamed:@"bottom_bar"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
}

-(void) customizeViewSetting:(UIViewController*) inputView :(UITableView*) tableView
{
    
    [self ios7customization:inputView:tableView];
    //view
    inputView.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"common_bg"]];
    
    //table
    if (tableView!=nil) {
        //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //tableView.rowHeight = 100;
        //tableView.backgroundColor = [UIColor clearColor];
        
        
        tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"transparent"]];
    }
}

-(void) ios7customization: (UIViewController *)inputView :(UITableView*) tableView
{
    NSString *version = [[UIDevice currentDevice] systemVersion];
    
    BOOL isAtLeast6 = [version floatValue] >= 6.0;
    BOOL isAtLeast7 = [version floatValue] >= 7.0;
    //
    if (isAtLeast7) {
       [inputView setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
}

-(UITableViewCell*) getCell:(UITableView *)tableView : (NSString *) identifier :(int) position :(int) totalRows :(NSString *) cellText{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:identifier];
        
    }
    
    //customize cell image
    /*@try{
        cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://brianlin1992.webfactional.com/icons/%@.png",@"Cat"]]]];
        NSString *finalString = [[cellText stringByReplacingOccurrencesOfString:@"/" withString:@"-"] stringByReplacingOccurrencesOfString:@" " withString:@"-"];
        
        if ([NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://brianlin1992.webfactional.com/icons/%@.png",finalString]]] == nil) {
        }
        else
        {
            cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://brianlin1992.webfactional.com/icons/%@.png",finalString]]]];
        }
    }
    @catch(NSException *ex)
    {
        
        
    }*/
    
    //customize cell Label
    [cell.textLabel setFont:[UIFont fontWithName:@"SegoeUI-Semilight" size:18]];
    
    
    //cell.textLabel.
   /* cell.detailTextLabel.frame = CGRectMake(155, 10, 150, 40);
    cell.textLabel.text = @"Hai";
    cell.detailTextLabel.text = @"Hello";
    
    cell.detailTextLabel.textAlignment = UITextAlignmentLeft;
    cell.textLabel.textAlignment = UITextAlignmentLeft;
    
    
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    cell.textLabel.textColor = [UIColor whiteColor];//colorWithPatternImage:[UIImage imageNamed:@"bee.png"]];
    //cell.textLabel.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.55];
    //cell.textLabel.shadowOffset =  CGSizeMake(0.5,0.5);
    

    //customize cell background
    UIImage *background = [UIImage imageNamed:@"cell_middle.png"];
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    //[self getCellBGBasedOnPositionAndTotalRows:cell :position :totalRows];
    
    /*
     cell.det1Label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 40.0, 30.0)];
     cell.det1Label.text = @"abc";
     [cell.contentView addSubview:cell.det1Label];
     */
     //[self viewFontList];*/
    
    return cell;
}

-(UITableViewCell*) getGroupCell:(UITableView *)tableView : (NSString *) identifier :(int) position :(int) totalRows :(NSString *) cellText{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:identifier];
        
    }
    UIImage *background;
   
    //customize cell background
    if (position%2 == 1) {
        background = [UIImage imageNamed:@"cell_odd"];
    }
    else
    {
        background = [UIImage imageNamed:@"cell_even"];
    }
    
     UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
     cellBackgroundView.image = background;
     cell.backgroundView = cellBackgroundView;
     
     
    
    return cell;
}
-(UITableViewCell*) getButtonLongCell:(UITableView *)tableView : (NSString *) identifier :(int) position :(int) totalRows :(NSString *) cellText{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    cell.textLabel.textAlignment = UITextAlignmentCenter;
       
    //customize cell Label
   /* [cell.textLabel setFont:[UIFont fontWithName:@"SegoeUI-Semilight" size:18]];
    
    
    //cell.textLabel.
    cell.detailTextLabel.frame = CGRectMake(155, 10, 150, 40);
    cell.textLabel.text = @"Hai";
    cell.detailTextLabel.text = @"Hello";
    
    cell.detailTextLabel.textAlignment = UITextAlignmentLeft;
    cell.textLabel.textAlignment = UITextAlignmentLeft;
    
    
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    cell.textLabel.textColor = [UIColor whiteColor];//colorWithPatternImage:[UIImage imageNamed:@"bee.png"]];
    cell.textLabel.shadowColor = [UIColor clearColor];
    cell.textLabel.shadowOffset =  CGSizeMake(0.5,0.5);
    cell.textLabel.textAlignment = UITextAlignmentCenter;
    
    //customize cell background
    UIImage *background = [UIImage imageNamed:@"button_long"];
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;*/
    
    
    return cell;
}


/*-(CGFloat) getCateHeight {
    return 20;
}

-(void) viewFontList {
    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
    NSArray *fontNames;
    NSInteger indFamily, indFont;
    for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
    {
        NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
        fontNames = [[NSArray alloc] initWithArray:
                     [UIFont fontNamesForFamilyName:
                      [familyNames objectAtIndex:indFamily]]];
        for (indFont=0; indFont<[fontNames count]; ++indFont)
        {
            NSLog(@"    Font name: %@", [fontNames objectAtIndex:indFont]);
        }
    }
}

-(void)getCellBGBasedOnPositionAndTotalRows:(UITableViewCell *) cell:(int) position:(int) totalRows
{
    UIImage *rowBackground;
    UIImage *selectionBackground;
    NSInteger sectionRows = totalRows;
    NSInteger row = position;
    if (row == 0 && row == sectionRows - 1)
    {
        rowBackground = [UIImage imageNamed:@"topAndBottomRow.png"];
        selectionBackground = [UIImage imageNamed:@"topAndBottomRowSelected.png"];
    }
    else if (row == 0)
    {
        rowBackground = [UIImage imageNamed:@"topRow.png"];
        selectionBackground = [UIImage imageNamed:@"topRowSelected.png"];
    }
    else if (row == sectionRows - 1)
    {
        rowBackground = [UIImage imageNamed:@"bottomRow.png"];
        selectionBackground = [UIImage imageNamed:@"bottomRowSelected.png"];
    }
    else
    {
        rowBackground = [UIImage imageNamed:@"middleRow.png"];
        selectionBackground = [UIImage imageNamed:@"middleRowSelected.png"];
    }
    cell.backgroundView = [[UIImageView alloc] initWithImage:rowBackground];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:selectionBackground];
}
 */


NSString * const ToggleAppearanceStyles = @"ToggleAppearanceStyles";

- (void) appDelegateUICustomzation {
    
    
    //custom UI
    //UIImage *navBackgroundImage = [UIImage imageNamed:@"navbar_bg"];
    
    //[[UINavigationBar appearance] setBackgroundImage:navBackgroundImage forBarMetrics:UIBarMetricsDefault];

    //[[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque
                                                animated:NO];
    
    
    //[[UINavigationBar appearance] setTintColor:[UIColor purpleColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor whiteColor], UITextAttributeTextColor,
                                                           [UIColor clearColor],UITextAttributeTextShadowColor,
                                                           [NSValue valueWithUIOffset:UIOffsetMake(0, 1)],
                                                           UITextAttributeTextShadowOffset,
                                                           [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0], UITextAttributeFont, nil]];
    //CGRect frameRect = [[UINavigationBar appearance] frame];
    //frameRect.size.height = 40;
    CGFloat verticalOffset = 4;
    [[UINavigationBar appearance] setTitleVerticalPositionAdjustment:verticalOffset forBarMetrics:UIBarMetricsDefault];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor], UITextAttributeTextColor,nil] forState:UIControlStateNormal];
    //[[UIBarButtonItem appearance] setTitle:@""];
    
    
    //change searchBar
   // [[UISearchBar appearance] setSearchFieldBackgroundImage:[UIImage imageNamed:@"transparent"] forState:UIControlStateNormal];
   // [[UISearchBar appearance] setBackgroundImage:[UIImage imageNamed:@"searchBar"]];
    //[[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"bottom_bar"]];
    //text
    //[[UILabel appearanceWhenContainedIn:[UITextField class], nil] setFont:[UIFont fontWithName:@"SegoeUI-Semilight" size:13.0]];
    
    [[UILabel appearanceWhenContainedIn:[MyButtonMedium class], nil] setFont:[UIFont fontWithName:@"SegoeUI-Semilight" size:18.0]];
    [[UILabel appearanceWhenContainedIn:[MyButtonMedium class], nil] setTextColor:[UIColor whiteColor]];
    //UISegmented

     //UIBarButtonItem
    //NSNotification *note = [NSNotification notificationWithName:ToggleAppearanceStyles object:NULL userInfo:@{@"flag" : @(YES)}];
    //[self toggleAppearanceStyles:note];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toggleAppearanceStyles:) name:ToggleAppearanceStyles object:NULL];
    
    
}
-(void)toggleAppearanceStyles:(NSNotification *)note {
    
   
    UIImage *barButtonBgImage = nil;
    UIImage *barButtonBgImageActive = nil;
    
    if([note.userInfo[@"flag"] boolValue]) {
        
        barButtonBgImage = [[UIImage imageNamed:@"g_barbutton.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 4, 15, 4)];
        barButtonBgImageActive = [[UIImage imageNamed:@"g_barbutton_active.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 4, 15, 4)];
    }
    
    [[UIBarButtonItem appearance] setBackgroundImage:barButtonBgImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackgroundImage:barButtonBgImageActive forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    
        

}


@end
