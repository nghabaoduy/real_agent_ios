//
//  MyTableSection.m
//  Property App
//
//  Created by Brian on 15/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyTableSection.h"

@implementation MyTableSection

@synthesize headerBtn, headerLbl, headerBG;
int paddingTop = 5;
int paddingLeft = 20;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
       
        //BG
        UIImage* bgImage = [UIImage imageNamed:@"section_gray"];
        headerBG = [[UIImageView alloc] initWithImage:bgImage];
        headerBG.frame = CGRectMake(0,
                                    0,
                                    320,
                                    52);
        
        [self addSubview:headerBG];

         //create label
        headerLbl = [[UILabel alloc] initWithFrame:CGRectMake(paddingLeft, paddingTop, 60, 23) ];
        [headerLbl setText:@"Mellisa Alizabeth"];
         UIFont *font = [UIFont fontWithName:@"SegoeUI-Semilight" size:18];
        [headerLbl setFont:font];
        [headerLbl setTextAlignment:UITextAlignmentLeft];
        [headerLbl sizeToFit];
        [headerLbl setBackgroundColor:[UIColor clearColor]];
        [self addSubview:headerLbl];
        
        // create the button object
        headerBtn = [[UIButton alloc] initWithFrame:CGRectZero];
        headerBtn.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"button_add"]];
        headerBtn.opaque = NO;
        headerBtn.frame = CGRectMake(paddingLeft + headerLbl.frame.size.width + 10, paddingTop, 23.0, 23.0);
        [self addSubview:headerBtn];
        
               //self.backgroundColor = [UIColor colorWithPatternImage:headerBG];

    }
    return self;
}

-(void) setText: (NSString *) text
{
    [headerLbl setText:text];
    [headerLbl sizeToFit];
    headerBtn.frame = CGRectMake(paddingLeft + headerLbl.frame.size.width + 10, paddingTop, 23.0, 23.0);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

// create the parent view that will hold header Label


@end
