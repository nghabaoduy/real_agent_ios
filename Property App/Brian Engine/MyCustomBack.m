//
//  MyCustomBack.m
//  Property App
//
//  Created by Brian on 29/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyCustomBack.h"

@implementation MyCustomBack

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setTitle:@"Back" forState:UIControlStateNormal];
        [self setBackgroundImage:[UIImage imageNamed:@"button_back"] forState:UIControlStateNormal];
    }
    return self;
}

@end
