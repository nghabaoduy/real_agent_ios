//
//  PortfolioSection.m
//  Property App
//
//  Created by Brian on 15/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "PortfolioSection.h"
@implementation PortfolioSection

@synthesize portView,user;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
       [self.headerBtn addTarget:self action:@selector(addClientProperty) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}


-(void) addClientProperty
{
    if (self.portView != NULL && user != NULL) {
        [self.portView addClientProperty:user];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
