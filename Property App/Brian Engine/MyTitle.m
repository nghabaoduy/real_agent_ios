//
//  MyTitle.m
//  Property App
//
//  Created by Brian on 29/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyTitle.h"

@implementation MyTitle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    [self setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0]];
}

@end
