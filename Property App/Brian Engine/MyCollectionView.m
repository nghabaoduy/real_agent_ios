//
//  MyCollectionView.m
//  Property App
//
//  Created by Brian on 9/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyCollectionView.h"

@implementation MyCollectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)awakeFromNib {
    [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"common_bg"]]];
}


@end
