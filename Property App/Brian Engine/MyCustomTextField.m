//
//  MyCustomTextField.m
//  Property App
//
//  Created by Brian on 14/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyCustomTextField.h"

@implementation MyCustomTextField

@synthesize myPlaceHolder;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
                // Initialization code
    }
    return self;
}
- (void)awakeFromNib {
    myPlaceHolder = [[UILabel alloc] initWithFrame:CGRectMake(1, 1, 60, 25) ];
    [myPlaceHolder setText:@"Partenza:"];
    
    [myPlaceHolder setTextColor:[UIColor grayColor]];
    [myPlaceHolder setTextAlignment:UITextAlignmentLeft];
    self.leftView = myPlaceHolder;
    self.leftViewMode = UITextFieldViewModeAlways;

}

-(void) setPlaceholder:(NSString *)placeholderText
{
   // NSLog(@"placeholder set: %@",placeholderText);
    UIFont *font = [UIFont fontWithName:@"SegoeUI-Semilight" size:12];
    [myPlaceHolder setFont:font];
    
    
    self.myPlaceHolder.text = [NSString stringWithFormat:@"  %@",placeholderText];
    [self.myPlaceHolder sizeToFit];
    
    NSRange isSpacedRange = [placeholderText rangeOfString:@"*" options:NSCaseInsensitiveSearch];
    if(isSpacedRange.location != NSNotFound) {
        [myPlaceHolder setTextColor:[UIColor redColor]];
    }
    
}

@end
