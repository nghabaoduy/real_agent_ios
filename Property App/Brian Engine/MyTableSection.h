//
//  MyTableSection.h
//  Property App
//
//  Created by Brian on 15/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableSection : UIView

@property (nonatomic, strong) UIButton * headerBtn;
@property (nonatomic, strong) UILabel *headerLbl;
@property (nonatomic, strong) UIImageView *headerBG;

-(void) setText: (NSString *) text;

@end
