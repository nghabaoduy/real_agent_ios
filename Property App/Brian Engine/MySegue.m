//
//  MySegue.m
//  Property App
//
//  Created by Brian on 29/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MySegue.h"

@implementation MySegue

- (void) perform {
    
    UIViewController *src = (UIViewController *) self.sourceViewController;
    [UIView transitionWithView:src.navigationController.view duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [src.navigationController popToViewController:[src.navigationController.viewControllers objectAtIndex:0] animated:NO];;
                    }
                    completion:NULL];
}


@end
