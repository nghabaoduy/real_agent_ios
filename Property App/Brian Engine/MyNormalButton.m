//
//  MyNormalButton.m
//  Property App
//
//  Created by Brian on 14/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyNormalButton.h"

@implementation MyNormalButton

- (void)awakeFromNib {
    

    UIImage *normalButtonImage = [[UIImage imageNamed:@"button_normal"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 6)];
    [self setBackgroundImage:normalButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [self setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor], UITextAttributeTextColor,nil] forState:UIControlStateNormal];
}


@end
