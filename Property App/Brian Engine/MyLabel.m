//
//  MyLabel.m
//  Property App
//
//  Created by Brian on 9/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyLabel.h"

@implementation MyLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    
    
    [self setFont:[UIFont fontWithName:@"SegoeUI-Semilight" size:17]];
    
}

@end
