//
//  MyTextField.m
//  Property App
//
//  Created by Brian on 9/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyTextField.h"

@implementation MyTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
    //[self setBackgroundColor:[UIColor clearColor]];
    //self.borderStyle = UITextBorderStyleNone;
    //self.background = [UIImage imageNamed:@"textboxBG"];
    //set height
    CGRect frameRect = self.frame;
    frameRect.size.height = 50;
    self.frame = frameRect;
    
}

@end
