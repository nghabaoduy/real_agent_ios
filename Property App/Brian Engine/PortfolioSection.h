//
//  PortfolioSection.h
//  Property App
//
//  Created by Brian on 15/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyTableSection.h"
#import "PortfolioSection.h"
#import "ProPortfolioViewController.h"
#import "User.h"
@interface PortfolioSection : MyTableSection

@property (nonatomic,strong) ProPortfolioViewController *portView;
@property (nonatomic, strong) User *user;
@end
