//
//  MyNextButton.m
//  Property App
//
//  Created by Brian on 30/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyNextButton.h"

@implementation MyNextButton
- (void)awakeFromNib {
    
    
//    UIImage *backButtonImage = [[UIImage imageNamed:@"button_next"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 6)];
    //[self setBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor], UITextAttributeTextColor,nil] forState:UIControlStateNormal];
    [self setTitle:@"Next"];
    
}

@end
