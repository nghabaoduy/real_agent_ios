//
//  MyButtonMedium.m
//  Property App
//
//  Created by Brian on 9/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyButtonMedium.h"

@implementation MyButtonMedium

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
      
   
    [self setBackgroundImage:[UIImage imageNamed:@"button_medium"] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
}

@end
