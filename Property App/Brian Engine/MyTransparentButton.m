//
//  MyTransparentButton.m
//  Property App
//
//  Created by Brian on 15/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "MyTransparentButton.h"

@implementation MyTransparentButton

- (void)awakeFromNib {
    
    
    UIImage *normalButtonImage = [[UIImage imageNamed:@"button_transparent"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 6)];
    [self setBackgroundImage:normalButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setValue:[UIColor clearColor] forKey:UITextAttributeTextColor];
    [attributes setValue:[UIColor clearColor] forKey:UITextAttributeTextShadowColor];
    [attributes setValue:[NSValue valueWithUIOffset:UIOffsetMake(0.0, 0.0)] forKey:UITextAttributeTextShadowOffset];
    
    [self setTitleTextAttributes:attributes forState:UIControlStateNormal];
}

@end
