//
//  MyCustomTextField.h
//  Property App
//
//  Created by Brian on 14/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCustomTextField : UITextField

@property (nonatomic, strong) UILabel *myPlaceHolder;
@end
