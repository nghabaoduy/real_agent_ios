//
//  MyUIEngine.h
//  PhoneBookController
//
//  Created by Brian on 8/9/13.
//  Copyright (c) 2013 Brian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DetailPropertyTabBarVC.h"


@interface MyUIEngine : NSObject

+ (id)sharedUIEngine;

-(UITableViewCell *) getCell:(UITableView *)tableView : (NSString *) identifier :(int) position :(int) totalRows :(NSString *) cellText;

-(UITableViewCell*) getGroupCell:(UITableView *)tableView : (NSString *) identifier :(int) position :(int) totalRows :(NSString *) cellText;

-(UITableViewCell*) getButtonLongCell:(UITableView *)tableView : (NSString *) identifier :(int) position :(int) totalRows :(NSString *) cellText;

-(void) customizeViewSetting:(UIViewController*) inputView :(UITableView*) tableView;
//-(CGFloat) getCateHeight ;
-(void) customSearchBar:(UISearchBar *)searchBar;
-(void) customToolbar:(UIToolbar *)toolbar;

-(void)appDelegateUICustomzation;


@end
