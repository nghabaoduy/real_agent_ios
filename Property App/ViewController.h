//
//  ViewController.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

#import "MyUIViewController.h"

@interface ViewController : MyUIViewController <UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIScrollView *theScrollView;
    
}
@property (nonatomic, assign) UITextField *activeTextField;

- (IBAction)dismissKeyboard:(id)sender;


@property (nonatomic, strong) IBOutlet UITextField * username;
@property (nonatomic, strong) IBOutlet UITextField * password;

@property BOOL * isLogout;

- (BOOL) Authenticate: (NSString *) username : (NSString *) password;

- (void) LoginSuccessful;
- (void) LoginFailed : (NSString*) error;
-(void) inputSignUpLogin: (NSString *) phone : (NSString *) pass;

@end
