//
//  ViewController.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 29/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//
#import <AddressBook/AddressBook.h>
#import "ViewController.h"
#import "SignUpViewController.h"
#import "MainViewController.h"
#import "DatabaseEngine.h"
#import "SWRevealViewController.h"
#import "ResetPasswordViewController.h"
#import "LoadingViewController.h"
#import "DataEngine.h"

@interface ViewController ()
{
    CGFloat * KEYBOARD_ANIMATION_DURATION;
    CGFloat * MINIMUM_SCROLL_FRACTION;
    CGFloat * MAXIMUM_SCROLL_FRACTION;
    CGFloat * PORTRAIT_KEYBOARD_HEIGHT;
    CGFloat * LANDSCAPE_KEYBOARD_HEIGHT;
}

@end

@implementation ViewController
@synthesize username = _username;
@synthesize password = _password;
@synthesize activeTextField;
@synthesize isLogout;


- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    
    
    [textField resignFirstResponder];
    
    return YES;
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void) defaultSetting
{
    [_username setText:@"85333333"];
    [_password setText:@"12345"];
    [[DatabaseEngine sharedInstance] UserLogin:_username.text :_password.text: self];
    
    
}
- (IBAction) SignUp:(id)sender
{
	//[PFUser requestPasswordResetForEmail:@"love.strings.93@gmail.com"];
    SignUpViewController * signUpView = [self.storyboard instantiateViewControllerWithIdentifier:@"signUpView"];
    signUpView.loginView = self;
    [self presentViewController:signUpView animated:YES completion:nil];
}

-(void) inputSignUpLogin: (NSString *) phone : (NSString *) pass
{
    self.username.text = phone;
    self.password.text = pass;
}

- (IBAction) Login :(id)sender
{
    [self.username resignFirstResponder];
    [self.password resignFirstResponder];
	[[DatabaseEngine sharedInstance] UserLogin:_username.text :_password.text: self];
    [self actionRun];
}


- (void) actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    NSLog(@"viewcontroller ::: Login successful!");
    
    [[DataEngine sharedInstance] saveLoginInfo:self.username.text :self.password.text];
    
	LoadingViewController * mainView = [self.storyboard instantiateViewControllerWithIdentifier:@"loadingView"];
	[self presentViewController:mainView animated:NO completion:nil];
}

-(void) actionFail:(NSString *)message
{
    [super actionFail:message];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Fail"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            // First time access has been granted, add the contact
            
        });
    }
    
	// Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.delegate = self;
    
    self.username.placeholder = @"Phone No: ";
    self.password.placeholder = @"Password: ";
    //[self defaultSetting];
    if (!isLogout) {
        [self autoLogin];
    }
    
	
}

-(void) autoLogin
{
    NSDictionary *loginDict = [[DataEngine sharedInstance] loadLoginInfo];
    
    NSString *saveUsername = [loginDict objectForKey:@"saveUsername"];
    NSString *savePassword = [loginDict objectForKey:@"savePassword"];
    NSString *isLogoutStr = [loginDict objectForKey:@"isLogout"];
    
    if (saveUsername.length > 0 && [isLogoutStr isEqualToString:@"NO"]) {
        [self startLoading];
        [_username setText:saveUsername];
        [_password setText:savePassword];
        [[DatabaseEngine sharedInstance] UserLogin:_username.text :_password.text: self];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([touch.view isKindOfClass:[UIButton class]]){
        return NO;
    }
    return YES; // handle the touch
}

- (void) dismissKeyboard{
    [self.activeTextField resignFirstResponder];
}



- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
    
    
    // Step 3: Scroll the target text field into view.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(aRect, self.activeTextField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.activeTextField.frame.origin.y - (keyboardSize.height-15));
        [theScrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    theScrollView.contentInset = contentInsets;
    theScrollView.scrollIndicatorInsets = contentInsets;
}

// Set activeTextField to the current active textfield

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
}

// Set activeTextField to nil

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextField = nil;
}


// Dismiss the keyboard

- (IBAction)dismissKeyboard:(id)sender
{
    [self.activeTextField resignFirstResponder];
}

- (IBAction)ForgotPassword:(id)sender {
	ResetPasswordViewController * mainView = [self.storyboard instantiateViewControllerWithIdentifier:@"resetPasswordView"];
	[self presentViewController:mainView animated:YES completion:nil];
}



@end
