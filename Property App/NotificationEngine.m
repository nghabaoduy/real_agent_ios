//
//  NotificationEngine.m
//  Property App
//
//  Created by Brian on 22/10/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "NotificationEngine.h"
#import "MessageChatVC.h"
#import "DataEngine.h"
#import "MessageInfo.h"

#import "DetailPropertyNavVC.h"
#import "DetailPropertyTabBarVC.h"

#import "DetailGroupTabbar.h"

#import "NewMyGroupNavVC.h"
#import "DetailGroupRootVC.h"


@implementation NotificationEngine

static NotificationEngine *sharedObject;

+ (NotificationEngine*)sharedInstance
{
	if (sharedObject == nil) {
		sharedObject = [[super allocWithZone:NULL] init];
	}
	return sharedObject;
}

-(id)init {
	self = [super init];
	if(self) {
        
	}
	return self;
}
-(void) registerPush: (NSData *) deviceToken
{
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels = @[@"global"];
    // [currentInstallation addUniqueObject:@"Giants" forKey:@"channels"];
    [currentInstallation saveInBackground];
}

-(void) setPushUser:(PFObject *)userObj
{
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setObject:userObj forKey:@"user"];
    
    [currentInstallation saveInBackground];
}

-(void) pushToAll:(NSString *) message
{
    // Create our Installation query
    PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"deviceType" equalTo:@"ios"];
    // Send push notification to query
    [PFPush sendPushMessageToQueryInBackground:pushQuery
                                   withMessage:message];
}
-(void) pushToUserObj:(PFObject *) userObj :(NSDictionary *) messageDict
{
    // Create our Installation query
    PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"deviceType" equalTo:@"ios"];
    [pushQuery whereKey:@"user" equalTo:userObj];
    // Send push notification to query
    PFPush *push = [[PFPush alloc] init];
    [push setQuery:pushQuery];
    [push setData:messageDict];
    [push sendPushInBackground];
    
    //debug for self
    //[self pushToSelf:messageDict];
    
}
-(void) pushToSelf:(NSDictionary *) messageDict
{
    // Create our Installation query
    PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"deviceType" equalTo:@"ios"];
    [pushQuery whereKey:@"user" equalTo:[DataEngine sharedInstance].curUser.userObject];
    // Send push notification to query
    PFPush *push = [[PFPush alloc] init];
    [push setQuery:pushQuery];
    [push setData:messageDict];
    [push sendPushInBackground];
}

-(void) simulatePush
{
    /*PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"deviceType" equalTo:@"ios"];
    PFPush *push = [[PFPush alloc] init];
    [push setQuery:pushQuery];
    [push setMessage:@"The Giants just scored!"];
    [push sendPushInBackground];
    return;
    */
    NSString *className = @"Group";
    NSString *objId = @"OY0BsYcfmq";
    
    PFQuery *query = [PFQuery queryWithClassName:className];
    [query whereKey:@"objectId" equalTo:objId];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            PFObject *pfObj = [objects objectAtIndex:0];
            //startpush
            
            NSString *chatContent = @"abc";//[NSString stringWithFormat:@"%@ group got new sharing property",[groupInfo objectForKey:@"title"]];
            NSMutableDictionary *pushDict = [[NSMutableDictionary alloc] init];
            [pushDict setObject:chatContent forKey:@"alert"];
            [pushDict setObject:pfObj forKey:@"pushPFObject"];
            [pushDict setObject:className forKey:@"className"];
            [pushDict setObject:@"group" forKey:@"pushType"];
            
            
            // Create our Installation query
            PFQuery *pushQuery = [PFInstallation query];
            [pushQuery whereKey:@"deviceType" equalTo:@"ios"];
            [pushQuery whereKey:@"user" equalTo:[DataEngine sharedInstance].curUser.userObject];
            // Send push notification to query
            PFPush *push = [[PFPush alloc] init];
            [push setQuery:pushQuery];
            [push setData:pushDict];
            [push sendPushInBackground];
             NSLog(@"----------------------------------");
             NSLog(@"Push send");
             NSLog(@"----------------------------------");
            //endpush
        }
    }];
    
    
}

-(void) handleNotification:(NSDictionary *)userInfo
{
    NSLog(@"----------------------------------");
    NSDictionary *subDict1 = [userInfo objectForKey:@"aps"];
    
    /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"push"
                                                    message:[subDict1 objectForKey:@"alert"]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];*/
    
    

    NSString *alertMessage = [subDict1 objectForKey:@"alert"];
    NSLog(@"alertMessage = %@",alertMessage);
    NSDictionary *subDict2 = [userInfo objectForKey:@"pushPFObject"];
    PFObject *pushObject = [PFObject objectWithClassName:[subDict2 objectForKey:@"className"] dictionary:subDict2];
    NSLog(@"pushObject = %@",pushObject);
    NSString *pushType = [userInfo objectForKey:@"pushType"];
    NSLog(@"pushType = %@",pushType);
    NSLog(@"----------------------------------");
    if (pushType == NULL) {
        NSLog(@"Null pushType");
        return;
    }
    
    PFQuery *query = [PFQuery queryWithClassName:[subDict2 objectForKey:@"className"]];
    [query whereKey:@"objectId" equalTo:[pushObject objectForKey:@"objectId"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            PFObject *pfObj = [objects objectAtIndex:0];
            
            if ([pushType isEqualToString:@"message"]) {
                /*UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MessageStoryboard" bundle:nil];
                 MessageChatVC * messageView = [newStoryboard instantiateViewControllerWithIdentifier:@"messageChat"];
                 MessageInfo *messInfo= [[MessageInfo alloc] init];
                 messInfo.messageObject = pushObject;
                 messageView.messageInfo = messInfo;*/
                // [currentView presentViewController:messageView animated:NO completion:nil];
            }
            else if([pushType isEqualToString:@"alert"]) {
                //UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"MessageStoryboard" bundle:nil];
                //MessageNavVC * messageView = [newStoryboard instantiateViewControllerWithIdentifier:@"messageNav"];
                //[currentView presentViewController:messageView animated:NO completion:nil];
            }
            else if([pushType isEqualToString:@"group"]) {
                
                [self navigateToGroupView:pfObj];
            }
        }
    }];
    
    
    
    
}

-(void) navigateToGroupView:(PFObject *)groupObj
{
    NSLog(@"navigateToGroupView runs: %@",groupObj);

    UIViewController *currentView = [self getTopController];
    
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"NewMyGroupStoryboard" bundle:nil];
    DetailGroupRootVC * destination = [newStoryboard instantiateViewControllerWithIdentifier:@"detailGroupRoot"];
    DetailGroupTabbar *groupTabbar = [[destination viewControllers] lastObject];
    [groupTabbar initTabbarWithGroupObj:groupObj:currentView:destination];
    
    
    
}


- (UIViewController*) getTopController {
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topViewController.presentedViewController)
    { topViewController = topViewController.presentedViewController;
    }
    return topViewController;
}

-(void) analyzePassingInfo
{
    if ([self.passInfo componentsSeparatedByString:@"-"].count >=2) {
        NSString *type = [[self.passInfo componentsSeparatedByString:@"-"] objectAtIndex:0];
        if ([type isEqualToString:@"property"] && self.mainView!=nil) {
            [self switchMainToDetail:[[self.passInfo componentsSeparatedByString:@"-"] objectAtIndex:1] :self.mainView];
        }
    }
    self.passInfo = @"";
    
    

}
-(void) switchMainToDetail:(NSString *) propertyId :(UIViewController *) viewController
{
    NSLog(@"passing property id = %@",propertyId);
    PFQuery *query = [PFQuery queryWithClassName:@"Property"];
    [query whereKey:@"objectId" equalTo:propertyId];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            Property *curPro = [[Property alloc] initWithProperty:object];
            NSLog(@"passing property id = %@",curPro.address);
            UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"DetailStoryboard" bundle:nil];
            
            DetailPropertyNavVC * navView =[newStoryboard instantiateViewControllerWithIdentifier:@"detailProperty"];
            
            DetailPropertyTabBarVC * tabBar = (DetailPropertyTabBarVC*) navView.topViewController;
            tabBar.isHide = YES;
            
            tabBar.isShare = NO;
            [tabBar initTabByProperty:curPro :newStoryboard];
            
            [viewController presentViewController:navView animated:YES completion:nil];
        }
    }];
    
    
}


@end
