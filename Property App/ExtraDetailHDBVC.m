//
//  ExtraDetailHDBVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 11/5/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "ExtraDetailHDBVC.h"
#import "ImagePickVC.h"
#import "MyUIEngine.h"
#import "MyCustomBack.h"
#import "DataEngine.h"
#import "Agent.h"
#import "Client.h"

@interface ExtraDetailHDBVC ()

@end

@implementation ExtraDetailHDBVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [[MyUIEngine sharedUIEngine] customizeViewSetting:self :nil];
    
    
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self DisplayAddress];
    
    if ([_property.listingType isEqualToString:@"Sales"]) {
        [conditionCell setHidden:YES];
    } else
    {
        [valuationCell setHidden:YES];
    }
    
    levelList = [[NSArray alloc]initWithObjects:
                 @"[Select]",
                 @"Ground",
                 @"Low",
                 @"Mid",
                 @"High", nil];
    
    //districtList = [[DataEngine sharedInstance] getDistrictList];
    conditionList = [[NSArray alloc] initWithObjects:
                  @"[select]",
                  @"Bare",
                  @"Partial",
                  @"Full Fitted",
                  nil];
    roomPostitionList = [[NSArray alloc] initWithObjects:
                  @"[select]",
                  @"Corner",
                  @"Intermediate",
                  @"Door to Door",
                  nil];
    
    mainDoorList = [[NSArray alloc] initWithObjects:
                    @"[select]",
                    @"West",
                    @"East",
                    @"North",
                    @"South",
                  nil];
    
    liftLevelList = [[NSArray alloc] initWithObjects:
                     @"[select]",
                     @"Yes",
                     @"No", nil];
    
    curAgent = (Agent*) [[DataEngine sharedInstance] GetCurUser];
    adderList = [[NSMutableArray alloc] init];
	//[adderList addObject:curAgent];
	for (Client* client in curAgent.myClientList) {
		[adderList addObject:client];
	}
    NSLog(@"adderList = %@",adderList);
    adderNameList = [[NSMutableArray alloc] init];
    [adderNameList addObject:@""];
	for (Client* client in curAgent.myClientList) {
		[adderNameList addObject:client.fullName];
	}
    NSLog(@"adderNameList = %@",adderNameList);
    
    [self initTextfieldPlaceHolder];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@synthesize property = _property;


Agent* curAgent;

- (void) DisplayAddress
{
    [address setText:_property.address];
    [postalCode setText:_property.postCode];
    
    [blockNo setText:_property.blkNo];
    [levelNo setText:_property.levelNumber];
    [unitNo setText:_property.unitNo];
    [district setText:_property.district];
    //[area setText:_property.area];
    //[price setText:_property.price];
    
    
}
-(void) addBackBtn
{
    NSLog(@"addBackBtn runs");
    UIButton *Btn =[[UIButton alloc] initWithFrame:CGRectMake(0.0f,0.0f,68.0f,30.0f)];
    
    [Btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [Btn setBackgroundImage:[UIImage imageNamed:@"button_back2"] forState:UIControlStateNormal];
    [Btn setTitle:@"   Back" forState:UIControlStateNormal];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithCustomView:Btn];
    //[addButton setBackgroundImage:[UIImage imageNamed:@"button_back2"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [addButton setTitle:@"   Back"];
    [self.navigationItem setLeftBarButtonItem:addButton];
}


-(void) back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dismissKeyboard {
    
    int rentP = 0;
    int P = 0;
    
    if (![price isEqual:@""]) {
        P = [price.text intValue];
    }
    
    [activeTextField resignFirstResponder];
}

- (void) initTextfieldPlaceHolder
{
    
    
    
    address.placeholder = @"Address: ";
    postalCode.placeholder = @"Postal Code: ";
    blockNo.placeholder = @"Block: ";
    levelNo.placeholder = @"Level: ";
    unitNo.placeholder = @"Unit: ";
    district.placeholder = @"District: ";
    area.placeholder = @"Floor Size (Sqm): ";
    price.placeholder = @"Price:* ";
    adder.placeholder = @"Client:* ";
    adder.text = _property.adder.fullName;
    
    level.placeholder = @"Level: ";
    hdbType.placeholder = @"HDB Type: ";
    liftLevel.placeholder = @"Lift Level: ";
    mainDoorDirection.placeholder = @"Main Door Direction: ";
    roomPosition.placeholder = @"Flat Position: ";
    town.placeholder = @"Town: ";
    valuation.placeholder = @"Valuation: ";
    condition.placeholder = @"Condition: ";
    
    address.enabled = NO;
    postalCode.enabled = NO;
    blockNo.enabled = NO;
    district.enabled = NO;
    adder.enabled = NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_property.listingType isEqualToString:@"Sales"]) {
        if (indexPath.section == 1 && indexPath.row == 7) {
            return 0.0;
        }
        else
        {
            return 44.0;
        }
    }
    else
    {
        if (indexPath.section == 1 && indexPath.row == 6) {
            return 0.0;
        }
        else
        {
            return 44.0;
        }
    }
}

- (BOOL) ValidateBlock
{
    if (![blockNo.text isEqualToString:@""]) {
        return YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:@"Block No cannot be blank"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    return NO;
}

- (BOOL) ValidateDistrict
{
    if (![district.text isEqualToString:@""]) {
        return YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:@"District cannot be blank"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    return NO;
}



- (BOOL) ValidatePrice
{
    if (![price.text isEqualToString:@""]) {
        return YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:@"Price cannot be blank"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    return NO;
    
}



- (BOOL) ValidateAdder
{
    if (![adder.text isEqualToString:@""]) {
        return YES;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:@"Adder cannot be blank"
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
    return NO;
}


- (void) showPicker
{
    menu = [[UIActionSheet alloc] initWithTitle:nil
                                       delegate:nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    
    // Add Picker
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 0, 0)];
    picker.delegate = self;
    picker.showsSelectionIndicator = YES;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    [barItems addObject:doneBtn];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    [barItems addObject:cancelBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    
    [menu addSubview:pickerToolbar];
    [menu addSubview:picker];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 464)];
    
    // Add the picker
    /*
     UIDatePicker *pickerView = [[UIDatePicker alloc] init];
     pickerView.datePickerMode = UIDatePickerModeDate;
     [menu addSubview:pickerView];
     [menu showInView:self.view];
     [menu setBounds:CGRectMake(0,0,320, 500)];*/
    
    CGRect pickerRect = picker.bounds;
    pickerRect.origin.y = 0;
    picker.bounds = pickerRect;
}

-(void)categoryDoneButtonPressed{
    //Add for wheel
    //categoryLable.text = selectedCategory;
    if (activeTextField == district) {
        [district setText:selectedCategory];
    } else if (activeTextField == level)
    {
        [level setText:selectedCategory];
    } else if (activeTextField == condition)
    {
        [condition setText:selectedCategory];
    } else if (activeTextField == adder)
    {
        [adder setText:selectedCategory];
    } else if (activeTextField == roomPosition)
    {
        [roomPosition setText:selectedCategory];
    } else if (activeTextField == mainDoorDirection)
    {
        [mainDoorDirection setText:selectedCategory];
    } else if (activeTextField == liftLevel)
    {
        [liftLevel setText:selectedCategory];
    }
    [menu dismissWithClickedButtonIndex:1 animated:YES];
    
}



-(void)categoryCancelButtonPressed{
    [menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    //Add Wheel
    // Handle the selection
    if (activeTextField == level) {
        selectedCategory = [NSString stringWithFormat:@"%@",[levelList objectAtIndex:row]];
    } else if (activeTextField == condition)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[conditionList objectAtIndex:row]];
    } else if (activeTextField == roomPosition)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[roomPostitionList objectAtIndex:row]];
    } else if (activeTextField == adder)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[adderNameList objectAtIndex:row]];
    } else if (activeTextField == mainDoorDirection)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[mainDoorList objectAtIndex:row]];
    } else if (activeTextField == liftLevel)
    {
        selectedCategory =[NSString stringWithFormat:@"%@",[liftLevelList objectAtIndex:row]];
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    //Add Wheel
    
    if (activeTextField == level) {
        return [levelList count];
    } else if (activeTextField == condition)
    {
        return [conditionList count];
    } else if (activeTextField == roomPosition)
    {
        return [roomPostitionList count];
    } else if (activeTextField == adder)
    {
        return [adderNameList count];
    } else if (activeTextField == mainDoorDirection)
    {
        return [mainDoorList count];
    } else if (activeTextField == liftLevel)
    {
        return [liftLevelList count];
    }
    return 0;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (activeTextField == level) {
        return [levelList objectAtIndex:row];
    } else if (activeTextField == condition)
    {
        return [conditionList objectAtIndex:row];
    } else if (activeTextField == roomPosition)
    {
        return [roomPostitionList objectAtIndex:row];
    } else if (activeTextField == adder)
    {
        return [adderNameList objectAtIndex:row];
    } else if (activeTextField == mainDoorDirection)
    {
        return [mainDoorList objectAtIndex:row];
    } else if (activeTextField == liftLevel)
    {
        return [liftLevelList objectAtIndex:row];
    }
    
    return 0;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //Add Wheel
    [self animateTextField: textField up: YES];
    activeTextField = textField;
    if ([textField isEqual:level] || [textField isEqual:condition] || [textField isEqual:roomPosition] || [textField isEqual:adder] || [textField isEqual:mainDoorDirection] || [textField isEqual:liftLevel])
    {
        [textField resignFirstResponder];
        [self showPicker];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"extraDetailNext"]) {
        _property.address = address.text;
        _property.postCode = postalCode.text;
        _property.blkNo = blockNo.text;
        _property.levelNumber = levelNo.text;
        _property.unitNo = unitNo.text;
        _property.district = district.text;
        _property.area = area.text;
        _property.price = price.text;
        _property.adder = [adderList objectAtIndex:[adderNameList indexOfObject:adder.text]-1];
        NSLog(@"adder name = %@",_property.adder.fullName);
        
        _property.condition = condition.text;
        _property.roomPosition = roomPosition.text;
        _property.hdbType = hdbType.text;
        _property.liftLevel = liftLevel.text;
        _property.mainDoorDirection = mainDoorDirection.text;
        _property.town = town.text;
        _property.valuation = valuation.text;
        _property.condition = condition.text;
        _property.groundLevel = level.text;
        
        
        ImagePickVC * destination = segue.destinationViewController;
        if ([self.navigationItem.title isEqualToString:@"Step 1/3"]) {
            [destination.navigationItem setTitle:@"Step 2/3"];
        }
        destination.property = _property;
    }
}




- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"extraDetailNext"]) {
        if ([self ValidateBlock] &&
            [self ValidateDistrict] &&
            [self ValidatePrice] &&
            [self ValidateAdder] )
        {
            return YES;
        }
        else
        {
            //Show log
            return NO;
        }
    }
    return NO;
    
}
#pragma mark - Table view data source


@end
