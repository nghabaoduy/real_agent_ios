//
//  LocationPickVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 21/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Property.h"

@interface LocationPickVC : UIViewController
@property (strong, nonatomic) Property * property;
@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *backBtn;
-(void) GetMapLocation : (NSArray*) data ;
@end
