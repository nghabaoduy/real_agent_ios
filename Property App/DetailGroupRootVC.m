//
//  DetailGroupRootVC.m
//  Property App
//
//  Created by Brian on 23/2/14.
//  Copyright (c) 2014 Nguyen Ha Bao Duy. All rights reserved.
//

#import "DetailGroupRootVC.h"



@interface DetailGroupRootVC ()

@end



@implementation DetailGroupRootVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
