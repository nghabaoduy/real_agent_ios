//
//  DetailPropertyVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 24/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "DetailGroupListingVC.h"

@interface DetailPropertyVC : UITableViewController <MFMailComposeViewControllerDelegate>
{
    
    NSString * type;
    //display
    IBOutlet UIImageView * icon;
    IBOutlet UILabel * propertyName;
    IBOutlet UILabel * propertyType;
    IBOutlet UILabel * adress;
    IBOutlet UITextView *adressTV;
    IBOutlet UILabel * postalCode;
    
    //Detail
    IBOutlet UITextField * price;
    IBOutlet UITextField * noOfBedroom;
    IBOutlet UITextField * district;
    IBOutlet UITextField * floorSize;
    IBOutlet UITextField * tenure;
    
    //Status
    IBOutlet UITextField * status;
    IBOutlet UITextField * rentedPrice;
    
    //Project Information
    IBOutlet UITextField * projectName;
    IBOutlet UITextField * psf;
    IBOutlet UITextField * topYear;
    IBOutlet UITextField * rentalYield;
    
    //Contact Agen
    IBOutlet UIImageView * agentIcon;
    IBOutlet UILabel * agentName;
    IBOutlet UILabel * agentMobile;
    IBOutlet UILabel * agentEmail;
    
    User * curUser;
    
    IBOutlet UITableViewCell * schedule;
    
    IBOutlet UITableViewCell * addToShare;
}

@property (nonatomic, retain) Property* property;

@property (nonatomic) BOOL isSharing;
@property (nonatomic) BOOL allowUnitNo;
@property (nonatomic, retain) PFObject * curGroup;
@property (nonatomic, retain) ProPortfolioViewController * proportView;

@property (nonatomic) BOOL isMyProperty;
- (void) hideSchedule;

- (void) showDialog : (NSString *) message;
@end
