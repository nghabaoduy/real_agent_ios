//
//  PPLoadingVC.m
//  Property App
//
//  Created by Brian on 29/9/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "PPLoadingVC.h"
#import "DetailPropertyTabBarVC.h"
#import "DetailPropertyVC.h"
#import "GalleryPropertyVC.h"
#import "LocationPropertyVC.h"
#import "DetailPropertyNavVC.h"
#import "DetailComercialVC.h"

#import "SWRevealViewController.h"
#import "DatabaseEngine.h"

@interface PPLoadingVC ()

@end

@implementation PPLoadingVC
@synthesize property = _property;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.property != NULL) {
        [self upload];
    }
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)upload
{
    //Upload function
    self.property.isSubmited = @"YES";
    [self.property printInfo];
	[_property UploadProperty: self];
   
}
-(void) sucess:(NSString *)message
{
    UIStoryboard * newStoryboard = [UIStoryboard storyboardWithName:@"DetailStoryboard" bundle:nil];
    
    
    
    DetailPropertyNavVC * navView =[newStoryboard instantiateViewControllerWithIdentifier:@"detailProperty"];
    
    DetailPropertyTabBarVC * tabBar = (DetailPropertyTabBarVC*) navView.topViewController;
    [tabBar initTabByProperty:self.property :newStoryboard ];
    
    UIStoryboard * mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
    SWRevealViewController * mainView = [mainStoryboard instantiateViewControllerWithIdentifier:@"sw"];
    tabBar.homeVC = mainView;
    
    [self presentViewController:navView animated:YES completion:nil];
    
    [[DatabaseEngine sharedInstance] getAllAlertList:self.property];
}
-(void) fail:(NSString *)message
{
    self.property.isSubmited = @"NO";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Fail"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
    [alert show];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
