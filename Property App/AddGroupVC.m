//
//  AddGroupVC.m
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/8/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import "AddGroupVC.h"
#import "PhoneBookVC.h"
#import "DataEngine.h"
#import "DatabaseEngine.h"

@interface AddGroupVC ()

@end

@implementation AddGroupVC

@synthesize participantList;
- (void)actionRun
{
    [super actionRun];
    NSLog(@"starting to run");
    User * user = [[DataEngine sharedInstance] GetCurUser];
    [participantList addObject:user.userObject];
    [[DatabaseEngine sharedInstance] AddGroupInfo:user.userObject :subject.text :participantList :self];
}

- (void)actionSuceed:(NSString *)message
{
    [super actionSuceed:message];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)actionFail:(NSString *)message
{
    [super actionFail:message];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
    [alert show];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self updateView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"addParticipantSegue"]) {
        
        self.participantList = [[NSMutableArray alloc]init];
        PhoneBookVC * destination = segue.destinationViewController;
    }
}

/*- (void) addToMain : (NSArray*) data
{
    NSLog(@"run data");
    participantList = data;
    
}*/

-(void) updateView
{
    NSLog(@"participantList = %@",participantList);
    [participants setText:[NSString stringWithFormat:@"(%i/50)", [participantList count]]];
    if (![subject.text isEqualToString:@""] && [participantList count] > 0) {
        doneBtn.enabled = YES;
    }
    else
    {
        doneBtn.enabled = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    
}

- (IBAction) ValidationDone:(id)sender
{
    if (![subject.text isEqualToString:@""] && [participantList count] > 0) {
        doneBtn.enabled = YES;
    }
}


- (IBAction)Done:(id)sender
{
    NSLog(@"done pressed");
    [self actionRun];
}

@end
