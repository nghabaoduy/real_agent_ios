//
//  MessageInfo.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/17/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "User.h"

@interface MessageInfo : NSObject

@property (nonatomic) PFObject * messageObject;
@property (nonatomic) User * connector;

@end
