//
//  PartnersVC.h
//  Property App
//
//  Created by Nguyen Ha Bao Duy on 10/30/13.
//  Copyright (c) 2013 Nguyen Ha Bao Duy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyUITableViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "User.h"

@interface PartnersVC : MyUITableViewController <MFMailComposeViewControllerDelegate>
{
    User * curUser;
}


@property (nonatomic, retain) NSString * partnerType;
@property (nonatomic, retain) NSArray * partnerList;

@property (nonatomic, retain) NSString * callActive;
@property (nonatomic, retain) NSString * emailActive;

- (void) call;
- (void) mail;
@end
